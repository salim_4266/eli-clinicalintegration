﻿Imports System.Data
Imports System.Runtime.Serialization
Imports System.Collections.Generic

<Runtime.InteropServices.ComVisible(False)>
Public Class DBUtility
    Private Shared _connection As IDbConnection
    Public Property DbObject() As Object
        Get
            Return _connection
        End Get
        Set(ByVal value As Object)
            Dim c = TryCast(value, IObjectReference)
            If c IsNot Nothing Then
                _connection = DirectCast(c.GetRealObject(Nothing), IDbConnection)
            Else
                _connection = DirectCast(value, IDbConnection)
            End If
        End Set
    End Property
End Class
