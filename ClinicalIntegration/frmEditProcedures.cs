﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmEditProcedures : Form
    {
        public Form ParentMDI { get; set; }
        public Int64 SnomedCode { get; set; }
        public string SnomedDesc { get; set; }
        public string ProcedureDesc { get; set; }
        public string Health { get; set; }
        public string Reason { get; set; }
        public string Active { get; set; }
        public bool IsNew { get; set; }
        public Int64 GoalId { get; set; }


        public frmEditProcedures()
        {
            InitializeComponent();
        }

        private void frmEditProcedures_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillStatusCB();
            FillSnomedDetail();
            btnSave.TabStop = false;
            btnSave.FlatStyle = FlatStyle.Flat;
            btnSave.FlatAppearance.BorderSize = 0;
            btnCancel.TabStop = false;
            btnCancel.FlatStyle = FlatStyle.Flat;
            btnCancel.FlatAppearance.BorderSize = 0;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            if (GetFormValidate())
            {
                ProceduresModel objPM = new ProceduresModel()
                {
                    Id = GoalId,
                    PatientId = Convert.ToInt64(ClinicalComponent.PatientId),
                    AppointmentID = Convert.ToInt64(ClinicalComponent.AppointmentId),
                    AppointmentDate = ClinicalComponent.AppointmentDate,
                    EnteredDate = DateTime.Now,
                    ConceptID = SnomedCode,
                    Term = SnomedDesc,
                    Proceduredetail = txtInstruction.Text,
                    Status = cmStatus.SelectedItem.ToString(),
                    ResourceId = Convert.ToInt32(ClinicalComponent.UserId),
                    LastModofiedDate = DateTime.Now,
                    Health = txthealth.Text,
                    Reason = txtReason.Text


                };

                Procedures objProcedure = new Procedures();
                objProcedure.SaveProcedures(IsNew, objPM);

                frmProceduresList frmGI = new frmProceduresList();
                frmGI.MdiParent = this.ParentMDI;
                frmGI.ParentMDI = this.ParentMDI;
                frmGI.Show();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
            frmProceduresList frmp = new frmProceduresList();
            frmp.MdiParent = this.ParentMDI;
            frmp.ParentMDI = this.ParentMDI;
            frmp.Show();
        }

        private void btnSnomedFinder_Click(object sender, EventArgs e)
        {
            frmSnomedFinder snomedfinder = new frmSnomedFinder();
            snomedfinder.isEdit = IsNew;
            snomedfinder.id = GoalId;
            snomedfinder.ProcedureDesc = txtInstruction.Text;
            snomedfinder.Health = txthealth.Text;
            snomedfinder.Reason = txtReason.Text;
            snomedfinder.editstatus = cmStatus.SelectedItem.ToString();
            snomedfinder.CallingFrom = "Procedure";
            snomedfinder.MdiParent = this.ParentMDI;
            snomedfinder.ParentMDI = this.ParentMDI;
            snomedfinder.Show();
            this.Close();
        }

        private void FillSnomedDetail()
        {

            if (!string.IsNullOrEmpty(SnomedDesc))
            {
                txtDesc.Text = SnomedDesc;
                txtSnomedCode.Text = SnomedCode.ToString();
                txtInstruction.Text = ProcedureDesc;
                cmStatus.SelectedIndex = cmStatus.Items.IndexOf(Active);
                txthealth.Text = Health;
                txtReason.Text = Reason;


            }
            else if (IsNew)
            {
                txtSnomedCode.Text = "";
                txtDesc.Text = "";
                cmStatus.SelectedIndex = cmStatus.Items.IndexOf("Active");
            }
        }

        private void FillStatusCB()
        {
            cmStatus.Items.Insert(0, "Active");
            cmStatus.Items.Insert(1, "Inactive");
            cmStatus.SelectedIndex = 0;
        }

        private bool GetFormValidate()
        {
            bool isFlag = true;
            if (string.IsNullOrEmpty(txtSnomedCode.Text))
            {
                isFlag = false;
                MessageBox.Show("Please select 'Code/Description", "Error");

            }
            else if (string.IsNullOrEmpty(txtDesc.Text))
            {
                isFlag = false;
                MessageBox.Show("Please select 'Code/Description'", "Error");

            }
            return isFlag;
        }


    }
}
