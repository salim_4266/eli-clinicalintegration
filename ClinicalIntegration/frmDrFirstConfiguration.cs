﻿using DBHelper;
using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmDrFirstConfiguration : Form
    {
        public Object DBObject { get; set; }
        public Form ParentMDI { get; set; }
        public frmDrFirstConfiguration()
        {
            InitializeComponent();
        }
        private void frmDrFirstConfiguration_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;

            DrFirstConfiguration objConfiguration = new DrFirstConfiguration();
            if (objConfiguration.ConfigurationId != "")
            {
                btnSave.Tag = objConfiguration.ConfigurationId;
                txtVendorName.Text = objConfiguration.VendorName;
                txtVendorPassword.Text = objConfiguration.VendorPassword;
                txtVersion.Text = objConfiguration.Version;
                txtUploadUrl.Text = objConfiguration.UploadUrl;
                txtSSOUrl.Text = objConfiguration.SSOUrl;
                txtPracticeUserName.Text = objConfiguration.PracticeUserName;
                txtPracticeSystemName.Text = objConfiguration.SystemName;
                txtDownloadUrl.Text = objConfiguration.DownloadUrl;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string configurationId = btnSave.Tag.ToString();
            int returnId = new DrFirstConfiguration()
            {
                ConfigurationId = configurationId,
                DownloadUrl = txtDownloadUrl.Text,
                PracticeUserName = txtPracticeUserName.Text,
                SSOUrl = txtSSOUrl.Text,
                SystemName = txtPracticeSystemName.Text,
                UploadUrl = txtUploadUrl.Text,
                VendorName = txtVendorName.Text,
                VendorPassword = txtVendorPassword.Text,
                Version = txtVersion.Text
            }.Save();
            if (returnId >0)
                MessageBox.Show("Practice configuration updated successfully!");
            else
                MessageBox.Show("There is some issue while updating the information, Please contact IO Support!");
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
