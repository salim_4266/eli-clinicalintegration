﻿using DBHelper;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmPermission : Form
    {
        public string UserId { get; set; }
        public Object DBObject { get; set; }
        
        public frmPermission()
        {
            InitializeComponent();
        }

        private void frmPermission_Load(object sender, EventArgs e)
        {   
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            ClinicalComponent.UserId = this.UserId;
            Permissions objutil = new Permissions();
            DataTable dt = new DataTable();
            dt = objutil.GetUserinfo("get");
            ((ListBox)checkedListBox1).DataSource = dt;
            ((ListBox)checkedListBox1).DisplayMember = "ResourceName";
            ((ListBox)checkedListBox1).ValueMember = "ResourceId";           
            SetPermissionChecked(dt);
        } 

        private void btnAccess_Click(object sender, EventArgs e)
        {
            var cc = checkedListBox1.CheckedItems;
            if (cc.Count <= 0)
            {
                MessageBox.Show("Please check the checkbox!", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                foreach (DataRowView dr in cc)
                {
                    var id = dr.Row.ItemArray[0];
                    Permissions objutill = new Permissions();
                    objutill.AddPermission(Convert.ToInt32(dr.Row.ItemArray[0]), dr.Row.ItemArray[2].ToString());
                }

                MessageBox.Show("Access is allowed","Permission");
            }
        }

        public void SetPermissionChecked(DataTable dt)
        {           
            string  value = "";
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                DataRowView view = checkedListBox1.Items[i] as DataRowView;
                value = view["EnterdDate"].ToString();
                if (value != "")
                {
                    checkedListBox1.SetItemChecked(i, true);                   
                }
                else
                    checkedListBox1.SetItemChecked(i, false);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var cc = checkedListBox1.CheckedItems;
            if (cc.Count <= 0)
            {
                MessageBox.Show("Please check the checkbox!", "Permission", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                if (MessageBox.Show("All selected User(s) will reset thair previous privilege. Are you want to Proceed.","Permission", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    foreach (DataRowView dr in cc)
                    {
                        var id = dr.Row.ItemArray[0];
                        Permissions objutill = new Permissions();
                        objutill.RemovePermission(Convert.ToInt32(dr.Row.ItemArray[0]));
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Permissions objutil = new Permissions();
            DataTable dt = new DataTable();
            dt = objutil.GetUserinfo("get");
            ((ListBox)checkedListBox1).DataSource = dt;
            ((ListBox)checkedListBox1).DisplayMember = "ResourceName";
            ((ListBox)checkedListBox1).ValueMember = "ResourceId";
            SetPermissionChecked(dt);
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
