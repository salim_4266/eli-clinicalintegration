﻿namespace ClinicalIntegration
{
    partial class frmDrFirstConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtUploadUrl = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDownloadUrl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSSOUrl = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.VendorPassword = new System.Windows.Forms.Label();
            this.txtVendorPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPracticeUserName = new System.Windows.Forms.TextBox();
            this.CancelButton = new System.Windows.Forms.Button();
            this.txtPracticeSystemName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Upload URL";
            // 
            // txtUploadUrl
            // 
            this.txtUploadUrl.Location = new System.Drawing.Point(32, 114);
            this.txtUploadUrl.Multiline = true;
            this.txtUploadUrl.Name = "txtUploadUrl";
            this.txtUploadUrl.Size = new System.Drawing.Size(788, 25);
            this.txtUploadUrl.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Download URL";
            // 
            // txtDownloadUrl
            // 
            this.txtDownloadUrl.Location = new System.Drawing.Point(33, 174);
            this.txtDownloadUrl.Multiline = true;
            this.txtDownloadUrl.Name = "txtDownloadUrl";
            this.txtDownloadUrl.Size = new System.Drawing.Size(783, 26);
            this.txtDownloadUrl.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "SSO URL";
            // 
            // txtSSOUrl
            // 
            this.txtSSOUrl.Location = new System.Drawing.Point(34, 234);
            this.txtSSOUrl.Multiline = true;
            this.txtSSOUrl.Name = "txtSSOUrl";
            this.txtSSOUrl.Size = new System.Drawing.Size(781, 25);
            this.txtSSOUrl.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Version";
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(35, 292);
            this.txtVersion.Multiline = true;
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(222, 25);
            this.txtVersion.TabIndex = 9;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnSave.Location = new System.Drawing.Point(710, 472);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(113, 42);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Done";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(373, 401);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "Vendor Name";
            // 
            // txtVendorName
            // 
            this.txtVendorName.Location = new System.Drawing.Point(376, 419);
            this.txtVendorName.Multiline = true;
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(327, 25);
            this.txtVendorName.TabIndex = 12;
            // 
            // VendorPassword
            // 
            this.VendorPassword.AutoSize = true;
            this.VendorPassword.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VendorPassword.Location = new System.Drawing.Point(35, 400);
            this.VendorPassword.Name = "VendorPassword";
            this.VendorPassword.Size = new System.Drawing.Size(102, 15);
            this.VendorPassword.TabIndex = 13;
            this.VendorPassword.Text = "Vendor Password";
            // 
            // txtVendorPassword
            // 
            this.txtVendorPassword.Location = new System.Drawing.Point(36, 418);
            this.txtVendorPassword.Multiline = true;
            this.txtVendorPassword.Name = "txtVendorPassword";
            this.txtVendorPassword.Size = new System.Drawing.Size(302, 25);
            this.txtVendorPassword.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(32, 335);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 15);
            this.label8.TabIndex = 17;
            this.label8.Text = "Practice Username";
            // 
            // txtPracticeUserName
            // 
            this.txtPracticeUserName.Location = new System.Drawing.Point(35, 354);
            this.txtPracticeUserName.Multiline = true;
            this.txtPracticeUserName.Name = "txtPracticeUserName";
            this.txtPracticeUserName.Size = new System.Drawing.Size(305, 25);
            this.txtPracticeUserName.TabIndex = 18;
            // 
            // CancelButton
            // 
            this.CancelButton.BackColor = System.Drawing.Color.LightSeaGreen;
            this.CancelButton.Location = new System.Drawing.Point(29, 472);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(114, 44);
            this.CancelButton.TabIndex = 19;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = false;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // txtPracticeSystemName
            // 
            this.txtPracticeSystemName.Location = new System.Drawing.Point(373, 353);
            this.txtPracticeSystemName.Multiline = true;
            this.txtPracticeSystemName.Name = "txtPracticeSystemName";
            this.txtPracticeSystemName.Size = new System.Drawing.Size(332, 25);
            this.txtPracticeSystemName.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(370, 335);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 15);
            this.label4.TabIndex = 20;
            this.label4.Text = "Practice System Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(30, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(225, 18);
            this.label7.TabIndex = 22;
            this.label7.Text = "DrFirst Practice Level Configuration ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calisto MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Maroon;
            this.label9.Location = new System.Drawing.Point(30, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(281, 15);
            this.label9.TabIndex = 23;
            this.label9.Text = "This will be one time configuration to enable Erx ";
            // 
            // frmDrFirstConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(853, 572);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtPracticeSystemName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.txtPracticeUserName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtVendorPassword);
            this.Controls.Add(this.VendorPassword);
            this.Controls.Add(this.txtVendorName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtVersion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtSSOUrl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDownloadUrl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUploadUrl);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDrFirstConfiguration";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "DrFirst Practice Configuration";
            this.Load += new System.EventHandler(this.frmDrFirstConfiguration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUploadUrl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDownloadUrl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSSOUrl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtVendorName;
        private System.Windows.Forms.Label VendorPassword;
        private System.Windows.Forms.TextBox txtVendorPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPracticeUserName;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.TextBox txtPracticeSystemName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
    }
}