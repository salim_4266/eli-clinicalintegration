﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmExternalPortalLink : Form
    {
        public string ExternalLink { get; set; }

        public frmExternalPortalLink()
        {
            InitializeComponent();
        }

        private void frmExternalPortalLink_Load(object sender, EventArgs e)
        {
            if (this.ExternalLink != "")
            {
                if (this.ExternalLink != "")
                {
                    webBrowser1.Navigate(this.ExternalLink);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}

