﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmOutBoundSentItems : Form
    {
        public string ProviderKey { get; set; }
        public string ProviderName { get; set; }
        public string FolderPath { get; set; }
        private string DepExternalId { get; set; }
        public frmOutBoundSentItems()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[rowIndex];
                if (!string.IsNullOrEmpty(dataGridView1.Rows[rowIndex].Cells[0].Value.ToString()))
                {
                    button2.Enabled = true;
                    DepExternalId = row.Cells[0].Value.ToString();
                }
            }
        }

        private void frmOutBoundSentItems_Load(object sender, EventArgs e)
        {
            InitialLoad();
        }
        private void DefaultButtonImages()
        {
            button2.Image = Properties.Resources.btn_view;
            button4.Image = Properties.Resources.btn_close;
            DisableButtonBorder(button2);
        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }

        private void InitialLoad()
        {
            button2.Enabled = false;
            panel1.Hide();
            DefaultButtonImages();

            // Load All Sent Items
            dataGridView1.DataSource = new DEPMessages().LoadOutBoxMessages(ProviderKey);
            dataGridView1.AutoGenerateColumns = false;
        }

        private void btnAttachment_Click(object sender, EventArgs e)
        {
            string[] FileDetails = ((Button)sender).Tag.ToString().Split('%');
            if (FileDetails.Count() == 2)
            {
                string FilePath = FileDetails[0];
                string FileType = FileDetails[1].ToLower();
                if (FileType.Contains("pdf") || FileType.Contains("jpeg") || FileType.Contains("jpg") || FileType.Contains("png") || FileType.Contains("txt"))
                {
                    System.Diagnostics.Process.Start(FilePath);
                }
                else if (FileType.Contains("html"))
                {
                    System.Diagnostics.Process.Start(FilePath);
                }
                else if (FileType.Contains("zip"))
                {
                    System.Diagnostics.Process.Start(FilePath);
                }
                if (FileType.Contains("xml"))
                {
                    string LinkedPath = AppDomain.CurrentDomain.BaseDirectory.ToString().Replace("\\Debug", "");
                    string path2Exe = LinkedPath + @"\CCDAVIEWER\FileViewCCDA.exe";
                    MessageBox.Show(path2Exe);
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = path2Exe;
                    startInfo.Arguments = FilePath;
                    Process.Start(startInfo);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string returnStatus = new ACIOperation().GetSentStatus(DepExternalId, new RequestTokenEntity().GeneratJWTToken().TokenDetail, new ACI_Defaults(), 0);
            if (returnStatus.Contains("Success"))
            {
                lblSentUpdatedStatus.ForeColor = System.Drawing.Color.Green;
                lblSentUpdatedStatus.Text = returnStatus;
            }
            else
            {
                lblSentUpdatedStatus.ForeColor = System.Drawing.Color.Maroon;
                lblSentUpdatedStatus.Text = returnStatus;
            }
            returnStatus = new ACIOperation().GetDeliveryStatus(DepExternalId, new RequestTokenEntity().GeneratJWTToken().TokenDetail, new ACI_Defaults());
            if (returnStatus.Contains("Success"))
            {
                lbldeliveryUpdatedStatus.ForeColor = System.Drawing.Color.Green;
                lbldeliveryUpdatedStatus.Text = returnStatus;
            }
            else
            {
                lbldeliveryUpdatedStatus.ForeColor = System.Drawing.Color.Maroon;
                lbldeliveryUpdatedStatus.Text = returnStatus;
            }
            

            panel2.Controls.Clear();
            bool attachment = false;
            int height = 0;
            string _selectQry = "Select b.Dep_RequestDate as Col1, b.ToAddress as Col7, b.FromAddress as Col8, b.MessageSubject as Col3, b.Message as Col4, ISNULL(a.FileName1,'') as Col5, ISNULL(a.FilePath, '') as Col6, ISNULL(a.FileType,'') as Col9 From ACI_DEP b with (nolock) LEFT join dbo.ACI_DEP_OutBound_Attachments a with (nolock) on a.ACI_DepKey = b.AciDepKey Where b.AciDepKey = '" + DepExternalId + "'";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                DataTable dtTemp = new DataTable();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                }
                foreach (DataRow dr in dtTemp.Rows)
                {
                    txtDescription.Text = dr["Col4"].ToString();
                    txtSubject.Text = dr["Col3"].ToString();
                    txtDepDate.Text = dr["Col1"].ToString();
                    txtFromAddress.Text = dr["Col8"].ToString();
                    txtToAddress.Text = dr["Col7"].ToString();

                    if (dr["Col5"].ToString() != "" && dr["Col6"].ToString() != "" && dr["Col7"].ToString() != "")
                    {
                        attachment = true;
                        Button btnAttachment = new Button();
                        btnAttachment.Size = new Size(panel2.Width - 10, 30);
                        btnAttachment.Tag = dr["Col6"].ToString() + "%" + dr["Col9"].ToString();
                        btnAttachment.Text = dr["Col5"].ToString();
                        btnAttachment.Location = new Point(1, height);
                        panel2.Controls.Add(btnAttachment);
                        btnAttachment.Click += new EventHandler(this.btnAttachment_Click);
                        height = height + 34;
                    }
                }
                panel2.Visible = attachment;
                panel1.Show();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DepExternalId = "";
            panel1.Hide();
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
    }
}
