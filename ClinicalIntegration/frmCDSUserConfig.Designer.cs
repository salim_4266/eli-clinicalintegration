﻿namespace ClinicalIntegration
{
    partial class frmCDSUserConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdProcedure = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdProcedure)).BeginInit();
            this.SuspendLayout();
            // 
            // grdProcedure
            // 
            this.grdProcedure.AllowUserToAddRows = false;
            this.grdProcedure.AllowUserToDeleteRows = false;
            this.grdProcedure.AllowUserToResizeColumns = false;
            this.grdProcedure.AllowUserToResizeRows = false;
            this.grdProcedure.BackgroundColor = System.Drawing.Color.White;
            this.grdProcedure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProcedure.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column9,
            this.Column4});
            this.grdProcedure.Location = new System.Drawing.Point(33, 55);
            this.grdProcedure.MultiSelect = false;
            this.grdProcedure.Name = "grdProcedure";
            this.grdProcedure.ReadOnly = true;
            this.grdProcedure.RowTemplate.ReadOnly = true;
            this.grdProcedure.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.grdProcedure.Size = new System.Drawing.Size(635, 329);
            this.grdProcedure.TabIndex = 21;
            this.grdProcedure.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProcedure_CellClick);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_done;
            this.button2.Location = new System.Drawing.Point(572, 407);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 36);
            this.button2.TabIndex = 24;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkGreen;
            this.label1.Location = new System.Drawing.Point(33, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "List of Users with Configuration Details";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ResourceName";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "Resource Name";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 140;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "CDSConfiguration";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "Config Intervention";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column2.Width = 140;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "Alerts";
            this.Column9.Frozen = true;
            this.Column9.HeaderText = "Enable Intervention";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column9.Width = 130;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Info";
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "Enable Reference Resources";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column4.Width = 180;
            // 
            // frmCDSUserConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(706, 469);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.grdProcedure);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCDSUserConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User Based Configuration";
            this.Load += new System.EventHandler(this.frmCDSUserConfig_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdProcedure)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdProcedure;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
    }
}