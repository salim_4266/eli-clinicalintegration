﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmCDSRefrences : Form
    {
        public frmCDSRefrences()
        {
            InitializeComponent();
        }


        #region "variables"
        public string RuleId;
        public string DeveloperName;
        public string ModefiedData;
        private string RuleName;
        private string RuleFiledArea;
        #endregion

        private void frmCDSRefrences_Load(object sender, EventArgs e)
        {
            BindData();
        }
        public void GetRuleInfo()
        {
            string strSql = null;
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            DataTable dt = new DataTable();
            strSql = "SELECT IR.RuleName,IRP.FieldArea FROM DBO.IE_Rules IR with(nolock) INNER JOIN  DBO.IE_RulesPhrases IRP with(nolock) ON IR.RuleId=IRP.RuleId WHERE IR.RuleId=@RuleId;";
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter parRuleId = cmd.CreateParameter();
                parRuleId.Direction = ParameterDirection.Input;
                parRuleId.Value = RuleId;
                parRuleId.ParameterName = "@RuleId";
                parRuleId.DbType = DbType.Int32;
                cmd.Parameters.Add(parRuleId);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["RuleName"].ToString().Trim()))
                        {
                            RuleName = dt.Rows[index]["RuleName"].ToString().Trim();
                            TextBox5.Text = RuleName;
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["FieldArea"].ToString().Trim()))
                        {
                            RuleFiledArea = dt.Rows[index]["FieldArea"].ToString().Trim();
                            ShowInfoBrowser(RuleFiledArea);
                        }
                    }
                }
            }
        }

        #region "PrivateMethods"
        public void BindData()
        {
            string strSql = null;
            strSql = "SELECT ReferenceDescription, BibliographicsCitation,CONVERT(DATE, ISNULL(a.ModifyDT, a.CreateDT)) AS RevisionDate, ISNULL(b.ResourceName,'Admin') AS DeveloperName ";
            strSql += "FROM dbo.IE_Rules with(nolock) a LEFT JOIN dbo.Resources b with(nolock) ON ISNULL(a.ModifyBy,a.CreateBy) = b.ResourceId WHERE a.RuleId =" + RuleId;

            DataTable dt = new DataTable();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter parRuleId = cmd.CreateParameter();
                parRuleId.Direction = ParameterDirection.Input;
                parRuleId.Value = RuleId;
                parRuleId.ParameterName = "@RuleId";
                parRuleId.DbType = DbType.Int32;
                cmd.Parameters.Add(parRuleId);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["DeveloperName"].ToString().Trim()))
                        {
                            DeveloperName = dt.Rows[index]["DeveloperName"].ToString().Trim();
                            txtDevName.Text = DeveloperName;
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["RevisionDate"].ToString().Trim()))
                        {
                            ModefiedData = dt.Rows[index]["RevisionDate"].ToString().Trim();
                            txtRDate.Text = System.DateTime.Parse(ModefiedData).ToShortDateString();
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["ReferenceDescription"].ToString().Trim()))
                        {
                            TextBox5.Text= dt.Rows[index]["ReferenceDescription"].ToString().Trim();
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["BibliographicsCitation"].ToString().Trim()))
                        {
                            TextBox6.Text  = dt.Rows[index]["BibliographicsCitation"].ToString().Trim();
                        }
                        if (string.IsNullOrEmpty(TextBox6.Text))
                        {
                            btnInfo.Visible = false;
                        }
                    }
                }
            }
        }
        private void ShowInfoBrowser(string RuleFiledArea)
        {
            switch (RuleFiledArea)
            {
                case "PATIENT":
                    TextBox6.Text = "http://www.who.int/mediacentre/factsheets/fs404/en/";
                    break;
                case "PROBLEMS":
                    TextBox6.Text = "http://www.webmd.com/diabetes/type-2-diabetes-guide/type-2-diabetes#1";
                    break;
                case "MEDALLERGIES":
                    TextBox6.Text = "http://www.healthline.com/health/allergies/bee-sting-anaphylaxis";
                    break;
                case "MEDS":
                    TextBox6.Text = "http://www.healthline.com/drugs/warfarin/oral-tablet";
                    break;
                case "LABS":
                    TextBox6.Text = "https://www.allinahealth.org/CCS/doc/Thomson%20Consumer%20Lab%20Database/49/150341.htm";
                    break;
            }
        }
        #endregion

        private void btnInfo_Click(object sender, EventArgs e)
        {
            frmInfoScreen objfrmInfoScreen = new frmInfoScreen();
            objfrmInfoScreen.RuleId = RuleId;
            objfrmInfoScreen.IsBibilographic = true;
            objfrmInfoScreen.ShowDialog();
        }
    }
}
