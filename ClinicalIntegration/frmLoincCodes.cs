﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmLoincCodes : Form
    {
        public Form ParentMDI { get; set; }
        public bool IsEditMode { get; set; }
        public string LoincNumber { get; set; }
        public LabOrderModel objLabModel { get; set; }
        public frmLoincCodes()
        {
            InitializeComponent();
        }
        private void frmLoincCodes_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            comboBox1.SelectedItem = "LOINC Number";
            //dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.RowTemplate.Height = 32;
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string _Category = "";
            if (comboBox1.SelectedItem.ToString() == "LOINC Number")
            {
                _Category = "Loinc_num";
            }
            else if (comboBox1.SelectedItem.ToString() == "COMPONENT")
            {
                _Category = "Component";
            }
            else
            {
                _Category = "LONG_COMMON_NAME";
            }

            string LoincNumber = textBox1.Text;
            LabOrder objLO = new LabOrder();
            dataGridView1.DataSource = objLO.GetDataSet_LOINCNumber(LoincNumber, _Category);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (e.RowIndex >= 0)
            {
                string LoincNumber = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                frmNewOrder frm = new frmNewOrder();
                frm.IsLoincChanged = false;
                if (LoincNumber != objLabModel.LOINCNumber && this.IsEditMode)
                    frm.IsLoincChanged = true;
                frm.objLabModel = this.objLabModel;
                objLabModel.LOINCNumber = LoincNumber;
                frm.objLabModel = objLabModel;
                frm.MdiParent = this.ParentMDI;
                frm.ParentMDI = this.ParentMDI;
                frm.IsEditMode = this.IsEditMode;
                frm.Show();
                this.Close();
            }
        }
    }
}
