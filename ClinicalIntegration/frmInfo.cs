﻿using DBHelper;
using System;
using System.Data;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmInfo : Form
    {
        private string url { get; set; }
        private string homeUrl { get; set; }
        public string DrugName { get; set; }
        public string RxNormValue { get; set; }
        public string language { get; set; }
        public string PatientId { get; set; }
        public bool IsMedList { get; set; }
        public Object DBObject { get; set; }
        public bool IsProblemList { get; set; }
        public bool IsHome { get; set; }
        public string ExternalId { get; set; }
        public string ResourceId { get; set; }
        public string AppointmentId { get; set; }
        public string IcdCode { get; set; }
        public string CodeType { get; set; }

        public frmInfo()
        {
            InitializeComponent();
            btnPrev.Enabled = false;
            btnNext.Enabled = false;
        }

        private void btnUrl_Click(object sender, EventArgs e)
        {
            if (webBrowser1.Url != null)
            {
                url = webBrowser1.Url.AbsoluteUri;
                MessageBox.Show(url);
            }
        }

        private void frmInfo_Load(object sender, EventArgs e)
        {
            DataTable dtMed = new DataTable();
            DataTable dtLang = new DataTable();
            try
            {
                if (!IsProblemList)
                {
                    DBUtility db = new DBUtility();
                    db.DbObject = DBObject;
                    ClinicalComponent._Connection = db.DbObject as IDbConnection;
                    string _langQry = "Select Case When l.name like 'spanish%' then 'es' else 'en' end as Language from model.PatientLanguages pl with (nolock) inner join model.Languages l with (nolock) on pl.languageid=l.id where PatientId= " + int.Parse(PatientId);

                    string _MedIdQry = "Select Top 1 Rx.RxNorm from dbo.PatientClinical_Temp pc with (nolock) inner join [dbo].[FDBtoRxNorm1] rx with (nolock) on pc.drawfilename = rx.FDBId where clinicaltype = 'A' and pc.FindingDetail like 'Rx-8/" + DrugName + "%'";

                    if (ClinicalComponent._Connection.State == ConnectionState.Closed)
                        ClinicalComponent._Connection.Open();

                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = _langQry;
                        using (var reader = cmd.ExecuteReader())
                        {
                            dtLang.Load(reader);
                            if (dtLang.Rows.Count > 0)
                            {
                                language = dtLang.Rows[0]["language"].ToString();
                            }
                        }
                        cmd.CommandText = _MedIdQry;
                        using (var reader = cmd.ExecuteReader())
                        {
                            dtMed.Load(reader);
                            if (dtMed.Rows.Count > 0)
                            {
                                RxNormValue = Convert.ToString(dtMed.Rows[0]["RxNorm"]);
                            }
                        }
                        if (IsMedList == true && RxNormValue != "")
                        {
                            url = string.Format(ClinicalComponent.MedInfoUrl, RxNormValue, language);
                            if (IsUrlValid())
                            {
                                this.ExternalId = RxNormValue;
                                homeUrl = url;
                                PostMaterial2ERP(url);
                                webBrowser1.Navigate(url);
                            }
                            else
                            {
                                MessageBox.Show("No data available at MedLinePlus");
                                this.Close();
                            }
                        }
                    }
                    ClinicalComponent._Connection.Close();
                }
                else
                {
                    if (IsMedList == false && !string.IsNullOrEmpty(IcdCode))
                    {
                        if (DBObject != null)
                        {
                            DBUtility db = new DBUtility();
                            db.DbObject = DBObject;
                            ClinicalComponent._Connection = db.DbObject as IDbConnection;
                        }

                        string _langQry = "Select Case when l.name like 'spanish%' then 'es' else 'en' end as Language from model.PatientLanguages pl with (nolock) inner join model.Languages l with (nolock) on pl.languageid = l.id Where PatientId = " + int.Parse(PatientId);

                        if (ClinicalComponent._Connection.State == ConnectionState.Closed)
                            ClinicalComponent._Connection.Open();
                        using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = _langQry;
                            using (var reader = cmd.ExecuteReader())
                            {
                                dtLang.Load(reader);
                                if (dtLang.Rows.Count > 0)
                                {
                                    language = dtLang.Rows[0]["language"].ToString();
                                }
                            }
                        }
                        ClinicalComponent._Connection.Close();
                        language = language == null ? "en" : language;

                        if (CodeType == "ICD9")
                            url = string.Format(ClinicalComponent.ProblemInfoValidationUrl_Icd9, IcdCode, language);
                        else
                            url = string.Format(ClinicalComponent.ProblemInfoValidationUrl_Icd10, IcdCode, language);

                        if (IsUrlValid())
                        {
                            this.ExternalId = IcdCode;
                            homeUrl = url;
                            PostMaterial2ERP(url);
                            webBrowser1.Navigate(url);
                        }
                        else
                        {
                            MessageBox.Show("No data available at MedLinePlus");
                            this.Close();
                        }
                    }
                }
            }
            catch { }
        }

        private bool IsUrlValid()
        {
            bool retStatus = false;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                DataSet dsData = new DataSet();
                dsData.ReadXml(response.GetResponseStream());
                if (dsData.Tables["link"] != null)
                {
                    url = dsData.Tables["link"].Rows[0]["href"].ToString();
                    retStatus = true;
                }
            }
            catch
            {
                retStatus = false;
            }
            return retStatus;
        }

        private void PostMaterial2ERP(string URL)
        {
            EducationMaterial objEducationMaterial = new EducationMaterial()
            {
                DoctorExternalId = this.ResourceId,
                PatientExternalId = this.PatientId,
                ExternalId = this.ExternalId,
                DocumentName = IsMedList == true ? "Medication_" + this.ExternalId : "Problem_" + this.ExternalId,
                DocumentType = "Link",
                DocumentURL = URL,
                IsActive = "true",
                CodeType = IsMedList == true ? "Medication" : "Problem",
                PatientAppointmentId = this.AppointmentId
            };
            if (!objEducationMaterial.IsExist())
            {
                objEducationMaterial.Post();
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(homeUrl))
            {
                webBrowser1.Navigate(homeUrl);
                ManageNavigation();
                IsHome = true;
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            IsHome = false;
            if (webBrowser1.CanGoBack)
                webBrowser1.GoBack();
            ManageNavigation();

        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            IsHome = false;
            if (webBrowser1.CanGoForward)
                webBrowser1.GoForward();
            ManageNavigation();
        }
        private void ManageNavigation()
        {
            if (!IsHome)
            {
                if (webBrowser1.CanGoBack && !webBrowser1.CanGoForward)
                {
                    btnPrev.Enabled = true;
                    btnNext.Enabled = false;
                }
                else if (webBrowser1.CanGoForward && !webBrowser1.CanGoBack)
                {
                    btnNext.Enabled = true;
                    btnPrev.Enabled = false;
                }
                else if (webBrowser1.CanGoForward && webBrowser1.CanGoBack)
                {
                    btnPrev.Enabled = true;
                    btnNext.Enabled = true;
                }
                else
                {
                    btnPrev.Enabled = false;
                    btnNext.Enabled = false;
                }
            }
            else
            {
                IsHome = false;
                btnPrev.Enabled = false;
                btnNext.Enabled = false;
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

            if (!IsHome)
            {
                if (webBrowser1.CanGoBack && !webBrowser1.CanGoForward)
                {
                    btnPrev.Enabled = true;
                    btnNext.Enabled = false;
                }
                else if (webBrowser1.CanGoForward && !webBrowser1.CanGoBack)
                {
                    btnNext.Enabled = true;
                    btnPrev.Enabled = false;
                }
                else if (webBrowser1.CanGoForward && webBrowser1.CanGoBack)
                {
                    btnPrev.Enabled = true;
                    btnNext.Enabled = true;
                }
                else
                {
                    btnPrev.Enabled = false;
                    btnNext.Enabled = false;
                }
            }
            else
            {
                IsHome = false;
                btnPrev.Enabled = false;
                btnNext.Enabled = false;
            }
        }
    }
}
