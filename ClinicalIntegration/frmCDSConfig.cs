﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;


namespace ClinicalIntegration
{
    public partial class frmCDSConfig : Form
    {
        public object DBObject { get; set; }
        public string UserId { get; set; }
        public frmCDSConfig()
        {
            InitializeComponent();
        }

        public static List<CDSRoles> lstCDSRoles { get; set; }
        private void frmCDSConfig_Load_1(object sender, EventArgs e)
        {
            try
            {
                DBHelper.DBUtility db = new DBHelper.DBUtility();
                db.DbObject = DBObject;
                ClinicalComponent._Connection = db.DbObject as IDbConnection;
                ClinicalComponent.UserId = this.UserId;
                CDSConfig objCDS = new CDSConfig();
                grdProcedure.AutoGenerateColumns = false;
                lstCDSRoles = objCDS.GetCDSRoles();
                grdProcedure.DataSource = lstCDSRoles;
                UpdateInformationStatus();
                grdProcedure.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
                grdProcedure.EnableHeadersVisualStyles = false;
                grdProcedure.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
                grdProcedure.EnableHeadersVisualStyles = false;
                grdProcedure.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                grdProcedure.ColumnHeadersHeight = 34;
                grdProcedure.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
                button2.TabStop = false;
                button2.FlatStyle = FlatStyle.Flat;
                button2.FlatAppearance.BorderSize = 0;
            }
            catch
            {
            }
        }
        private void UpdateInformationStatus()
        {
            for (int i = 0; i < lstCDSRoles.Count; i++)
            {
                if (lstCDSRoles[i].Alerts)
                {
                }
                else
                {
                    DataGridViewCheckBoxCell chkCell = (DataGridViewCheckBoxCell)grdProcedure.Rows[i].Cells[3];
                    chkCell.Value = false;
                    chkCell.FlatStyle = FlatStyle.Flat;
                    chkCell.Style.ForeColor = Color.DarkGray;
                    chkCell.ReadOnly = true;
                }
            }
        }
        private void grdProcedure_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string resourceType = Convert.ToString(grdProcedure.Rows[e.RowIndex].Cells[0].Value);
                if (e.ColumnIndex == 4)
                {
                    DataGridViewCheckBoxCell chkConfig = (DataGridViewCheckBoxCell)grdProcedure.Rows[e.RowIndex].Cells[1];
                    DataGridViewCheckBoxCell chkInter = (DataGridViewCheckBoxCell)grdProcedure.Rows[e.RowIndex].Cells[2];
                    DataGridViewCheckBoxCell chkInfo = (DataGridViewCheckBoxCell)grdProcedure.Rows[e.RowIndex].Cells[3];
                    frmCDSUserConfig objfrmCDSUserConfig = new frmCDSUserConfig();
                    objfrmCDSUserConfig.enableConfig = Convert.ToBoolean(chkConfig.Value);
                    objfrmCDSUserConfig.enableIntervention = Convert.ToBoolean(chkInter.Value);
                    objfrmCDSUserConfig.enableInfo = Convert.ToBoolean(chkInfo.Value);
                    objfrmCDSUserConfig.ResourceType = Convert.ToString(resourceType.ToCharArray()[0]).Trim();
                    objfrmCDSUserConfig.ShowDialog();
                }
                else
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)grdProcedure.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    if (chk.ReadOnly == true && chk.Style.ForeColor == Color.DarkGray)
                    {
                        return;
                    }
                    string _updateQry = "";
                    int columnIndex = e.ColumnIndex;
                    switch (columnIndex)
                    {
                        case 1:
                            _updateQry = "UpdATE CDSRoles sET CDSConfiguration = CASE(CDSConfiguration) When 0 Then 1 Else 0 End wHERE ResourceType = '" + resourceType.ToCharArray()[0] + "'";
                            break;
                        case 2:
                            _updateQry = "UpdATE CDSRoles sET Alerts = CASE(Alerts) When 0 Then 1 Else 0  End,Info = CASE(Alerts) When 0 Then 1 Else 0 end wHERE ResourceType = '" + resourceType.ToCharArray()[0] + "'";
                            break;
                        case 3:
                            _updateQry = "UpdATE CDSRoles sET Info = CASE(Info) When 0 Then 1 Else 0 End wHERE ResourceType = '" + resourceType.ToCharArray()[0] + "'";
                            break;
                        default:
                            break;
                    }
                    CDSConfig objCDS = new CDSConfig();
                    objCDS.updateCDS(_updateQry);
                    lstCDSRoles = objCDS.GetCDSRoles();
                    grdProcedure.DataSource = lstCDSRoles;
                    UpdateInformationStatus();
                }
            }
            catch
            {
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
