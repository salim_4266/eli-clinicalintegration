﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public class RuleInfo
    {
        public int RuleId;
        public string RuleName;
        public string Result;
        public int EventId;
        public int Priority;
        public bool RuleValue;
        public bool ShowAlert;
        public bool PatientRule;
        public string OverrideComments;
        public DateTime TimeStamp;
        public int PatientId;
        public int EventRaiseId;

    }
    public class Medications
    {
        public int ClinicalId;
        public string MED_ROUTED_DF_MED_ID_DESC;
        public string MED_STRENGTH;
    }
    public class RulePhraseInfo
    {
        public int RuleId;
        public int Slno;
        public string FldArea;
        public string FldName;
        public string Operation;
        public ArrayList FldValue;
        public string AndOrFlag;
        public bool PhraseValue;
    }
    public class FldParamInfo
    {
        public string FldArea;
        public string FldName;
        public ArrayList FldValue;
        public int FldType;
    }

    public class ClsAlerts
    {
        public long m_PatientID;
        public long nEncounterID;
        public bool m_bShowIEAlters;
        public long m_CPTCTID;
        public string m_CPTCode;
        public string m_DiagCode;
        public string MedDesc;
        private ArrayList FldParamList;
        private ArrayList RuleList;
        private ArrayList RulePhrasesList;
        private int Rid = 0;
        private string m_stringlinkmsgDesc;
        public int UserId;
        public string AlertType;
        public string SnomedCPT;
        public string AllergyDesc;
        public Object DBObject { get; set; }

        // public List<LinkLabel> LinkLblList;
        public LinkLabel[] miscData;
        List<LinkLabel> LinkLblList = new List<LinkLabel>();
        private const string m_LoincSystolic = "8480-6";
        private const string m_LoincDiastolic = "8462-4";
        public void InitializeRuleParamers(string AlertNameType)
        {
            try
            {
                AlertType = AlertNameType;
                RuleList = new ArrayList();
                RulePhrasesList = new ArrayList();
                FldParamList = new ArrayList();
                GetRules();
                GetRulePhrases();
                GetFieldParams();
                GetFieldValues();
                ResetRuleStatus(true, true);
                EvaluateRules(1);
            }
            catch
            {


            }
        }
        public void GetFieldValues()
        {
            try
            {
                string strSql = "Select ";
                foreach (FldParamInfo myFldParam in FldParamList)
                {
                    if (myFldParam.FldArea.ToUpper() == "PATIENT")
                    {
                        if (myFldParam.FldName.ToUpper() == "AGE")
                        {
                            strSql += " DATEDIFF(yyyy,(SUBSTRING(BirthDate,1,4)+'-' + SUBSTRING(BirthDate,5,2) + '-' + SUBSTRING(BirthDate,7,2)),GETDATE())-CASE ";
                            strSql += " WHEN((MONTH(BirthDate)*100 + DAY(BirthDate)) > ";
                            strSql += " (MONTH(GETDATE())*100 + DAY(GETDATE()))) THEN 1 ";
                            strSql += " ELSE 0 END AS AGE ";
                        }
                        else if (myFldParam.FldName.ToUpper() == "GENDER")
                        {
                            string qryGender = string.Empty;
                            qryGender = "Select Type from model.patients p with(nolock) inner join  model.SexType_Enumeration s with(nolock) on p.sexid=s.id  where p.id=" + Convert.ToString(ClinicalComponent.PatientId) + "";
                            DataTable dtPatientGender = new DataTable();
                            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                                ClinicalComponent._Connection.Open();
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = qryGender;
                                using (var reader = cmd.ExecuteReader())
                                {
                                    dtPatientGender.Load(reader);
                                }
                            }
                            if (dtPatientGender.Rows.Count != 0)
                            {
                                myFldParam.FldValue.Add(Convert.ToString(dtPatientGender.Rows[0]["Type"]));
                            }
                        }
                        else
                        {
                            strSql += myFldParam.FldName + ",";
                        }
                    }
                    if (myFldParam.FldArea.ToUpper() == "ENCOUNTER" & myFldParam.FldName.ToLower().Contains("BMI".ToLower()))
                    {
                        myFldParam.FldValue = GetPtnBMI();
                    }

                    else if (myFldParam.FldArea.ToUpper() == "ENCOUNTER" & myFldParam.FldName.ToLower().Contains("Blood Pressure Systolic".ToLower()))
                    {
                        myFldParam.FldValue = GetVitalSignByLoinc(m_LoincSystolic, ClinicalComponent.PatientId);
                    }
                    else if (myFldParam.FldArea.ToUpper() == "ENCOUNTER" & myFldParam.FldName.ToLower().Contains("Blood Pressure Diastolic".ToLower()))
                    {
                        myFldParam.FldValue = GetVitalSignByLoinc(m_LoincDiastolic, ClinicalComponent.PatientId);
                    }
                    if (myFldParam.FldArea.ToUpper() == "PROBLEMS" || myFldParam.FldArea.ToUpper() == "ExamElements".ToUpper() || myFldParam.FldArea.ToUpper() == "MEDS")
                    {
                        myFldParam.FldValue = GetProblemCPTMEDSInformation(myFldParam.FldArea.ToUpper());

                    }
                    else if (myFldParam.FldArea.ToUpper() == "MEDALLERGIES" & myFldParam.FldName.ToUpper() == "MEDALLERGIES")
                    {
                        myFldParam.FldValue = GetAllergiesInfo();
                    }
                }
                strSql = strSql.Substring(0, strSql.Length - 1) + " from PatientDemoGraphics with(nolock) Where PatientId=" + m_PatientID;
                DataTable dtPatient = new DataTable();
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = strSql;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtPatient.Load(reader);
                    }
                }

                if (dtPatient == null)
                    return;

                foreach (FldParamInfo myFldParamL in FldParamList)
                {
                    if (dtPatient.Rows.Count > 0)
                    {
                        if (myFldParamL.FldName.ToLower() == "age" & myFldParamL.FldArea.ToLower() == "patient")
                        {
                            myFldParamL.FldValue.Add(Convert.ToString(dtPatient.Rows[0]["AGE"]));
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public ArrayList GetAllergiesInfo()
        {
            try
            {
                ArrayList AllergyList = new ArrayList();
                DataTable dt = new DataTable();
                String query = "select Top(1) Description from AllergyView with(nolock) where conceptId in (select top(1) ConceptID from fdb.tblcompositeallergy with(nolock) Where description  like '%" + AllergyDesc.Trim() + "%')";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                if ((dt != null))
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int index = 0; index <= dt.Rows.Count - 1; index++)
                        {
                            if (!string.IsNullOrEmpty(dt.Rows[index]["Description"].ToString().Trim()))
                            {
                                AllergyList.Add(dt.Rows[index]["Description"].ToString().Trim());
                            }
                        }
                    }
                }
                return AllergyList;
            }
            catch
            {
            }
            return new ArrayList();
        }


        public ArrayList GetProblemCPTMEDSInformation(string strCodes)
        {
            string strSql = null;
            ArrayList ProblemList = new ArrayList();
            ProblemList.Clear();
            if (!strCodes.ToLower().Contains(AlertType.ToLower()))
                return ProblemList;
            DBHelper.DBUtility db = new DBHelper.DBUtility();
            IDbCommand Cmd = ClinicalComponent._Connection.CreateCommand();

            switch (strCodes)
            {
                case "PROBLEMS":
                    db.DbObject = DBObject;
                    ClinicalComponent._Connection = db.DbObject as IDbConnection;
                    Cmd.Parameters.Clear();

                    DataTable dt = new DataTable();
                    String query = "Select ICD10 from dbo.Snomed_2016 with(nolock) where conceptid =" + SnomedCPT + " ";
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        using (var reader = cmd.ExecuteReader())
                        {
                            dt.Load(reader);
                        }
                    }

                    if ((dt != null))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            for (int index = 0; index <= dt.Rows.Count - 1; index++)
                            {
                                if (!string.IsNullOrEmpty(dt.Rows[index]["ICD10"].ToString().Trim()))
                                {
                                    ProblemList.Add(dt.Rows[index]["ICD10"].ToString().Trim());
                                }
                            }
                        }
                    }
                    return ProblemList;
                case "MEDS":
                    //if (MedDesc.Contains('%')||MedDesc.ToLower().Contains("mg"))
                    //    strSql = "Select top(1) MED_ROUTED_DF_MED_ID_DESC as Med_Desc from fdb.tblCompositeDrug where med_MedId_desc like '%" + MedDesc + "%'";
                    //else
                    strSql = "Select top(1) MED_ROUTED_DF_MED_ID_DESC as Med_Desc from fdb.tblCompositeDrug with(nolock) Where MED_ROUTED_DF_MED_ID_DESC like '%" + MedDesc + "%'";
                    db.DbObject = DBObject;
                    ClinicalComponent._Connection = db.DbObject as IDbConnection;
                    Cmd.Parameters.Clear();
                    DataTable dtMed = new DataTable();
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strSql;
                        using (var reader = cmd.ExecuteReader())
                        {
                            dtMed.Load(reader);
                        }
                    }
                    if ((dtMed != null))
                    {
                        if (dtMed.Rows.Count > 0)
                        {
                            for (int index = 0; index <= dtMed.Rows.Count - 1; index++)
                            {
                                if (!string.IsNullOrEmpty(dtMed.Rows[index]["Med_Desc"].ToString().Trim()))
                                {
                                    ProblemList.Add(dtMed.Rows[index]["Med_Desc"].ToString().Trim());
                                }
                            }
                        }
                    }
                    return ProblemList;
            }
            return ProblemList;
        }

        public void ResetRuleStatus(bool RuleValue, bool RulePhraseValue)
        {
            if (RuleValue)
            {
                foreach (RuleInfo myRule in RuleList)
                {
                    myRule.RuleValue = false;
                }
            }
            if (RulePhraseValue)
            {
                foreach (RulePhraseInfo myRulePhrase in RulePhrasesList)
                {
                    myRulePhrase.PhraseValue = false;
                }
            }
        }

        public void EvaluateRules(int EventId)
        {
            ResetRuleStatus(false, true);
            foreach (RuleInfo myRuleInfo in RuleList)
            {
                if (myRuleInfo.EventId == EventId && myRuleInfo.RuleValue == false && string.IsNullOrEmpty(myRuleInfo.OverrideComments))
                {
                    if (myRuleInfo.PatientRule == true)
                    {
                        EvaluateRule(myRuleInfo.RuleId, myRuleInfo.PatientId);
                    }
                    else
                    {
                        EvaluateRule(myRuleInfo.RuleId, 0);
                    }
                }
            }
        }

        public void EvaluateRule(int RuleId, int iPatientId)
        {
            try
            {
                bool bPhraseValue = false;
                ArrayList myFldValue = null;
                int myFldType = 0;
                string myFldArea = "";
                string strAndOrExpression = "";
                int iPos = 0;
                string strLeft = null;
                string strMiddle = null;
                string strRight = null;
                m_stringlinkmsgDesc = "";

                //ReDim miscData() As LinkLabel
                //Dim LinkLblList As New List(Of LinkLabel)
                foreach (RulePhraseInfo myRulePhrase in RulePhrasesList)
                {
                    if (myRulePhrase.RuleId == RuleId)
                    {
                        bPhraseValue = true;
                        myFldValue = null;
                        myFldType = 0;
                        //Get the FieldValue and FieldType

                        foreach (FldParamInfo myFldParam in FldParamList)
                        {
                            if (myFldParam.FldArea.ToUpper() == myRulePhrase.FldArea.ToUpper() && myFldParam.FldName.ToUpper() == myRulePhrase.FldName.ToUpper())
                            {
                                myFldValue = myFldParam.FldValue;
                                myFldType = myFldParam.FldType;
                                myFldArea = myFldParam.FldArea.ToUpper();
                                break; // TODO: might not be correct. Was : Exit For
                            }
                            else if (myFldParam.FldArea.ToUpper() == "ENCOUNTER" && myRulePhrase.FldArea.ToUpper() == "ENCOUNTER" && myRulePhrase.FldName == "8462-4" && myFldParam.FldName.ToLower() == "Blood Pressure Diastolic".ToLower())
                            {
                                myFldValue = myFldParam.FldValue;
                                myFldType = myFldParam.FldType;
                                myFldArea = myFldParam.FldArea.ToUpper();
                                break;
                            }

                            else if (myFldParam.FldArea.ToUpper() == "ENCOUNTER" && myRulePhrase.FldArea.ToUpper() == "ENCOUNTER" && myRulePhrase.FldName == "8480-6" && myFldParam.FldName.ToLower() == "Blood Pressure Systolic".ToLower())
                            {
                                myFldValue = myFldParam.FldValue;
                                myFldType = myFldParam.FldType;
                                myFldArea = myFldParam.FldArea.ToUpper();
                                break;
                            }
                            //Exit For
                            //else if (myFldParam.FldArea.ToUpper() == "LABS" & myRulePhrase.FldArea.ToUpper() == "LABS")
                            //{
                            //    myFldValue = myFldParam.FldValue;
                            //    myFldType = myFldParam.FldType;
                            //    myFldArea = myFldParam.FldArea.ToUpper();
                            //    myFldValue = GetproblemListByLoinc(myRulePhrase.FldName);
                            //    break; // TODO: might not be correct. Was : Exit For
                            //}
                        }
                        if (myFldValue == null)
                        {
                            break; // TODO: might not be correct. Was : Exit For
                        }
                        if (myFldValue.Count == 0 & (myFldArea == "PATIENT" || myFldArea == "ENCOUNTER") || myRulePhrase.FldValue.Count == 0)
                        {
                            bPhraseValue = false;
                        }
                        else if (myFldArea.ToUpper() == "QUERY" | myFldArea.ToUpper() == "QUERY")
                        {
                            switch (myRulePhrase.Operation)
                            {
                                case "IN":
                                    if (iPatientId > 0)
                                    {
                                        if (m_PatientID == iPatientId)
                                        {
                                            bPhraseValue = EvaluateQuery("=", myRulePhrase.FldValue);
                                            break; // TODO: might not be correct. Was : Exit Select
                                        }
                                        else
                                        {
                                            bPhraseValue = false;
                                        }
                                    }
                                    else
                                    {
                                        bPhraseValue = EvaluateQuery("=", myRulePhrase.FldValue);
                                        break; // TODO: might not be correct. Was : Exit Select
                                    }
                                    break;
                                case "Not IN":
                                    if (iPatientId > 0)
                                    {
                                        if (m_PatientID == iPatientId)
                                        {
                                            bPhraseValue = EvaluateQuery("<>", myRulePhrase.FldValue);
                                            break; // TODO: might not be correct. Was : Exit Select
                                        }
                                        else
                                        {
                                            bPhraseValue = false;
                                        }
                                    }
                                    else
                                    {
                                        bPhraseValue = EvaluateQuery("<>", myRulePhrase.FldValue);
                                        break; // TODO: might not be correct. Was : Exit Select
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            switch (myRulePhrase.Operation)
                            {
                                case "=":
                                case "<":
                                case ">":
                                case "<=":
                                case ">=":
                                case "<>":
                                    if (iPatientId > 0)
                                    {
                                        if (m_PatientID == iPatientId)
                                        {
                                            bPhraseValue = EvaluatePhrase(myFldValue, myRulePhrase.Operation, myRulePhrase.FldValue);
                                            break; // TODO: might not be correct. Was : Exit Select
                                        }
                                        else
                                        {
                                            bPhraseValue = false;
                                        }
                                    }
                                    else
                                    {
                                        bPhraseValue = EvaluatePhrase(myFldValue, myRulePhrase.Operation, myRulePhrase.FldValue);
                                        break; // TODO: might not be correct. Was : Exit Select
                                    }
                                    break;
                                case "IN":
                                    Rid = 0;
                                    Rid = RuleId;
                                    if (iPatientId > 0)
                                    {
                                        if (m_PatientID == iPatientId)
                                        {
                                            bPhraseValue = EvaluatePhrase(myFldValue, "=", myRulePhrase.FldValue);
                                            break; // TODO: might not be correct. Was : Exit Select
                                        }
                                        else
                                        {
                                            bPhraseValue = false;
                                        }
                                    }
                                    else
                                    {
                                        bPhraseValue = EvaluatePhrase(myFldValue, "=", myRulePhrase.FldValue);
                                        break; // TODO: might not be correct. Was : Exit Select
                                    }
                                    break;
                                case "Not IN":
                                    if (iPatientId > 0)
                                    {
                                        if (m_PatientID == iPatientId)
                                        {
                                            bPhraseValue = !(EvaluatePhrase(myFldValue, "=", myRulePhrase.FldValue));
                                            break; // TODO: might not be correct. Was : Exit Select
                                        }
                                        else
                                        {
                                            bPhraseValue = false;
                                        }
                                    }
                                    else
                                    {
                                        bPhraseValue = !(EvaluatePhrase(myFldValue, "=", myRulePhrase.FldValue));
                                        break; // TODO: might not be correct. Was : Exit Select
                                    }
                                    break;
                                default:
                                    bPhraseValue = false;
                                    break; // TODO: might not be correct. Was : Exit Select
                            }
                        }
                        myRulePhrase.PhraseValue = bPhraseValue;
                    }
                }

                foreach (RulePhraseInfo myRulePhrase in RulePhrasesList)
                {
                    if (myRulePhrase.RuleId == RuleId)
                    {
                        strAndOrExpression += ((myRulePhrase.PhraseValue == true) ? "1 " : "0 ") + myRulePhrase.AndOrFlag + " ";
                    }
                }
                strAndOrExpression = strAndOrExpression.Trim().ToUpper();
                //Evaluate AND Operators
                iPos = strAndOrExpression.IndexOf("AND");
                while (iPos != -1)
                {
                    strLeft = "";
                    strRight = "";
                    strMiddle = "";
                    if (iPos > 2)
                    {
                        strLeft = strAndOrExpression.Substring(0, iPos - 2).Trim();
                    }
                    if (iPos + 6 < strAndOrExpression.Length)
                    {
                        strRight = strAndOrExpression.Substring(iPos + 6, strAndOrExpression.Length - iPos - 6).Trim();
                    }
                    strMiddle = strAndOrExpression.Substring(iPos - 2, 7).Trim();
                    switch (strMiddle)
                    {
                        case "1 AND 1":
                            strMiddle = " 1 ";
                            break; // TODO: might not be correct. Was : Exit Select
                        case "1 AND 0":
                        case "0 AND 1":
                        case "0 AND 0":
                            strMiddle = " 0 ";
                            break; // TODO: might not be correct. Was : Exit Select
                    }
                    strAndOrExpression = strLeft + strMiddle + strRight;
                    strAndOrExpression = strAndOrExpression.Trim();
                    iPos = strAndOrExpression.IndexOf("AND");
                }
                //Evaluate OR Operators
                iPos = strAndOrExpression.IndexOf("OR");
                while (iPos != -1)
                {
                    strLeft = "";
                    strRight = "";
                    strMiddle = "";
                    if (iPos > 2)
                    {
                        strLeft = strAndOrExpression.Substring(0, iPos - 2).Trim();
                    }
                    if (iPos + 5 < strAndOrExpression.Length)
                    {
                        strRight = strAndOrExpression.Substring(iPos + 5, strAndOrExpression.Length - iPos - 5).Trim();
                    }
                    strMiddle = strAndOrExpression.Substring(iPos - 2, 6).Trim();
                    switch (strMiddle)
                    {
                        case "1 OR 1":
                        case "1 OR 0":
                        case "0 OR 1":
                            strMiddle = " 1 ";
                            break; // TODO: might not be correct. Was : Exit Select
                        case "0 OR 0":
                            strMiddle = " 0 ";
                            break; // TODO: might not be correct. Was : Exit Select
                    }
                    strAndOrExpression = strLeft + strMiddle + strRight;
                    strAndOrExpression = strAndOrExpression.Trim();
                    iPos = strAndOrExpression.IndexOf("OR");
                }
                if (strAndOrExpression == "1")
                {
                    foreach (RuleInfo myRule in RuleList)
                    {
                        if (myRule.RuleId == RuleId)
                        {
                            if (myRule.ShowAlert & true) //bShowIEAlters here 
                            {
                                frmInferenceAlerts objInferenceAlerts = new frmInferenceAlerts();
                                if ((m_stringlinkmsgDesc.ToUpper() == "PROBLEMS"))
                                {
                                    GetLabelwithLinks(myRule.Result, ref objInferenceAlerts.txtMessage);
                                }
                                else
                                {
                                    objInferenceAlerts.txtMessage.Text = "";
                                    objInferenceAlerts.txtMessage.Text = myRule.Result;
                                }

                                objInferenceAlerts.txtMessage.Font = new System.Drawing.Font("", 10, FontStyle.Bold);
                                objInferenceAlerts.StartPosition = FormStartPosition.CenterScreen;
                                objInferenceAlerts.pnlComments.Visible = false;
                                objInferenceAlerts.RuleId = RuleId;
                                objInferenceAlerts.pnlComments.Visible = true;
                                objInferenceAlerts.RuleCondition = GetRuleCondition(Convert.ToString(RuleId));
                                if (DisplayInfoButton(Convert.ToString(RuleId)))
                                {
                                    objInferenceAlerts.Button2.Visible = true;
                                    objInferenceAlerts.Button1.Visible = true;
                                }
                                else
                                {
                                    objInferenceAlerts.Button2.Visible = false;
                                    objInferenceAlerts.Button1.Visible = false;
                                }
                                objInferenceAlerts.ShowDialog();

                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void GetLabelwithLinks(string msg, ref RichTextBox Txt)
        {
            Txt.Text = msg + Environment.NewLine;
            if (LinkLblList.Count > 0)
            {
                foreach (LinkLabel Control in LinkLblList)
                {
                    Control.Location = Txt.GetPositionFromCharIndex(Txt.TextLength);
                    Control.Size = new Size(500, 30);
                    Txt.Controls.Add(Control);
                }
            }

        }



        private bool DisplayInfoButton(string RuleId)
        {
            DataTable dt = new DataTable();
            String query = "Select Information from dbo.IE_RulesPermissions with(nolock) Where userid=" + UserId + " and RuleId =" + RuleId;
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["Information"].ToString().Trim()))
                        {
                            return Convert.ToBoolean(dt.Rows[index]["Information"]);
                        }
                    }
                }
            }
            return false;
        }
        private bool EvaluatePhrase(ArrayList LeftValue, string Operator, ArrayList RightValue)
        {

            bool bResult = false;
            string strLeft = null;
            string strRight = null;
            foreach (string strLeft_loopVariable in LeftValue)
            {
                strLeft = strLeft_loopVariable;
                foreach (string strRight_loopVariable in RightValue)
                {
                    strRight = strRight_loopVariable;
                    switch (Operator)
                    {
                        case "=":
                            if (AlertType == "MEDS" || AlertType == "MEDALLERGIES" || AlertType == "Problem")
                            {
                                strRight = strRight.Replace(@"'", "");
                            }
                            if (strLeft == strRight)
                            {
                                bResult = true; break; // TODO: might not be correct. Was : Exit For
                            }
                            break;
                        case "<":
                            if (Convert.ToInt32(strLeft) < Convert.ToInt32(strRight))
                            {
                                bResult = true; break; // TODO: might not be correct. Was : Exit For
                            }
                            break;
                        case ">":
                            if (Convert.ToInt32(strLeft) > Convert.ToInt32(strRight))
                            {
                                bResult = true; break; // TODO: might not be correct. Was : Exit For
                            }
                            break;
                        case "<=":
                            if (Convert.ToInt32(strLeft) <= Convert.ToInt32(strRight))
                            {
                                bResult = true; break; // TODO: might not be correct. Was : Exit For
                            }
                            break;
                        case ">=":
                            if (Convert.ToInt32(strLeft) >= Convert.ToInt32(strRight))
                            {
                                bResult = true; break; // TODO: might not be correct. Was : Exit For
                            }
                            break;
                        case "<>":
                            if (strLeft != strRight)
                            {
                                bResult = true; break; // TODO: might not be correct. Was : Exit For
                            }
                            break;
                    }
                }
                if (bResult)
                    break; // TODO: might not be correct. Was : Exit For
            }
            return bResult;
        }

        //=======================================================
        //Service provided by Telerik (www.telerik.com)
        //Conversion powered by NRefactory.
        //Twitter: @telerik
        //Facebook: facebook.com/telerik
        //=======================================================

        //public bool EvaluatePhrase(ArrayList LeftValue, string Operator, ArrayList RightValue)
        //{
        //    bool bResult = false;
        //    m_stringlinkmsgDesc = GetProblem(Rid);
        //    foreach (string strLeft in LeftValue)
        //    {
        //        foreach (string strRight in RightValue)
        //        {
        //            switch (Operator)
        //            {
        //                case "=":
        //                    if (Information.IsNumeric(strLeft.Trim().ToUpper()) & Information.IsNumeric(strRight.Trim().Replace("'", "").ToUpper()))
        //                    {
        //                        if (Convert.ToInt32(strLeft.Trim().ToUpper()) == Convert.ToInt32(strRight.Trim().Replace("'", "").ToUpper()))
        //                        {
        //                            if ((m_stringlinkmsgDesc.ToUpper() == "PROBLEMS"))
        //                            {
        //                                LinkLblList.Add(SetLinkLabel(strRight));
        //                            }
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (strLeft.ToUpper() == strRight.Trim().Replace("'", "").ToUpper())
        //                        {
        //                            if ((m_stringlinkmsgDesc.ToUpper() == "PROBLEMS"))
        //                            {
        //                                //LinkLblList.Add(SetLinkLabel(strRight))
        //                            }
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    break; // TODO: might not be correct. Was : Exit Select

        //                    break;
        //                case "<":
        //                    if (Information.IsNumeric(strLeft.Trim().ToUpper()) & Information.IsNumeric(strRight.Trim().Replace("'", "").ToUpper()))
        //                    {
        //                        if (Convert.ToInt32(strLeft.Trim().ToUpper()) < Convert.ToInt32(strRight.Trim().Replace("'", "").ToUpper()))
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (strLeft.Trim().ToUpper() < strRight.Trim().Replace("'", "").ToUpper())
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    break; // TODO: might not be correct. Was : Exit Select

        //                    break;
        //                case ">":
        //                    if (Information.IsNumeric(strLeft.Trim().ToUpper()) & Information.IsNumeric(strRight.Trim().Replace("'", "").ToUpper()))
        //                    {
        //                        if (Convert.ToInt32(strLeft.Trim().ToUpper()) > Convert.ToInt32(strRight.Trim().Replace("'", "").ToUpper()))
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (strLeft.Trim().ToUpper() > strRight.Trim().Replace("'", "").ToUpper())
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    break; // TODO: might not be correct. Was : Exit Select

        //                    break;
        //                case "<=":
        //                    if (Information.IsNumeric(strLeft.Trim().ToUpper()) & Information.IsNumeric(strRight.Trim().Replace("'", "").ToUpper()))
        //                    {
        //                        if (Convert.ToInt32(strLeft.Trim().ToUpper()) <= Convert.ToInt32(strRight.Trim().Replace("'", "").ToUpper()))
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (strLeft.Trim().ToUpper() <= strRight.Trim().Replace("'", "").ToUpper())
        //                        {
        //                            bResult = true;

        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    break; // TODO: might not be correct. Was : Exit Select

        //                    break;
        //                case ">=":
        //                    if (Information.IsNumeric(strLeft.Trim().ToUpper()) & Information.IsNumeric(strRight.Trim().Replace("'", "").ToUpper()))
        //                    {
        //                        if (Convert.ToInt32(strLeft.Trim().ToUpper()) >= Convert.ToInt32(strRight.Trim().Replace("'", "").ToUpper()))
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (strLeft.Trim().ToUpper() >= strRight.Trim().Replace("'", "").ToUpper())
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    break; // TODO: might not be correct. Was : Exit Select

        //                    break;
        //                case "<>":
        //                    if (Information.IsNumeric(strLeft.Trim().ToUpper()) & Information.IsNumeric(strRight.Trim().Replace("'", "").ToUpper()))
        //                    {
        //                        if (!(Convert.ToInt32(strLeft.Trim().ToUpper()) == Convert.ToInt32(strRight.Trim().Replace("'", "").ToUpper())))
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (!(strLeft.Trim().ToUpper() == strRight.Trim().Replace("'", "").ToUpper()))
        //                        {
        //                            bResult = true;
        //                            goto ExitLabel1;
        //                        }
        //                    }
        //                    break; // TODO: might not be correct. Was : Exit Select

        //                    break;
        //            }
        //        }
        //        ExitLabel1:
        //        if (bResult)
        //        {
        //            break; // TODO: might not be correct. Was : Exit For
        //        }
        //    }
        //    return bResult;
        //}

        //=======================================================
        //Service provided by Telerik (www.telerik.com)
        //Conversion powered by NRefactory.
        //Twitter: @telerik
        //Facebook: facebook.com/telerik
        //=======================================================

        private bool ExceuteQuery(string query, bool isnotoperator)
        {
            try
            {
                //IDbCommand cmd = Connection.CreateCommand();
                //cmd.Parameters.Clear();

                //dynamic param = cmd.CreateParameter();
                //param.ParameterName = "@PatientId";
                //param.Value = PatientID;
                //param.Direction = ParameterDirection.Input;
                //param.DbType = DbType.Int32;

                //cmd.Parameters.Add(param);

                //dynamic returnArgument = DbHelper.ExecuteQuery(query, cmd);

                //if (returnArgument == null && isnotoperator)
                //{
                //    return true;
                //}
                //else if (returnArgument == null && !isnotoperator)
                //{
                //    return false;
                //}

                //return Convert.ToBoolean(returnArgument);
                return true;

            }
            catch
            {
                if (isnotoperator)
                    return true;
                return false;
            }
        }


        private bool EvaluateQuery(string @operator, ArrayList Queries)
        {
            foreach (string query in Queries)
            {
                switch (@operator)
                {
                    case "=":
                        return ExceuteQuery(query, false);
                    case "<>":
                        return !ExceuteQuery(query, true);
                }
            }
            return false;
        }
        public void GetFieldParams()
        {
            //IDbCommand Cmd = Connection.CreateCommand();

            //DataTable dtRuleParams = DbHelper.ExecuteQuery("Usp_RulesEngine_GetFieldParams", false, Cmd);
            FldParamInfo myFldParam = default(FldParamInfo);
            //
            DataTable dtRuleParams = new DataTable();
            String query = "Usp_RulesEngine_GetFieldParams";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dtRuleParams.Load(reader);
                }
            }
            //

            if (dtRuleParams == null)
                return;
            try
            {
                FldParamList.Clear();
                foreach (DataRow dr in dtRuleParams.Rows)
                {
                    myFldParam = new FldParamInfo();
                    myFldParam.FldArea = dr["FldArea"].ToString();
                    myFldParam.FldName = dr["FldName"].ToString();
                    myFldParam.FldType = Convert.ToInt32(dr["FldType"]);
                    myFldParam.FldValue = new ArrayList();
                    FldParamList.Add(myFldParam);
                }
            }
            catch
            {
            }
        }
        public void GetRulePhrases()
        {
            try
            {

                RulePhraseInfo myRulePhrase = default(RulePhraseInfo);
                string strValues = null;
                string strValue = null;
                int iPos = 0;
                RulePhrasesList.Clear();
                DataTable dtRulePhrases = new DataTable();
                String query = "USP_RulesEngine_GetRulePhrases";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtRulePhrases.Load(reader);
                    }
                }
                if (dtRulePhrases == null)
                    return;
                foreach (DataRow dr in dtRulePhrases.Rows)
                {
                    myRulePhrase = new RulePhraseInfo();
                    myRulePhrase.RuleId = Convert.ToInt32(dr["RuleId"]);
                    myRulePhrase.Slno = Convert.ToInt32(dr["Slno"]);
                    myRulePhrase.FldArea = Convert.ToString(dr["FieldArea"]);
                    if (!myRulePhrase.FldArea.ToLower().Contains(AlertType.ToLower()))
                    {
                        continue;
                    }
                    myRulePhrase.FldName = Convert.ToString(dr["FieldName"]);
                    myRulePhrase.Operation = Convert.ToString(dr["Operation"]);
                    myRulePhrase.FldValue = new ArrayList();
                    if (Convert.ToString(dr["FieldArea"]).ToUpper() == "QUERY" & Convert.ToString(dr["FieldArea"]).ToUpper() == "QUERY")
                    {
                        strValues = Convert.ToString(dr[5]);
                        myRulePhrase.FldValue.Add(strValues);
                    }
                    else if (myRulePhrase.Operation == "IN" || myRulePhrase.Operation == "Not IN")
                    {
                        strValues = Convert.ToString(dr[5]);
                        iPos = strValues.IndexOf("','");
                        while (iPos != -1)
                        {
                            strValue = strValues.Substring(1, iPos - 1);
                            strValues = strValues.Substring(iPos + 2, strValues.Length - iPos - 2);
                            iPos = strValues.IndexOf("','");
                            myRulePhrase.FldValue.Add(strValue);
                        }
                        myRulePhrase.FldValue.Add(strValues);
                    }
                    else
                    {
                        strValue = Convert.ToString(Convert.ToString(dr[5]));
                        if (strValue.Substring(0, 1) == "'")
                        {
                            strValue = strValue.Substring(1, strValue.Length - 1);
                        }
                        if (strValue.Substring(strValue.Length - 1, 1) == "'")
                        {
                            strValue = strValue.Substring(0, strValue.Length - 1);
                        }
                        myRulePhrase.FldValue.Add(strValue);
                    }
                    myRulePhrase.AndOrFlag = Convert.ToString(dr[6]);
                    myRulePhrase.PhraseValue = false;
                    RulePhrasesList.Add(myRulePhrase);
                }

            }
            catch
            {
            }
        }
        public void GetRules()
        {
            // IDbCommand Cmd = Connection.CreateCommand();
            DBHelper.DBUtility db = new DBHelper.DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            IDbCommand Cmd = ClinicalComponent._Connection.CreateCommand();
            Cmd.Parameters.Clear();


            RuleList.Clear();
            RuleInfo myRule = default(RuleInfo);
            DataTable dtRules = new DataTable();
            String query = "USP_RulesEngine_GetRulesLatest";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter parResourceID = cmd.CreateParameter();
                parResourceID.Direction = ParameterDirection.Input;
                parResourceID.Value = UserId;
                parResourceID.ParameterName = "@userId";
                parResourceID.DbType = DbType.Int32;
                cmd.Parameters.Add(parResourceID);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dtRules.Load(reader);
                }
                if ((dtRules != null))
                {
                    foreach (DataRow dr in dtRules.Rows)
                    {
                        myRule = new RuleInfo();
                        if (Convert.ToInt32(dr["EventRaiseId"]) != 2)
                            continue;
                        myRule.RuleId = Convert.ToInt32(dr["RuleId"]);
                        myRule.RuleName = dr["RuleName"].ToString();
                        myRule.EventId = Convert.ToInt32(dr["EventId"]);
                        myRule.Result = dr["Result"].ToString();
                        myRule.ShowAlert = Convert.ToBoolean(dr[5]);
                        myRule.RuleValue = false;
                        myRule.OverrideComments = "";
                        myRule.PatientRule = Convert.ToBoolean(dr[6]);
                        myRule.PatientId = Convert.ToInt32(dr["PatientID"]);
                        RuleList.Add(myRule);
                    }
                }
            }
        }

        public ArrayList GetVitalSignByLoinc(String LoincNum, String PatientId)
        {

            ArrayList vitalSignAr = new ArrayList();
            string strSql = "Select Result, LoincNumber from dbo.PatientLabTestResults PLT with(nolock) INNER JOIN dbo.LoincTestResults LTR with(nolock) ";
            strSql += "On LTR.Id=PLT.LabTestResultId WHERE PLT.PatientId = '" + PatientId + "' And Loincnumber ='" + LoincNum.Trim() + "'";
            DataTable dt = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["Result"].ToString().Trim()))
                        {
                            vitalSignAr.Add(dt.Rows[index]["Result"].ToString().Trim());
                        }
                    }
                }
            }
            return vitalSignAr;
        }
        private string GetRuleCondition(string RuleId)
        {
            DataTable dt = new DataTable();
            string strSql = null;
            strSql = "Select Case FieldArea When 'MEDALLERGIES' THEN 'PATIENT' WHEN 'MEDS' THEN 'PATIENT' WHEN 'PROBLEMS' THEN 'PATIENT' WHEN 'LABS' THEN 'PATIENT LABS'   ELSE FieldArea END  +'  '+ FieldName +'  '+Operation +'  '+fieldvalue AS RuleCondition, AndorFlag from dbo.IE_RulesPhrases with(nolock) WHERE RULEID=" + RuleId;
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 1)
                {
                    string combiCond = string.Empty;
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["RuleCondition"].ToString().Trim()))
                        {
                            combiCond += Convert.ToString(dt.Rows[index]["RuleCondition"]) + "  " + Convert.ToString(dt.Rows[index]["AndorFlag"]) + Environment.NewLine;
                        }
                    }
                    return combiCond;
                }
                else if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["RuleCondition"].ToString().Trim()))
                        {
                            return Convert.ToString(dt.Rows[index]["RuleCondition"]);
                        }
                    }
                }
            }
            return string.Empty;
        }

        private ArrayList GetproblemListByLoinc(string LoincNum)
        {

            string strSql = null;
            ArrayList labResultAr = new ArrayList();
            strSql = "Select Result, LoincNumber from dbo.PatientLabTestResults PLT with(nolock) INNER JOIN dbo.LoincTestResults LTR with(nolock) ";
            strSql += "On LTR.Id=PLT.LabTestResultId WHERE PLT.PatientId = '" + ClinicalComponent.PatientId + "' And Loincnumber ='" + LoincNum + "'";

            DataTable dt = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["Result"].ToString().Trim()))
                        {
                            labResultAr.Add(dt.Rows[index]["Result"].ToString().Trim());
                        }
                    }
                }
            }
            return labResultAr;
        }

        public ArrayList GetPtnBMI()
        {
            string weight = null;
            string height = null;
            ArrayList BMI = new ArrayList();
            string Sql = "select FindingDetail from  DBO.PatientClinical with(nolock) Where Symptom ='/HEIGHT AND WEIGHT' and clinicaltype='F' AND FindingDetail not  like '%time=%' and patientid=" + ClinicalComponent.PatientId + " and appointmentid=" + ClinicalComponent.AppointmentId;
            DataTable dt = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = Sql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["FindingDetail"].ToString().Trim()))
                        {
                            //BMI.Add(dt.Rows(index)("FindingDetail").ToString.Trim)
                            if (dt.Rows[index]["FindingDetail"].ToString().Trim().ToLower().Contains("weight"))
                            {
                                weight = dt.Rows[index]["FindingDetail"].ToString().Trim();
                            }
                            else if (dt.Rows[index]["FindingDetail"].ToString().Trim().ToLower().Contains("height"))
                            {
                                height = dt.Rows[index]["FindingDetail"].ToString().Trim();
                            }
                        }
                    }
                    if (!(string.IsNullOrEmpty(height) & string.IsNullOrEmpty(weight)))
                    {
                        double wtValue = 0;
                        double htValue = 0;
                        string[] valueAr = null;
                        valueAr = height.Split('<').ToArray();
                        if (!string.IsNullOrEmpty(valueAr[1]))
                        {
                            htValue = Convert.ToDouble(valueAr[1].Replace(">", ""));
                        }
                        valueAr = weight.Split('<').ToArray();
                        if (!string.IsNullOrEmpty(valueAr[1]))
                        {
                            wtValue = Convert.ToDouble(valueAr[1].Replace(">", ""));
                        }

                        if (height.ToLower().Contains("feet"))
                        {
                            htValue = htValue * 12;
                        }
                        else if (height.ToLower().Contains("meters"))
                        {
                            htValue = htValue * 39.3700787;
                        }
                        else if (height.ToLower().Contains("cm"))
                        {
                            htValue = htValue * 0.393700787;
                        }

                        if (weight.ToLower().Contains("grams"))
                        {
                            wtValue = wtValue * 0.00220462262;
                        }
                        else if (weight.ToLower().Contains("ounces"))
                        {
                            wtValue = wtValue * 0.0625;
                        }
                        else if (weight.ToLower().Contains("kgs"))
                        {
                            wtValue = wtValue * 2.20462262;
                        }
                        if (!(string.IsNullOrEmpty(Convert.ToString(wtValue)) & string.IsNullOrEmpty(Convert.ToString(htValue))))
                        {
                            BMI.Add((wtValue * 703) / (htValue * htValue));
                        }
                    }
                }
            }
            return BMI;
        }


    }
}
