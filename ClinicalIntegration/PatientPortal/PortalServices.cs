﻿using System;
using System.Data;
using System.Net;

namespace ClinicalIntegration
{
    public class CredentialsEntity
    {
        public string ExternalId { get; set; }
        public string PracticeToken { get; set; }
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
    }

    public class PatientCredentialsEntity : CredentialsEntity
    {
        public string ERPAccountNumber { get; set; }
        public string PracticeName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string RedirectionLink
        {
            get
            {
                return @"https:\\Portal.eyereachpatients.com\Patients\" + ERPAccountNumber;
            }
        }
    }

    public class PatientCredential
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ExternalId { get; set; }
        public PatientCredentialsEntity LoadPatientCredentials(string _patientId, string _practiceToken, string _userName, string _password)
        {
            PatientCredentialsEntity objCredentials = new PatientCredentialsEntity();
            string _selectQry = "Select AccountName, AccountNumber From MVE.PP_AccountDetails with(nolock) Where IsActive = 1";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                DataTable dt = new DataTable();
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
                if (dt.Rows.Count > 0)
                {
                    objCredentials.ERPAccountNumber = dt.Rows[0]["AccountNumber"].ToString();
                    objCredentials.PracticeName = dt.Rows[0]["AccountName"].ToString();
                    objCredentials.ExternalId = _patientId;
                    objCredentials.PracticeToken = _practiceToken;
                    objCredentials.UserName = _userName;
                    objCredentials.Password = _password;
                }
            }
            return objCredentials;
        }
        public bool SendCredentials(string _patientId, string _practiceToken, string _userType)
        {
            bool retStatus = false;
            DataTable dtable = new DataTable();
            string cmdQuery = string.Empty;
            if(_userType=="Patient")
            {
                 cmdQuery = String.Format(@"Select Top(1) P.Id as ExternalId, pd.email as EmailAddress, p.FirstName, p.LastName From model.Patients P inner join patientdemographics pd with(nolock) on p.id=pd.patientid Left join model.PatientAddresses PA with(nolock) on PA.PatientId = P.Id LEFT Join model.PatientPhoneNumbers PPN with(nolock) on P.Id = PPN.PatientId Where P.Id  = {0}", _patientId);
            }
            else if(_userType=="Representative")
            {
                cmdQuery = String.Format(@"Select Top(1) pr.PatientRepresentativeId as ExternalId, pr.email as EmailAddress, pr.FirstName, pr.LastName From model.Patients P with(nolock) inner join model.PatientRepresentatives pr with(nolock) on p.Id=pr.PatientId Where pr.PatientRepresentativeId = {0}", _patientId);
            }
            
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = cmdQuery;
                using (var reader = cmd.ExecuteReader())
                {
                    dtable.Load(reader);
                }
                if (dtable.Rows.Count > 0)
                {
                    FirstName = dtable.Rows[0]["FirstName"].ToString();
                    LastName = dtable.Rows[0]["LastName"].ToString();
                    EmailAddress = dtable.Rows[0]["EmailAddress"].ToString();
                    ExternalId= dtable.Rows[0]["ExternalId"].ToString();
                }
                try
                {
                    CredentialsEntity objCredentials = new CredentialsEntity()
                    {
                        ExternalId = this.ExternalId,
                        PracticeToken = _practiceToken,
                        UserType = _userType,
                        FirstName = this.FirstName,
                        LastName = this.LastName,
                        EmailAddress = this.EmailAddress
                    };
                    var webclient = new WebClient();
                    webclient.Headers["Content-type"] = "application/json";
                    webclient.Encoding = System.Text.Encoding.UTF8;
                    string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objCredentials);
                    string result = webclient.UploadString(ClinicalComponent._CredentialLink, "POST", jsonToBeSent);
                    ResponseEntity objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseEntity>(result);
                    if (objResponse.ResponseType.ToString().ToLower() == "success")
                    {
                        retStatus = true;
                    }
                }
                catch(Exception ex)
                {
                     
                }
                return retStatus;
            }
        }
    }

    public class DoctorEntity
    {
        public string PracticeToken { get; set; }
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Active { get; set; }
        public string InHouse { get; set; }
        public string LocationExternalId { get; set; }
        public string SecureRecipientExternalId { get; set; }
        public DoctorEntity(string _doctorId)
        {
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select Value from model.ApplicationSettings with(nolock) Where Name = 'PracticeAuthToken'";
                this.PracticeToken = cmd.ExecuteScalar().ToString();
            }

            string _SelectQry = "Select ResourceFirstName as Col1, ResourceLastName as Col2, (Select Top(1) Id From model.ServiceLocations a with(nolock) inner join MVE.PP_PortalQueueResponse b with(nolock) on a.Id = b.PatientNo and b.ActionTypeLookupId = 6) as LocationId from dbo.Resources with (nolock) Where ResourceId = @userId";
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter IdbPatientId = cmd.CreateParameter();
                IdbPatientId.Direction = ParameterDirection.Input;
                IdbPatientId.Value = _doctorId;
                IdbPatientId.ParameterName = "@userId";
                IdbPatientId.DbType = DbType.Int64;
                cmd.Parameters.Add(IdbPatientId);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _SelectQry;
                DataTable dtTemp = new DataTable();
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                }
                if (dtTemp.Rows.Count > 0)
                {
                    this.ExternalId = _doctorId;
                    this.FirstName = dtTemp.Rows[0]["Col1"].ToString();
                    this.LastName = dtTemp.Rows[0]["Col2"].ToString();
                    this.Active = "false";
                    this.InHouse = "true";
                    this.LocationExternalId = dtTemp.Rows[0]["LocationId"].ToString();
                    this.SecureRecipientExternalId = _doctorId;
                }
            }

            ClinicalComponent._Connection.Close();
        }

        private bool IsExists(string _portalexternalId)
        {
            bool retStatus = false;
            try
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                string _selectQry = "Select 1 From MVE.PP_PortalQueueResponse with(nolock) Where ActionTypeLookupId = '12' and PatientNo = '" + this.ExternalId + "'";
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    if (cmd.ExecuteScalar().ToString() == "1")
                        retStatus = true;
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {
                if (ClinicalComponent._Connection.State.ToString() == "Open")
                    ClinicalComponent._Connection.Close();
            }
            return retStatus;
        }

        public bool Save2Logs(string _portalexternalId)
        {
            bool retStatus = false;
            if (!IsExists(_portalexternalId))
            {
                string _insertQry = "Insert into MVE.PP_PortalQueueResponse(MessageData, ActionTypeLookupId, ProcessedDate, PatientNo, IsActive, ExternalId) Values ('API', '12', GETDATE(), '" + this.ExternalId + "', '1', '" + _portalexternalId + "')";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                    retStatus = true;
                }
                ClinicalComponent._Connection.Close();
            }
            else
            {
                retStatus = true;
            }
            return retStatus;
        }

    }
}
