﻿using System;
using System.Data;
using System.Net;

namespace ClinicalIntegration
{
    public class PatientEducationMaterialModel
    {
        public string PracticeToken { get; set; }
        public string ExternalId { get; set; }
        public string PatientExternalId { get; set; }
        public string DoctorExternalId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentName { get; set; }
        public string DocumentURL { get; set; }
        public string IsActive { get; set; }
    }
    public class EducationMaterial
    {
        public string PracticeToken { get; set; }
        public string ExternalId { get; set; }
        public string PatientExternalId { get; set; }
        public string PatientAppointmentId { get; set; }
        public string DoctorExternalId { get; set; }
        public string DocumentType { get; set; }
        public string CodeType { get; set; }
        public string DocumentName { get; set; }
        public string DocumentURL { get; set; }
        public string IsActive { get; set; }

        public EducationMaterial()
        {
            string _selectQry = "Select Top(1) Value from model.ApplicationSettings Where Name = 'PracticeAuthToken'";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                this.PracticeToken = cmd.ExecuteScalar().ToString();
            }
            ClinicalComponent._Connection.Close();
        }

        public bool IsExist()
        {
            bool retStatus = false;
            try
            {
                string _selectQry = "Select 1 From MVE.PP_PatientEducationMaterial with (nolock) Where ExternalId = '" + this.ExternalId + "' and PatientId =  '" + this.PatientExternalId + "' and AppointmentId = '" + this.PatientAppointmentId + "' and ResourceId = '" + this.DoctorExternalId + "'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    retStatus = cmd.ExecuteScalar().ToString() == "1" ? true : false;
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {

            }
            return retStatus;
        }

        public bool Post()
        {
            bool retStatus = false;
            try
            {
                PatientEducationMaterialModel objPatientModel = new PatientEducationMaterialModel()
                {
                    DoctorExternalId = this.DoctorExternalId,
                    DocumentName = this.DocumentName,
                    DocumentType = this.DocumentType,
                    DocumentURL = this.DocumentURL,
                    ExternalId = this.ExternalId,
                    IsActive = this.IsActive,
                    PatientExternalId = this.PatientExternalId,
                    PracticeToken = this.PracticeToken
                };
                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objPatientModel);
                string result = webclient.UploadString(ClinicalComponent._EducationMaterialLink, "POST", jsonToBeSent);
                ResponseContentEntity objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseContentEntity>(result);
                if (objResponse.ResponseType.ToLower() == "success")
                {
                    this.Save();
                    retStatus = true;
                }
                
            }
            catch
            {

            }
            return retStatus;
        }

        public void Save()
        {
            string _insertQry = "Insert into MVE.PP_PatientEducationMaterial(ExternalId, PatientId, AppointmentId, ResourceId, DocumentName, DocumentType, Url, CodeType, CreatedDateUTC, Reviews) Values ('"+this.ExternalId+ "', '" + this.PatientExternalId + "', '" + this.PatientAppointmentId + "', '" + this.DoctorExternalId + "', '" + this.DocumentName + "', '" + this.DocumentType + "', '" + this.DocumentURL + "', '" + this.CodeType + "', '" + DateTime.Now + "', 0)";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _insertQry;
                cmd.ExecuteNonQuery();
            }
            ClinicalComponent._Connection.Close();
        }
    }
}
