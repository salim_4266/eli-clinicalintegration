﻿using DBHelper;
using System;
using System.Data;

namespace ClinicalIntegration
{
    public class ERPServices
    {
        public string type { get; set; }
        public string externalId { get; set; }
        public Object DBObject { get; set; }
        public bool Push()
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            bool retStatus = false;
            if (type == "user")
            {

            }
            if (type == "doctor")
            {
                retStatus = RegisterDoctor2ERP(this.externalId);
            }
            return retStatus;
        }

        private bool RegisterDoctor2ERP(string _externalId)
        {
            bool retStatus = false;
            DoctorEntity objDoctor = new DoctorEntity(_externalId);
            if (objDoctor.ExternalId != "")
            {
                retStatus = new PortalClient().SendDoctorDetail2ERP(objDoctor);
            }
            return retStatus;
        }

    }
}
