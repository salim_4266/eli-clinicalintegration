﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace ClinicalIntegration
{
    public class PatientGeneratedHealthData
    {
        public DataTable GetExternalLinks(string _Status)
        {
            DataTable dtPatientEtxernalLinks = new DataTable();
            string _SelectQry = "SELECT Id as Col1, ISNULL(ExternalLink,FileName) as Col2, Comment as Col3, CreatedDateTime as Col4, Case(ISNULL(IsArchive, 0)) When 0 then 'Incorporated' Else 'Deleted' End as Col5, TypeId as Col6, ISNULL(DocumentExternaId,0) as Col7 FROM MVE.PP_PatientExternalLink with(nolock) Where PatientId = @PatientId and IsArchive in (" + _Status + ") ";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter IdbPatientId = cmd.CreateParameter();
                IdbPatientId.Direction = ParameterDirection.Input;
                IdbPatientId.Value = ClinicalComponent.PatientId;
                IdbPatientId.ParameterName = "@PatientId";
                IdbPatientId.DbType = DbType.Int64;
                cmd.Parameters.Add(IdbPatientId);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _SelectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtPatientEtxernalLinks.Load(reader);
                }
            }
            ClinicalComponent._Connection.Close();
            return dtPatientEtxernalLinks;
        }

        public bool Archive(string LinkId)
        {
            bool retStatus = false;
            string _updateQry = "Update MVE.PP_PatientExternalLink Set IsArchive = 1 Where Id = '" + LinkId + "'";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _updateQry;
                cmd.ExecuteNonQuery();
                retStatus = true;
            }
            ClinicalComponent._Connection.Close();
            return retStatus;
        }

        public bool Archive(string _linkId, string _fileId, string _patientId, string _practiceToken)
        {
            bool retStatus = false;
            if (new PortalClient().DeleteFile(_fileId, _patientId, _practiceToken))
            {
                string _updateQry = "Update MVE.PP_PatientExternalLink Set IsArchive = 1 Where Id = '" + _linkId + "'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _updateQry;
                    cmd.ExecuteNonQuery();
                    retStatus = true;
                }
                ClinicalComponent._Connection.Close();
            }
            return retStatus;
        }


        public bool UrlRead(string LinkId)
        {
            bool retStatus = false;
            string _updateQry = "Update MVE.PP_PatientExternalLink Set IsUrlRead = 1, UPDATEDDATETIME = GETDATE() Where Id = '" + LinkId + "'";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _updateQry;
                cmd.ExecuteNonQuery();
                retStatus = true;
            }
            ClinicalComponent._Connection.Close();
            return retStatus;
        }

        private bool IsExist(PatientExternalHealthInformation objPEHI)
        {
            bool retstatus = false;
            try
            {
                string _selectQry = "Select 1 From MVE.PP_PatientExternalLink Where PatientId = '" + objPEHI.PatientId + "' and ExternalLink = '" + objPEHI.ExternalLink + "'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    retstatus = cmd.ExecuteScalar().ToString() == "1" ? true : false;
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {
                retstatus = false;
            }
            return retstatus;
        }

        private bool IsExist(PatientSharedFileEntity objPEHI)
        {
            bool retstatus = false;
            try
            {
                string _selectQry = "Select 1 From MVE.PP_PatientExternalLink Where PatientId = '" + objPEHI.PatientExternalId + "' and FileName = '" + objPEHI.FileName + "'and DocumentExternaId = '" + objPEHI.Id + "'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    retstatus = cmd.ExecuteScalar().ToString() == "1" ? true : false;
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {
                retstatus = false;
            }
            return retstatus;
        }

        public bool Save(PatientExternalHealthInformation objPEHI, string _appointmentId)
        {
            bool retstatus = true;
            try
            {
                if (!IsExist(objPEHI))
                {
                    string _insertQry = "INSERT INTO MVE.PP_PatientExternalLink(PatientId, TypeId, ExternalLink, Comment, UserId,  CreatedDateTime, UpdatedDateTime, IsArchive, IsUrlRead, AppointmentId) values (@PatientId, 'Link', @ExternalLink, @Comment, @UserId, GetDate(), Null, 0, 0, '" + _appointmentId + "');";
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        IDbDataParameter PatientId = cmd.CreateParameter();
                        PatientId.Direction = ParameterDirection.Input;
                        PatientId.Value = objPEHI.PatientId;
                        PatientId.ParameterName = "@PatientId";
                        PatientId.DbType = DbType.Int64;
                        cmd.Parameters.Add(PatientId);

                        IDbDataParameter ExternalLink = cmd.CreateParameter();
                        ExternalLink.Direction = ParameterDirection.Input;
                        ExternalLink.Value = objPEHI.ExternalLink;

                        ExternalLink.ParameterName = "@ExternalLink";
                        ExternalLink.DbType = DbType.String;
                        cmd.Parameters.Add(ExternalLink);

                        IDbDataParameter Comment = cmd.CreateParameter();
                        Comment.Direction = ParameterDirection.Input;
                        Comment.Value = objPEHI.Comment;
                        Comment.ParameterName = "@Comment";
                        Comment.DbType = DbType.String;
                        cmd.Parameters.Add(Comment);

                        IDbDataParameter UserId = cmd.CreateParameter();
                        UserId.Direction = ParameterDirection.Input;
                        UserId.Value = Convert.ToInt32(objPEHI.UserId);
                        UserId.ParameterName = "@UserId";
                        UserId.DbType = DbType.Int32;
                        cmd.Parameters.Add(UserId);

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = _insertQry;
                        cmd.ExecuteNonQuery();
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {

            }
            return retstatus;
        }

        public bool Save(List<PatientSharedFileEntity> _objTempContent, string _userId, string _practiceToken, string _appointmentId)
        {
            bool retStatus = false;
            try
            {
                if (_objTempContent.Count() > 0)
                {
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    foreach (PatientSharedFileEntity a in _objTempContent)
                    {
                        if (!IsExist(a))
                        {
                            int retQueryStatus = 0;
                            string _insertQry = "INSERT INTO MVE.PP_PatientExternalLink(PatientId, TypeId,  Comment, DisplayName, FileName, DocumentExternaId, Status, UserId,  CreatedDateTime, UpdatedDateTime, IsArchive, IsDownloaded, AppointmentId) values ('" + a.PatientExternalId + "', 'File', '" + a.PatientComments + "', '" + a.DisplayName + "', '" + a.FileName + "', '" + a.Id + "', '" + a.Status + "', '" + _userId + "', '" + a.PortalCreated + "', NULL, '0', '0', '" + _appointmentId + "')";
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = _insertQry;
                                retQueryStatus = cmd.ExecuteNonQuery();
                            }

                            if (retQueryStatus >= 1)
                            {
                                new PortalClient().MarkAsRecieved(a.Id, a.PatientExternalId, _practiceToken);
                                retQueryStatus = 0;
                            }
                        }
                    }
                    ClinicalComponent._Connection.Close();
                }
            }
            catch
            {

            }
            return retStatus;
        }

        public bool DownloadFile(string _recordId, string _fileId, string _patientId, string _practiceToken, string _fileName, string _pinpointDirectory)
        {
            bool retStatus = File.Exists(_pinpointDirectory + @"\PGHD\" + _patientId + @"\" + _fileName);
            if (!retStatus)
            {
                if (new PortalClient().GetFileContent(_fileId, _patientId, _practiceToken, _fileName, _pinpointDirectory))
                {
                    string _updateQry = "Update MVE.PP_PatientExternalLink Set IsDownloaded = '1' Where Id = '" + _recordId + "'";
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = _updateQry;
                        cmd.ExecuteNonQuery();
                        retStatus = true;
                    }
                    ClinicalComponent._Connection.Close();
                }
            }
            return retStatus;
        }

        public bool Update(PatientExternalHealthInformation objPEHI)
        {
            bool retstatus = false;
            string _error = "";
            try
            {
                string _updateQry = "Update MVE.PP_PatientExternalLink Set ExternalLink = '" + objPEHI.ExternalLink + "', UpdatedDateTime = GetDate(), UserId = " + (objPEHI.UserId != null ? objPEHI.UserId : "NULL") + ", Comment='" + objPEHI.Comment + "', UserLabel ='" + objPEHI.UserLabel + "', Case(ISNULL(IsUrl,0) When 0 Then '1' Else '0' End Where PatientId ='" + objPEHI.PatientId + "'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _updateQry;
                    cmd.ExecuteNonQuery();
                    retstatus = true;
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            return retstatus;
        }

    }
}
