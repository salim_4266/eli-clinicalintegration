﻿using System.Collections.Generic;

namespace ClinicalIntegration
{

    public class ResponseEntity
    {
        public string ResponseType { get; set; }
        public string ExternalId { get; set; }
    }

    public class ResponseContentEntity
    {
        public PatientExternalLink PatientItems { get; set; }
        public string ResponseType { get; set; }
    }

    public class ResponseContentEntityType2
    {
        public List<PatientSharedFileEntity> PatientItems { get; set; }
        public string ResponseType { get; set; }
    }

    public class ResponseContentEntityType3
    {
        public FileDetail PatientItems { get; set; }
        public string ResponseType { get; set; }
    }

    public class PatientLinkEntity
    {
        public string PracticeToken { get; set; }
        public string PatientExternalId { get; set; }
    }

    public class PatientHealthRecordEntity
    {
        public string PracticeToken { get; set; }
        public string PatientExternalId { get; set; }
        public string ExternalId { get; set; }
    }

    public class PatientExternalLink
    {
        public string PatientId { get; set; }
        public string PatientExternalId { get; set; }
        public string PatientLink { get; set; }
        public string Comment { get; set; }
        public bool AlreadySent { get; set; }

    }

    public class PatientExternalHealthInformation
    {
        public string Id { get; set; }
        public string ExternalLink { get; set; }
        public string UserId { get; set; }
        public string UserLabel { get; set; }
        public string CreatedDateTime { get; set; }
        public string UpdatedDateTime { get; set; }
        public string Comment { get; set; }
        public string PatientId { get; set; }
    }

    public class PatientSharedFileEntity
    {
        public string Id { get; set; }
        public string PatientExternalId { get; set; }
        public string PortalCreated { get; set; }
        public string DisplayName { get; set; }
        public string FileName { get; set; }
        public string PatientComments { get; set; }
        public string Status { get; set; }
    }

    public class FileDetail
    {
        public byte[] Content { get; set; }
        public string FileName { get; set; }
    }

}
