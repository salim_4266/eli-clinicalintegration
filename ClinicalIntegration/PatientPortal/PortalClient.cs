﻿using System;
using System.IO;
using System.Net;

namespace ClinicalIntegration
{
    public class PortalClient
    {
        public void GetPortalExternalLink(string _patientId, string _practiceToken, string _appointmentId)
        {
            PatientExternalHealthInformation objPEHI = new PatientExternalHealthInformation();
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                PatientLinkEntity objLinkEnity = new PatientLinkEntity()
                {
                    PatientExternalId = _patientId,
                    PracticeToken = _practiceToken
                };
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objLinkEnity);
                string result = webclient.UploadString(ClinicalComponent._PortalLink, "POST", jsonToBeSent);
                ResponseContentEntity objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseContentEntity>(result);
                objPEHI.PatientId = objLinkEnity.PatientExternalId;
                objPEHI.ExternalLink = objResponse.PatientItems.PatientLink;
                objPEHI.UserId = ClinicalComponent.UserId;
                objPEHI.Comment = objResponse.PatientItems.Comment;
                objPEHI.CreatedDateTime = DateTime.Now.ToString();
                PatientGeneratedHealthData objPGHD = new PatientGeneratedHealthData();
                if (objPEHI.ExternalLink != "")
                {
                    objPGHD.Save(objPEHI, _appointmentId);
                }
            }
            catch
            {

            }
        }

        public void GetPortalExternalFile(string _patientId, string _practiceToken, string _appointmentId)
        {
            PatientSharedFileEntity objPEHI = new PatientSharedFileEntity();
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                PatientLinkEntity objLinkEnity = new PatientLinkEntity()
                {
                    PatientExternalId = _patientId,
                    PracticeToken = _practiceToken
                };
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objLinkEnity);
                string result = webclient.UploadString(ClinicalComponent._PGHDFileLink, "POST", jsonToBeSent);
                ResponseContentEntityType2 objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseContentEntityType2>(result);
                new PatientGeneratedHealthData().Save(objResponse.PatientItems, ClinicalComponent.UserId, _practiceToken, _appointmentId);
            }
            catch
            {

            }
        }

        public bool GetFileContent(string _fileId, string _patientId, string _practiceToken, string _fileName, string _pinpointDirectory)
        {
            bool retStatus = false;
            PatientHealthRecordEntity objPHRE = new PatientHealthRecordEntity()
            {
                ExternalId = _fileId,
                PatientExternalId = _patientId,
                PracticeToken = _practiceToken
            };
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objPHRE);
                string result = webclient.UploadString(ClinicalComponent._PGHDDocumentLink, "POST", jsonToBeSent);
                ResponseContentEntityType3 objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseContentEntityType3>(result);
                if (objResponse.PatientItems.Content != null)
                {
                    string _path2Directory = _pinpointDirectory + @"PGHD\" + _patientId + @"\";
                    if (!Directory.Exists(_path2Directory))
                    {
                        Directory.CreateDirectory(_path2Directory);
                    }
                    _path2Directory = _path2Directory + _fileName;
                    File.WriteAllBytes(_path2Directory, objResponse.PatientItems.Content);
                }
                retStatus = true;
            }
            catch
            {

            }
            return retStatus;
        }
        public bool MarkAsRecieved(string _fileId, string _patientId, string _practiceToken)
        {
            bool retStatus = false;
            PatientHealthRecordEntity objPHRE = new PatientHealthRecordEntity()
            {
                ExternalId = _fileId,
                PatientExternalId = _patientId,
                PracticeToken = _practiceToken
            };
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objPHRE);
                string result = webclient.UploadString(ClinicalComponent._PGHDRecievedFile, "POST", jsonToBeSent);
                ResponseEntity objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseEntity>(result);
                if (objResponse.ResponseType.ToLower() == "success")
                {
                    retStatus = true;
                }
            }
            catch
            {

            }
            return retStatus;
        }

        public bool DeleteFile(string _fileId, string _patientId, string _practiceToken)
        {
            bool retStatus = false;
            PatientHealthRecordEntity objPHRE = new PatientHealthRecordEntity()
            {
                ExternalId = _fileId,
                PatientExternalId = _patientId,
                PracticeToken = _practiceToken
            };
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objPHRE);
                string result = webclient.UploadString(ClinicalComponent._PGHDDeletedFile, "POST", jsonToBeSent);
                ResponseEntity objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseEntity>(result);
                if (objResponse.ResponseType.ToLower() == "success")
                {
                    retStatus = true;
                }
            }
            catch
            {

            }
            return retStatus;
        }

        public bool SendDoctorDetail2ERP(DoctorEntity objResourceEntity)
        {
            bool retStatus = false;
            var webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = System.Text.Encoding.UTF8;
            try
            {
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objResourceEntity);
                string result = webclient.UploadString(ClinicalComponent._PostDoctorInformation, "POST", jsonToBeSent);
                ResponseEntity objResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseEntity>(result);
                if (objResponse.ResponseType.ToLower() == "success")
                    retStatus = objResourceEntity.Save2Logs(objResponse.ExternalId);
            }
            catch
            {
                retStatus = false;
            }
            return retStatus;
        }
    }
}


