﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ClinicalIntegration
{
    public class Permissions
    {
        public Int64 ResourceID { get; set; }
        public string UserName { get; set; }

        public DataTable GetUserinfo(string flag)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();

            String query = "dbo.USP_EmergencyPermission";

            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {

                IDbDataParameter Flag = cmd.CreateParameter();
                Flag.Direction = ParameterDirection.Input;
                Flag.Value = flag;
                Flag.ParameterName = "@Flag";
                Flag.DbType = DbType.String;
                cmd.Parameters.Add(Flag);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            return dt;
        }

        public void AddPermission(int ResourceId,string permission)
        {
            string _error = "";

            string query = string.Empty;

            try
            {
 
                query = "dbo.USP_EmergencyPermission";

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.Parameters.Clear();

                    IDbDataParameter parResourceID= cmd.CreateParameter();
                    parResourceID.Direction = ParameterDirection.Input;
                    parResourceID.Value = ResourceId;
                    parResourceID.ParameterName = "@ResourceId";
                    parResourceID.DbType = DbType.Int32;
                    cmd.Parameters.Add(parResourceID);

                    IDbDataParameter parRP = cmd.CreateParameter();
                    parRP.Direction = ParameterDirection.Input;
                    parRP.Value = permission;
                    parRP.ParameterName = "@ResourcePermission";
                    parRP.DbType = DbType.String;
                    cmd.Parameters.Add(parRP);
                    

                    IDbDataParameter parEnterdDate = cmd.CreateParameter();
                    parEnterdDate.Direction = ParameterDirection.Input;
                    parEnterdDate.Value = DateTime.Now;
                    parEnterdDate.ParameterName = "@EnterdDate";
                    parEnterdDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parEnterdDate);

                    IDbDataParameter Flag = cmd.CreateParameter();
                    Flag.Direction = ParameterDirection.Input;
                    Flag.Value = "save";
                    Flag.ParameterName = "@Flag";
                    Flag.DbType = DbType.String;
                    cmd.Parameters.Add(Flag);


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }


            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
        }

        public void RemovePermission(int ResourceId)
        {
            string _error = "";

            string query = string.Empty;

            try
            {
                  query = "dbo.USP_EmergencyPermission";

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.Parameters.Clear();

                    IDbDataParameter parResourceID = cmd.CreateParameter();
                    parResourceID.Direction = ParameterDirection.Input;
                    parResourceID.Value = ResourceId;
                    parResourceID.ParameterName = "@ResourceId";
                    parResourceID.DbType = DbType.Int32;
                    cmd.Parameters.Add(parResourceID);

                  
                    IDbDataParameter Flag = cmd.CreateParameter();
                    Flag.Direction = ParameterDirection.Input;
                    Flag.Value = "remove";
                    Flag.ParameterName = "@Flag";
                    Flag.DbType = DbType.String;
                    cmd.Parameters.Add(Flag);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
        }


    }

    
}
