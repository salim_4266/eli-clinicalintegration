﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ClinicalIntegration
{
    class Checksum
    {
        public DataTable GetTamperData(DateTime startdate,DateTime enddate)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;

            query = "model.GetTempetdate";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter parStartDate = cmd.CreateParameter();
                parStartDate.Direction = ParameterDirection.Input;
                parStartDate.Value = startdate;
                parStartDate.ParameterName = "@startDate";
                parStartDate.DbType = DbType.DateTime;
                cmd.Parameters.Add(parStartDate);

                IDbDataParameter parEndDate = cmd.CreateParameter();
                parEndDate.Direction = ParameterDirection.Input;
                parEndDate.Value = enddate;
                parEndDate.ParameterName = "@endDate";
                parEndDate.DbType = DbType.DateTime;
                cmd.Parameters.Add(parEndDate);


                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            return dt;
        }
    }
}
