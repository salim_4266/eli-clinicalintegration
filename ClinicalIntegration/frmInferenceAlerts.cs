﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmInferenceAlerts : Form
    {
        public int RuleId;
        public string RuleCondition
        {
            get { return txtComments.Text; }
            set { txtComments.Text = value; }
        }
        public frmInferenceAlerts()
        {
            InitializeComponent();
        }

        private void txtComments_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            frmInfoScreen objfrmInfoScreen = new frmInfoScreen();
            objfrmInfoScreen.RuleId = Convert.ToString(RuleId);
            objfrmInfoScreen.ShowDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frmCDSRefrences objfrmCDS = new frmCDSRefrences();
            objfrmCDS.RuleId = Convert.ToString(RuleId);
            objfrmCDS.ShowDialog();
        }

        private void frmInferenceAlerts_Load(object sender, EventArgs e)
        {
        }
    }
}
