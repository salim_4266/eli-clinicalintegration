﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmFamilydetails : Form
    {
        public Form ParentMDI { get; set; }
        public int PatientId { get; set; }
        public bool IsEdit { get; set; }
        bool IsEnabled { get; set; }
        FamilyDetails fd = new FamilyDetails();
        public DateTime AppointmentDt { get; set; }
        public DateTime OnSetdate { get; set; }

        public frmFamilydetails()
        {
            InitializeComponent();
            BtnEdit.Enabled = false;
        }

        private void btnNewHistory_Click(object sender, EventArgs e)
        {
            EditFamilyDetail frmEdit = new EditFamilyDetail();
            frmEdit.MdiParent = this.ParentMDI;
            frmEdit.ParentMDI = this.ParentMDI;
            frmEdit.IsNew = true;
            this.Close();
            frmEdit.Show();
        }

        private void grdVwFamily_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                IsEnabled = true;
                BtnEdit.Enabled = true;
                FamilyDetails fd = new FamilyDetails();
                DataGridViewRow row = grdVwFamily.Rows[e.RowIndex];
                BtnEdit.Tag = int.Parse(row.Cells["id"].Value.ToString());
            }
        }


        private void frmFamilydetails_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            LoadData();
            grdVwFamily.EnableHeadersVisualStyles = false;
            grdVwFamily.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            grdVwFamily.ColumnHeadersHeight = 25;
            grdVwFamily.ClearSelection();
        }
        public void LoadData()
        {
            DataAccess da = new DataAccess();
            comboBox1.SelectedItem = "Active";
            FillFamilyDetails(da.GetFamilyDetails("'1'"));
        }
        private void FillFamilyDetails(DataTable dt)
        {
            grdVwFamily.DataSource = dt.DefaultView;
            grdVwFamily.Columns[0].Visible = false;
            grdVwFamily.Columns["comments"].Visible = false;
            grdVwFamily.Columns["History"].Width = 300;
            grdVwFamily.Columns["Modified By"].Width = 124;
            grdVwFamily.ReadOnly = true;
            grdVwFamily.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            grdVwFamily.EnableHeadersVisualStyles = false;
            grdVwFamily.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            grdVwFamily.RowTemplate.Height = 32;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            string _status = "";
            if (comboBox1.SelectedItem.ToString() == "Active")
                _status = "'1'";
            else if (comboBox1.SelectedItem.ToString() == "Inactive")
                _status = "'0'";
            else
                _status = "'1','0'";

            DataAccess DA = new DataAccess();
            grdVwFamily.DataSource = DA.GetFamilyDetails(_status);
        }

        private void grdVwFamily_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (IsEnabled)
            {
                //IsEnabled = true;
                //BtnEdit.Enabled = true;
                
                FamilyDetails fd = new FamilyDetails();
                EditFamilyDetail frm = new EditFamilyDetail();
                frm.IsfamilyReq = true;
                frm.IsNew = false;
                frm.IsEdit = true;
                frm.family = fd;
                DataAccess da = new DataAccess();
                DataTable fdata = new DataTable();
                
                fdata = da.GetFamilyDetailsById(Convert.ToInt64(BtnEdit.Tag));
                
                if (fdata.Rows.Count > 0)
                {
                    fd.Id = int.Parse(fdata.Rows[0]["id"].ToString());
                    fd.AppointmentID = int.Parse(fdata.Rows[0]["appointmentid"].ToString());
                    fd.PatientId = int.Parse(fdata.Rows[0]["Patientid"].ToString());
                    fd.COMMENTS = fdata.Rows[0]["comments"].ToString();
                    fd.ICD10 = fdata.Rows[0]["ICD10"].ToString();
                    fd.ICD10DESCR = fdata.Rows[0]["ICD10DESCR"].ToString();
                    fd.ConceptID = fdata.Rows[0]["ConceptID"].ToString();
                    fd.Term = fdata.Rows[0]["term"].ToString();
                    fd.Status = (fdata.Rows[0]["status"].ToString()) == "True" ? "Active" : "Inactive";
                    fd.Relationship = int.Parse(fdata.Rows[0]["relationship"].ToString());
                    fd.EnteredDate = DateTime.Parse(fdata.Rows[0]["EnteredDate"].ToString());
                    fd.LastModifiedDate = DateTime.Parse(fdata.Rows[0]["LastModofiedDate"].ToString());
                    fd.LastModifiedBy = fdata.Rows[0]["LastModofiedBy"].ToString();
                    fd.DiagnosisDate = DateTime.Parse(fdata.Rows[0]["EnteredDate"].ToString());
                    fd.OnsetDate = DateTime.Parse(fdata.Rows[0]["OnsetDate"].ToString());
                }
                
                frm.fd = fd;
                frm.MdiParent = this.ParentMDI;
                frm.ParentMDI = this.ParentMDI;
                frm.Show();
                this.Close();
            }
            else
            {
                BtnEdit.Enabled = false;
            }
        }
    }
}