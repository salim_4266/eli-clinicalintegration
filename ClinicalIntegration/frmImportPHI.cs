﻿using DBHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Windows.Forms;
using System.ComponentModel;
using ClinicalIntegration.ServiceReference1;
using System.IO;
using System.Net.Mail;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmImportPHI : Form
    {
        public string ResourceId { get; set; }

        public string ResourceName { get; set; }
        public string ProviderKey { get; set; }
        private static string id { get; set; }
        public string FolderPath { get; set; }
        public string InboxUrl { get; set; }
        public Object connection;
        string ProviderPath;
        int totalMessages;
        bool isAttachments;
        public string ToEmailAddress { get; set; }
        List<string> ProcessedClinicalMessageId = new List<string>();
        BackgroundWorker bgcUpdoxInbox;
        List<string> fromAddressess = new List<string>();


        public string EmailSubject = String.Empty;
        public string EmailMessage = String.Empty;
        public string EmailAddress = String.Empty;

        private string Folderpath_txt
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "Txt\\";
            }
        }
        private string folderpath_zip
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "ZIP\\";
            }
        }

        private string folderpath_JPEG
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "JPG\\";
            }
        }

        private string folderpath_PNG
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "PNG\\";
            }
        }

        private string folderpath_pdf
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "PDF\\";
            }
        }

        private string folderpath_xml
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "XML\\";
            }
        }

        private string folderpath_html
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "HTML\\";
            }
        }

        public Object DBObject
        {
            get
            {
                return connection;
            }
            set
            {
                connection = value;
            }
        }
        public frmImportPHI()
        {
            InitializeComponent();
            bgcUpdoxInbox= new BackgroundWorker();
            

        }

        private void BgcUpdoxInbox_DoWork(object sender, DoWorkEventArgs e)
        {
            
            InvokeGetGetAllClinicalMessagesSummaryV2();
        }

        public string AttachmentPath { get; set; }
        private void btnImport_Click(object sender, EventArgs e)
        {
            //if (txtUserName.Text.Trim() == string.Empty)
            //    ToEmailAddress = "iopw.test@newcrop.cert.direct-ci.com";
            //else
            btnImport.Enabled = false;
            ToEmailAddress = txtUserName.Text.Trim();

            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;

            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select Value from model.ApplicationSettings with(nolock) Where Name = 'NewCropInBoxUrl'";
                this.InboxUrl = cmd.ExecuteScalar().ToString();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select ResourceFirstName +' '+ ResourceLastName as ResourceName from DBO.Resources with(nolock) Where ResourceId = " + ResourceId; //+ ResourceId;
                this.ResourceName = cmd.ExecuteScalar().ToString();
            }


            ToEmailAddress = txtUserName.Text.Trim();
            bgcUpdoxInbox.DoWork += new DoWorkEventHandler(BgcUpdoxInbox_DoWork);
            bgcUpdoxInbox.ProgressChanged += BgcUpdoxInbox_ProgressChanged;
            bgcUpdoxInbox.RunWorkerCompleted += BgcUpdoxInbox_RunWorkerCompleted;
                    
            bgcUpdoxInbox.WorkerReportsProgress = true;

            bgcUpdoxInbox.RunWorkerAsync();
            //Task getAllMessage = new Task(InvokeGetGetAllClinicalMessagesSummaryV2);
            //getAllMessage.Start();
        }

        private void BgcUpdoxInbox_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txtUpdatedEmail.Enabled = true;
            btnEmail.Enabled = true;
        }

        private void BgcUpdoxInbox_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string status = string.Empty;
            if (e.ProgressPercentage == totalMessages)
                status = "Processed all " + totalMessages.ToString() + " message(s).";
            else
            {
                if(isAttachments)
                status = "Processing message " + e.ProgressPercentage.ToString() + " with Attachment out of " + totalMessages.ToString() + " message.";
                else
                    status  = "Processing message " + e.ProgressPercentage.ToString() + " out of " + totalMessages.ToString() + " message.";
            }
            lblStatus.Text = status;
        }

        public void InvokeGetGetAllClinicalMessagesSummaryV2()
        {
            try
            {
                

                if (!string.IsNullOrEmpty(InboxUrl))
                {
                    char[] array2 = new char[] { '\\' };
                    FolderPath = @"C:\Pinpoint\";
                    FolderPath = FolderPath.TrimEnd(array2);
                    CreateProviderDirectories();

                    

                    //Credential Info
                    //gary.general@newcrop.cert.direct-ci.com;iopw.test@newcrop.cert.direct-ci.com
                    string name = "demo", password = "demo", partnername = "IOPW";


                    //Account Info
                    string accountId = "demo", siteId = "demo";

                    Credentials cred = new Credentials();
                    WSHttpBinding binding = new WSHttpBinding();
                    binding.Security.Mode = SecurityMode.Transport;
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                    binding.MessageEncoding = WSMessageEncoding.Mtom;
                    binding.MaxReceivedMessageSize = Int32.MaxValue;
                    EndpointAddress address =
                    new EndpointAddress(InboxUrl);
                    cred.Name = name;
                    cred.Password = password;
                    cred.PartnerName = partnername;
                    AccountRequest accReq = new AccountRequest();
                    accReq.AccountId = accountId;
                    accReq.SiteId = siteId;
                    ClinicalMessagingClient obj = new ClinicalMessagingClient(binding, address);//DEMOLOC1
                    ClinicalMessageSummaryResult res = obj.GetAllClinicalMessagesSummaryV2(cred, accReq, 0, "", "DEMOLOC1", "", ToEmailAddress, "NULL", "NULL", "NULL");
                    ClinicalMessageSummary[] msg = res.clinicalMessageSummaryArray;
                    int msgCnt = 0;
                    if (msg != null && msg.Length > 0)
                    {
                        totalMessages = msg.Length;
                        foreach (var item in msg)
                        {
                            msgCnt++;
                            GetClinicalMessageV2(item.ClinicalMessageGuid);
                            bgcUpdoxInbox.ReportProgress(msgCnt);
                        }
                    } 
                }
            }
            catch
            {
                
            }
        }

        public void ReadResponse(string clinicalMessageGuid)
        {
            try
            {
                //response = @"<s:Envelope
                //        xmlns:s=""http://www.w3.org/2003/05/soap-envelope""
                //        xmlns:a=""http://www.w3.org/2005/08/addressing"">
                //        <s:Header>
                //            <a:Action s:mustUnderstand=""1"">https://secure.newcropaccounts.com/V7/MTOM.V1/IClinicalMessaging/GetAllClinicalMessagesSummaryV2Response</a:Action>
                //        </s:Header>
                //        <s:Body>
                //            <GetAllClinicalMessagesSummaryV2Response
                //                xmlns=""https://secure.newcropaccounts.com/V7/MTOM.V1"">
                //                <GetAllClinicalMessagesSummaryV2Result
                //                    xmlns:b=""http://schemas.datacontract.org/2004/07/RX.WebServices.V7.Core""
                //                    xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"">
                //                    <b:clinicalMessageSummaryArray>
                //                        <b:ClinicalMessageSummary>
                //                            <b:ClinicalMessageGuid>1a6e78a0-cc3f-4542-a6b8-c31ebe70addc</b:ClinicalMessageGuid>
                //                            <b:ReceivedTimestamp>
                //                                <b:DateTimeComponent>0001-01-01T00:00:00</b:DateTimeComponent>
                //                                <b:StringComponent>2017-09-08T04:19:16.497Z</b:StringComponent>
                //                            </b:ReceivedTimestamp>
                //                        </b:ClinicalMessageSummary>
                //                        <b:ClinicalMessageSummary>
                //                            <b:ClinicalMessageGuid>0e480e2e-8c0e-44fb-a624-c56192010baa</b:ClinicalMessageGuid>
                //                            <b:ReceivedTimestamp>
                //                                <b:DateTimeComponent>0001-01-01T00:00:00</b:DateTimeComponent>
                //                                <b:StringComponent>2017-09-08T04:18:56.850Z</b:StringComponent>
                //                            </b:ReceivedTimestamp>
                //                        </b:ClinicalMessageSummary>
                //                    </b:clinicalMessageSummaryArray>
                //                    <b:result>
                //                        <b:Message/>
                //                        <b:RowCount>2</b:RowCount>
                //                        <b:Status>OK</b:Status>
                //                        <b:Timing>0</b:Timing>
                //                        <b:XmlResponse/>
                //                    </b:result>
                //                </GetAllClinicalMessagesSummaryV2Result>
                //            </GetAllClinicalMessagesSummaryV2Response>
                //        </s:Body>
                //    </s:Envelope>";

                //XDocument doc = XDocument.Parse(response);
                //XNamespace ns = "http://schemas.datacontract.org/2004/07/RX.WebServices.V7.Core";
                //IEnumerable<XElement> responses = doc.Descendants(ns + "ClinicalMessageSummary");
                //foreach (XElement res in responses)
                //{
                //    string clinicalMessageGuid = (string)res.Element(ns + "ClinicalMessageGuid");
                
                    if (!ProcessedClinicalMessageId.Contains(clinicalMessageGuid))
                        GetClinicalMessageV2(clinicalMessageGuid); 
                
                //}
            }
            catch
            {
                
            }

        }

        public static HttpWebRequest CreateWebRequest()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://preproduction.newcropaccounts.com/V7/MTOM.V1/ClinicalMessaging.svc");
           // webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "application/soap+xml;charset=UTF-8";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private void GetClinicalMessageV2(string clinicalMessageGuid)
        {
            Credentials cred = new Credentials();
            WSHttpBinding binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.MessageEncoding = WSMessageEncoding.Mtom;
            binding.MaxReceivedMessageSize = Int32.MaxValue;
            EndpointAddress address =
            new EndpointAddress(InboxUrl);
            cred.Name = "demo";
            cred.Password = "demo";
            cred.PartnerName = "IOPW";
            AccountRequest accReq = new AccountRequest();
            accReq.AccountId = "demo";
            accReq.SiteId = "demo";
            ClinicalMessagingClient obj = new ClinicalMessagingClient(binding, address);

            ClinicalMessageDetailResultV2 clinicalMsgDetRes = obj.GetClinicalMessageV2(cred, accReq, clinicalMessageGuid);
            ClinicalMessageDetailV2 res = clinicalMsgDetRes.clinicalMessageDetail;
            ClinicalMessageAttachment[] attachments = res.ClinicalMessageAttachments;
            if (attachments != null && attachments.Length > 0)
            {
                isAttachments = true;
                foreach (var attachment in attachments)
                {
                    var content = attachment;
                    if (attachment.documentName != null && attachment.documentName.Length > 0 && attachment.payload != null)
                    {
                        if (attachment.documentName.Contains(".xml")  || attachment.documentName.Contains(".XML"))
                        {
                            CreateFile(folderpath_xml, attachment.payload, attachment.documentName);
                            
                            ImportToDB(res, attachment.documentName, folderpath_xml, attachment.documentType);
                        }
                    }
                }
            }
            else
            {
                ImportToDB(res, string.Empty, string.Empty, string.Empty);
            }
            
        }

        private void ImportToDB(ClinicalMessageDetailV2 clinicalMessageDetailV2, string fileName, string filePath, string contentType)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            string queryQueueLogs = "INSERT INTO[dbo].[ACI_DEP_Inbound] " +
                "(ExternalId,[FromProviderAddress],[ToProviderAddress],[MessageSubject],[MessageBody],[DEPRequestDate],AT_FileName,filepath,AT_ContentType" +
               ") VALUES" +
                    "(@ExternalId,@FromProviderAddress,@ToProviderAddress,@MessageSubject,@MessageBody,@DEPRequestDate,@FileName,@FilePath,@contentType)";

            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            try
            {
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = queryQueueLogs;

                    var fromAddress = clinicalMessageDetailV2.FromAddress != null && clinicalMessageDetailV2.FromAddress != "" ? clinicalMessageDetailV2.FromAddress : string.Empty;
                    if (!fromAddressess.Contains(fromAddress))
                        fromAddressess.Add(fromAddress);

                    IDbDataParameter parFromProviderAddress = cmd.CreateParameter();
                    parFromProviderAddress.Direction = ParameterDirection.Input;
                    parFromProviderAddress.Value = clinicalMessageDetailV2.FromAddress != null && clinicalMessageDetailV2.FromAddress != "" ? clinicalMessageDetailV2.FromAddress : string.Empty;
                    parFromProviderAddress.ParameterName = "@FromProviderAddress";
                    parFromProviderAddress.DbType = DbType.String;
                    cmd.Parameters.Add(parFromProviderAddress);

                    if (clinicalMessageDetailV2.ToAddresses != null && clinicalMessageDetailV2.ToAddresses.Length > 0)
                    {
                        string ToAddress = string.Empty;
                        foreach (var toAddress in clinicalMessageDetailV2.ToAddresses)
                        {
                            ToAddress += toAddress.messagingAddress + ";";
                        }

                        IDbDataParameter parToProviderAddress = cmd.CreateParameter();
                        parToProviderAddress.Direction = ParameterDirection.Input;
                        parToProviderAddress.Value = ToAddress != null && ToAddress != "" ? ToAddress : string.Empty;
                        parToProviderAddress.ParameterName = "@ToProviderAddress";
                        parToProviderAddress.DbType = DbType.String;
                        cmd.Parameters.Add(parToProviderAddress);
                    }

                    IDbDataParameter parMessageBody = cmd.CreateParameter();
                    parMessageBody.Direction = ParameterDirection.Input;
                    parMessageBody.Value = clinicalMessageDetailV2.MessageText != null && clinicalMessageDetailV2.MessageText != "" ? clinicalMessageDetailV2.MessageText : string.Empty;
                    parMessageBody.ParameterName = "@MessageBody";
                    parMessageBody.DbType = DbType.String;
                    cmd.Parameters.Add(parMessageBody);

                    string format = "yyyy-MM-dd HH:mm:ss.fff";
                    //dt.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK");
                    string dt= (DateTime.Parse(clinicalMessageDetailV2.ReceivedTimestamp.StringComponent.ToString()).ToString(format));
                    IDbDataParameter parDEPRequestDate = cmd.CreateParameter();
                    parDEPRequestDate.Direction = ParameterDirection.Input; 
                    parDEPRequestDate.Value = dt;
                    parDEPRequestDate.ParameterName = "@DEPRequestDate";
                    parDEPRequestDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parDEPRequestDate);

                    IDbDataParameter parMessageSubject = cmd.CreateParameter();
                    parMessageSubject.Direction = ParameterDirection.Input;
                    parMessageSubject.Value = clinicalMessageDetailV2.Subject != null && clinicalMessageDetailV2.Subject != "" ? clinicalMessageDetailV2.Subject : string.Empty;
                    parMessageSubject.ParameterName = "@MessageSubject";
                    parMessageSubject.DbType = DbType.String;
                    cmd.Parameters.Add(parMessageSubject);

                    //IDbDataParameter parAciDepResponseID = cmd.CreateParameter();
                    //parAciDepResponseID.Direction = ParameterDirection.Input;
                    ////parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    //parAciDepResponseID.ParameterName = "@AciDepResponseID";
                    //parAciDepResponseID.DbType = DbType.Int64;
                    //cmd.Parameters.Add(parAciDepResponseID);

                    //IDbDataParameter parResourceType = cmd.CreateParameter();
                    //parResourceType.Direction = ParameterDirection.Input;
                    ////parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    //parResourceType.ParameterName = "@ResourceType";
                    //parResourceType.DbType = DbType.Int64;
                    //cmd.Parameters.Add(parResourceType);

                    IDbDataParameter parExternalId = cmd.CreateParameter();
                    parExternalId.Direction = ParameterDirection.Input;
                    parExternalId.Value = clinicalMessageDetailV2.ClinicalMessageGuid != null && clinicalMessageDetailV2.ClinicalMessageGuid.ToString() != "" ? clinicalMessageDetailV2.ClinicalMessageGuid.ToString() : clinicalMessageDetailV2.ClinicalMessageGuid.ToString();
                    parExternalId.ParameterName = "@ExternalId";
                    parExternalId.DbType = DbType.String;
                    cmd.Parameters.Add(parExternalId);

                    //IDbDataParameter parReferenceNumber = cmd.CreateParameter();
                    //parReferenceNumber.Direction = ParameterDirection.Input;
                    ////parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    //parReferenceNumber.ParameterName = "@ReferenceNumber";
                    //parReferenceNumber.DbType = DbType.Int64;
                    //cmd.Parameters.Add(parReferenceNumber);

                    //IDbDataParameter parSource = cmd.CreateParameter();
                    //parSource.Direction = ParameterDirection.Input;
                    ////parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    //parSource.ParameterName = "@Source";
                    //parSource.DbType = DbType.Int64;
                    //cmd.Parameters.Add(parSource);

                    //IDbDataParameter parExternalReferenceId = cmd.CreateParameter();
                    //parExternalReferenceId.Direction = ParameterDirection.Input;
                    ////parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    //parExternalReferenceId.ParameterName = "@ExternalReferenceId";
                    //parExternalReferenceId.DbType = DbType.Int64;
                    //cmd.Parameters.Add(parExternalReferenceId);

                    //IDbDataParameter parPatientAccountNumber = cmd.CreateParameter();
                    //parPatientAccountNumber.Direction = ParameterDirection.Input;
                    ////parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    //parPatientAccountNumber.ParameterName = "@PatientAccountNumber";
                    //parPatientAccountNumber.DbType = DbType.Int64;
                    //cmd.Parameters.Add(parPatientAccountNumber);

                    //IDbDataParameter parEncounterId = cmd.CreateParameter();
                    //parEncounterId.Direction = ParameterDirection.Input;
                    ////parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    //parEncounterId.ParameterName = "@EncounterId";
                    //parEncounterId.DbType = DbType.Int64;
                    //cmd.Parameters.Add(parEncounterId);

                    IDbDataParameter parFileName = cmd.CreateParameter();
                    parFileName.Direction = ParameterDirection.Input;
                    parFileName.Value = fileName;
                    parFileName.ParameterName = "@FileName";
                    parFileName.DbType = DbType.String;
                    cmd.Parameters.Add(parFileName);

                    IDbDataParameter parFilePath = cmd.CreateParameter();
                    parFilePath.Direction = ParameterDirection.Input;
                    parFilePath.Value = filePath+"\\"+fileName;
                    parFilePath.ParameterName = "@FilePath";
                    parFilePath.DbType = DbType.String;
                    cmd.Parameters.Add(parFilePath);

                    IDbDataParameter parContentType = cmd.CreateParameter();
                    parContentType.Direction = ParameterDirection.Input;
                    parContentType.Value = contentType;
                    parContentType.ParameterName = "@contentType";
                    parContentType.DbType = DbType.String;
                    cmd.Parameters.Add(parContentType);

                    cmd.CommandType = CommandType.Text;
                    int queueId = cmd.ExecuteNonQuery();
                    ProcessedClinicalMessageId.Add(clinicalMessageDetailV2.ClinicalMessageGuid);
                }
            }
            catch
            {
                throw;
            }
        }

        private void btnImportSend_Click(object sender, EventArgs e)
        {
            
        }

        private void frmImportPHI_Load(object sender, EventArgs e)
        {
            //FolderPath= @"C:\Pinpoint";
            if (string.IsNullOrEmpty(txtUserName.Text))
                btnImport.Enabled = false;
            else
            {
                btnImport.Enabled = true;
            }
                
            char[] array2 = new char[] { '\\' };
            FolderPath = FolderPath;
            CreateProviderDirectories();

            //Task getAllMessage = new Task(InvokeGetGetAllClinicalMessagesSummaryV2);
            //getAllMessage.Start();
        }
        private void CreateProviderDirectories()
        {
            FolderPath = FolderPath + "\\ProviderAttachments\\";
            if (!Directory.Exists(FolderPath))
            {
                Directory.CreateDirectory(FolderPath);
            }

            ProviderPath = FolderPath + "\\" + ProviderKey;
            if (!Directory.Exists(ProviderPath))
            {
                Directory.CreateDirectory(ProviderPath);
            }

            if (!Directory.Exists(ProviderPath + "\\TXT\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\TXT\\");
            }

            if (!Directory.Exists(ProviderPath + "\\PDF\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\PDF\\");
            }

            if (!Directory.Exists(ProviderPath + "\\HTML\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\HTML\\");
            }

            if (!Directory.Exists(ProviderPath + "\\JSONS\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\JSONS\\");
            }

            if (!Directory.Exists(ProviderPath + "\\XML\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\XML\\");
            }

            if (!Directory.Exists(ProviderPath + "\\JPG\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\JPG\\");
            }

            if (!Directory.Exists(ProviderPath + "\\PNG\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\PNG\\");
            }

            if (!Directory.Exists(ProviderPath + "\\ZIP\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\ZIP\\");
            }
        }
        
        private static void CreateFile(string FolderPath, byte[] fileContent, string filename)
        {
            try
            {
                FileStream file = File.Create(FolderPath + filename);
                file.Write(fileContent, 0, fileContent.Length);
                file.Close();
            }
            catch
            {
                
            }

        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtUpdatedEmail.Text))
                {
                    string emailBody = "Updated mail address for: Provider: "+ ResourceName + " is "+ txtUpdatedEmail.Text;
                    if (fromAddressess != null && fromAddressess.Count > 0)
                    {
                        foreach (var fromAddr in fromAddressess)
                        {
                            //Sendmail
                            using (SmtpClient SmtpMail = new SmtpClient("smtp.elasticemail.com", 587))
                            {
                                NetworkCredential basicAuthInfo = new NetworkCredential("help@iopracticeware.com", "1f614c42-13d1-4003-bc47-5ea45507a6e7");
                                SmtpMail.Credentials = basicAuthInfo;
                                
                                MailMessage message = new MailMessage("help@iopracticeware.com", fromAddr, "Updated email Id", emailBody);
                                message.From = new MailAddress("help@iopracticeware.com", "Provider Inbox");
                                message.Priority = MailPriority.Normal;
                                message.IsBodyHtml = true;
                                SmtpMail.EnableSsl = true;
                                SmtpMail.Credentials = basicAuthInfo;
                                SmtpMail.Send(message);
                                
                            }

                            

                        }
                    }
                }
                btnImport.Enabled = true;
            }
            catch
            { }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUserName.Text))
            {
                btnImport.Enabled = true;
            }
            else
                btnImport.Enabled = false;
        }
    }
}
