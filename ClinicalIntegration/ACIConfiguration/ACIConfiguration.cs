﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    public class ModelACIConfiguration
    {
        public int VendorId { get; set; }
        public int MDO_PracticeId { get; set; }
        public string BaseURL { get; set; }
        public string ProductKey { get; set; }
        public string PairKey { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Dashboard_BaseUrl { get; set; }
        public string Baseurl_PublishDEP { get; set; }
        public string Baseurl_GetPublishDEPResult { get; set; }
        public string Baseurl_GetMessageDeliveryNotification { get; set; }
        public string Baseurl_RetriveInboundDEPData { get; set; }

    }
    public class ACIConfiguration
    {
        public ModelACIConfiguration GetConfiguration()
        {
            ModelACIConfiguration ObjModelACIConfiguration = new ModelACIConfiguration();
            try
            {
                String query = "SELECT TOP 1  ACI_VendorId, MDO_PracticeId, ACI_BaseURL, ACI_ProductKey, ACI_PairKey, ACI_USERNAME, ACI_ROLE, ACI_Dashboard_BaseUrl, ACI_Baseurl_PublishDEP, ACI_Baseurl_GetPublishDEPResult, ACI_Baseurl_GetMessageDeliveryNotification, ACI_Baseurl_RetriveInboundDEPData FROM dbo.aci_json_defaults with (nolock)";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ObjModelACIConfiguration.VendorId = Convert.ToInt32(Convert.ToString(reader["ACI_VendorId"]));
                            ObjModelACIConfiguration.MDO_PracticeId = Convert.ToInt32(Convert.ToString(reader["MDO_PracticeId"]));
                            ObjModelACIConfiguration.BaseURL = Convert.ToString(reader["ACI_BaseURL"]);
                            ObjModelACIConfiguration.ProductKey = Convert.ToString(reader["ACI_ProductKey"]);
                            ObjModelACIConfiguration.PairKey = Convert.ToString(reader["ACI_PairKey"]);
                            ObjModelACIConfiguration.UserName = Convert.ToString(reader["ACI_USERNAME"]);
                            ObjModelACIConfiguration.Role = Convert.ToString(reader["ACI_ROLE"]);
                            ObjModelACIConfiguration.Dashboard_BaseUrl = Convert.ToString(reader["ACI_Dashboard_BaseUrl"]);
                            ObjModelACIConfiguration.Baseurl_PublishDEP = Convert.ToString(reader["ACI_Baseurl_PublishDEP"]);
                            ObjModelACIConfiguration.Baseurl_GetPublishDEPResult = Convert.ToString(reader["ACI_Baseurl_GetPublishDEPResult"]);
                            ObjModelACIConfiguration.Baseurl_GetMessageDeliveryNotification = Convert.ToString(reader["ACI_Baseurl_GetMessageDeliveryNotification"]);
                            ObjModelACIConfiguration.Baseurl_RetriveInboundDEPData = Convert.ToString(reader["ACI_Baseurl_RetriveInboundDEPData"]);
                        }
                    }
                }
            }
            catch
            {

            }
            return ObjModelACIConfiguration;
        }
        public int AddUpateConfiguration(ModelACIConfiguration objModelACIConfiguration)
        {
            int retStatus = 0;
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                String query = "AddUpdateACI_json_defaults";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;

                IDbDataParameter IdbParam1 = cmd.CreateParameter();
                IdbParam1.Direction = ParameterDirection.Input;
                IdbParam1.Value = objModelACIConfiguration.VendorId;
                IdbParam1.ParameterName = "@ACI_VendorId";
                IdbParam1.DbType = DbType.Int32;
                cmd.Parameters.Add(IdbParam1);

                IDbDataParameter IdbParam2 = cmd.CreateParameter();
                IdbParam2.Direction = ParameterDirection.Input;
                IdbParam2.Value = objModelACIConfiguration.MDO_PracticeId;
                IdbParam2.ParameterName = "@MDO_PracticeId";
                IdbParam2.DbType = DbType.Int32;
                cmd.Parameters.Add(IdbParam2);

                IDbDataParameter IdbParam3 = cmd.CreateParameter();
                IdbParam3.Direction = ParameterDirection.Input;
                IdbParam3.Value = objModelACIConfiguration.BaseURL;
                IdbParam3.ParameterName = "@ACI_BaseURL";
                IdbParam3.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam3);

                IDbDataParameter IdbParam4 = cmd.CreateParameter();
                IdbParam4.Direction = ParameterDirection.Input;
                IdbParam4.Value = objModelACIConfiguration.ProductKey;
                IdbParam4.ParameterName = "@ACI_ProductKey";
                IdbParam4.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam4);

                IDbDataParameter IdbParam5 = cmd.CreateParameter();
                IdbParam5.Direction = ParameterDirection.Input;
                IdbParam5.Value = objModelACIConfiguration.PairKey;
                IdbParam5.ParameterName = "@ACI_PairKey";
                IdbParam5.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam5);

                IDbDataParameter IdbParam6 = cmd.CreateParameter();
                IdbParam6.Direction = ParameterDirection.Input;
                IdbParam6.Value = objModelACIConfiguration.UserName;
                IdbParam6.ParameterName = "@ACI_USERNAME";
                IdbParam6.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam6);

                IDbDataParameter IdbParam7 = cmd.CreateParameter();
                IdbParam7.Direction = ParameterDirection.Input;
                IdbParam7.Value = objModelACIConfiguration.Role;
                IdbParam7.ParameterName = "@ACI_ROLE";
                IdbParam7.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam7);

                IDbDataParameter IdbParam8 = cmd.CreateParameter();
                IdbParam8.Direction = ParameterDirection.Input;
                IdbParam8.Value = objModelACIConfiguration.Dashboard_BaseUrl;
                IdbParam8.ParameterName = "@ACI_Dashboard_BaseUrl";
                IdbParam8.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam8);

                IDbDataParameter IdbParam9 = cmd.CreateParameter();
                IdbParam9.Direction = ParameterDirection.Input;
                IdbParam9.Value = objModelACIConfiguration.Baseurl_PublishDEP;
                IdbParam9.ParameterName = "@ACI_Baseurl_PublishDEP";
                IdbParam9.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam9);

                IDbDataParameter IdbParam10 = cmd.CreateParameter();
                IdbParam10.Direction = ParameterDirection.Input;
                IdbParam10.Value = objModelACIConfiguration.Baseurl_GetPublishDEPResult;
                IdbParam10.ParameterName = "@ACI_Baseurl_GetPublishDEPResult";
                IdbParam10.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam10);

                IDbDataParameter IdbParam11 = cmd.CreateParameter();
                IdbParam11.Direction = ParameterDirection.Input;
                IdbParam11.Value = objModelACIConfiguration.Baseurl_GetMessageDeliveryNotification;
                IdbParam11.ParameterName = "@ACI_Baseurl_GetMessageDeliveryNotification";
                IdbParam11.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam11);

                IDbDataParameter IdbParam12 = cmd.CreateParameter();
                IdbParam12.Direction = ParameterDirection.Input;
                IdbParam12.Value = objModelACIConfiguration.Baseurl_RetriveInboundDEPData;
                IdbParam12.ParameterName = "@ACI_Baseurl_RetriveInboundDEPData";
                IdbParam12.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam12);

                retStatus = cmd.ExecuteNonQuery();
            }
            return retStatus;
        }
    }
}
