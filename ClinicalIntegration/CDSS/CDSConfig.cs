﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ClinicalIntegration
{
    public class CDSRoles
    {
        public int Id { get; set; }
        public string ResourceType { get; set; }
        public Boolean CDSConfiguration { get; set; }
        public Boolean Alerts { get; set; }
        public Boolean Info { get; set; }

        public string UserList { get; set; }

    }
    public class CDSUserRoles
    {
        public int Id { get; set; }
        public string ResourceType { get; set; }
        public Boolean CDSConfiguration { get; set; }
        public Boolean Alerts { get; set; }
        public Boolean Info { get; set; }
        public string ResourceName { get; set; }

    }
    public class CDSConfig
    {
        public List<CDSRoles> GetCDSRoles()
        {
            List<CDSRoles> objList = new List<CDSRoles>();
            DataTable dt = new DataTable();
            try
            {
                String query = "Select Id,case ResourceType When 'D' then 'Doctor' When 'T' then 'Technician' When 'F' then 'Front Desk' When 'P' then 'Practice Manager' When 'S' then 'S Billing Supervisor' End as ResourceType, CDSConfiguration, Alerts, Info, 'User List' as Col3 from dbo.CDSRoles with (nolock) Where ResourceType is not null and ResourceType <> 'O'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                        objList = LoadRoles(dt);
                    }
                }
            }
            catch
            {
            }
            return objList;
        }

        public List<CDSRoles> LoadRoles(DataTable dt)
        {
            List<CDSRoles> objRuleLst = new List<CDSRoles>();
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CDSRoles objRule = new CDSRoles();
                    objRule.Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    objRule.ResourceType = Convert.ToString(dt.Rows[i]["resourceType"].ToString());
                    objRule.CDSConfiguration = Convert.ToBoolean(dt.Rows[i]["CDSConfiguration"].ToString());
                    objRule.Alerts = Convert.ToBoolean(dt.Rows[i]["Alerts"].ToString());
                    objRule.Info = Convert.ToBoolean(dt.Rows[i]["Info"].ToString());
                    objRule.UserList = Convert.ToString(dt.Rows[i]["Col3"].ToString());
                    objRuleLst.Add(objRule);
                }
            }
            catch
            {

            }
            return objRuleLst;
        }

        public void updateCDS(string query)
        {
            try
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        public List<CDSUserRoles> GetCDSUserRoles(string resourceType)
        {
            List<CDSUserRoles> objList = new List<CDSUserRoles>();
            DataTable dt = new DataTable();
            try
            {
                String query = "Select B.ResourceId, A.ResourceName, B.ResourceType, B.CDSConfiguration, B.Alerts, B.Info from dbo.Resources A with (nolock) INNER JOIN dbo.CDS_UserPermissions B with (nolock) on A.ResourceId =B.ResourceId WHERE B.ResourceType='" + resourceType.Trim() + "'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                        objList = LoaduserRoles(dt);
                    }
                }
            }
            catch
            {
            }
            return objList;
        }

        public List<CDSUserRoles> LoaduserRoles(DataTable dt)
        {
            List<CDSUserRoles> objRuleLst = new List<CDSUserRoles>();
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CDSUserRoles objRule = new CDSUserRoles();
                    objRule.Id = Convert.ToInt32(dt.Rows[i]["ResourceId"].ToString());
                    objRule.ResourceName = Convert.ToString(dt.Rows[i]["ResourceName"].ToString());
                    objRule.ResourceType = Convert.ToString(dt.Rows[i]["ResourceType"].ToString());
                    objRule.Alerts = Convert.ToBoolean(dt.Rows[i]["Alerts"].ToString());
                    objRule.Info = Convert.ToBoolean(dt.Rows[i]["Info"].ToString());
                    objRule.CDSConfiguration = Convert.ToBoolean(dt.Rows[i]["CDSConfiguration"].ToString());
                    objRuleLst.Add(objRule);
                }
            }
            catch
            {

            }
            return objRuleLst;
        }
    }
}
