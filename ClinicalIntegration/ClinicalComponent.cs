﻿using System;
using System.Data;
using System.Runtime.InteropServices;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public class ClinicalComponent
    {
        public static IDbConnection _Connection;

        public static string ImplantableDeviceUDIURL = "https://accessgudid.nlm.nih.gov/api/v1/parse_udi.json?udi={0}";

        public static string ImplantableDeviceURL = "https://accessgudid.nlm.nih.gov/api/v1/devices/lookup.json?di={0}";
        public static string PatientName { get; set; }
        public static string PatientId { get; set; }
        public static string AppointmentId { get; set; }
        public static string AppointmentDate { get; set; }
        public static string UserId { get; set; }
        private static string _UserName { get; set; }
        private static string _ProviderName { get; set; }
        private static string _ResourceId { get; set; }


        //public static string MedInfoUrl = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.88&mainSearchCriteria.v.c={0}&informationRecipient.languageCode.c={1}";


        public static string MedInfoUrl = "https://connect.medlineplus.gov/service?mainSearchCriteria.v.cs=2.16.840.1.113883.6.88&mainSearchCriteria.v.c={0}&informationRecipient.languageCode.c={1}";

        public static string ProblemInfoValidationUrl_Icd10 = "	https://connect.medlineplus.gov/service?mainSearchCriteria.v.cs=2.16.840.1.113883.6.90&mainSearchCriteria.v.c={0}&informationRecipient.languageCode.c={1}";

        public static string ProblemInfoValidationUrl_Icd9 = "	https://connect.medlineplus.gov/service?mainSearchCriteria.v.cs=2.16.840.1.113883.6.103&mainSearchCriteria.v.c={0}&informationRecipient.languageCode.c={1}";

        private static string _PortalBaseUrl = "https://portal.iopracticeware.com/";

        public static string _PortalLink
        {
            get { return _PortalBaseUrl + "api/Patient/ExternalLink"; }
        }

        public static string _PGHDFileLink
        {
            get { return _PortalBaseUrl + "api/Patient/SharedFiles"; }
        }

        public static string _PGHDDocumentLink
        {
            get { return _PortalBaseUrl + "api/Patient/DownloadFile"; }
        }

        public static string _PostDoctorInformation
        {
            get { return _PortalBaseUrl + "api/Doctor/Post"; }
        }

        public static string _PGHDRecievedFile
        {
            get { return _PortalBaseUrl + "api/Patient/FileRecieved"; }
        }

        public static string _PGHDDeletedFile
        {
            get { return _PortalBaseUrl + "api/Patient/DeleteFile"; }
        }

        public static string _EducationMaterialLink
        {
            get { return _PortalBaseUrl + "api/Patient/EducationMaterial"; }
        }

        public static string _CredentialLink
        {
            get { return _PortalBaseUrl + "api/Patient/SendCredentials"; }
        }
      
        public static string ProviderId
        {
            get
            {
                if (_ResourceId == null)
                    _ResourceId = GetProviderId(AppointmentId);
                return _ResourceId;
            }
            set { }
        }
        public static string UserName
        {
            get
            {
                if (_UserName == null)
                    _UserName = GetResourceName(UserId);
                return _UserName;
            }
            set { }
        }
        public static string ProviderName
        {
            get
            {
                if (_ProviderName == null)
                    _ProviderName = GetResourceName(ProviderId);
                return _ProviderName;
            }
            set { }
        }
        public static string GetResourceName(string _resourceId)
        {
            string retStatus = "";
            string _error = "";
            try
            {
                string _selectQry = "Select ResourceName From dbo.Resources with (nolock) Where ResourceId = @ResourceId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter ResourceId = cmd.CreateParameter();
                    ResourceId.Direction = ParameterDirection.Input;
                    ResourceId.Value = _resourceId;
                    ResourceId.ParameterName = "@ResourceId";
                    ResourceId.DbType = DbType.Int32;
                    cmd.Parameters.Add(ResourceId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    retStatus = cmd.ExecuteScalar().ToString();
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }

        public static string GetProviderId(string _appointmentId)
        {
            string retStatus = "";
            string _error = "";
            try
            {
                string _selectQry = "Select ResourceId1 From dbo.Appointments with (nolock) Where AppointmentId = @AppointmentId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter ResourceId = cmd.CreateParameter();
                    ResourceId.Direction = ParameterDirection.Input;
                    ResourceId.Value = _appointmentId;
                    ResourceId.ParameterName = "@AppointmentId";
                    ResourceId.DbType = DbType.Int32;
                    cmd.Parameters.Add(ResourceId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    retStatus = cmd.ExecuteScalar().ToString();
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }

    }
}
