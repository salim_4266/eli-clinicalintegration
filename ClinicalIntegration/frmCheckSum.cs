﻿using System;
using System.Data;
using System.Windows.Forms;
using DBHelper;

namespace ClinicalIntegration
{
    public partial class frmCheckSum : Form
    {
        public string UserId { get; set; }
        public Object DBObject { get; set; }

        public frmCheckSum()
        {
            InitializeComponent();
        }

        private void frmCheckSum_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            ClinicalComponent.UserId = this.UserId;
            btnclose.TabStop = false;
            btnclose.FlatStyle = FlatStyle.Flat;
            btnclose.FlatAppearance.BorderSize = 0;
        }
        private void btnget_Click(object sender, EventArgs e)
        {
            Checksum objCS = new Checksum();            
            DataTable dt = objCS.GetTamperData(dateTimePicker1.Value, dateTimePicker2.Value);
            foreach (DataRow row in dt.Rows)
            {
                var value = TimeZone.CurrentTimeZone.ToLocalTime(Convert.ToDateTime(row["TamperDate"]));
                row.SetField("TamperDate", value);
            }
            grdview.DataSource = dt.DefaultView;
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
