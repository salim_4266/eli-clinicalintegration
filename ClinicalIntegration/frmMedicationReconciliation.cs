﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmMedicationReconciliation : Form
    {
        public Form ParentMDI { get; set; }

        public string Filter { get; set; }
        private static DataTable dt;
        private static DataTable dtMerg;
        private static DataTable dtRxNorm;
        private static DataTable dtEAlg;
        public frmMedicationReconciliation()
        {
            InitializeComponent();
        }
        private void DefaultGridUI(DataGridView dg)
        {

            foreach (DataGridViewColumn col in dg.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font("Arial", 13F, FontStyle.Bold, GraphicsUnit.Pixel);

            }

        }
        private void frmMedicationReconciliation_Load(object sender, EventArgs e)
        {
            try
            {
                this.Dock = DockStyle.Fill;
                DefaultGridUI(grdMedications);
                DefaultGridUI(dgReconcilation);
                DefaultGridUI(dgMergedList);
                dt = new DataTable();
                dtRxNorm = new DataTable();
                //ReconcileList objRconcil = new ReconcileList();
                //dtRxNorm = objRconcil.GetMedicationRxNorms();
                GetReconcileMedicationList();
                GetMedicationList();
                //DisableButtonBorder(btnReconcile);
                dgReconcilation.AutoGenerateColumns = false;
                dgMergedList.AutoGenerateColumns = false;

                grdMedications.RowTemplate.Height = 32;
                grdMedications.AutoGenerateColumns = false;
                grdMedications.ReadOnly = true;

                grdMedications.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
                grdMedications.EnableHeadersVisualStyles = false;
                grdMedications.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
                grdMedications.EnableHeadersVisualStyles = false;
                grdMedications.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                grdMedications.ColumnHeadersHeight = 25;
                grdMedications.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
                GridUI(dgReconcilation);
                GridUI(dgMergedList);
            }
            catch
            {
            }

        }

        private void GridUI(DataGridView dg)
        {
            dg.RowTemplate.Height = 32;
            dg.AutoGenerateColumns = false;
            dg.ReadOnly = true;

            dg.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            dg.EnableHeadersVisualStyles = false;
            dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dg.EnableHeadersVisualStyles = false;
            dg.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dg.ColumnHeadersHeight = 25;
            dg.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
            dg.Columns[0].HeaderCell.Style.Font = new Font("Tahoma", 9.75F, FontStyle.Bold);
        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }

        public void GetReconcileMedicationList()
        {
            ReconcileList objRconcil = new ReconcileList();
            // DataTable dt = new DataTable();
            dt = objRconcil.GetMedicationReconcileList(Convert.ToInt64(ClinicalComponent.PatientId));
            dgReconcilation.AutoGenerateColumns = false;
            dgReconcilation.DataSource = dt;
            dgReconcilation.AutoGenerateColumns = false;
        }


        public void GetMedicationList()
        {
            ReconcileList objRconcil = new ReconcileList();
            DataTable dt = new DataTable();
            dt = objRconcil.GetMedicationList(Convert.ToInt64(ClinicalComponent.PatientId));
            foreach (System.Data.DataColumn col in dt.Columns) col.ReadOnly = false;
            foreach (DataRow dr in dt.Rows)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(dr["LastModefiedDate"])))
                    dr["LastModefiedDate"] = Convert.ToDateTime(dr["LastModefiedDate"].ToString()).ToLocalTime();
            }
            grdMedications.AutoGenerateColumns = false;
            dt = RemoveDuplicateRows(dt, "DrugName");
            grdMedications.DataSource = dt;
            grdMedications.AutoGenerateColumns = false;
            dtEAlg = dt;

        }
        public DataTable RemoveDuplicateRows(DataTable table, string DistinctColumn)
        {
            try
            {
                ArrayList UniqueRecords = new ArrayList();
                ArrayList DuplicateRecords = new ArrayList();
                string DrugNames = string.Empty;
                foreach (DataRow dRow in table.Rows)
                {
                    string[] drgNm = Convert.ToString(dRow["FindingDetail"].ToString()).Split(new string[] { "-2" }, StringSplitOptions.None).ToArray();
                    drgNm[0] = drgNm[0].Replace("RX-8/", string.Empty);
                    dRow["DrugName"] = Convert.ToString(drgNm[0]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dRow["DrugName"])))
                    {
                        DrugNames += "'" + Convert.ToString(dRow["DrugName"]) + "',";
                    }
                }
                DrugNames = DrugNames.TrimEnd(',');
                ReconcileList objRconcil = new ReconcileList();
                dtRxNorm = objRconcil.GetMedicationRxNorms(DrugNames);
                foreach (DataRow dRow in table.Rows)
                {
                    string[] drgNm = Convert.ToString(dRow["FindingDetail"].ToString()).Split(new string[] { "-2" }, StringSplitOptions.None).ToArray();
                    drgNm[0] = drgNm[0].Replace("RX-8/", string.Empty);
                    dRow["DrugName"] = Convert.ToString(drgNm[0]);
                    for (int i = 0; i < dtRxNorm.Rows.Count; i++)
                    {
                        if (Convert.ToString(dtRxNorm.Rows[i]["DrugName"]) == Convert.ToString(dRow["DrugName"]).ToLower())
                        {
                            dRow["RxNorm"] = Convert.ToString(dtRxNorm.Rows[i]["RxNorm"]);
                        }
                    }
                }
                foreach (DataRow dRow in table.Rows)
                {
                    if (UniqueRecords.Contains(dRow[DistinctColumn]))
                        DuplicateRecords.Add(dRow);
                    else
                        UniqueRecords.Add(dRow[DistinctColumn]);
                }
                foreach (DataRow dRow in DuplicateRecords)
                {
                    table.Rows.Remove(dRow);
                }
                return table;
            }
            catch
            {
                return null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string clinicalId = string.Empty;
            ReconcileList objRconcil = new ReconcileList();
            for (int i = 0; i < dgMergedList.Rows.Count; i++)
            {
                if (Convert.ToString(dtMerg.Rows[i]["ExistingMedication"]) != "0")
                {
                    continue;
                }
                for (int j = 0; j < grdMedications.Rows.Count; j++)
                {
                    if ((Convert.ToString(grdMedications.Rows[j].Cells[1].Value).TrimEnd().TrimStart().ToUpper()) == Convert.ToString(dgMergedList.Rows[i].Cells[1].Value).TrimEnd().TrimStart().ToUpper())
                    {
                        clinicalId = Convert.ToString(dtEAlg.Rows[j]["ClinicalId"]);
                        break;
                    }
                }
                objRconcil.AddMedicationList(Convert.ToInt64(ClinicalComponent.PatientId), dtMerg.Rows[i], i, clinicalId, Convert.ToInt64(ClinicalComponent.AppointmentId.Trim()));
                GetMedicationList();
            }
            GetReconcileMedicationList();
            GetMedicationList();
            dgMergedList.DataSource = new DataTable();
        }

        private void btnReconcile_Click(object sender, EventArgs e)
        {

            ReconcileList objRconcil = new ReconcileList();
            dtMerg = dt.Clone();
            foreach (System.Data.DataColumn col in dtMerg.Columns) col.ReadOnly = false;
            for (int i = 0; i < dgReconcilation.Rows.Count; i++)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[i].Cells[1];
                if (chk != null)
                {
                    if (!Convert.ToBoolean(chk.Value))
                        continue;
                    //
                    dtMerg.ImportRow(dt.Rows[i]);
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["LastModefiedDate"] = DateTime.Now.ToString();
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["ExistingMedication"] = "0";

                    //
                }
            }
            GetMergedMedications(dtMerg);
            //dgMergedList.DataSource = dtMerg;
        }
        public void GetMergedMedications(DataTable dtRecon)
        {
            ReconcileList objRconcil = new ReconcileList();
            DataTable dt = new DataTable();
            dt = dtRecon.Clone();
            foreach (DataRow dr in dtRecon.Rows)
            {
                dt.ImportRow(dr);
            }
            DataTable dt1 = new DataTable();
            dt1 = dtEAlg;
            if (dt.Rows.Count == 0)
            {
                dgMergedList.AutoGenerateColumns = false;
                dgMergedList.DataSource = dt1;
                dgMergedList.AutoGenerateColumns = false;
                return;
            }
            //dt = dt1.Clone();
            DataTable dtChild = new DataTable();
            dtChild = dt.Clone();
            bool adddrdt = true;
            if (dt1 != null && dt1.Rows.Count > 0)
                foreach (DataRow dr in dt1.Rows)
                {
                    adddrdt = true;
                    foreach (DataRow drdtt in dt.Rows)
                    {
                        if (Convert.ToString(drdtt["Medication"]).ToLower() == Convert.ToString(dr["DrugName"]).ToLower())
                        {
                            adddrdt = false;
                            break;
                        }
                    }
                    if (adddrdt)
                    {
                        DataRow drdt = dtChild.NewRow();
                        drdt["MEDID"] = 0;
                        drdt["MED_STRENGTH"] = "";
                        drdt["Medication"] = dr["DrugName"];
                        drdt["RxNorm"] = dr["RxNorm"];
                        drdt["Status"] = dr["Status"];

                        drdt["frequency"] = "";
                        drdt["Id"] = 0;

                        drdt["ExistingMedication"] = "1";
                        drdt["LastModefiedDate"] = dr["LastModefiedDate"];
                        drdt["Instructions"] = "";
                        //drdt["problem"] = dr["Problem"];
                        //drdt["id"] = dr["id"];
                        dtChild.Rows.Add(drdt);
                    }


                }
            foreach (DataRow dr in dtChild.Rows)
            {
                dt.ImportRow(dr);
            }
            dgMergedList.AutoGenerateColumns = false;
            dgMergedList.DataSource = dt;
            dtMerg = dt;
            dgMergedList.AutoGenerateColumns = false;
        }
        private void dgReconcilation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {

                string medication = dt.Rows[e.RowIndex][2].ToString();
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[e.RowIndex].Cells[1];
                if (Convert.ToBoolean(chk.Value))
                {
                    chk.Value = false;
                }
                else
                {
                    chk.Value = true;
                    ReconcileList objRconcil = new ReconcileList();
                    if (objRconcil.CanDisplayAlert())
                        objRconcil.CallAlert(medication.Trim(), "MEDS");
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dgMergedList_SelectionChanged(object sender, EventArgs e)
        {
            dgMergedList.ClearSelection();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ReconcileList objRconcil = new ReconcileList();
            dtMerg = dt.Clone();
            foreach (System.Data.DataColumn col in dtMerg.Columns) col.ReadOnly = false;
            for (int i = 0; i < dgReconcilation.Rows.Count; i++)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[i].Cells[1];
                if (chk != null)
                {
                    if (!Convert.ToBoolean(chk.Value))
                        continue;
                    //
                    dtMerg.ImportRow(dt.Rows[i]);
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["LastModefiedDate"] = DateTime.Now.ToString();
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["ExistingMedication"] = "0";

                    //
                }
            }
            GetMergedMedications(dtMerg);
        }
    }
}
