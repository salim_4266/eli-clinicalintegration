﻿using DBHelper;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmRCPServices : Form
    {
        public frmRCPServices()
        {
            InitializeComponent();
        }

        public string jsonNumber { get; set; }
        public string externalId { get; set; }
        public string resourceId { get; set; }
        public string jsonDirectoryPath { get; set; }
        public Object DBObject { get; set; }
        private void frmRCPServices_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            bool retStatus = new RCPClient().Push(this.jsonNumber, this.externalId, this.resourceId, this.jsonDirectoryPath);
            if (retStatus)
            {
                this.Close();
            }
            else
            {
                this.Close();
            }
        }
    }
}