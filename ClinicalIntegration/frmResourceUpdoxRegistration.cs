﻿using DBHelper;
using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmResourceUpdoxRegistration : Form
    {
        public string ResourceId { get; set; }
        public Object DBObject { get; set; }
        private bool IsProvider { get; set; }
        private bool IsAdmin { get; set; }

        private ResourceDetail objResource = null;
        public frmResourceUpdoxRegistration()
        {
            InitializeComponent();
        }

        private void frmResourceUpdoxRegistration_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            ClinicalComponent.UserId = this.ResourceId;
            LoadUpdoxDetails();
        }

        private void LoadUpdoxDetails()
        {
            objResource = new ResourceDetail(ClinicalComponent.UserId);
            txtResourceAliasName.Text = objResource.FirstName + " " + objResource.LastName;
            if (objResource.UpdoxDetail.DirectAddress != "")
            {
                textBox5.Enabled = false;
                textBox5.Text = objResource.UpdoxDetail.DirectAddress;
            }
            this.IsProvider = objResource.UpdoxDetail.isProvider == "True" ? true : false;
            this.IsAdmin = objResource.UpdoxDetail.isAdmin == "True" ? true : false;
            comboBox1.SelectedText = objResource.UpdoxDetail.TimeZone;
            checkBox1.Checked = objResource.UpdoxDetail.isActive == "True" ? true : false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedText != "" && comboBox1.SelectedText != "--Select--")
            {
                MessageBox.Show("Please select Time Zone");
                return;
            }
            if (textBox5.Text.Trim() == "")
            {
                MessageBox.Show("Please enter Direct Address!");
                return;
            }

            UpdoxLib objUpDoxLib = new UpdoxLib();
            objResource.UpdoxDetail.LoginId = "";
            objResource.UpdoxDetail.Password = "";
            if (comboBox1.SelectedItem != null)
                objResource.UpdoxDetail.TimeZone = comboBox1.SelectedItem.ToString();
            objResource.UpdoxDetail.DirectAddress = textBox5.Text;
            objResource.UpdoxDetail.isActive = checkBox1.Checked == true ? "True" : "False";
            objResource.UpdoxDetail.isProvider = this.IsProvider == true ? "True" : "False";
            objResource.UpdoxDetail.isAdmin = this.IsAdmin == true ? "True" : "False";

            if (Convert.ToBoolean(objResource.IsUserExists))
            {
                if (objUpDoxLib.Updox_UserUpdate(objResource) == 1)
                {
                    MessageBox.Show("Information updated successfully");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while updating the user information, Please contact IO Support!");
                }
            }
            else
            {
                if (objUpDoxLib.Updox_UserCreate(objResource) == 1)
                {
                    MessageBox.Show("User registered successfully!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while registering the user, Please contact IO Support!");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
