﻿using DBHelper;
using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmACIConfiguration : Form
    {
        public Object DBObject { get; set; }
        static int _VendorId;
        public frmACIConfiguration()
        {
            InitializeComponent();
        }

        private void frmACIConfiguration_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            ACIConfiguration objACIConfiguration = new ACIConfiguration();
            ModelACIConfiguration objModelACIConfiguration = objACIConfiguration.GetConfiguration();
            if (objModelACIConfiguration.VendorId > 0)
            {
                _VendorId = objModelACIConfiguration.VendorId;
                txtVendorId.Text = Convert.ToString(objModelACIConfiguration.VendorId);
                txtPracticeId.Text = Convert.ToString(objModelACIConfiguration.MDO_PracticeId);
                txtBaseURL.Text = objModelACIConfiguration.BaseURL;
                txtProductKey.Text = objModelACIConfiguration.ProductKey;
                txtUserName.Text = objModelACIConfiguration.UserName;
                txtRole.Text = objModelACIConfiguration.Role;
                txtDBURL.Text = objModelACIConfiguration.Dashboard_BaseUrl;
                txtBURLPbDEP.Text = objModelACIConfiguration.Baseurl_PublishDEP;
                txtBURLGetPbDEPRslt.Text = objModelACIConfiguration.Baseurl_GetPublishDEPResult;
                txtBURLGetMsgDelvNotif.Text = objModelACIConfiguration.Baseurl_GetMessageDeliveryNotification;
                txtBURLRetriInbDEPData.Text = objModelACIConfiguration.Baseurl_RetriveInboundDEPData;
                txtPairKey.Text = objModelACIConfiguration.PairKey;
            }
        }
        
        private Boolean ValidateInputs()
        {
            bool IsValidated = true;
            string msgValidation = string.Empty;
            if (txtVendorId.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation = "VendorID, ";
            }
            if (txtPracticeId.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "PracticeID, ";
            }
            if (txtBaseURL.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "BaseURL, ";
            }
            if (txtProductKey.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "ProductKey, ";
            }
            if (txtUserName.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "UserName, ";
            }
            if (txtPairKey.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "PairKey, ";
            }
            if (txtRole.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "Role, ";
            }
            if (!IsValidated)
            {
                MessageBox.Show("Please Enter " + msgValidation);
            }
            return IsValidated;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string DomainURL = textBox1.Text.Trim();
            if (textBox1.Text != "")
            {
                //https://staging.eyecareleaders.com/Publishbatch/api/
                txtBaseURL.Text = DomainURL + "/Publishbatch/api/";

                //https://staging.eyecareleaders.com/MIPSPerformanceUI
                txtDBURL.Text = DomainURL + "/MIPSPerformanceUI";

                //https://staging.eyecareleaders.com/DirectEdge/api/v1/DirectEdge/PublishDEPData/
                txtBURLPbDEP.Text = DomainURL + "/DirectEdge/api/v1/DirectEdge/PublishDEPData/";

                //https://staging.eyecareleaders.com/DirectEdge/api/v1/DirectEdge/GetPublishDEPResult/
                txtBURLGetPbDEPRslt.Text = DomainURL + "/DirectEdge/api/v1/DirectEdge/GetPublishDEPResult/";

                //https://staging.eyecareleaders.com/DirectEdge/api/v1/SecureExchange/RetriveInboundSecureExchnageMessageStatus/
                txtBURLGetMsgDelvNotif.Text = DomainURL + "/DirectEdge/api/v1/DirectEdge/GetMessageDeliveryNotification/";

                //https://staging.eyecareleaders.com/DirectEdge/api/v1/DirectEdge/RetriveInboundDEPDataUpdox/
                txtBURLRetriInbDEPData.Text = DomainURL + "/DirectEdge/api/v1/DirectEdge/RetriveInboundDEPDataUpdox/";
            }
            else
            {
                txtBaseURL.Text = "";
                txtDBURL.Text = "";
                txtBURLPbDEP.Text = "";
                txtBURLGetPbDEPRslt.Text = "";
                txtBURLGetMsgDelvNotif.Text = "";
                txtBURLRetriInbDEPData.Text = "";
            }
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (ValidateInputs())
                {
                    ModelACIConfiguration objModelACIConfiguration = new ModelACIConfiguration();
                    objModelACIConfiguration.VendorId = Convert.ToInt32(txtVendorId.Text.Trim());
                    objModelACIConfiguration.MDO_PracticeId = Convert.ToInt32(txtPracticeId.Text.Trim());
                    objModelACIConfiguration.BaseURL = Convert.ToString(txtBaseURL.Text.Trim());
                    objModelACIConfiguration.ProductKey = Convert.ToString(txtProductKey.Text.Trim());
                    objModelACIConfiguration.UserName = txtUserName.Text.Trim();
                    objModelACIConfiguration.Role = txtRole.Text.Trim();
                    objModelACIConfiguration.Dashboard_BaseUrl = txtDBURL.Text.Trim();
                    objModelACIConfiguration.Baseurl_PublishDEP = txtBURLPbDEP.Text.Trim();
                    objModelACIConfiguration.Baseurl_GetPublishDEPResult = txtBURLGetPbDEPRslt.Text.Trim();
                    objModelACIConfiguration.Baseurl_GetMessageDeliveryNotification = txtBURLGetMsgDelvNotif.Text.Trim();
                    objModelACIConfiguration.Baseurl_RetriveInboundDEPData = txtBURLRetriInbDEPData.Text.Trim();
                    objModelACIConfiguration.PairKey = txtPairKey.Text.Trim();
                    ACIConfiguration objACIConfiguration = new ACIConfiguration();
                    if (objACIConfiguration.AddUpateConfiguration(objModelACIConfiguration) > 0)
                    {
                        MessageBox.Show("Configuration Saved Successfully");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("There is some issue while saving data, Please check with Support Team");
                    }
                    
                }
            }
            catch
            {
                MessageBox.Show("There is some issue while saving data, Please check with Support Team");
            }
        }
    }
}
