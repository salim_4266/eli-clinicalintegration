﻿using DBHelper;
using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmParentForm : Form
    {
        public string PatientId { get; set; }
        public string AppointmentId { get; set; }
        public string AppointmentDate { get; set; }
        public string UserId { get; set; }
        public string PatientName { get; set; }
        public Object DBObject { get; set; }

        public frmParentForm()
        {
            InitializeComponent();
        }

        private void ParentForm_Load(object sender, EventArgs e)
        {
            try
            {
                DBUtility db = new DBUtility();
                db.DbObject = DBObject;
                ClinicalComponent._Connection = db.DbObject as IDbConnection;
                ClinicalComponent.PatientId = this.PatientId;
                ClinicalComponent.AppointmentId = this.AppointmentId;
                ClinicalComponent.UserId = this.UserId;
                ClinicalComponent.PatientName = this.PatientName;
                ClinicalComponent.AppointmentDate = this.AppointmentDate;

                label1.Text = "Patient Name - " + ClinicalComponent.PatientName;
                label2.Text = "Appointment Date - " + ClinicalComponent.AppointmentDate;
                label3.Text = "UserName - " + ClinicalComponent.UserName;

                DefaultButtonImages();
                ButtonStlyChange();
                try
                {
                    ActivateButton(button1);
                    ValidateOpenForm();
                    frmImplantableDevice frm = new frmImplantableDevice();
                    frm.MdiParent = this;
                    frm.ParentMDI = this;
                    frm.Show();
                }
                catch
                {

                }
            }
            catch
            {

            }
        }
        private void DefaultButtonImages()
        {
            button1.Image = Properties.Resources.btn_implantable_device;
            button2.Image = Properties.Resources.btn_family_history;
            button3.Image = Properties.Resources.btn_problem_list;
            button4.Image = Properties.Resources.btn_functional_status;
            button5.Image = Properties.Resources.btn_lab_order;
            button6.Image = Properties.Resources.btn_image_order;
            button7.Image = Properties.Resources.btn_procedure;
            button8.Image = Properties.Resources.btn_goal_instruction;
        }
        private void ButtonStlyChange()
        {
            DisableButtonBorder(button1);
            DisableButtonBorder(button2);
            DisableButtonBorder(button3);
            DisableButtonBorder(button4);
            DisableButtonBorder(button5);
            DisableButtonBorder(button6);
            DisableButtonBorder(button7);
            DisableButtonBorder(button8);
        }
        private void ActivateButton(Button btn)
        {
            DefaultButtonImages();
            switch (btn.Name)
            {
                case "button1":
                    button1.Image = Properties.Resources.btn_implantable_device_active;
                    break;
                case "button2":
                    button2.Image = Properties.Resources.btn_family_history_active;
                    break;
                case "button3":
                    button3.Image = Properties.Resources.btn_problem_list_active;
                    break;
                case "button4":
                    button4.Image = Properties.Resources.btn_functional_status_active;
                    break;
                case "button5":
                    button5.Image = Properties.Resources.btn_lab_order_active;
                    break;
                case "button6":
                    button6.Image = Properties.Resources.btn_image_order_active;
                    break;
                case "button7":
                    button7.Image = Properties.Resources.btn_procedure_active;
                    break;
                case "button8":
                    button8.Image = Properties.Resources.btn_goal_instruction_active;
                    break;
            }
        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Button button = (Button)sender;
                ActivateButton(button);
                ValidateOpenForm();
                frmImplantableDevice frm = new frmImplantableDevice();
                frm.MdiParent = this;
                frm.ParentMDI = this;
                frm.Show();
            }
            catch 
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmFamilydetails frm = new frmFamilydetails();
            frm.LoadData();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmProblemList frm = new frmProblemList();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmFunctionalStatusList frm = new frmFunctionalStatusList();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmLabOrders frm = new frmLabOrders();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmImageOrders frm = new frmImageOrders();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void btnGoal_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmGoalInstructionList frm = new frmGoalInstructionList();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmProceduresList frm = new frmProceduresList();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButton(button);
            ValidateOpenForm();
            frmGoalInstructionList frm = new frmGoalInstructionList();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void ValidateOpenForm()
        {
            FormCollection fc = Application.OpenForms;
            if (fc != null && fc.Count > 0)
            {
                for (int i = 1; i < fc.Count; i++)
                {
                    if (fc[i] != null && fc[i].IsDisposed != true && !fc[i].IsMdiContainer)
                    {
                        fc[i].Dispose();
                    }
                }
            }
        }
    }
}
