﻿namespace ClinicalIntegration
{
    partial class frmGoalInstruction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmStatus = new System.Windows.Forms.ComboBox();
            this.Status = new System.Windows.Forms.Label();
            this.btnFinder = new System.Windows.Forms.Button();
            this.btnSnomedFinder = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtSnomedCode = new System.Windows.Forms.TextBox();
            this.txtGoalInstruction = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.txthealth = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmStatus
            // 
            this.cmStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmStatus.FormattingEnabled = true;
            this.cmStatus.Location = new System.Drawing.Point(134, 109);
            this.cmStatus.Name = "cmStatus";
            this.cmStatus.Size = new System.Drawing.Size(236, 21);
            this.cmStatus.TabIndex = 20;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(85, 112);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(37, 13);
            this.Status.TabIndex = 19;
            this.Status.Text = "Status";
            // 
            // btnFinder
            // 
            this.btnFinder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinder.Location = new System.Drawing.Point(335, 72);
            this.btnFinder.Name = "btnFinder";
            this.btnFinder.Size = new System.Drawing.Size(35, 22);
            this.btnFinder.TabIndex = 18;
            this.btnFinder.Text = "....";
            this.btnFinder.UseVisualStyleBackColor = true;
            this.btnFinder.Click += new System.EventHandler(this.btnSnomedFinder_Click);
            // 
            // btnSnomedFinder
            // 
            this.btnSnomedFinder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSnomedFinder.Location = new System.Drawing.Point(335, 38);
            this.btnSnomedFinder.Name = "btnSnomedFinder";
            this.btnSnomedFinder.Size = new System.Drawing.Size(35, 22);
            this.btnSnomedFinder.TabIndex = 17;
            this.btnSnomedFinder.Text = "...";
            this.btnSnomedFinder.UseVisualStyleBackColor = true;
            this.btnSnomedFinder.Click += new System.EventHandler(this.btnSnomedFinder_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_quit;
            this.btnCancel.Location = new System.Drawing.Point(134, 330);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(96, 36);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Snomed CT Decription";
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(134, 73);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            this.txtDesc.Size = new System.Drawing.Size(200, 20);
            this.txtDesc.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Snomed CT";
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_done;
            this.btnSave.Location = new System.Drawing.Point(276, 330);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 36);
            this.btnSave.TabIndex = 12;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtSnomedCode
            // 
            this.txtSnomedCode.Location = new System.Drawing.Point(134, 39);
            this.txtSnomedCode.Name = "txtSnomedCode";
            this.txtSnomedCode.ReadOnly = true;
            this.txtSnomedCode.Size = new System.Drawing.Size(200, 20);
            this.txtSnomedCode.TabIndex = 11;
            // 
            // txtGoalInstruction
            // 
            this.txtGoalInstruction.Location = new System.Drawing.Point(134, 220);
            this.txtGoalInstruction.Multiline = true;
            this.txtGoalInstruction.Name = "txtGoalInstruction";
            this.txtGoalInstruction.Size = new System.Drawing.Size(236, 95);
            this.txtGoalInstruction.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Goal/Instruction";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtReason);
            this.groupBox1.Controls.Add(this.txthealth);
            this.groupBox1.Controls.Add(this.cmStatus);
            this.groupBox1.Controls.Add(this.txtSnomedCode);
            this.groupBox1.Controls.Add(this.txtGoalInstruction);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.txtDesc);
            this.groupBox1.Controls.Add(this.btnFinder);
            this.groupBox1.Controls.Add(this.btnSnomedFinder);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(37, 136);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(430, 393);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Goal/Instruction";
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(132, 183);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(236, 20);
            this.txtReason.TabIndex = 26;
            // 
            // txthealth
            // 
            this.txthealth.Location = new System.Drawing.Point(134, 147);
            this.txthealth.Name = "txthealth";
            this.txthealth.Size = new System.Drawing.Size(236, 20);
            this.txthealth.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Reason/Referral ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Health Concerns";
            // 
            // frmGoalInstruction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 577);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 79);
            this.Name = "frmGoalInstruction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmGoalInstruction";
            this.Load += new System.EventHandler(this.frmGoalInstruction_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmStatus;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.Button btnFinder;
        private System.Windows.Forms.Button btnSnomedFinder;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtSnomedCode;
        private System.Windows.Forms.TextBox txtGoalInstruction;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtReason;
        private System.Windows.Forms.TextBox txthealth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}