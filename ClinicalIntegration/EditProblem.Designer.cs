﻿namespace ClinicalIntegration
{
    partial class EditProblem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.txt_Icd10 = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dtp_Onset = new System.Windows.Forms.DateTimePicker();
            this.txt_snomed = new System.Windows.Forms.TextBox();
            this.btn_snomed = new System.Windows.Forms.Button();
            this.btnIcd = new System.Windows.Forms.Button();
            this.dtp_diagnosis = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Diagnosis Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Onset Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Snomed CT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(51, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "ICD 10";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(54, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Status";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(112, 156);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(235, 21);
            this.cmbStatus.TabIndex = 8;
            // 
            // txt_Icd10
            // 
            this.txt_Icd10.Location = new System.Drawing.Point(112, 124);
            this.txt_Icd10.Name = "txt_Icd10";
            this.txt_Icd10.ReadOnly = true;
            this.txt_Icd10.Size = new System.Drawing.Size(200, 20);
            this.txt_Icd10.TabIndex = 10;
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_done;
            this.btnSubmit.Location = new System.Drawing.Point(253, 213);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(96, 36);
            this.btnSubmit.TabIndex = 11;
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_quit;
            this.button2.Location = new System.Drawing.Point(112, 213);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 36);
            this.button2.TabIndex = 12;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dtp_Onset
            // 
            this.dtp_Onset.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_Onset.Location = new System.Drawing.Point(112, 61);
            this.dtp_Onset.Name = "dtp_Onset";
            this.dtp_Onset.RightToLeftLayout = true;
            this.dtp_Onset.Size = new System.Drawing.Size(235, 20);
            this.dtp_Onset.TabIndex = 14;
            // 
            // txt_snomed
            // 
            this.txt_snomed.Location = new System.Drawing.Point(112, 93);
            this.txt_snomed.Name = "txt_snomed";
            this.txt_snomed.ReadOnly = true;
            this.txt_snomed.Size = new System.Drawing.Size(200, 20);
            this.txt_snomed.TabIndex = 15;
            // 
            // btn_snomed
            // 
            this.btn_snomed.Location = new System.Drawing.Point(312, 92);
            this.btn_snomed.Name = "btn_snomed";
            this.btn_snomed.Size = new System.Drawing.Size(35, 22);
            this.btn_snomed.TabIndex = 16;
            this.btn_snomed.Text = "...";
            this.btn_snomed.UseVisualStyleBackColor = true;
            this.btn_snomed.Click += new System.EventHandler(this.btnIcd_Click);
            // 
            // btnIcd
            // 
            this.btnIcd.Location = new System.Drawing.Point(312, 123);
            this.btnIcd.Name = "btnIcd";
            this.btnIcd.Size = new System.Drawing.Size(35, 22);
            this.btnIcd.TabIndex = 19;
            this.btnIcd.Text = "...";
            this.btnIcd.UseVisualStyleBackColor = true;
            this.btnIcd.Click += new System.EventHandler(this.btnIcd_Click);
            // 
            // dtp_diagnosis
            // 
            this.dtp_diagnosis.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_diagnosis.Location = new System.Drawing.Point(112, 35);
            this.dtp_diagnosis.Name = "dtp_diagnosis";
            this.dtp_diagnosis.RightToLeftLayout = true;
            this.dtp_diagnosis.Size = new System.Drawing.Size(235, 20);
            this.dtp_diagnosis.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtp_diagnosis);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnIcd);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn_snomed);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txt_snomed);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtp_Onset);
            this.groupBox1.Controls.Add(this.cmbStatus);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.txt_Icd10);
            this.groupBox1.Controls.Add(this.btnSubmit);
            this.groupBox1.Location = new System.Drawing.Point(12, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 293);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Problem";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // EditProblem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1090, 478);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 79);
            this.Name = "EditProblem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Problem";
            this.Load += new System.EventHandler(this.EditProblem_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.TextBox txt_Icd10;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DateTimePicker dtp_Onset;
        private System.Windows.Forms.TextBox txt_snomed;
        private System.Windows.Forms.Button btn_snomed;
        private System.Windows.Forms.Button btnIcd;
        private System.Windows.Forms.DateTimePicker dtp_diagnosis;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}