﻿namespace ClinicalIntegration
{
    partial class frmCDSRefrences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.TextBox5 = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.txtDevName = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtRDate = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnInfo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBox6
            // 
            this.TextBox6.Location = new System.Drawing.Point(33, 279);
            this.TextBox6.Multiline = true;
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.ReadOnly = true;
            this.TextBox6.Size = new System.Drawing.Size(660, 67);
            this.TextBox6.TabIndex = 35;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(30, 260);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(278, 13);
            this.Label6.TabIndex = 34;
            this.Label6.Text = "Alt. Bibliographics Citation (Clinical Research / GuideLine)";
            // 
            // TextBox5
            // 
            this.TextBox5.Location = new System.Drawing.Point(34, 177);
            this.TextBox5.Multiline = true;
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.ReadOnly = true;
            this.TextBox5.Size = new System.Drawing.Size(660, 67);
            this.TextBox5.TabIndex = 33;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(31, 158);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(113, 13);
            this.Label5.TabIndex = 32;
            this.Label5.Text = "Reference Description";
            // 
            // txtDevName
            // 
            this.txtDevName.Location = new System.Drawing.Point(33, 121);
            this.txtDevName.Name = "txtDevName";
            this.txtDevName.ReadOnly = true;
            this.txtDevName.Size = new System.Drawing.Size(660, 20);
            this.txtDevName.TabIndex = 31;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(30, 102);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(130, 13);
            this.Label4.TabIndex = 30;
            this.Label4.Text = "Developer for Intervention";
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(536, 63);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.ReadOnly = true;
            this.TextBox3.Size = new System.Drawing.Size(158, 20);
            this.TextBox3.TabIndex = 29;
            this.TextBox3.Text = " 9.1";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(486, 66);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(46, 13);
            this.Label3.TabIndex = 28;
            this.Label3.Text = "Release";
            // 
            // txtRDate
            // 
            this.txtRDate.Location = new System.Drawing.Point(117, 64);
            this.txtRDate.Name = "txtRDate";
            this.txtRDate.ReadOnly = true;
            this.txtRDate.Size = new System.Drawing.Size(310, 20);
            this.txtRDate.TabIndex = 27;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(30, 68);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(74, 13);
            this.Label2.TabIndex = 26;
            this.Label2.Text = "Revision Date";
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(117, 25);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.ReadOnly = true;
            this.TextBox1.Size = new System.Drawing.Size(580, 20);
            this.TextBox1.TabIndex = 25;
            this.TextBox1.Text = " IOPracticeware INC";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 29);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(82, 13);
            this.Label1.TabIndex = 24;
            this.Label1.Text = "Funding Source";
            // 
            // btnInfo
            // 
            this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInfo.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInfo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnInfo.Location = new System.Drawing.Point(266, 367);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(161, 40);
            this.btnInfo.TabIndex = 39;
            this.btnInfo.Text = "View Web Information";
            this.btnInfo.UseVisualStyleBackColor = false;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // frmCDSRefrences
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 429);
            this.Controls.Add(this.btnInfo);
            this.Controls.Add(this.TextBox6);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.TextBox5);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.txtDevName);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.TextBox3);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.txtRDate);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.Label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCDSRefrences";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Reference Source Attributes";
            this.Load += new System.EventHandler(this.frmCDSRefrences_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox TextBox5;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox txtDevName;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtRDate;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnInfo;
    }
}