﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmImageOrders : Form
    {
        public Form ParentMDI { get; set; }

        bool IsEnabled { get; set; }

        public frmImageOrders()
        {
            InitializeComponent();
            BtnEdit.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmNewImageOrders frm = new frmNewImageOrders();
            ImageOrderModel objImagemodel = new ImageOrderModel();
            frm.MdiParent = this.ParentMDI;
            frm.ParentMDI = this.ParentMDI;
            frm.IsEditMode = false;
            frm.objImageModel = objImagemodel;
            frm.Show();
            this.Close();
        }

        private void frmImageOrders_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            CmbFilter.SelectedItem = "All";
            ImageOrder objImageOrder = new ImageOrder();
            dataGridView1.DataSource = objImageOrder.GetPatientImageOrder("'0','1'");
            dataGridView1.RowTemplate.Height = 32;

            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersHeight = 25;
            dataGridView1.ClearSelection();
            BtnEdit.TabStop = false;
            BtnEdit.FlatStyle = FlatStyle.Flat;
            BtnEdit.FlatAppearance.BorderSize = 0;
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            //LoadOrderDetails(dt);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                IsEnabled = true;
                BtnEdit.Enabled = true;
                ImageOrder objIOD = new ImageOrder();
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                BtnEdit.Tag = int.Parse(row.Cells["Col1"].Value.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (IsEnabled)
            {
                string OrderId = (BtnEdit).Tag.ToString();
                if (OrderId != null)
                {

                    frmNewImageOrders frm = new frmNewImageOrders();
                    ImageOrder objIO = new ImageOrder();
                    frm.objImageModel = objIO.ReteriveOrderDetail(OrderId);
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.IsEditMode = true;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while adding new order, Please try again");
                }
                //if (IsEnabled)
                //{
                //    //IsEnabled = true;
                //    //BtnEdit.Enabled = true;
                //    ImageOrder IO = new ImageOrder();
                //    frmNewImageOrders frm = new frmNewImageOrders();
                //    //frm.IsfamilyReq = true;
                //    //frm.IsNew = false;
                //    //frm.IsEdit = true;
                //    //frm.family = fd;
                //    //DataAccess da = new DataAccess();
                //    DataTable fdata = new DataTable();
                //    fdata = IO.ReteriveOrderDetail(OrderId)(Convert.ToInt16(BtnEdit.Tag));
                //    if (fdata.Rows.Count > 0)
                //    {
                //        IO.OrderId = int.Parse(fdata.Rows[0]["id"].ToString());
                //        IO.AppointmentID = int.Parse(fdata.Rows[0]["appointmentid"].ToString());
                //        fd.PatientId = int.Parse(fdata.Rows[0]["Patientid"].ToString());
                //        fd.COMMENTS = fdata.Rows[0]["comments"].ToString();
                //        fd.ICD10 = fdata.Rows[0]["ICD10"].ToString();
                //        fd.ICD10DESCR = fdata.Rows[0]["ICD10DESCR"].ToString();
                //        fd.ConceptID = fdata.Rows[0]["ConceptID"].ToString();
                //        fd.Term = fdata.Rows[0]["term"].ToString();
                //        fd.Status = (fdata.Rows[0]["status"].ToString()) == "True" ? "Active" : "Inactive";
                //        fd.Relationship = int.Parse(fdata.Rows[0]["relationship"].ToString());
                //        fd.EnteredDate = DateTime.Parse(fdata.Rows[0]["EnteredDate"].ToString());
                //        fd.LastModifiedDate = DateTime.Parse(fdata.Rows[0]["LastModofiedDate"].ToString());
                //        fd.LastModifiedBy = fdata.Rows[0]["LastModofiedBy"].ToString();
                //        fd.DiagnosisDate = DateTime.Parse(fdata.Rows[0]["EnteredDate"].ToString());
                //        fd.OnsetDate = DateTime.Parse(fdata.Rows[0]["OnsetDate"].ToString());
                //    }
                //    frm.IO = IO;
                //    frm.MdiParent = this.ParentMDI;
                //    frm.ParentMDI = this.ParentMDI;
                //    frm.Show();
                //    this.Close();
                //}
                //else
                //{
                //    BtnEdit.Enabled = false;
                //}

            }
        }
        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void CmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";

            if (CmbFilter.SelectedItem.ToString() == "Pending")
                _Status = "'1'";
            else if (CmbFilter.SelectedItem.ToString() == "Completed")
                _Status = "'0'";
            else
                _Status = "'1','0'";

            ImageOrder objImageOrder = new ImageOrder();
            dataGridView1.DataSource = objImageOrder.GetPatientImageOrder(_Status);
        }
    }
    //private void LoadOrderDetails(DataTable objDataTable)
    //{
    //    int i = 1;
    //    bool alterColor = true;
    //    int CoorindateY = 29;
    //    pnlOrderist.Controls.Clear();
    //    foreach (DataRow dr in objDataTable.Rows)
    //    {
    //        Panel PanelOrder = new Panel();
    //        PanelOrder.Width = 948;
    //        PanelOrder.Height = 28;
    //        PanelOrder.Location = new Point(0, CoorindateY);
    //        PanelOrder.Size = new Size(951, 28);
    //        if (alterColor)
    //        {
    //            PanelOrder.BackColor = Color.White;
    //            alterColor = false;
    //        }
    //        else
    //        {
    //            PanelOrder.BackColor = Color.WhiteSmoke;
    //            alterColor = true;
    //        }


    //        Label lblEnteredDate = new Label();
    //        lblEnteredDate.Text = Convert.ToDateTime(dr["Col2"]).Date.ToString("MM-dd-yyyy");
    //        lblEnteredDate.Location = new Point(13, 8);
    //        lblEnteredDate.Size = new Size(67, 13);
    //        PanelOrder.Controls.Add(lblEnteredDate);

    //        Label lblEnteredBy = new Label();
    //        lblEnteredBy.Text = dr["Col3"].ToString();
    //        lblEnteredBy.Location = new Point(109, 8);
    //        lblEnteredBy.Size = new Size(104, 13);
    //        PanelOrder.Controls.Add(lblEnteredBy);

    //        Label lblOrderType = new Label();
    //        lblOrderType.Text = dr["Col4"].ToString();
    //        lblOrderType.Location = new Point(213, 8);
    //        lblOrderType.Size = new Size(104, 13);
    //        PanelOrder.Controls.Add(lblOrderType);

    //        Label lblOrderForm = new Label();
    //        lblOrderForm.Text = dr["Col10"].ToString();
    //        lblOrderForm.Tag = dr["Col5"].ToString();
    //        lblOrderForm.Location = new Point(317, 8);
    //        lblOrderForm.Size = new Size(107, 13);
    //        PanelOrder.Controls.Add(lblOrderForm);

    //        Label lblProcedure = new Label();
    //        lblProcedure.Text = dr["Col6"].ToString();
    //        lblProcedure.Location = new Point(424, 8);
    //        ToolTip toolTipDescription = new ToolTip();
    //        toolTipDescription.SetToolTip(lblProcedure, dr["Col6"].ToString());
    //        lblProcedure.Size = new Size(133, 13);
    //        PanelOrder.Controls.Add(lblProcedure);

    //        Label lblStatus = new Label();
    //        lblStatus.Text = dr["Col7"].ToString();
    //        lblStatus.Location = new Point(557, 8);
    //        lblStatus.Size = new Size(57, 13);
    //        PanelOrder.Controls.Add(lblStatus);

    //        Label lblModifiedDate = new Label();
    //        lblModifiedDate.Text = Convert.ToDateTime(dr["Col8"]).Date.ToString("MM-dd-yyyy");
    //        lblModifiedDate.Location = new Point(658, 8);
    //        lblModifiedDate.Size = new Size(73, 13);
    //        PanelOrder.Controls.Add(lblModifiedDate);

    //        Label lblModifiedBy = new Label();
    //        lblModifiedBy.Text = dr["Col9"].ToString();
    //        lblModifiedBy.Location = new Point(779, 8);
    //        lblModifiedBy.Size = new Size(62, 13);
    //        PanelOrder.Controls.Add(lblModifiedBy);

    //        Button btnEditOrder = new Button();
    //        btnEditOrder.Text = "Edit";
    //        btnEditOrder.Location = new Point(880, 8);
    //        btnEditOrder.Size = new Size(65, 20);
    //        btnEditOrder.Tag = dr["Col1"].ToString();
    //        btnEditOrder.Click += new EventHandler(btnEditOrder_Click);
    //        PanelOrder.Controls.Add(btnEditOrder);
    //        pnlOrderist.Controls.Add(PanelOrder);
    //        CoorindateY += 29;

}

//private void btnEditOrder_Click(object sender, EventArgs e)
//{
//    string OrderId = ((Button)sender).Tag.ToString();
//if (OrderId != "")
//{
//    this.Close();
//    frmNewImageOrders frm = new frmNewImageOrders();
//    ImageOrder objIO = new ImageOrder();
//    frm.objImageModel = objIO.ReteriveOrderDetail(OrderId);
//    frm.MdiParent = this.ParentMDI;
//    frm.ParentMDI = this.ParentMDI;
//    frm.IsEditMode = true;
//    frm.Show();
//}
//else
//{
//    MessageBox.Show("There is some issue while adding new order, Please try again");
//}
//}

//private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
//{
//    string _Status = "";

//    if (CmbFilter.SelectedItem.ToString() == "Pending")
//        _Status = "'1'";
//    else if (CmbFilter.SelectedItem.ToString() == "Completed")
//        _Status = "'0'";
//    else
//        _Status = "'1','0'";

//    ImageOrder objImageOrder = new ImageOrder();
//    dataGridView1 = objImageOrder.GetPatientImageOrder(_Status);
//    LoadOrderDetails(dt);
//}
//    }
//}

