﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ClinicalIntegration
{
    public class DataAccess
    {
        public DataTable GetFamilyDetails(string _status)
        {
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;

            query = "select fh.Id,CONVERT(VARCHAR(10), EnteredDate, 101) as 'Entered Date',fr.Name+'/'+CONVERT(varchar(50),conceptid)+'/'+ICD10DESCR as History,";
            query += " case when Fh.status = 0 then 'InActive' else 'Active' end as 'Status',";
            query += " LastModofiedDate as 'Modified Date',fh.comments,RS.ResourceName as 'Modified By' from model.FamilyHistory fh with (nolock)";
            query += " inner join [model].[FamilyRelationships] fr with (nolock) on fr.Id = fh.relationship ";
            query += " INNER JOIN  DBO.Resources RS with (nolock) ON RS.resourceid = FH.lastModofiedBy where fh.AppointmentID<=@AppointmentID and  fh.patientid = @PatientId and fh.status in (" + _status + ")"; query += "Order By EnteredDate Desc";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter parPatient = cmd.CreateParameter();
                parPatient.Direction = ParameterDirection.Input;
                parPatient.Value = ClinicalComponent.PatientId;
                parPatient.ParameterName = "@PatientId";
                parPatient.DbType = DbType.Int64;
                cmd.Parameters.Add(parPatient);

                IDbDataParameter parAppointmentID = cmd.CreateParameter();
                parAppointmentID.Direction = ParameterDirection.Input;
                parAppointmentID.Value = ClinicalComponent.AppointmentId;
                parAppointmentID.ParameterName = "@AppointmentID";
                parAppointmentID.DbType = DbType.Int64;
                cmd.Parameters.Add(parAppointmentID);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }


            return dt;
        }

        public DataTable GetFamilyDetailsById(long id)
        {
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;

            query = "select * from model.FamilyHistory with (nolock) where Id = @Id";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter parPatient = cmd.CreateParameter();
                parPatient.Direction = ParameterDirection.Input;
                parPatient.Value = id;
                parPatient.ParameterName = "@Id";
                parPatient.DbType = DbType.Int64;
                cmd.Parameters.Add(parPatient);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }


            return dt;
        }

        public DataTable GetFamilyRelations()
        {
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;

            query = "select 0 as Id,'Select' as Name union select Id,Name from [model].[FamilyRelationships] with (nolock)";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                //IDbDataParameter parPatient = cmd.CreateParameter();
                //parPatient.Direction = ParameterDirection.Input;
                //parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                //parPatient.ParameterName = "@PatientId";
                //parPatient.DbType = DbType.Int64;
                //cmd.Parameters.Add(parPatient);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }


            return dt;
        }

        public void SaveFamilyDetails(bool isNew, FamilyDetails fd)
        {
            string query = string.Empty;
            if (isNew)
            {
                
                query = "INSERT INTO [model].[FamilyHistory] ([PatientId],[AppointmentID],[EnteredDate],";
                query += "[ConceptID],[Term],[ICD10],[ICD10DESCR],[Relationship],[Status],[COMMENTS],[LastModofiedBy],[LastModofiedDate],[OnsetDate])";
                query += "VALUES(@PatientId,@AppointmentID,@EnteredDate,@ConceptID,@Term,@ICD10,@ICD10DESCR,@Relationship,";
                query += "@Status,@COMMENTS,@LastModofiedBy,GetDate(),@OnsetDate)";

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = query;
                    
                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = fd.PatientId;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);

                    IDbDataParameter parAppointmentID = cmd.CreateParameter();
                    parAppointmentID.Direction = ParameterDirection.Input;
                    parAppointmentID.Value = fd.AppointmentID;
                    parAppointmentID.ParameterName = "@AppointmentID";
                    parAppointmentID.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentID);

                    IDbDataParameter parEnteredDate = cmd.CreateParameter();
                    parEnteredDate.Direction = ParameterDirection.Input;
                    parEnteredDate.Value = fd.EnteredDate;
                    parEnteredDate.ParameterName = "@EnteredDate";
                    parEnteredDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parEnteredDate);

                    IDbDataParameter parConceptID = cmd.CreateParameter();
                    parConceptID.Direction = ParameterDirection.Input;
                    parConceptID.Value = (fd.ConceptID);
                    parConceptID.ParameterName = "@ConceptID";
                    parConceptID.DbType = DbType.String;
                    cmd.Parameters.Add(parConceptID);

                    IDbDataParameter parTerm = cmd.CreateParameter();
                    parTerm.Direction = ParameterDirection.Input;
                    parTerm.Value = fd.Term;
                    parTerm.ParameterName = "@Term";
                    parTerm.DbType = DbType.String;
                    cmd.Parameters.Add(parTerm);

                    IDbDataParameter parICD10 = cmd.CreateParameter();
                    parICD10.Direction = ParameterDirection.Input;
                    parICD10.Value = fd.ICD10;
                    parICD10.ParameterName = "@ICD10";
                    parICD10.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10);

                    IDbDataParameter parICD10DESCR = cmd.CreateParameter();
                    parICD10DESCR.Direction = ParameterDirection.Input;
                    parICD10DESCR.Value = fd.ICD10DESCR;
                    parICD10DESCR.ParameterName = "@ICD10DESCR";
                    parICD10DESCR.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10DESCR);

                    IDbDataParameter parRelationship = cmd.CreateParameter();
                    parRelationship.Direction = ParameterDirection.Input;
                    parRelationship.Value = fd.Relationship;
                    parRelationship.ParameterName = "@Relationship";
                    parRelationship.DbType = DbType.Int64;
                    cmd.Parameters.Add(parRelationship);

                    IDbDataParameter parStatus = cmd.CreateParameter();
                    parStatus.Direction = ParameterDirection.Input;
                    parStatus.Value = fd.Status;
                    parStatus.ParameterName = "@Status";
                    parStatus.DbType = DbType.Int32;
                    cmd.Parameters.Add(parStatus);

                    IDbDataParameter parCOMMENTS = cmd.CreateParameter();
                    parCOMMENTS.Direction = ParameterDirection.Input;
                    parCOMMENTS.Value = fd.COMMENTS;
                    parCOMMENTS.ParameterName = "@COMMENTS";
                    parCOMMENTS.DbType = DbType.String;
                    cmd.Parameters.Add(parCOMMENTS);

                    IDbDataParameter parLastModifiedBy = cmd.CreateParameter();
                    parLastModifiedBy.Direction = ParameterDirection.Input;
                    parLastModifiedBy.Value = !string.IsNullOrEmpty(ClinicalComponent.UserId) ? Int32.Parse(ClinicalComponent.UserId) : 0;
                    parLastModifiedBy.ParameterName = "@LastModofiedBy";
                    parLastModifiedBy.DbType = DbType.Int32;
                    cmd.Parameters.Add(parLastModifiedBy);
                    
                    //IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                    //parLastModifiedDate.Direction = ParameterDirection.Input;
                    //parLastModifiedDate.Value = fd.LastModifiedDate;
                    //parLastModifiedDate.ParameterName = "@LastModofiedDate";
                    //parLastModifiedDate.DbType = DbType.DateTime;
                    //cmd.Parameters.Add(parLastModifiedDate);

                    IDbDataParameter parOnsetDate = cmd.CreateParameter();
                    parOnsetDate.Direction = ParameterDirection.Input;
                    parOnsetDate.Value = fd.OnsetDate;
                    parOnsetDate.ParameterName = "@OnsetDate";
                    parOnsetDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parOnsetDate);


                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    
                }
            }
            else
            {
                query = "UPDATE [model].[FamilyHistory] SET [EnteredDate] =@EnteredDate ";
                query += ",[ConceptID] = @ConceptID ,[Term] = @Term, [ICD10] = @ICD10,[ICD10DESCR] = @ICD10DESCR,[Relationship] = @Relationship,[Status] =@Status ";
                query += ",[COMMENTS] = @COMMENTS,[LastModofiedBy] = @LastModofiedBy ,[LastModofiedDate] = GetDate(), [OnsetDate] = @OnsetDate WHERE Id=@Id";

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                try
                {
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.Parameters.Clear();


                        IDbDataParameter parId = cmd.CreateParameter();
                        parId.Direction = ParameterDirection.Input;
                        parId.Value = fd.Id;
                        parId.ParameterName = "@Id";
                        parId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parId);

                        IDbDataParameter parEnteredDate = cmd.CreateParameter();
                        parEnteredDate.Direction = ParameterDirection.Input;
                        parEnteredDate.Value = fd.EnteredDate;
                        parEnteredDate.ParameterName = "@EnteredDate";
                        parEnteredDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parEnteredDate);

                        IDbDataParameter parConceptID = cmd.CreateParameter();
                        parConceptID.Direction = ParameterDirection.Input;
                        parConceptID.Value = fd.ConceptID;
                        parConceptID.ParameterName = "@ConceptID";
                        parConceptID.DbType = DbType.String;
                        cmd.Parameters.Add(parConceptID);

                        IDbDataParameter parTerm = cmd.CreateParameter();
                        parTerm.Direction = ParameterDirection.Input;
                        parTerm.Value = fd.Term;
                        parTerm.ParameterName = "@Term";
                        parTerm.DbType = DbType.String;
                        cmd.Parameters.Add(parTerm);

                        IDbDataParameter parICD10 = cmd.CreateParameter();
                        parICD10.Direction = ParameterDirection.Input;
                        parICD10.Value = fd.ICD10;
                        parICD10.ParameterName = "@ICD10";
                        parICD10.DbType = DbType.String;
                        cmd.Parameters.Add(parICD10);

                        IDbDataParameter parICD10DESCR = cmd.CreateParameter();
                        parICD10DESCR.Direction = ParameterDirection.Input;
                        parICD10DESCR.Value = fd.ICD10DESCR;
                        parICD10DESCR.ParameterName = "@ICD10DESCR";
                        parICD10DESCR.DbType = DbType.String;
                        cmd.Parameters.Add(parICD10DESCR);

                        IDbDataParameter parRelationship = cmd.CreateParameter();
                        parRelationship.Direction = ParameterDirection.Input;
                        parRelationship.Value = fd.Relationship;
                        parRelationship.ParameterName = "@Relationship";
                        parRelationship.DbType = DbType.Int64;
                        cmd.Parameters.Add(parRelationship);

                        IDbDataParameter parStatus = cmd.CreateParameter();
                        parStatus.Direction = ParameterDirection.Input;
                        parStatus.Value = fd.Status;
                        parStatus.ParameterName = "@Status";
                        parStatus.DbType = DbType.Int32;
                        cmd.Parameters.Add(parStatus);

                        IDbDataParameter parCOMMENTS = cmd.CreateParameter();
                        parCOMMENTS.Direction = ParameterDirection.Input;
                        parCOMMENTS.Value = fd.COMMENTS;
                        parCOMMENTS.ParameterName = "@COMMENTS";
                        parCOMMENTS.DbType = DbType.String;
                        cmd.Parameters.Add(parCOMMENTS);

                        IDbDataParameter parLastModifiedBy = cmd.CreateParameter();
                        parLastModifiedBy.Direction = ParameterDirection.Input;
                        parLastModifiedBy.Value = ClinicalComponent.UserId;
                        parLastModifiedBy.ParameterName = "@LastModofiedBy";
                        parLastModifiedBy.DbType = DbType.Int32;
                        cmd.Parameters.Add(parLastModifiedBy);

                        IDbDataParameter parOnsetDate = cmd.CreateParameter();
                        parOnsetDate.Direction = ParameterDirection.Input;
                        parOnsetDate.Value = fd.OnsetDate;
                        parOnsetDate.ParameterName = "@OnsetDate";
                        parOnsetDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parOnsetDate);

                        //IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                        //parLastModifiedDate.Direction = ParameterDirection.Input;
                        //parLastModifiedDate.Value = fd.LastModifiedDate;
                        //parLastModifiedDate.ParameterName = "@LastModofiedDate";
                        //parLastModifiedDate.DbType = DbType.DateTime;
                        //cmd.Parameters.Add(parLastModifiedDate);

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();

                    }
                }
                catch 
                {

                }
            }
        }


    }
}

