﻿using System;

namespace ClinicalIntegration
{
    public class FamilyDetails
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int AppointmentID { get; set; }
        public string EnteredBy { get; set; }
        public DateTime EnteredDate { get; set; }
        public string ConceptID { get; set; }
        public string Term { get; set; }
        public string ICD10 { get; set; }
        public string ICD10DESCR { get; set; }
        public int Relationship { get; set; }
        public string Status { get; set; }
        public string COMMENTS { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public DateTime OnsetDate { get; set; }
        public DateTime DiagnosisDate { get; set; }
    }
}
