﻿using System.Collections.Generic;

namespace ClinicalIntegration
{
    public class Identifier
    {
        public string deviceId { get; set; }
        public string deviceIdType { get; set; }
        public string deviceIdIssuingAgency { get; set; }
        public string containsDINumber { get; set; }
        public string pkgQuantity { get; set; }
        public string pkgDiscontinueDate { get; set; }
        public string pkgStatus { get; set; }
        public string pkgType { get; set; }
    }

    public class Identifiers
    {
        public List<Identifier> identifiers { get; set; }
    }

    public class CustomerContact
    {
        public string phone { get; set; }
        public object phoneExtension { get; set; }
        public string email { get; set; }
    }

    public class Contacts
    {
        public CustomerContact customerContact { get; set; }
    }

    public class Gmdn
    {
        public string gmdnPTName { get; set; }

        public string gmdnPTDefinition { get; set; }
    }

    public class GmdnTerms
    {
        public Gmdn gmdn { get; set; }
    }

    public class FdaProductCode
    {
        public string productCode { get; set; }

        public string productCodeName { get; set; }
    }

    public class ProductCodes
    {
        public object fdaProductCode { get; set; }
    }

    public class StorageHandlingHigh
    {

        public string unit { get; set; }

        public string value { get; set; }
    }

    public class StorageHandlingLow
    {

        public string unit { get; set; }

        public string value { get; set; }
    }

    public class StorageHandling
    {

        public string storageHandlingType { get; set; }

        public StorageHandlingHigh storageHandlingHigh { get; set; }

        public StorageHandlingLow storageHandlingLow { get; set; }

        public string storageHandlingSpecialConditionText { get; set; }
    }

    public class EnvironmentalConditions
    {
        public IList<StorageHandling> storageHandling { get; set; }
    }

    public class Sterilization
    {
        public string deviceSterile { get; set; }

        public string sterilizationPriorToUse { get; set; }

        public object methodTypes { get; set; }
    }

    public class Device
    {

        public string deviceRecordStatus { get; set; }

        public string devicePublishDate { get; set; }

        public object deviceCommDistributionEndDate { get; set; }

        public string deviceCommDistributionStatus { get; set; }

        public Identifiers identifiers { get; set; }

        public string brandName { get; set; }

        public string versionModelNumber { get; set; }

        public string catalogNumber { get; set; }

        public string companyName { get; set; }

        public string deviceCount { get; set; }

        public string deviceDescription { get; set; }

        public string DMExempt { get; set; }

        public string premarketExempt { get; set; }

        public string deviceHCTP { get; set; }

        public string deviceKit { get; set; }

        public string deviceCombinationProduct { get; set; }

        public string singleUse { get; set; }

        public string lotBatch { get; set; }

        public string serialNumber { get; set; }

        public string manufacturingDate { get; set; }

        public string expirationDate { get; set; }

        public string donationIdNumber { get; set; }

        public string labeledContainsNRL { get; set; }

        public string labeledNoNRL { get; set; }

        public string MRISafetyStatus { get; set; }
        public string rx { get; set; }
        public string otc { get; set; }
        public Contacts contacts { get; set; }
        public GmdnTerms gmdnTerms { get; set; }
        public ProductCodes productCodes { get; set; }
        public DeviceSize deviceSizes { get; set; }
        public EnvironmentalConditions environmentalConditions { get; set; }
        public Sterilization sterilization { get; set; }
    }

    public class DeviceSize
    {
        public string sizeType { get; set; }
        public string sizeText { get; set; }
        public size size { get; set; }
    }

    public class size
    {
        public string unit { get; set; }
        public string value { get; set; }

    }

    public class Gudid
    {
        public Device device { get; set; }
    }

    public class DeviceDI
    {
        public Gudid gudid { get; set; }
    }

    public class RootObject
    {
        public string error { get; set; }
        public NdcGroup ndcGroup { get; set; }
    }
    public class NdcList
    {
        public List<string> ndc { get; set; }
    }

    public class NdcGroup
    {
        public string rxcui { get; set; }
        public NdcList ndcList { get; set; }
    }

}
