﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    public class PatientImplantables
    {
        public DeviceIdentifierModel objImplantableDevice { get; set; }
        public DataTable GetPatientImplantableDeviceDetail(string _Status)
        {
            DataTable ImplantationDevice = new DataTable();
            try
            {
                string query = "Select CONVERT(date, b.RecordDate) as Col0, b.Id as Col1, a.UniqueDeviceIdentifier as Col2, ImplantationDate as Col3, Case(b.Status) when 1 then 'Active' else 'Inactive' End  as Col4, GmdnPTDefinition as Col6,  GMDNPTName  as  Col7 , b.Laternity as Col8 From dbo.ImplantableDevices a with(nolock) inner join dbo.PatientImplantableDevices b with(nolock) on a.Id = b.ImplantableDeviceId where b.PatientId = @PatientId and Status in (" + _Status + ") Order By ImplantationDate Desc";
                if (ClinicalComponent._Connection.State == ConnectionState.Open)
                {
                    ClinicalComponent._Connection.Close();
                }
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter PatientId = cmd.CreateParameter();
                    PatientId.Direction = ParameterDirection.Input;
                    PatientId.Value = ClinicalComponent.PatientId;
                    PatientId.ParameterName = "@PatientId";
                    PatientId.DbType = DbType.Int32;
                    cmd.Parameters.Add(PatientId);

                    //IDbDataParameter AppointmentId = cmd.CreateParameter();
                    //AppointmentId.Direction = ParameterDirection.Input;
                    //AppointmentId.Value = ClinicalComponent.AppointmentId;
                    //AppointmentId.ParameterName = "@AppointmentId";
                    //AppointmentId.DbType = DbType.Int32;
                    //cmd.Parameters.Add(AppointmentId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        ImplantationDevice.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {

            }
            //ClinicalComponent._Connection.Close();
            return ImplantationDevice;
        }

        
        public PatientIdentifierDeviceModel GetPatientImplantableDeviceById(string DeviceId)
        {
            DataTable ImplantationDevice = new DataTable();
            string query = "Select a.Id as ImplantableDeviceId, Case(a.Status) When 1 Then 'Active' Else 'Inactive' End as Status, a.ImplantationDate,a.Laternity,b.* From dbo.PatientImplantableDevices a with(nolock) inner join dbo.ImplantableDevices b with(nolock) on a.ImplantableDeviceId = b.Id Where a.Id = @DeviceId";
            ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter PatientId = cmd.CreateParameter();
                PatientId.Direction = ParameterDirection.Input;
                PatientId.Value = DeviceId;
                PatientId.ParameterName = "@DeviceId";
                PatientId.DbType = DbType.Int32;
                cmd.Parameters.Add(PatientId);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    ImplantationDevice.Load(reader);
                }
            }
            ClinicalComponent._Connection.Close();
            PatientIdentifierDeviceModel objModel = new PatientIdentifierDeviceModel();
            if (ImplantationDevice.Rows.Count > 0)
            {
                objModel.BrandName = ImplantationDevice.Rows[0]["BrandName"].ToString();
                objModel.CompanyName = ImplantationDevice.Rows[0]["CompanyName"].ToString();
                objModel.DeviceDescription = ImplantationDevice.Rows[0]["DeviceDescription"].ToString();
                objModel.DeviceId = ImplantationDevice.Rows[0]["BrandName"].ToString();
                objModel.GMDNPTName = ImplantationDevice.Rows[0]["GMDNPTName"].ToString();
                objModel.GmdnPTDefinition = ImplantationDevice.Rows[0]["GmdnPTDefinition"].ToString();
                objModel.expirationDate = ImplantationDevice.Rows[0]["ExpirtaionDate"].ToString();
                objModel.manufacturing_date = ImplantationDevice.Rows[0]["ManufacturingDate"].ToString();
                objModel.lot_number = ImplantationDevice.Rows[0]["LotNumber"].ToString();
                objModel.IssuingAgency = ImplantationDevice.Rows[0]["IssuingAgency"].ToString();
                objModel.HCTPStatus = ImplantationDevice.Rows[0]["HCTPStatus"].ToString();
                objModel.HCTPCode = ImplantationDevice.Rows[0]["HCTPCode"].ToString();
                objModel.MRISafetyInformation = ImplantationDevice.Rows[0]["MRISafetyInformation"].ToString();
                objModel.RequiresRubberLabelingTypeId = ImplantationDevice.Rows[0]["RequiresRubberLabelingTypeId"].ToString();
                objModel.UniqueIdentificationNumber = ImplantationDevice.Rows[0]["UniqueDeviceIdentifier"].ToString();
                objModel.ImplantableDeviceId = ImplantationDevice.Rows[0]["ImplantableDeviceId"].ToString();
                objModel.serialNumber = ImplantationDevice.Rows[0]["SerialNumber"].ToString();
                objModel.VersionOrModel = ImplantationDevice.Rows[0]["VersionOrModel"].ToString();
                objModel.DateofImplantation = ImplantationDevice.Rows[0]["ImplantationDate"].ToString();
                objModel.Lateraity = ImplantationDevice.Rows[0]["Laternity"].ToString();
                objModel.DeviceStatus = ImplantationDevice.Rows[0]["Status"].ToString();
            }

            return objModel;
        }

        public bool SaveImplantableDeviceinDB(string _ImplantationDate, string patientLateraity, string deviceStatus)
        {
            string _error = "";
            bool retStatus = true;
            try
            {
                object ImplatableDeviceId = 0;
                string _insertQry = "Insert into dbo.ImplantableDevices(UniqueDeviceIdentifier, IssuingAgency, DeviceId, DeviceDescription, ManufacturingDate, ExpirtaionDate, LotNumber, SerialNumber, BrandName, CompanyName, GMDNPTName, GmdnPTDefinition, MRISafetyInformation, VersionOrModel, RequiresRubberLabelingTypeId, HCTPStatus, HCTPCode) Values  (@UniqueDeviceIdentifier, @IssuingAgency, @DeviceId, @DeviceDescription, @ManufacturingDate, @ExpirtaionDate, @LotNumber, @SerialNumber, @BrandName, @CompanyName, @GMDNPTName, @GmdnPTDefinition, @MRISafetyInformation, @VersionOrModel, @RequiresRubberLabelingTypeId, @HCTPStatus, @HCTPCode) Select SCOPE_IDENTITY() as ImplatableDeviceId";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = _insertQry;
                    IDbDataParameter UniqueDeviceIdentifier = cmd.CreateParameter();
                    UniqueDeviceIdentifier.Direction = ParameterDirection.Input;
                    UniqueDeviceIdentifier.Value = objImplantableDevice.UniqueIdentificationNumber;
                    UniqueDeviceIdentifier.ParameterName = "@UniqueDeviceIdentifier";
                    UniqueDeviceIdentifier.DbType = DbType.String;
                    cmd.Parameters.Add(UniqueDeviceIdentifier);

                    IDbDataParameter IssuingAgency = cmd.CreateParameter();
                    IssuingAgency.Direction = ParameterDirection.Input;
                    IssuingAgency.Value = objImplantableDevice.IssuingAgency;
                    IssuingAgency.ParameterName = "@IssuingAgency";
                    IssuingAgency.DbType = DbType.String;
                    cmd.Parameters.Add(IssuingAgency);

                    IDbDataParameter DeviceId = cmd.CreateParameter();
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = objImplantableDevice.DeviceId;
                    DeviceId.ParameterName = "@DeviceId";
                    DeviceId.DbType = DbType.String;
                    cmd.Parameters.Add(DeviceId);

                    IDbDataParameter DeviceDescription = cmd.CreateParameter();
                    DeviceDescription.Direction = ParameterDirection.Input;
                    DeviceDescription.Value = objImplantableDevice.DeviceDescription;
                    DeviceDescription.ParameterName = "@DeviceDescription";
                    DeviceDescription.DbType = DbType.String;
                    cmd.Parameters.Add(DeviceDescription);

                    IDbDataParameter ManufacturingDate = cmd.CreateParameter();
                    ManufacturingDate.Direction = ParameterDirection.Input;
                    ManufacturingDate.Value = objImplantableDevice.manufacturing_date ?? string.Empty;
                    ManufacturingDate.ParameterName = "@ManufacturingDate";
                    ManufacturingDate.DbType = objImplantableDevice.manufacturing_date == null ? DbType.String : DbType.DateTime;
                    cmd.Parameters.Add(ManufacturingDate);

                    IDbDataParameter ExpirtaionDate = cmd.CreateParameter();
                    ExpirtaionDate.Direction = ParameterDirection.Input;
                    ExpirtaionDate.Value = objImplantableDevice.expirationDate;
                    ExpirtaionDate.ParameterName = "@ExpirtaionDate";
                    ExpirtaionDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(ExpirtaionDate);

                    IDbDataParameter LotNumber = cmd.CreateParameter();
                    LotNumber.Direction = ParameterDirection.Input;
                    LotNumber.Value = objImplantableDevice.lot_number ?? string.Empty;
                    LotNumber.ParameterName = "@LotNumber";
                    LotNumber.DbType = DbType.String;
                    cmd.Parameters.Add(LotNumber);

                    IDbDataParameter SerialNumber = cmd.CreateParameter();
                    SerialNumber.Direction = ParameterDirection.Input;
                    SerialNumber.Value = objImplantableDevice.serialNumber;
                    SerialNumber.ParameterName = "@SerialNumber";
                    SerialNumber.DbType = DbType.String;
                    cmd.Parameters.Add(SerialNumber);

                    IDbDataParameter BrandName = cmd.CreateParameter();
                    BrandName.Direction = ParameterDirection.Input;
                    BrandName.Value = objImplantableDevice.BrandName;
                    BrandName.ParameterName = "@BrandName";
                    BrandName.DbType = DbType.String;
                    cmd.Parameters.Add(BrandName);

                    IDbDataParameter CompanyName = cmd.CreateParameter();
                    CompanyName.Direction = ParameterDirection.Input;
                    CompanyName.Value = objImplantableDevice.CompanyName;
                    CompanyName.ParameterName = "@CompanyName";
                    CompanyName.DbType = DbType.String;
                    cmd.Parameters.Add(CompanyName);

                    IDbDataParameter GMDNPTName = cmd.CreateParameter();
                    GMDNPTName.Direction = ParameterDirection.Input;
                    GMDNPTName.Value = objImplantableDevice.GMDNPTName;
                    GMDNPTName.ParameterName = "@GMDNPTName";
                    GMDNPTName.DbType = DbType.String;
                    cmd.Parameters.Add(GMDNPTName);

                    IDbDataParameter GmdnPTDefinition = cmd.CreateParameter();
                    GmdnPTDefinition.Direction = ParameterDirection.Input;
                    GmdnPTDefinition.Value = objImplantableDevice.GmdnPTDefinition;
                    GmdnPTDefinition.ParameterName = "@GmdnPTDefinition";
                    GmdnPTDefinition.DbType = DbType.String;
                    cmd.Parameters.Add(GmdnPTDefinition);

                    IDbDataParameter MRISafetyInformation = cmd.CreateParameter();
                    MRISafetyInformation.Direction = ParameterDirection.Input;
                    MRISafetyInformation.Value = objImplantableDevice.MRISafetyInformation;
                    MRISafetyInformation.ParameterName = "@MRISafetyInformation";
                    MRISafetyInformation.DbType = DbType.String;
                    cmd.Parameters.Add(MRISafetyInformation);

                    IDbDataParameter VersionOrModel = cmd.CreateParameter();
                    VersionOrModel.Direction = ParameterDirection.Input;
                    VersionOrModel.Value = objImplantableDevice.VersionOrModel;
                    VersionOrModel.ParameterName = "@VersionOrModel";
                    VersionOrModel.DbType = DbType.String;
                    cmd.Parameters.Add(VersionOrModel);

                    IDbDataParameter RequiresRubberLabelingTypeId = cmd.CreateParameter();
                    RequiresRubberLabelingTypeId.Direction = ParameterDirection.Input;
                    RequiresRubberLabelingTypeId.Value = objImplantableDevice.RequiresRubberLabelingTypeId != null ? objImplantableDevice.RequiresRubberLabelingTypeId : "";
                    RequiresRubberLabelingTypeId.ParameterName = "@RequiresRubberLabelingTypeId";
                    RequiresRubberLabelingTypeId.DbType = DbType.String;
                    cmd.Parameters.Add(RequiresRubberLabelingTypeId);

                    IDbDataParameter HTCPStatus = cmd.CreateParameter();
                    HTCPStatus.Direction = ParameterDirection.Input;
                    HTCPStatus.Value = objImplantableDevice.HCTPStatus == "true" ? true : false;
                    HTCPStatus.ParameterName = "@HCTPStatus";
                    HTCPStatus.DbType = DbType.Boolean;
                    cmd.Parameters.Add(HTCPStatus);

                    IDbDataParameter HTCPCode = cmd.CreateParameter();
                    HTCPCode.Direction = ParameterDirection.Input;
                    HTCPCode.Value = objImplantableDevice.HCTPCode;
                    HTCPCode.ParameterName = "@HCTPCode";
                    HTCPCode.DbType = DbType.String;
                    cmd.Parameters.Add(HTCPCode);

                    cmd.CommandType = CommandType.Text;
                    ImplatableDeviceId = cmd.ExecuteScalar();

                    if (ImplatableDeviceId.ToString() != "0")
                    {
                        _insertQry = "Insert into dbo.PatientImplantableDevices(PatientId, AppointmentId, ImplantableDeviceId, Status, RecordDate, ImplantationDate, Laternity) Values (@PatientId, @AppointmentId, @ImplantableDeviceId, @Status, GETDATE(), @ImplantationDate, @Laternity)";
                        cmd.Parameters.Clear();
                        cmd.CommandText = _insertQry;
                        IDbDataParameter PatientId = cmd.CreateParameter();
                        PatientId.Direction = ParameterDirection.Input;
                        PatientId.Value = ClinicalComponent.PatientId;
                        PatientId.ParameterName = "@PatientId";
                        PatientId.DbType = DbType.Int32;
                        cmd.Parameters.Add(PatientId);

                        IDbDataParameter AppointmentId = cmd.CreateParameter();
                        AppointmentId.Direction = ParameterDirection.Input;
                        AppointmentId.Value = ClinicalComponent.AppointmentId;
                        AppointmentId.ParameterName = "@AppointmentId";
                        AppointmentId.DbType = DbType.Int32;
                        cmd.Parameters.Add(AppointmentId);

                        IDbDataParameter ImplantableDeviceId = cmd.CreateParameter();
                        ImplantableDeviceId.Direction = ParameterDirection.Input;
                        ImplantableDeviceId.Value = Convert.ToInt32(ImplatableDeviceId);
                        ImplantableDeviceId.ParameterName = "@ImplantableDeviceId";
                        ImplantableDeviceId.DbType = DbType.Int32;
                        cmd.Parameters.Add(ImplantableDeviceId);

                        IDbDataParameter Status = cmd.CreateParameter();
                        Status.Direction = ParameterDirection.Input;
                        Status.Value = deviceStatus == "Active" ? 1 : 0;
                        Status.ParameterName = "@Status";
                        Status.DbType = DbType.Int16;
                        cmd.Parameters.Add(Status);

                        IDbDataParameter ImplantationDate = cmd.CreateParameter();
                        ImplantationDate.Direction = ParameterDirection.Input;
                        ImplantationDate.Value = Convert.ToDateTime(_ImplantationDate);
                        ImplantationDate.ParameterName = "@ImplantationDate";
                        ImplantationDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(ImplantationDate);

                        IDbDataParameter Laternity = cmd.CreateParameter();
                        Laternity.Direction = ParameterDirection.Input;
                        Laternity.Value = patientLateraity;
                        Laternity.ParameterName = "@Laternity";
                        Laternity.DbType = DbType.String;
                        cmd.Parameters.Add(Laternity);

                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message.ToString();
                retStatus = false;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }

        public bool UpdatePatientImplantableDeviceinDB(string _ImplantationDate, string PatientImplanationId, string Lateraity, string Status)
        {
            string _error = "";
            bool retStatus = true;
            try
            {
                string _insertQry = "Update dbo.PatientImplantableDevices Set ImplantationDate = @ImplantationDate, Laternity = @Laternity, Status = @Status Where Id =  @PatientImplanationId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = _insertQry;
                    IDbDataParameter ImplantationDate = cmd.CreateParameter();
                    ImplantationDate.Direction = ParameterDirection.Input;
                    ImplantationDate.Value = _ImplantationDate;
                    ImplantationDate.ParameterName = "@ImplantationDate";
                    ImplantationDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(ImplantationDate);

                    IDbDataParameter Laternity = cmd.CreateParameter();
                    Laternity.Direction = ParameterDirection.Input;
                    Laternity.Value = Lateraity;
                    Laternity.ParameterName = "@Laternity";
                    Laternity.DbType = DbType.String;
                    cmd.Parameters.Add(Laternity);

                    IDbDataParameter IdbStatus = cmd.CreateParameter();
                    IdbStatus.Direction = ParameterDirection.Input;
                    IdbStatus.Value = Status.ToLower() == "active" ? 1 : 0;
                    IdbStatus.ParameterName = "@Status";
                    IdbStatus.DbType = DbType.Int16;
                    cmd.Parameters.Add(IdbStatus);

                    IDbDataParameter IdbPatientImplanationId = cmd.CreateParameter();
                    IdbPatientImplanationId.Direction = ParameterDirection.Input;
                    IdbPatientImplanationId.Value = PatientImplanationId;
                    IdbPatientImplanationId.ParameterName = "@PatientImplanationId";
                    IdbPatientImplanationId.DbType = DbType.Int32;
                    cmd.Parameters.Add(IdbPatientImplanationId);

                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message.ToString();
                retStatus = false;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }
    }
}
