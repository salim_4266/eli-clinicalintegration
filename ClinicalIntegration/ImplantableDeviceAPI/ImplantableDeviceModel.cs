﻿
namespace ClinicalIntegration
{
    public class DeviceIdentifiers
    {
        public string DeviceId { get; set; }
        public DeviceUDI UniqueDeviceIdentifier { get; set; }
        public DeviceDI DeviceDetail { get; set; }
    }

    public class DeviceIdentifierModel
    {
        public string ErrorMessage { get; set; }
        public string DeviceId { get; set; }
        public string DeviceDescription { get; set; }
        public string BrandName { get; set; }
        public string GMDNPTName { get; set; }
        public string VersionOrModel { get; set; }
        public string manufacturing_date { get; set; }
        public string CompanyName { get; set; }
        public string expirationDate { get; set; }
        public string MRISafetyInformation { get; set; }
        public string RequiresRubberLabelingTypeId { get; set; }
        public string lot_number { get; set; }
        public string serialNumber { get; set; }
        public string GmdnPTDefinition { get; set; }
        public string HCTPStatus { get; set; }
        public string HCTPCode { get; set; }
        public string IssuingAgency { get; set; }
        public string UniqueIdentificationNumber { get; set; }
    }

    public class PatientIdentifierDeviceModel
    {
        public string Lateraity { get; set; }
        public string DateofImplantation { get; set; }
        public string IssuingAgency { get; set; }
        public string UniqueIdentificationNumber { get; set; }
        public string ImplantableDeviceId { get; set; }
        public string ErrorMessage { get; set; }
        public string DeviceId { get; set; }
        public string DeviceDescription { get; set; }
        public string BrandName { get; set; }
        public string GMDNPTName { get; set; }
        public string VersionOrModel { get; set; }
        public string manufacturing_date { get; set; }
        public string CompanyName { get; set; }
        public string expirationDate { get; set; }
        public string MRISafetyInformation { get; set; }
        public string RequiresRubberLabelingTypeId { get; set; }
        public string lot_number { get; set; }
        public string serialNumber { get; set; }
        public string GmdnPTDefinition { get; set; }
        public string HCTPStatus { get; set; }
        public string HCTPCode { get; set; }
        public string DeviceStatus { get; set; }
    }

    public class DeviceUDI
    {
        public string udi { get; set; }
        public string issuing_agency { get; set; }
        public string di { get; set; }
        public string manufacturing_date_original { get; set; }
        public string manufacturing_date_original_format { get; set; }
        public string manufacturing_date { get; set; }
        public string expiration_date_original { get; set; }
        public string expiration_date_original_format { get; set; }
        public string expiration_date { get; set; }
        public string lot_number { get; set; }
        public string serial_number { get; set; }
        public string donation_id { get; set; }
    }
}
