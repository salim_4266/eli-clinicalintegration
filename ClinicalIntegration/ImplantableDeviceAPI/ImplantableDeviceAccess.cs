﻿using System;
using System.Net;
using System.Web;

namespace ClinicalIntegration
{
    public class ImplantableDeviceAccess
    {
        public DeviceIdentifierModel GetImplantableDeviceDetailsFromAPIByUDIID(string UDINo)
        {
            string result = string.Empty;
            string EncodeUDINumber = HttpUtility.UrlEncode(UDINo);
            string _ImplantableUDIUrl = string.Format(ClinicalComponent.ImplantableDeviceUDIURL, EncodeUDINumber);
            DeviceIdentifierModel objDevice = new DeviceIdentifierModel();
           
            try
            {
                using (WebClient webclient = new WebClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                    result = webclient.DownloadString(_ImplantableUDIUrl);
                    DeviceUDI objUDI = Newtonsoft.Json.JsonConvert.DeserializeObject<DeviceUDI>(result);
                    if (objUDI != null && objUDI.di != "")
                    {
                        string EncodeDiNumber = HttpUtility.UrlEncode(objUDI.di);
                        
                        string _ImplantableDeviceUrl = string.Format(ClinicalComponent.ImplantableDeviceURL, EncodeDiNumber);
                        string deviceResult = webclient.DownloadString(_ImplantableDeviceUrl);
                        DeviceDI objDD = Newtonsoft.Json.JsonConvert.DeserializeObject<DeviceDI>(deviceResult);
                        if (objDD.gudid != null)
                        {
                            objDevice.IssuingAgency = objUDI.issuing_agency;
                            objDevice.BrandName = objDD.gudid.device.brandName;
                            objDevice.CompanyName = objDD.gudid.device.companyName;
                            objDevice.DeviceDescription = objDD.gudid.device.deviceDescription;
                            objDevice.DeviceId = objUDI.di;
                            objDevice.ErrorMessage = "";
                            objDevice.expirationDate = objUDI.expiration_date;
                            objDevice.GmdnPTDefinition = objDD.gudid.device.gmdnTerms.gmdn.gmdnPTDefinition;
                            objDevice.GMDNPTName = objDD.gudid.device.gmdnTerms.gmdn.gmdnPTName;
                            objDevice.HCTPCode = objDD.gudid.device.deviceHCTP == "false" ? "" : objUDI.donation_id;
                            objDevice.HCTPStatus = objDD.gudid.device.deviceHCTP;
                            objDevice.lot_number = objUDI.lot_number;
                            objDevice.manufacturing_date = objUDI.manufacturing_date;
                            objDevice.MRISafetyInformation = objDD.gudid.device.MRISafetyStatus;
                            objDevice.RequiresRubberLabelingTypeId = objDD.gudid.device.labeledContainsNRL;
                            objDevice.serialNumber = objUDI.serial_number;
                            objDevice.VersionOrModel = objDD.gudid.device.versionModelNumber;
                            objDevice.UniqueIdentificationNumber = UDINo;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objDevice.ErrorMessage = ex.Message.ToString();
            }
            finally
            {
            }
            //System.Windows.Forms.MessageBox.Show(objDevice.ErrorMessage);
            return objDevice;
        }
    }
}
