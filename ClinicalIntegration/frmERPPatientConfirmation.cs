﻿using DBHelper;
using System;
using System.Data;
using System.Threading;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmERPPatientConfirmation : Form
    {
        public frmERPPatientConfirmation()
        {
            InitializeComponent();
        }

        public string _userName { get; set; }
        public string _password { get; set; }
        public string _practiceToken { get; set; }
        public string _externalId { get; set; }
        public string _patientName { get; set; }
        public string _userType { get; set; }
        public bool _isemailaddress { get; set; }
        public Object DBObject { get; set; }

        private PatientCredentialsEntity objCredentials { get; set; }

        private void frmERPPatientConfirmation_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            ClinicalComponent.PatientId = this._externalId;
            if (this._userType == "Patient")
            {
                label1.Text = "Patient successfully registered and Portal Account has been created.";
            }
            else
            {
                label1.Text = "Representative successfully registered and Portal Account has been created.";
            }

            if (this._userName != "" && this._password != "" && this._practiceToken != "" && this._externalId != "")
            {
                PatientCredential objPC = new PatientCredential();
                objCredentials = objPC.LoadPatientCredentials(this._externalId, this._practiceToken, this._userName, this._password);
                if (objCredentials.ERPAccountNumber != "")
                {
                    if (this._userType == "Patient")
                        textBox1.Text = "LASTNAME" + "FIRSTNAME" + this._externalId;
                    else
                        textBox1.Text = "LASTNAME" + "FIRSTNAME" + "-" + this._externalId;
                    textBox2.Text = "To generate the password, click on forget password on below mentioned link.";
                    textBox3.Text = objCredentials.RedirectionLink;
                    lblPracticeName.Text = objCredentials.PracticeName;
                    if(!_isemailaddress)
                    {
                        button2.Hide();
                    }
                    else
                    {
                        button2.Show();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
          Thread thread = new Thread(new ThreadStart(PrintERPTemplate));
          thread.SetApartmentState(ApartmentState.STA);
          thread.Start();
          thread.Join();
          while (thread.IsAlive) System.Windows.Forms.Application.DoEvents();
            
        }

        private void PrintERPTemplate()
        {
            bool retStatus = true;
            try
            {
                
                string HTMLTemplate = "<html><body><h4>Dear xxxPatientNamexxx ,</h4><h3>Greetings!</h3><h3>We are pleased to inform you that your patient portal is active now please save below mentioned username and password for your Patient Portal.</h3><table style='width:800px;'><tr><td style='width:23%;text-align:left;'>User Name</td><td style='width:77%;text-align:left;'> xxxUserNamexxx </td></tr><tr> <td> Password </td> <td> &#60; LastName&#62;_&#60;DOB(YYYYMMDD)&#62;</td> </tr> <tr> <td> Link </td> <td> xxxURLxxx </td></tr></table><h3>Thank You</h3></body></html>";
                HTMLTemplate = HTMLTemplate.Replace("xxxPatientNamexxx", this._patientName);
                HTMLTemplate = HTMLTemplate.Replace("xxxUserNamexxx", objCredentials.UserName);
                //if (this._userType == "Patient")
                //{
                //    HTMLTemplate = HTMLTemplate.Replace("xxxUserNamexxx", "LastNameFirstName" + this._externalId);
                //}
                //else
                //{
                //    HTMLTemplate = HTMLTemplate.Replace("xxxUserNamexxx", "LastNameFirstName-" + this._externalId);
                //}
                //HTMLTemplate = HTMLTemplate.Replace("xxxPasswordxxx", objCredentials.Password);
                string TempURL = "https://portal.eyereachpatients.com/Patients/" + objCredentials.ERPAccountNumber;
                HTMLTemplate = HTMLTemplate.Replace("xxxURLxxx", TempURL);
                WebBrowser objWebBorwser = new WebBrowser();
                objWebBorwser.Navigate("about:blank");
                objWebBorwser.Document.OpenNew(true);
                objWebBorwser.Document.Write(HTMLTemplate);
                objWebBorwser.ShowPrintDialog();
            }
            catch 
            {
                retStatus = false;
            }
            if (retStatus)
            {
                MessageBox.Show("Document sent to printing, Please check!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (objCredentials.ERPAccountNumber != "")
            {
                PatientCredential objPC = new PatientCredential();
                bool retStatus = objPC.SendCredentials(this._externalId, objCredentials.PracticeToken, _userType);
                if (retStatus)
                {
                    MessageBox.Show("Credentials has been sent");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while sending Credentials, Please try after some time");
                }
            }
        }
    }
}
