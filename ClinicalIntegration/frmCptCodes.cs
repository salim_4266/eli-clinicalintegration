﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmCptCodes : Form

    {
        public Form ParentMDI { get; set; }
        public bool IsEditMode { get; set; }
        public ImageOrderModel objImageModel { get; set; }
        public frmCptCodes()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string _Category = "";

             if (comboBox1.SelectedItem.ToString() == "Code")
            {
                _Category = "Code";
            }
            else
            {
                _Category = "Description";
            }
            string Code = textBox1.Text;
            ImageOrder objIO = new ImageOrder();
            DataTable dt = objIO.GetDataSet_CPTCode(Code, _Category);
            LoadOrderDetails(dt);
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            string CptCode = ((Button)sender).Text.ToString();
            if (CptCode != "")
            {
                frmNewImageOrders frm = new frmNewImageOrders();
                objImageModel.CptCode = CptCode;
                frm.objImageModel = objImageModel;
                frm.MdiParent = this.ParentMDI;
                frm.ParentMDI = this.ParentMDI;
                frm.IsEditMode = this.IsEditMode;
                frm.Show();
                this.Close();
            }
        }
        private void frmCptCodes_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = "Code";
            dataGridView1.AllowUserToAddRows = false;
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }

        private void LoadOrderDetails(DataTable objDataTable)
        {
            dataGridView1.DataSource = objDataTable;
            dataGridView1.AutoGenerateColumns = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (e.RowIndex >= 0)
            {
                string CptCode = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                if (CptCode != "")
                {
                    frmNewImageOrders frm = new frmNewImageOrders();
                    objImageModel.CptCode = CptCode;
                    frm.objImageModel = objImageModel;
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.IsEditMode = this.IsEditMode;
                    frm.Show();
                    this.Close();
                }
            }
        }
    }
}