﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmImplantableDevice : Form
    {
        public Form ParentMDI { get; set; }

        public string Filter { get; set; }

        public string _Category { get; set; }

        bool IsEnabled { get; set; }

        public frmImplantableDevice()
        {
            InitializeComponent();
            btnEdit.Enabled = false;
        }

        private void frmImplantableDevice_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            btnEdit.TabStop = false;
            btnEdit.FlatStyle = FlatStyle.Flat;
            btnEdit.FlatAppearance.BorderSize = 0;
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;


            CmbFilter.SelectedItem = "Active";
            PatientImplantables objPImlpantables = new PatientImplantables();
            dataGridView1.DataSource = objPImlpantables.GetPatientImplantableDeviceDetail("'1'");
            dataGridView1.RowTemplate.Height = 32;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersHeight = 25;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.ClearSelection();
            //dataGridView1.DefaultCellStyle.SelectionBackColor = dataGridView1.DefaultCellStyle.BackColor;
            //dataGridView1.DefaultCellStyle.SelectionForeColor = dataGridView1.DefaultCellStyle.ForeColor;
        }

        //private void button1_Click(object sender, EventArgs e)
        //{

        //    frmNewDevice frm = new frmNewDevice();
        //    frm.MdiParent = this.ParentMDI;
        //    frm.ParentMDI = this.ParentMDI;
        //    frm.Show();
        //    this.Close();
        //}

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            IsEnabled = true;
            btnEdit.Enabled = true;
            var senderGrid = (DataGridView)sender;
            if (e.RowIndex >= 0)
            {
                PatientImplantables objP = new PatientImplantables();
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                btnEdit.Tag = int.Parse(row.Cells["Id"].Value.ToString());
            }
            //string deviceId = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            //frmDeviceDetail frm = new frmDeviceDetail();
            //frm.IsEditMode = true;
            //frm.MdiParent = this.ParentMDI;
            //frm.ParentMDI = this.ParentMDI;
            //PatientImplantables objP = new PatientImplantables();
            //frm.objPatientIdentifierDeviceModel = objP.GetPatientImplantableDeviceById(deviceId);

        }

        private void CmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";

            if (CmbFilter.SelectedItem.ToString() == "Active")
                _Status = "'1'";
            else if (CmbFilter.SelectedItem.ToString() == "Inactive")
                _Status = "'0'";
            else
                _Status = "'1','0'";

            PatientImplantables ojbImp = new PatientImplantables();
            dataGridView1.DataSource = ojbImp.GetPatientImplantableDeviceDetail(_Status);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (IsEnabled)
            {
                string deviceId = (btnEdit).Tag.ToString();
                if (deviceId != null)
                {

                    frmDeviceDetail frm = new frmDeviceDetail();
                    PatientImplantables objp = new PatientImplantables();
                    frm.objPatientIdentifierDeviceModel = objp.GetPatientImplantableDeviceById(deviceId);
                    frm.IsEditMode = true;
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while adding new device, Please try again");
                }

            }
        }
        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            frmNewDevice frm = new frmNewDevice();
            frm.MdiParent = this.ParentMDI;
            frm.ParentMDI = this.ParentMDI;
            frm.Show();
            this.Close();
        }
    }
}