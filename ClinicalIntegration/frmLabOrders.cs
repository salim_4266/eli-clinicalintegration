﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmLabOrders : Form
    {
        public Form ParentMDI { get; set; }
        bool IsEnabled { get; set; }

        bool IsLabTestEnabled { get; set; }
        public frmLabOrders()
        {
            InitializeComponent();
            btnEdit.Enabled = false;
            btnLabResult.Enabled = false;
        }

        private void frmLabOrders_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            comboBox1.SelectedItem = "All";
            LabOrder objLabOrder = new LabOrder();
            dataGridView1.DataSource = objLabOrder.GetPatientLabOrder("'0','1'");
            dataGridView1.RowTemplate.Height = 32;

            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dataGridView1.ColumnHeadersHeight = 25;
            dataGridView1.ClearSelection();
            btnEdit.TabStop = false;
            btnEdit.FlatStyle = FlatStyle.Flat;
            btnEdit.FlatAppearance.BorderSize = 0;
            btnLabResult.TabStop = false;
            btnLabResult.FlatStyle = FlatStyle.Flat;
            btnLabResult.FlatAppearance.BorderSize = 0;
            New.TabStop = false;
            New.FlatStyle = FlatStyle.Flat;
            //LoadOrderDetails(dt);
        }

        //private void LoadOrderDetails(DataTable objDataTable)
        //{
        //    bool alterColor = true;
        //    int CoorindateY = 0;
        //    pnlOrderist.Controls.Clear(); 
        //    foreach (DataRow dr in objDataTable.Rows)
        //    {
        //        Panel PanelOrder = new Panel();
        //        PanelOrder.Width = 1029;
        //        PanelOrder.Height = 28;
        //        PanelOrder.Location = new Point(0, CoorindateY);
        //        PanelOrder.Size = new Size(1029, 28);
        //        if (alterColor)
        //        {
        //            PanelOrder.BackColor = Color.White;
        //            alterColor = false;
        //        }
        //        else
        //        {
        //            PanelOrder.BackColor = Color.LightGray;
        //            alterColor = true;
        //        }

        //        Label lblOrder = new Label();
        //        lblOrder.Text = Convert.ToDateTime(dr["Col2"]).Date.ToString("MM-dd-yyyy");
        //        lblOrder.Location = new Point(11, 8);
        //        lblOrder.Size = new Size(65, 20);
        //        PanelOrder.Controls.Add(lblOrder);

        //        Label lblLoincNumber = new Label();
        //        lblLoincNumber.Text = dr["Col3"].ToString();
        //        lblLoincNumber.Location = new Point(79, 8);
        //        lblLoincNumber.Size = new Size(80, 20);
        //        PanelOrder.Controls.Add(lblLoincNumber);

        //        Label lblDescription = new Label();
        //        lblDescription.Text = dr["Col4"].ToString();
        //        lblDescription.Location = new Point(162, 8);
        //        ToolTip toolTipDescription = new ToolTip();
        //        toolTipDescription.SetToolTip(lblDescription, dr["Col4"].ToString());
        //        lblDescription.Size = new Size(425, 11);
        //        PanelOrder.Controls.Add(lblDescription);

        //        Label lblStatus = new Label();
        //        lblStatus.Text = dr["Col5"].ToString();
        //        lblStatus.Location = new Point(594, 8);
        //        lblStatus.Size = new Size(60, 22);
        //        PanelOrder.Controls.Add(lblStatus);

        //        Label lblModifiedDate = new Label();
        //        lblModifiedDate.Text = Convert.ToDateTime(dr["Col6"]).Date.ToString("MM-dd-yyyy");
        //        lblModifiedDate.Location = new Point(659, 8);
        //        lblModifiedDate.Size = new Size(65, 22);
        //        PanelOrder.Controls.Add(lblModifiedDate);

        //        Label lblModifiedBy = new Label();
        //        lblModifiedBy.Text = dr["Col7"].ToString();
        //        lblModifiedBy.Location = new Point(750, 8);
        //        lblModifiedBy.Size = new Size(60, 22);
        //        PanelOrder.Controls.Add(lblModifiedBy);

        //        Button btnViewReport = new Button();
        //        btnViewReport.Text = "Edit";
        //        btnViewReport.Location = new Point(833, 5);
        //        btnViewReport.Size = new Size(50, 23);
        //        btnViewReport.Tag = dr["Col1"].ToString();
        //        btnViewReport.Click += new EventHandler(btnEditOrder_Click);
        //        ToolTip toolTipbtnViewReport = new ToolTip();
        //        toolTipDescription.SetToolTip(btnViewReport, "Click here, To edit order details");
        //        PanelOrder.Controls.Add(btnViewReport);

        //        Button btnLabResult = new Button();
        //        btnLabResult.Text = "Test Result";
        //        btnLabResult.Location = new Point(895, 5);
        //        btnLabResult.Size = new Size(80, 23);
        //        btnLabResult.Tag = dr["Col1"].ToString();
        //        btnLabResult.Click += new EventHandler(btnLabResult_Click);
        //        ToolTip toolTipbtnLabResult = new ToolTip();
        //        toolTipDescription.SetToolTip(btnLabResult, "Click here, To update results for this order");
        //        PanelOrder.Controls.Add(btnLabResult);

        //        pnlOrderist.Controls.Add(PanelOrder);
        //        CoorindateY += 29;
        //    }
        //}

        private void New_Click(object sender, EventArgs e)
        {
            this.Close();
            frmNewOrder frm = new frmNewOrder();
            LabOrderModel objLabModel = new LabOrderModel();
            objLabModel.OrderDate = ClinicalComponent.AppointmentDate;
            frm.MdiParent = this.ParentMDI;
            frm.ParentMDI = this.ParentMDI;
            frm.IsEditMode = false;
            frm.IsLoincChanged = false;
            frm.objLabModel = objLabModel;
            frm.Show();
        }

        //private void btnEditOrder_Click(object sender, EventArgs e)
        //{
        //    if (IsEnabled)
        //    { 
        //    string OrderId  = ((Button)sender).Tag.ToString();
        //    if (OrderId != "")
        //    {
        //        frmNewOrder frm = new frmNewOrder();
        //        LabOrder objLabOrder = new LabOrder();
        //        frm.objLabModel = objLabOrder.ReteriveOrderDetail(OrderId);
        //        frm.MdiParent = this.ParentMDI;
        //        frm.ParentMDI = this.ParentMDI;
        //        frm.IsEditMode = true;
        //        frm.IsLoincChanged = false;
        //        frm.Show();
        //        this.Close();
        //    }
        //}
        //}

        private void btnLabResult_Click(object sender, EventArgs e)
        {
            string OrderId = (btnLabResult).Tag.ToString();
            if (OrderId != "")
            {
                frmLabTestResults frm = new frmLabTestResults();
                LabOrder objLabOrder = new LabOrder();
                frm.OrderId = OrderId;
                frm.MdiParent = this.ParentMDI;
                frm.ParentMDI = this.ParentMDI;
                frm.Show();
                this.Close();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";
            if (comboBox1.SelectedItem.ToString() == "Pending")
                _Status = "'1'";
            else if (comboBox1.SelectedItem.ToString() == "Completed")
                _Status = "'0'";
            else
                _Status = "'1','0'";
            LabOrder objLabOrd = new LabOrder();
            dataGridView1.DataSource = objLabOrd.GetPatientLabOrder(_Status);
            //LoadOrderDetails(dt);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                IsEnabled = true;
                IsLabTestEnabled = true;
                btnEdit.Enabled = true;
                btnLabResult.Enabled = true;
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                btnEdit.Tag = row.Cells["Col1"].Value.ToString();
                btnLabResult.Tag = row.Cells["Col1"].Value.ToString();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string OrderId = (btnEdit).Tag.ToString();
            if (OrderId != "")
            {
                frmNewOrder frm = new frmNewOrder();
                LabOrder objLabOrder = new LabOrder();
                frm.objLabModel = objLabOrder.ReteriveOrderDetail(OrderId);
                frm.MdiParent = this.ParentMDI;
                frm.ParentMDI = this.ParentMDI;
                frm.IsEditMode = true;
                frm.IsLoincChanged = false;
                frm.Show();
                this.Close();

            }

        }
        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void btnLabResult_Click_1(object sender, EventArgs e)
        {
            string OrderId = (btnLabResult).Tag.ToString();
            if (OrderId != "")
            {
                frmLabTestResults frm = new frmLabTestResults();
                frm.OrderId = OrderId;
                frm.MdiParent = this.ParentMDI;
                frm.ParentMDI = this.ParentMDI;
                frm.Show();
                this.Close();
            }
        }

    }
}
