﻿using DBHelper;
using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmUpdoxConfiguration : Form
    {
        public Object DBObject { get; set; }
        public frmUpdoxConfiguration()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateInputs())
                {
                    UpdoxConfiguration objUpdoxConfiguration = new UpdoxConfiguration();
                    ModelUpdoxConfiguration objModelUpdoxConfiguration = new ModelUpdoxConfiguration();
                    objModelUpdoxConfiguration.AccountId = txtAccountId.Text.Trim();
                    objModelUpdoxConfiguration.Environment = txtEnvironment.Text.Trim();
                    objModelUpdoxConfiguration.IsActive = (cmbStatus.SelectedIndex == 1) ? true : false;
                    objUpdoxConfiguration.AddUpateConfiguration(objModelUpdoxConfiguration);
                    MessageBox.Show("Saved Succesfully");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Oops, something went wrong, Please try again after some time");
            }
        }

        private Boolean ValidateInputs()
        {
            bool IsValidated = true;
            string msgValidation = string.Empty;
            if (txtAccountId.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "AccountId, ";
            }
            if (txtEnvironment.Text.Trim() == string.Empty)
            {
                IsValidated = false;
                msgValidation += "Environment, ";
            }
            if (cmbStatus.SelectedIndex == 0)
            {
                IsValidated = false;
                msgValidation += "Status";
            }
            if (!IsValidated)
            {
                MessageBox.Show("Please Enter " + msgValidation);
            }
            return IsValidated;
        }
        private void frmUpdoxConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                BindStatus();
                DBUtility db = new DBUtility();
                db.DbObject = DBObject;
                ClinicalComponent._Connection = db.DbObject as IDbConnection;
                UpdoxConfiguration objUpdoxConfiguration = new UpdoxConfiguration();
                ModelUpdoxConfiguration objModelUpdoxConfiguration = objUpdoxConfiguration.GetConfiguration();
                if (objModelUpdoxConfiguration.ID > 0)
                {
                    txtAccountId.Text = objModelUpdoxConfiguration.AccountId.ToString();
                    txtEnvironment.Text = objModelUpdoxConfiguration.Environment.ToString();
                    cmbStatus.SelectedIndex = objModelUpdoxConfiguration.IsActive ? 1 : 2;
                }
            }
            catch
            {

            }
        }
        private void BindStatus()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Status");
            DataRow dr = dt.NewRow();
            dr[0] = "Select";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr[0] = "Active";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr[0] = "InActive";
            dt.Rows.Add(dr);
            cmbStatus.DataSource = dt;
            cmbStatus.DisplayMember = "Status";
            cmbStatus.SelectedIndex = 0;
        }
    }
}
