﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class EditFamilyDetail : Form
    {
        public Form ParentMDI { get; set; }
        public string ICD10 { get; set; }
        public string ICD10Desc { get; set; }
        public string SnomedDesc { get; set; }
        public string SnomedID { get; set; }
        public bool IsfamilyReq { get; set; }
        public bool IsfromSnomed { get; set; }
        public bool IsNew { get; set; }
        public bool IsEdit { get; set; }
        public int Id { get; set; }


        public FamilyDetails fd;
        public DateTime AppointmentDt { get; set; }
        public DateTime OnSetdate { get; set; }

        public string Relation { get; set; }
        public string Comments { get; set; }
        public string Status { get; set; }

        public String familyId { get; set; }
        public bool IsEditFamilyFrm { get; set; }
        public FamilyDetails family { get; set; }
        public EditFamilyDetail()
        {
            InitializeComponent();
        }
        public void LoadFamilyDetails(FamilyDetails fd)
        {
            FillRelations();
            FillStatus();
            txtComments.Text = fd.COMMENTS;
            cmbRelations.SelectedValue = fd.Relationship;
            cmbStatus.SelectedItem = fd.Status.ToString();
            txtICD10.Text = fd.ICD10DESCR;
            txtSnomed.Text = fd.ConceptID;
            dtApptment.Value = fd.EnteredDate;
            dtOnsetDate.Value = fd.OnsetDate;
            txtComments.Text = fd.COMMENTS;
            SnomedID = fd.ConceptID;
            ICD10 = fd.ICD10;
            ICD10Desc = fd.ICD10DESCR;
            SnomedDesc = fd.Term;
            Id = fd.Id;
        }
        private void EditFamilyDetail_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillRelations();
            //FillStatus();
            if (IsNew)
            {
                cmbStatus.SelectedItem = Status == null ? "Active" : Status;
            }
            if (IsfamilyReq && !IsNew)
                LoadFamilyDetails(family);
            SnomedToolTip.SetToolTip(btnSnomedMapping, "Snomed / ICD10 Finder");

            if (IsfromSnomed)
            {
                if (!string.IsNullOrEmpty(ICD10Desc))
                {
                    txtICD10.Text = ICD10Desc;
                    txtSnomed.Text = SnomedID;
                    dtOnsetDate.Value = OnSetdate;
                    dtApptment.Value = AppointmentDt;
                    cmbRelations.SelectedValue = Relation;
                    cmbStatus.SelectedItem = Status;
                    txtComments.Text = Comments;
                }
            }
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button2.TabStop = false;
            button2.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
        }
        private void FillStatus()
        {
            //Dictionary<string, string> status = new Dictionary<string, string>();
            //status.Add("1", "Active");
            //status.Add("0", "InActive");
            //cmbStatus.DataSource = new BindingSource(status, null);
            //cmbStatus.DisplayMember = "Value";
            //cmbStatus.ValueMember = "Key";
            //string value = ((KeyValuePair<string, string>)cmbStatus.SelectedItem).Value;
        }
        private void FillRelations()
        {
            DataAccess da = new DataAccess();
            da.GetFamilyRelations();
            cmbRelations.DataSource = da.GetFamilyRelations();
            cmbRelations.DisplayMember = "Name";
            cmbRelations.ValueMember = "Id";
        }
        private void btnSnomedMapping_Click(object sender, EventArgs e)
        {
            if (IsEdit)
            {
                frmDiagnosisSnomed frmSnomed = new frmDiagnosisSnomed();
                frmSnomed.IsFamilyRequest = true;
                frmSnomed.IsFamilyHistoryEdit = true;
                frmSnomed.familyDetDate = dtApptment.Value;
                frmSnomed.familyDetOnsetDate = dtOnsetDate.Value;
                frmSnomed.familyRelation = cmbRelations.SelectedValue.ToString();
                frmSnomed.familyStatus = cmbStatus.SelectedItem.ToString();
                frmSnomed.familyComments = txtComments.Text;
                frmSnomed.FamilyHistoryId = Id.ToString();
                frmSnomed.EditNew = false;
                frmSnomed.IsEdit = this.IsEdit;
                frmSnomed.IsNew = this.IsNew;
                frmSnomed.family = this;
                frmSnomed.ParentMDI = this.ParentMDI;
                frmSnomed.MdiParent = this.MdiParent;
                frmSnomed.Show();
                txtICD10.Text = ICD10Desc;
                txtSnomed.Text = SnomedID;
                this.Close();
            }
            else
            {
                frmDiagnosisSnomed frmSnomed = new frmDiagnosisSnomed();
                frmSnomed.IsFamilyRequest = true;
                frmSnomed.IsFamilyHistoryEdit = true;
                frmSnomed.IsEdit = this.IsEdit;
                frmSnomed.IsNew = this.IsNew;
                frmSnomed.familyDetDate = dtApptment.Value;
                frmSnomed.familyDetOnsetDate = dtOnsetDate.Value;
                frmSnomed.familyRelation = cmbRelations.SelectedValue.ToString();
                frmSnomed.familyComments = txtComments.Text;
                frmSnomed.familyStatus = cmbStatus.SelectedItem == null ? "Active" : cmbStatus.SelectedItem.ToString();
                frmSnomed.family = this;
                frmSnomed.ParentMDI = this.ParentMDI;
                frmSnomed.MdiParent = this.MdiParent;
                frmSnomed.Show();
                txtICD10.Text = ICD10Desc;
                txtSnomed.Text = SnomedID;
                this.Close();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (cmbRelations.SelectedValue.ToString() == "0")
            {
                MessageBox.Show("Please select Relationship");
                return;
            }

            if (txtSnomed.Text == "")
            {
                MessageBox.Show("Please select Snomed Code");
                return;
            }


            if (txtICD10.Text == "")
            {
                MessageBox.Show("Please select Icd10 Code");
                return;
            }

            if (cmbStatus.SelectedItem == null)
            {
                MessageBox.Show("Please select status");
                return;
            }

            FamilyDetails fd = new FamilyDetails();
            fd.Id = this.Id;
            fd.AppointmentID = int.Parse(ClinicalComponent.AppointmentId);
            fd.COMMENTS = txtComments.Text;
            fd.ConceptID = SnomedID;
            fd.Term = SnomedDesc;
            fd.EnteredBy = ClinicalComponent.UserId;
            fd.EnteredDate = dtApptment.Value;
            fd.LastModifiedDate = dtOnsetDate.Value;
            fd.LastModifiedBy = ClinicalComponent.UserId;
            fd.ICD10 = ICD10;
            fd.ICD10DESCR = ICD10Desc;
            fd.PatientId = int.Parse(ClinicalComponent.PatientId);
            fd.Relationship = int.Parse(cmbRelations.SelectedValue.ToString());
            var status = cmbStatus.SelectedItem.ToString() == "Active" ? "1" : "0";
            fd.Status = (status.ToString());
            fd.OnsetDate = dtOnsetDate.Value;

            DataAccess da = new DataAccess();
            if (IsNew)
                da.SaveFamilyDetails(true, fd);
            else if (IsEdit)
                da.SaveFamilyDetails(false, fd);

            frmFamilydetails frm = new frmFamilydetails();
            frm.ParentMDI = this.ParentMDI;
            frm.MdiParent = this.ParentMDI;
            frm.LoadData();
            frm.Show();
            this.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            frmFamilydetails frm = new frmFamilydetails();
            frm.ParentMDI = this.ParentMDI;
            frm.MdiParent = this.ParentMDI;
            frm.LoadData();
            frm.Show();
            this.Close();
        }
    }
}
