﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmNewOrder : Form
    {
        public Form ParentMDI { get; set; }
        public bool IsEditMode { get; set; }
        public LabOrderModel objLabModel { get; set; }
        public bool IsLoincChanged { get; set; }
        public frmNewOrder()
        {
            InitializeComponent();
        }
        private void frmNewOrder_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            dateTimePicker1.Value = objLabModel.OrderDate == null ? Convert.ToDateTime(ClinicalComponent.AppointmentDate) : Convert.ToDateTime(objLabModel.OrderDate);
            txtProvider.Text = ClinicalComponent.ProviderName;
            txtAssigned.Text = ClinicalComponent.UserName;
            txtOrderReason.Text = objLabModel.OrderForReason == null ? "" : objLabModel.OrderForReason.ToString();
            comboBox2.SelectedItem = objLabModel.Status == null ? "Pending" : objLabModel.Status;
            textBox4.Text = objLabModel.LOINCNumber == null ? "" : objLabModel.LOINCNumber;
            button2.TabStop = false;
            button2.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
            button3.TabStop = false;
            button3.FlatStyle = FlatStyle.Flat;
            button3.FlatAppearance.BorderSize = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LabOrderModel objLabModel = new LabOrderModel()
            {
                OrderId = IsEditMode == true ? this.objLabModel.OrderId : "",
                OrderDate = dateTimePicker1.Value.ToString(),
                OrderType = "Lab",
                Status = comboBox2.SelectedItem == null ? "Pending" : comboBox2.SelectedItem.ToString(),
                OrderForReason = txtOrderReason.Text
            };
            frmLoincCodes frm = new frmLoincCodes();
            frm.objLabModel = objLabModel;
            frm.MdiParent = this.ParentMDI;
            frm.ParentMDI = this.ParentMDI;
            frm.IsEditMode = this.IsEditMode;
            frm.LoincNumber = textBox4.Text;
            this.Close();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox4.Text.Trim() == "")
            {
                MessageBox.Show("Please select Loinc Number");
                return;
            }

            if (IsEditMode)
            {
                LabOrderModel objLMO = new LabOrderModel()
                {
                    OrderId = this.objLabModel.OrderId,
                    LOINCNumber = textBox4.Text,
                    Status = comboBox2.SelectedItem.ToString() == "Pending" ? "1" : "0",
                    OrderDate = dateTimePicker1.Value.ToString(),
                    LastModifiedBy = ClinicalComponent.UserId,
                    LastModifiedDate = DateTime.Now.ToString(),
                    OrderForReason = txtOrderReason.Text
                };

                LabOrder objLO = new LabOrder();
                bool retStatus = objLO.UpdateExistingOrderDetail(objLMO, this.IsLoincChanged);
                if (retStatus)
                {
                    frmLabOrders frm = new frmLabOrders();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while adding new order, Please try again");
                    return;
                }
            }
            else
            {
                if (comboBox2.SelectedItem == null)
                {
                    MessageBox.Show("Please select status");
                    return;
                }

                LabOrderModel objLMO = new LabOrderModel()
                {
                    LOINCNumber = textBox4.Text,
                    OrderDate = dateTimePicker1.Value.ToString(),
                    OrderType = "Lab",
                    LastModifiedBy = ClinicalComponent.UserId,
                    Status = comboBox2.SelectedItem.ToString(),
                    OrderForReason = txtOrderReason.Text
                };

                LabOrder objLO = new LabOrder();
                bool retStatus = objLO.PlaceNewOrder(objLMO);
                if (retStatus)
                {
                    frmLabOrders frm = new frmLabOrders();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while adding new order, Please try again");
                }
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            this.Close();
            frmLabOrders frm = new frmLabOrders();
            frm.MdiParent = this.ParentMDI;
            frm.ParentMDI = this.ParentMDI;
            frm.Show();
        }
    }
}
