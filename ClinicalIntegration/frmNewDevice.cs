﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmNewDevice : Form
    {
        public Form ParentMDI { get; set; }

        public frmNewDevice()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string UDINumber = textBox1.Text;
            if (UDINumber != "")
            {
                ImplantableDeviceAccess objIDA = new ImplantableDeviceAccess();
                DeviceIdentifierModel objDM = objIDA.GetImplantableDeviceDetailsFromAPIByUDIID(UDINumber);
                if (objDM.ErrorMessage == "")
                {
                    this.Close();
                    frmDeviceDetail frm = new frmDeviceDetail();
                    frm.objDeviceModel = objDM;
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                }
                else
                {
                    MessageBox.Show("UDI Number seems incorrect, Please try again");
                }
            }
            else
            {
                MessageBox.Show("Please insert UDI number");
            }
        }

        private void frmNewDevice_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
        }
    }
}
