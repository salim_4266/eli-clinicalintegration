﻿namespace ClinicalIntegration
{
    partial class frmACIConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtVendorId = new System.Windows.Forms.TextBox();
            this.txtPracticeId = new System.Windows.Forms.TextBox();
            this.txtBaseURL = new System.Windows.Forms.TextBox();
            this.txtProductKey = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtBURLGetMsgDelvNotif = new System.Windows.Forms.TextBox();
            this.txtBURLGetPbDEPRslt = new System.Windows.Forms.TextBox();
            this.txtBURLPbDEP = new System.Windows.Forms.TextBox();
            this.txtDBURL = new System.Windows.Forms.TextBox();
            this.txtRole = new System.Windows.Forms.TextBox();
            this.txtPairKey = new System.Windows.Forms.TextBox();
            this.txtBURLRetriInbDEPData = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtVendorId
            // 
            this.txtVendorId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVendorId.Location = new System.Drawing.Point(25, 87);
            this.txtVendorId.Name = "txtVendorId";
            this.txtVendorId.Size = new System.Drawing.Size(393, 20);
            this.txtVendorId.TabIndex = 0;
            // 
            // txtPracticeId
            // 
            this.txtPracticeId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPracticeId.Location = new System.Drawing.Point(475, 98);
            this.txtPracticeId.Name = "txtPracticeId";
            this.txtPracticeId.Size = new System.Drawing.Size(363, 20);
            this.txtPracticeId.TabIndex = 1;
            // 
            // txtBaseURL
            // 
            this.txtBaseURL.BackColor = System.Drawing.Color.White;
            this.txtBaseURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBaseURL.Location = new System.Drawing.Point(434, 31);
            this.txtBaseURL.Name = "txtBaseURL";
            this.txtBaseURL.ReadOnly = true;
            this.txtBaseURL.Size = new System.Drawing.Size(391, 23);
            this.txtBaseURL.TabIndex = 2;
            // 
            // txtProductKey
            // 
            this.txtProductKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProductKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductKey.Location = new System.Drawing.Point(475, 50);
            this.txtProductKey.Name = "txtProductKey";
            this.txtProductKey.Size = new System.Drawing.Size(363, 21);
            this.txtProductKey.TabIndex = 3;
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Location = new System.Drawing.Point(477, 150);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(361, 20);
            this.txtUserName.TabIndex = 4;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnSave.Location = new System.Drawing.Point(766, 514);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 33);
            this.btnSave.TabIndex = 49;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // txtBURLGetMsgDelvNotif
            // 
            this.txtBURLGetMsgDelvNotif.BackColor = System.Drawing.Color.White;
            this.txtBURLGetMsgDelvNotif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBURLGetMsgDelvNotif.Location = new System.Drawing.Point(12, 78);
            this.txtBURLGetMsgDelvNotif.Name = "txtBURLGetMsgDelvNotif";
            this.txtBURLGetMsgDelvNotif.ReadOnly = true;
            this.txtBURLGetMsgDelvNotif.Size = new System.Drawing.Size(390, 23);
            this.txtBURLGetMsgDelvNotif.TabIndex = 54;
            // 
            // txtBURLGetPbDEPRslt
            // 
            this.txtBURLGetPbDEPRslt.BackColor = System.Drawing.Color.White;
            this.txtBURLGetPbDEPRslt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBURLGetPbDEPRslt.Location = new System.Drawing.Point(12, 31);
            this.txtBURLGetPbDEPRslt.Name = "txtBURLGetPbDEPRslt";
            this.txtBURLGetPbDEPRslt.ReadOnly = true;
            this.txtBURLGetPbDEPRslt.Size = new System.Drawing.Size(390, 23);
            this.txtBURLGetPbDEPRslt.TabIndex = 53;
            // 
            // txtBURLPbDEP
            // 
            this.txtBURLPbDEP.BackColor = System.Drawing.Color.White;
            this.txtBURLPbDEP.Location = new System.Drawing.Point(436, 128);
            this.txtBURLPbDEP.Name = "txtBURLPbDEP";
            this.txtBURLPbDEP.ReadOnly = true;
            this.txtBURLPbDEP.Size = new System.Drawing.Size(390, 23);
            this.txtBURLPbDEP.TabIndex = 52;
            // 
            // txtDBURL
            // 
            this.txtDBURL.BackColor = System.Drawing.Color.White;
            this.txtDBURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBURL.Location = new System.Drawing.Point(435, 79);
            this.txtDBURL.Name = "txtDBURL";
            this.txtDBURL.ReadOnly = true;
            this.txtDBURL.Size = new System.Drawing.Size(390, 23);
            this.txtDBURL.TabIndex = 51;
            // 
            // txtRole
            // 
            this.txtRole.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRole.Location = new System.Drawing.Point(25, 139);
            this.txtRole.Name = "txtRole";
            this.txtRole.Size = new System.Drawing.Size(400, 20);
            this.txtRole.TabIndex = 50;
            // 
            // txtPairKey
            // 
            this.txtPairKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPairKey.Location = new System.Drawing.Point(25, 197);
            this.txtPairKey.Multiline = true;
            this.txtPairKey.Name = "txtPairKey";
            this.txtPairKey.Size = new System.Drawing.Size(816, 80);
            this.txtPairKey.TabIndex = 55;
            // 
            // txtBURLRetriInbDEPData
            // 
            this.txtBURLRetriInbDEPData.BackColor = System.Drawing.Color.White;
            this.txtBURLRetriInbDEPData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBURLRetriInbDEPData.Location = new System.Drawing.Point(12, 127);
            this.txtBURLRetriInbDEPData.Name = "txtBURLRetriInbDEPData";
            this.txtBURLRetriInbDEPData.ReadOnly = true;
            this.txtBURLRetriInbDEPData.Size = new System.Drawing.Size(391, 23);
            this.txtBURLRetriInbDEPData.TabIndex = 56;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button7.Enabled = false;
            this.button7.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(178)))), ((int)(((byte)(211)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.Black;
            this.button7.Location = new System.Drawing.Point(13, 15);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(434, 29);
            this.button7.TabIndex = 58;
            this.button7.Text = "RCP Configuration";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(10, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 15);
            this.label1.TabIndex = 59;
            this.label1.Text = "Inbound DEP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(9, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 15);
            this.label2.TabIndex = 60;
            this.label2.Text = "Publish DEP Result :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(433, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 15);
            this.label3.TabIndex = 61;
            this.label3.Text = "Publish DEP Url";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(10, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 15);
            this.label4.TabIndex = 62;
            this.label4.Text = "Message Notification";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(432, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 15);
            this.label5.TabIndex = 63;
            this.label5.Text = "MIPS Dashboard URL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label6.Location = new System.Drawing.Point(22, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 15);
            this.label6.TabIndex = 64;
            this.label6.Text = "Role :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label7.Location = new System.Drawing.Point(23, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 65;
            this.label7.Text = "Vendor Id :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label8.Location = new System.Drawing.Point(473, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 15);
            this.label8.TabIndex = 66;
            this.label8.Text = "Practice Id :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(431, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 15);
            this.label9.TabIndex = 67;
            this.label9.Text = "API Url";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label10.Location = new System.Drawing.Point(473, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 15);
            this.label10.TabIndex = 70;
            this.label10.Text = "UserName :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label11.Location = new System.Drawing.Point(23, 177);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 15);
            this.label11.TabIndex = 69;
            this.label11.Text = "PairKey :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label12.Location = new System.Drawing.Point(473, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 15);
            this.label12.TabIndex = 68;
            this.label12.Text = "ProductKey :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label13.Location = new System.Drawing.Point(12, 299);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 15);
            this.label13.TabIndex = 72;
            this.label13.Text = "Domain Url";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(90, 297);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(391, 20);
            this.textBox1.TabIndex = 71;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.txtBURLGetMsgDelvNotif);
            this.panel1.Controls.Add(this.txtBaseURL);
            this.panel1.Controls.Add(this.txtDBURL);
            this.panel1.Controls.Add(this.txtBURLPbDEP);
            this.panel1.Controls.Add(this.txtBURLGetPbDEPRslt);
            this.panel1.Controls.Add(this.txtBURLRetriInbDEPData);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 344);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(853, 161);
            this.panel1.TabIndex = 73;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DarkRed;
            this.label14.Location = new System.Drawing.Point(13, 327);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(384, 15);
            this.label14.TabIndex = 74;
            this.label14.Text = "These values will get automatically populated after fillng Domain Url";
            // 
            // frmACIConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(877, 559);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.txtPairKey);
            this.Controls.Add(this.txtRole);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.txtProductKey);
            this.Controls.Add(this.txtPracticeId);
            this.Controls.Add(this.txtVendorId);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmACIConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RCP Configuration";
            this.Load += new System.EventHandler(this.frmACIConfiguration_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtVendorId;
        private System.Windows.Forms.TextBox txtPracticeId;
        private System.Windows.Forms.TextBox txtBaseURL;
        private System.Windows.Forms.TextBox txtProductKey;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtBURLGetMsgDelvNotif;
        private System.Windows.Forms.TextBox txtBURLGetPbDEPRslt;
        private System.Windows.Forms.TextBox txtBURLPbDEP;
        private System.Windows.Forms.TextBox txtDBURL;
        private System.Windows.Forms.TextBox txtRole;
        private System.Windows.Forms.TextBox txtPairKey;
        private System.Windows.Forms.TextBox txtBURLRetriInbDEPData;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label14;
    }
}