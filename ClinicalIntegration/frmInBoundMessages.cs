﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmInBoundMessages : Form
    {
        public string ProviderKey { get; set; }
        public string ProviderEmailAddress { get; set; }
        public string StaffId { get; set; }
        public string FolderPath { get; set; }
        private string contenttype { get; set; }
        private static string id { get; set; }
        private static string finalpathnamewithkey { get; set; }
        private string Folderpath_txt
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "Txt\\";
            }
        }
        private string folderpath_zip
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "ZIP\\";
            }
        }
        private string folderpath_JPEG
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "JPG\\";
            }
        }
        private string folderpath_PNG
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "PNG\\";
            }
        }
        private string folderpath_pdf
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "PDF\\";
            }
        }
        private string folderpath_xml
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "XML\\";
            }
        }
        private string folderpath_html
        {
            get
            {
                return FolderPath + "\\" + ProviderKey + "\\" + "HTML\\";
            }
        }
        private string ProviderLastName { get; set; }
        private string ProviderFirstName { get; set; }
        private string ProviderDirectAddress { get; set; }
        private ACI_Defaults objACI { get; set; }
        public frmInBoundMessages()
        {
            InitializeComponent();
        }

        private void FetchNewMails()
        {
            try
            {
                string finalurl = objACI.baseurl4 + objACI.productkey;
                InboundProviderModel InboundProvider = new InboundProviderModel(this.StaffId, this.ProviderDirectAddress);
                string providerinboundjson = JsonConvert.SerializeObject(InboundProvider);
                string authorizationToken = "Bearer " + new RequestTokenEntity().GeneratJWTToken().TokenDetail;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(finalurl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Add("ProductKey", objACI.productkey);
                    client.DefaultRequestHeaders.Add("Authorization", authorizationToken);
                    var content = new StringContent(providerinboundjson.ToString(), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = client.PostAsync(objACI.productkey, content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;  // response string from the api
                        string finalresponse = responseString.Replace("\r\n", "");
                        string finalresponse1 = finalresponse.Replace(" ", string.Empty);
                        string finalresponse2 = finalresponse1.Substring(0, 8);
                        string finalresponse3 = finalresponse2.Replace(@"{\", "");
                        string finalresponse4 = finalresponse3.Replace("{", "");
                        string finalresponse5 = finalresponse4.Replace(@"""", @"");
                        if (finalresponse5 != "Status")
                        {
                            InboundProvider.SaveRequestLog(InboundProvider.FromDate, InboundProvider.ToDate);
                            List<InboundJson> jsonList = JsonConvert.DeserializeObject<List<InboundJson>>(responseString);
                            foreach (var json in jsonList)
                            {
                                if (json.attachments != null && json.attachments.Any())
                                {
                                    foreach (var itemResult in json.attachments)
                                    {
                                        Insertattachement(attachments: itemResult, json: json);
                                    }
                                }
                                else
                                {
                                    inbounderrorresponse json1 = new inbounderrorresponse();
                                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                                        ClinicalComponent._Connection.Open();
                                    using (IDbCommand cmd_insert_response1 = ClinicalComponent._Connection.CreateCommand())
                                    {
                                        cmd_insert_response1.CommandText = "ACI_DEP_INBOUNDRESPONSE";
                                        cmd_insert_response1.CommandType = CommandType.StoredProcedure;

                                        IDbDataParameter IdbParam1 = cmd_insert_response1.CreateParameter();
                                        IdbParam1.Direction = ParameterDirection.Input;
                                        IdbParam1.Value = "";
                                        IdbParam1.ParameterName = "@ACIDEPKEYID";
                                        IdbParam1.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam1);

                                        IDbDataParameter IdbParam2 = cmd_insert_response1.CreateParameter();
                                        IdbParam2.Direction = ParameterDirection.Input;
                                        IdbParam2.Value = this.StaffId;
                                        IdbParam2.ParameterName = "@staffkey";
                                        IdbParam2.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam2);

                                        IDbDataParameter IdbParam3 = cmd_insert_response1.CreateParameter();
                                        IdbParam3.Direction = ParameterDirection.Input;
                                        IdbParam3.Value = json1.Status ?? string.Empty;
                                        IdbParam3.ParameterName = "@Status";
                                        IdbParam3.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam3);

                                        IDbDataParameter IdbParam4 = cmd_insert_response1.CreateParameter();
                                        IdbParam4.Direction = ParameterDirection.Input;
                                        IdbParam4.Value = json1.AuditTrackingID ?? 0;
                                        IdbParam4.ParameterName = "@audittrackingid";
                                        IdbParam4.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam4);

                                        IDbDataParameter IdbParam5 = cmd_insert_response1.CreateParameter();
                                        IdbParam5.Direction = ParameterDirection.Input;
                                        IdbParam5.Value = json1.StatusCode ?? 0;
                                        IdbParam5.ParameterName = "@StatusCode";
                                        IdbParam5.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam5);

                                        IDbDataParameter IdbParam6 = cmd_insert_response1.CreateParameter();
                                        IdbParam6.Direction = ParameterDirection.Input;
                                        IdbParam6.Value = json.Description ?? string.Empty;
                                        IdbParam6.ParameterName = "@description";
                                        IdbParam6.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam6);

                                        IDbDataParameter IdbParam7 = cmd_insert_response1.CreateParameter();
                                        IdbParam7.Direction = ParameterDirection.Input;
                                        IdbParam7.Value = json.ResourceType;
                                        IdbParam7.ParameterName = "@Resourcetype";
                                        IdbParam7.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam7);

                                        IDbDataParameter IdbParam8 = cmd_insert_response1.CreateParameter();
                                        IdbParam8.Direction = ParameterDirection.Input;
                                        IdbParam8.Value = json.ExternalId;
                                        IdbParam8.ParameterName = "@ExternalID";
                                        IdbParam8.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam8);

                                        IDbDataParameter IdbParam9 = cmd_insert_response1.CreateParameter();
                                        IdbParam9.Direction = ParameterDirection.Input;
                                        IdbParam9.Value = json.ReferenceNumber;
                                        IdbParam9.ParameterName = "@ReferenceNumber";
                                        IdbParam9.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam9);

                                        IDbDataParameter IdbParam10 = cmd_insert_response1.CreateParameter();
                                        IdbParam10.Direction = ParameterDirection.Input;
                                        IdbParam10.Value = json.Source;
                                        IdbParam10.ParameterName = "@Source";
                                        IdbParam10.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam10);

                                        IDbDataParameter IdbParam11 = cmd_insert_response1.CreateParameter();
                                        IdbParam11.Direction = ParameterDirection.Input;
                                        IdbParam11.Value = json.ExternalReferenceId ?? string.Empty;
                                        IdbParam11.ParameterName = "@ExternalReferenceId";
                                        IdbParam11.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam11);

                                        IDbDataParameter IdbParam12 = cmd_insert_response1.CreateParameter();
                                        IdbParam12.Direction = ParameterDirection.Input;
                                        IdbParam12.Value = json.PatientAccountNumber ?? string.Empty;
                                        IdbParam12.ParameterName = "@PatientAccountNumber";
                                        IdbParam12.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam12);

                                        IDbDataParameter IdbParam13 = cmd_insert_response1.CreateParameter();
                                        IdbParam13.Direction = ParameterDirection.Input;
                                        IdbParam13.Value = json.EncounterId ?? string.Empty;
                                        IdbParam13.ParameterName = "@EncounterId";
                                        IdbParam13.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam13);

                                        IDbDataParameter IdbParam14 = cmd_insert_response1.CreateParameter();
                                        IdbParam14.Direction = ParameterDirection.Input;
                                        IdbParam14.Value = json.ClinicalDocumentType ?? string.Empty;
                                        IdbParam14.ParameterName = "@ClinicalDocumentType";
                                        IdbParam14.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam14);

                                        IDbDataParameter IdbParamDEP = cmd_insert_response1.CreateParameter();
                                        IdbParamDEP.Direction = ParameterDirection.Input;
                                        IdbParamDEP.Value = json.DEPRequestDate ?? string.Empty;
                                        IdbParamDEP.ParameterName = "@DEPRequestDate";
                                        IdbParamDEP.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParamDEP);

                                        IDbDataParameter IdbParam15 = cmd_insert_response1.CreateParameter();
                                        IdbParam15.Direction = ParameterDirection.Input;
                                        IdbParam15.Value = json.OrderingECNPI ?? string.Empty;
                                        IdbParam15.ParameterName = "@OrderingECNPI";
                                        IdbParam15.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam15);

                                        IDbDataParameter IdbParam16 = cmd_insert_response1.CreateParameter();
                                        IdbParam16.Direction = ParameterDirection.Input;
                                        IdbParam16.Value = "";
                                        IdbParam16.ParameterName = "@AT_ContentDisposition";
                                        IdbParam16.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam16);

                                        IDbDataParameter IdbParam17 = cmd_insert_response1.CreateParameter();
                                        IdbParam17.Direction = ParameterDirection.Input;
                                        IdbParam17.Value = contenttype;
                                        IdbParam17.ParameterName = "@AT_ContentType";
                                        IdbParam17.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam17);

                                        IDbDataParameter IdbParamATCONTENT = cmd_insert_response1.CreateParameter();
                                        IdbParamATCONTENT.Direction = ParameterDirection.Input;
                                        IdbParamATCONTENT.Value = content;
                                        IdbParamATCONTENT.ParameterName = "@AT_Content";
                                        IdbParamATCONTENT.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParamATCONTENT);

                                        IDbDataParameter IdbParam18 = cmd_insert_response1.CreateParameter();
                                        IdbParam18.Direction = ParameterDirection.Input;
                                        IdbParam18.Value = "";
                                        IdbParam18.ParameterName = "@AT_FileName";
                                        IdbParam18.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam18);

                                        IDbDataParameter IdbParam19 = cmd_insert_response1.CreateParameter();
                                        IdbParam19.Direction = ParameterDirection.Input;
                                        IdbParam19.Value = "NULL";
                                        IdbParam19.ParameterName = "@Filepath";
                                        IdbParam19.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam19);

                                        IDbDataParameter IdbParam20 = cmd_insert_response1.CreateParameter();
                                        IdbParam20.Direction = ParameterDirection.Input;
                                        IdbParam20.Value = json.DirectAddress.FromProviderAddress ?? string.Empty;
                                        IdbParam20.ParameterName = "@FromProviderAddress";
                                        IdbParam20.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam20);

                                        IDbDataParameter IdbParam21 = cmd_insert_response1.CreateParameter();
                                        IdbParam21.Direction = ParameterDirection.Input;
                                        IdbParam21.Value = json.DirectAddress.FromAddressPassword ?? string.Empty;
                                        IdbParam21.ParameterName = "@FromAddressPassword";
                                        IdbParam21.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam21);

                                        IDbDataParameter IdbParam22 = cmd_insert_response1.CreateParameter();
                                        IdbParam22.Direction = ParameterDirection.Input;
                                        IdbParam22.Value = json.DirectAddress.ToProviderAddress ?? string.Empty;
                                        IdbParam22.ParameterName = "@ToProviderAddress";
                                        IdbParam22.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam22);

                                        IDbDataParameter IdbParam23 = cmd_insert_response1.CreateParameter();
                                        IdbParam23.Direction = ParameterDirection.Input;
                                        IdbParam23.Value = json.DirectAddress.HISP_Type ?? string.Empty;
                                        IdbParam23.ParameterName = "@HISP_Type";
                                        IdbParam23.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam23);

                                        IDbDataParameter IdbParam24 = cmd_insert_response1.CreateParameter();
                                        IdbParam24.Direction = ParameterDirection.Input;
                                        IdbParam24.Value = json.DirectAddress.TransactionType ?? string.Empty;
                                        IdbParam24.ParameterName = "@TransactionType";
                                        IdbParam24.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam24);

                                        IDbDataParameter IdbParam25 = cmd_insert_response1.CreateParameter();
                                        IdbParam25.Direction = ParameterDirection.Input;
                                        IdbParam25.Value = json.DirectAddress.MessageSubject ?? string.Empty;
                                        IdbParam25.ParameterName = "@MessageSubject";
                                        IdbParam25.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam25);

                                        IDbDataParameter IdbParam26 = cmd_insert_response1.CreateParameter();
                                        IdbParam26.Direction = ParameterDirection.Input;
                                        IdbParam26.Value = json.DirectAddress.MessageBody ?? string.Empty;
                                        IdbParam26.ParameterName = "@MessageBody";
                                        IdbParam26.DbType = DbType.String;
                                        cmd_insert_response1.Parameters.Add(IdbParam26);
                                    }
                                    ClinicalComponent._Connection.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }
        
        private void frmInBoundMessages_Load(object sender, EventArgs e)
        {
            objACI = new ACI_Defaults();
            SetUpIntials();
        }

        private void CreateFile(string FolderPath, string Json, string filename)
        {
            try
            {
                string path = FolderPath + "\\" + filename;
                StreamWriter sr = new StreamWriter(path, true);
                sr.Write(Json);
                sr.Close();
                InsertAttachmentFilePath2Database(path, id, contenttype);
            }
            catch { }
        } //file creation method

        private void CreateImageFile(string FolderPath, byte[] Json, string filename, string Ext)
        {
            try
            {
                if (Ext == ".jpeg" || Ext == ".JPEG" || Ext == ".jpg" || Ext == ".JPG")
                {
                    string imagefilepath = filename;
                    string path = FolderPath + "\\" + filename;
                    MemoryStream ms = new MemoryStream(Json, 0, Json.Length);
                    ms.Write(Json, 0, Json.Length);
                    Image image = Image.FromStream(ms, true);
                    image.Save(path);
                    InsertAttachmentFilePath2Database(path, id, contenttype);
                }
                else
                {
                    string imagefilepath = filename;
                    string path = FolderPath + "\\" + filename;
                    MemoryStream ms = new MemoryStream(Json, 0, Json.Length);
                    ms.Write(Json, 0, Json.Length);
                    Image image = Image.FromStream(ms, true);
                    image.Save(path);
                    InsertAttachmentFilePath2Database(path, id, contenttype);
                }
            }
            catch { }
        } // Image file creation method

        private void CreatePDForZIP(string FolderPath, byte[] Json, string filename)
        {
            try
            {
                string finalpathname = FolderPath + "\\" + filename;
                finalpathnamewithkey = FolderPath + "\\" + id;
                File.WriteAllBytes(finalpathname, Json);
                if (!Directory.Exists(finalpathnamewithkey))
                {
                    Directory.CreateDirectory(finalpathnamewithkey);
                }
                string[] textfiles = Directory.GetFiles(FolderPath, "*.zip");
                string finaltextpath = folderpath_zip + '\\' + filename;
                foreach (String item in textfiles)
                {
                    string fileToMove = FolderPath + ("\\" + filename);
                    string moveTo = finalpathnamewithkey + "\\" + filename;
                    File.Move(fileToMove, moveTo);
                    //ZipFile.ExtractToDirectory(moveTo, finalpathnamewithkey);
                    string finalpathnamewithfile = finalpathnamewithkey + "\\" + "INDEX.HTM";
                    InsertAttachmentFilePath2Database(finalpathnamewithfile, id, contenttype);
                }
            }
            catch { }
        } //ZIP file creation method

        private void CreatePDF(string FolderPath, byte[] Json, string filename)
        {
            try
            {
                string finalpathname = FolderPath + "\\" + filename;
                File.WriteAllBytes(finalpathname, Json);
                InsertAttachmentFilePath2Database(finalpathname, id, contenttype);
            }
            catch
            {
            }
        }

        public void InsertAttachmentFilePath2Database(string file_path, string insert_id, string file_content)
        {
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandText = "Update ACI_DEP_Inbound SET FILEPATH = '" + file_path + "'" + " Where Id = '" + insert_id + "'" + " AND AT_ContentType = '" + file_content + "'";
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
            ClinicalComponent._Connection.Close();
        }

        public bool Insertattachement(Attachment attachments, InboundJson json)
        {
            try
            {
                DateTime _date = Convert.ToDateTime(json.DEPRequestDate);
                int date2 = Convert.ToInt32(_date.ToString("yyyy"));
                int std_date = 1900;
                if (date2 >= std_date)
                {
                    inbounderrorresponse json1 = new inbounderrorresponse();
                    string content = attachments.Content;
                    string contentdisposition = attachments.ContentDisposition;
                    contenttype = attachments.ContentType;
                    string Filename = attachments.FileName;

                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd_insert_response1 = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd_insert_response1.CommandText = "ACI_DEP_INBOUNDRESPONSE";
                        cmd_insert_response1.CommandType = CommandType.StoredProcedure;

                        IDbDataParameter IdbParam1 = cmd_insert_response1.CreateParameter();
                        IdbParam1.Direction = ParameterDirection.Input;
                        IdbParam1.Value = "";
                        IdbParam1.ParameterName = "@ACIDEPKEYID";
                        IdbParam1.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam1);

                        IDbDataParameter IdbParam2 = cmd_insert_response1.CreateParameter();
                        IdbParam2.Direction = ParameterDirection.Input;
                        IdbParam2.Value = StaffId;
                        IdbParam2.ParameterName = "@staffkey";
                        IdbParam2.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam2);

                        IDbDataParameter IdbParam3 = cmd_insert_response1.CreateParameter();
                        IdbParam3.Direction = ParameterDirection.Input;
                        IdbParam3.Value = json1.Status ?? string.Empty;
                        IdbParam3.ParameterName = "@Status";
                        IdbParam3.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam3);

                        IDbDataParameter IdbParam4 = cmd_insert_response1.CreateParameter();
                        IdbParam4.Direction = ParameterDirection.Input;
                        IdbParam4.Value = json1.AuditTrackingID ?? 0;
                        IdbParam4.ParameterName = "@audittrackingid";
                        IdbParam4.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam4);

                        IDbDataParameter IdbParam5 = cmd_insert_response1.CreateParameter();
                        IdbParam5.Direction = ParameterDirection.Input;
                        IdbParam5.Value = json1.StatusCode ?? 0;
                        IdbParam5.ParameterName = "@StatusCode";
                        IdbParam5.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam5);

                        IDbDataParameter IdbParam6 = cmd_insert_response1.CreateParameter();
                        IdbParam6.Direction = ParameterDirection.Input;
                        IdbParam6.Value = json.Description ?? string.Empty;
                        IdbParam6.ParameterName = "@description";
                        IdbParam6.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam6);

                        IDbDataParameter IdbParam7 = cmd_insert_response1.CreateParameter();
                        IdbParam7.Direction = ParameterDirection.Input;
                        IdbParam7.Value = json.ResourceType;
                        IdbParam7.ParameterName = "@Resourcetype";
                        IdbParam7.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam7);

                        IDbDataParameter IdbParam8 = cmd_insert_response1.CreateParameter();
                        IdbParam8.Direction = ParameterDirection.Input;
                        IdbParam8.Value = json.ExternalId;
                        IdbParam8.ParameterName = "@ExternalID";
                        IdbParam8.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam8);

                        IDbDataParameter IdbParam9 = cmd_insert_response1.CreateParameter();
                        IdbParam9.Direction = ParameterDirection.Input;
                        IdbParam9.Value = json.ReferenceNumber;
                        IdbParam9.ParameterName = "@ReferenceNumber";
                        IdbParam9.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam9);

                        IDbDataParameter IdbParam10 = cmd_insert_response1.CreateParameter();
                        IdbParam10.Direction = ParameterDirection.Input;
                        IdbParam10.Value = json.Source;
                        IdbParam10.ParameterName = "@Source";
                        IdbParam10.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam10);

                        IDbDataParameter IdbParam11 = cmd_insert_response1.CreateParameter();
                        IdbParam11.Direction = ParameterDirection.Input;
                        IdbParam11.Value = json.ExternalReferenceId ?? string.Empty;
                        IdbParam11.ParameterName = "@ExternalReferenceId";
                        IdbParam11.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam11);

                        IDbDataParameter IdbParam12 = cmd_insert_response1.CreateParameter();
                        IdbParam12.Direction = ParameterDirection.Input;
                        IdbParam12.Value = json.PatientAccountNumber ?? string.Empty;
                        IdbParam12.ParameterName = "@PatientAccountNumber";
                        IdbParam12.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam12);

                        IDbDataParameter IdbParam13 = cmd_insert_response1.CreateParameter();
                        IdbParam13.Direction = ParameterDirection.Input;
                        IdbParam13.Value = json.EncounterId ?? string.Empty;
                        IdbParam13.ParameterName = "@EncounterId";
                        IdbParam13.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam13);

                        IDbDataParameter IdbParam14 = cmd_insert_response1.CreateParameter();
                        IdbParam14.Direction = ParameterDirection.Input;
                        IdbParam14.Value = json.ClinicalDocumentType ?? string.Empty;
                        IdbParam14.ParameterName = "@ClinicalDocumentType";
                        IdbParam14.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam14);

                        IDbDataParameter IdbParamDEP = cmd_insert_response1.CreateParameter();
                        IdbParamDEP.Direction = ParameterDirection.Input;
                        IdbParamDEP.Value = json.DEPRequestDate ?? string.Empty;
                        IdbParamDEP.ParameterName = "@DEPRequestDate";
                        IdbParamDEP.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParamDEP);

                        IDbDataParameter IdbParam15 = cmd_insert_response1.CreateParameter();
                        IdbParam15.Direction = ParameterDirection.Input;
                        IdbParam15.Value = json.OrderingECNPI ?? string.Empty;
                        IdbParam15.ParameterName = "@OrderingECNPI";
                        IdbParam15.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam15);

                        IDbDataParameter IdbParam16 = cmd_insert_response1.CreateParameter();
                        IdbParam16.Direction = ParameterDirection.Input;
                        IdbParam16.Value = contentdisposition;
                        IdbParam16.ParameterName = "@AT_ContentDisposition";
                        IdbParam16.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam16);

                        IDbDataParameter IdbParam17 = cmd_insert_response1.CreateParameter();
                        IdbParam17.Direction = ParameterDirection.Input;
                        IdbParam17.Value = contenttype;
                        IdbParam17.ParameterName = "@AT_ContentType";
                        IdbParam17.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam17);

                        IDbDataParameter IdbParamATCONTENT = cmd_insert_response1.CreateParameter();
                        IdbParamATCONTENT.Direction = ParameterDirection.Input;
                        IdbParamATCONTENT.Value = content;
                        IdbParamATCONTENT.ParameterName = "@AT_Content";
                        IdbParamATCONTENT.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParamATCONTENT);

                        IDbDataParameter IdbParam18 = cmd_insert_response1.CreateParameter();
                        IdbParam18.Direction = ParameterDirection.Input;
                        IdbParam18.Value = Filename;
                        IdbParam18.ParameterName = "@AT_FileName";
                        IdbParam18.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam18);

                        IDbDataParameter IdbParam19 = cmd_insert_response1.CreateParameter();
                        IdbParam19.Direction = ParameterDirection.Input;
                        IdbParam19.Value = "NULL";
                        IdbParam19.ParameterName = "@Filepath";
                        IdbParam19.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam19);

                        IDbDataParameter IdbParam20 = cmd_insert_response1.CreateParameter();
                        IdbParam20.Direction = ParameterDirection.Input;
                        IdbParam20.Value = json.DirectAddress.FromProviderAddress ?? string.Empty;
                        IdbParam20.ParameterName = "@FromProviderAddress";
                        IdbParam20.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam20);

                        IDbDataParameter IdbParam21 = cmd_insert_response1.CreateParameter();
                        IdbParam21.Direction = ParameterDirection.Input;
                        IdbParam21.Value = json.DirectAddress.FromAddressPassword ?? string.Empty;
                        IdbParam21.ParameterName = "@FromAddressPassword";
                        IdbParam21.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam21);

                        IDbDataParameter IdbParam22 = cmd_insert_response1.CreateParameter();
                        IdbParam22.Direction = ParameterDirection.Input;
                        IdbParam22.Value = json.DirectAddress.ToProviderAddress ?? string.Empty;
                        IdbParam22.ParameterName = "@ToProviderAddress";
                        IdbParam22.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam22);

                        IDbDataParameter IdbParam23 = cmd_insert_response1.CreateParameter();
                        IdbParam23.Direction = ParameterDirection.Input;
                        IdbParam23.Value = json.DirectAddress.HISP_Type ?? string.Empty;
                        IdbParam23.ParameterName = "@HISP_Type";
                        IdbParam23.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam23);

                        IDbDataParameter IdbParam24 = cmd_insert_response1.CreateParameter();
                        IdbParam24.Direction = ParameterDirection.Input;
                        IdbParam24.Value = json.DirectAddress.TransactionType ?? string.Empty;
                        IdbParam24.ParameterName = "@TransactionType";
                        IdbParam24.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam24);

                        IDbDataParameter IdbParam25 = cmd_insert_response1.CreateParameter();
                        IdbParam25.Direction = ParameterDirection.Input;
                        IdbParam25.Value = json.DirectAddress.MessageSubject ?? string.Empty;
                        IdbParam25.ParameterName = "@MessageSubject";
                        IdbParam25.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam25);

                        IDbDataParameter IdbParam26 = cmd_insert_response1.CreateParameter();
                        IdbParam26.Direction = ParameterDirection.Input;
                        IdbParam26.Value = json.DirectAddress.MessageBody ?? string.Empty;
                        IdbParam26.ParameterName = "@MessageBody";
                        IdbParam26.DbType = DbType.String;
                        cmd_insert_response1.Parameters.Add(IdbParam26);

                        DataTable dt = new DataTable();
                        using (var reader = cmd_insert_response1.ExecuteReader())
                        {
                            dt.Load(reader);
                        }
                        if (dt.Rows.Count > 0)
                        {
                            id = dt.Rows[0]["id"].ToString();
                        }
                    }
                    if (Convert.ToInt32(id) != 0)
                    {
                        DECODEBASE64(content, Filename);
                    }
                }
            }
            catch
            {
            }
            return true;
        }

        public void DECODEBASE64(string content, string filename)
        {
            string encodedString;
            string decodedString;
            try
            {
                encodedString = content.Trim();
                string finalencodedstring = encodedString.Replace(" ", "");
                byte[] data = Convert.FromBase64String(finalencodedstring);
                decodedString = Encoding.UTF8.GetString(data);
                string attachmentfilename = id + "_" + filename;
                string ext = Path.GetExtension(attachmentfilename);
                if (ext == ".txt" || ext == ".TXT")
                {
                    CreateFile(Folderpath_txt, decodedString, attachmentfilename);
                }
                else if (ext == ".html" || ext == ".HTML")
                {
                    CreateFile(folderpath_html, decodedString, attachmentfilename);
                }
                else if (ext == ".pdf" || ext == ".PDF")
                {
                    CreatePDF(folderpath_pdf, data, attachmentfilename);
                }
                else if (ext == ".zip" || ext == ".ZIP")
                {
                    CreatePDForZIP(folderpath_zip, data, attachmentfilename);
                }
                else if (ext == ".xml" || ext == ".XML")
                {
                    CreateFile(folderpath_xml, decodedString, attachmentfilename);
                }
                else if (ext == ".jpg" || ext == ".jpeg" || ext == ".JPG" || ext == ".JPEG")
                {
                    CreateImageFile(folderpath_JPEG, data, attachmentfilename, ext);
                }
                else if (ext == ".png" || ext == ".PNG")
                {
                    CreateImageFile(folderpath_PNG, data, attachmentfilename, ext);
                }
            }
            catch (Exception)
            {
            }
        }

        private void LoadAllMails()
        {
            FetchNewMails();
            System.Threading.Thread.Sleep(2000);
            button2.Enabled = false;
            string _selectQry = "Select Distinct a.ExternalId as Col1, DEPRequestDate as Col2, MessageSubject as Col4, a.FromProviderAddress as Col3, a.ToProviderAddress as Col5 From DBO.ACI_DEP_Inbound a with(nolock) inner join DBO.UpdoxProviders b with(nolock) on a.ToProviderAddress like '%' + b.DirectAddress + '%' and b.ResourceId = '" + this.StaffId + "' Order by DEPRequestDate Desc";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                DataTable dtTemp = new DataTable();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                }
                dataGridView2.DataSource = dtTemp;
                dataGridView2.AutoGenerateColumns = false;
            }
        }

        public void SetUpIntials()
        {
            panel1.Hide();
            button3.Image = Properties.Resources.btn_close;
            button3.TabStop = false;
            button3.FlatStyle = FlatStyle.Flat;
            button3.FlatAppearance.BorderSize = 0;
            button2.Image = Properties.Resources.btn_view;
            button2.TabStop = false;
            button2.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
            try
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandText = "Select R.RESOURCELASTNAME, R.RESOURCEFIRSTNAME, ACIP.DirectAddress from DBO.UpdoxProviders ACIP with (nolock) Left Join Resources R with (nolock) on R.RESOURCEID = ACIP.ResourceId where ACIP.ResourceId = '" + this.StaffId + "'";
                    cmd.CommandType = CommandType.Text;
                    DataTable dt = new DataTable();
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                    if (dt.Rows.Count > 0)
                    {
                        ProviderFirstName = dt.Rows[0]["RESOURCEFIRSTNAME"].ToString();
                        ProviderLastName = dt.Rows[0]["RESOURCELASTNAME"].ToString();
                        ProviderDirectAddress = dt.Rows[0]["DirectAddress"].ToString();
                    }
                }
                LoadAllMails();
            }
            catch
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnlAttachments.Controls.Clear();
            bool attachment = false;
            panel1.Show();
            string ExternalId = button2.Tag.ToString();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandText = "Select Convert(varchar(21),DEPRequestDate,100) as Col2, MessageSubject as Col3, a.MessageBody as Col4, a.AT_FileName as Col5, a.filepath as Col6,a.AT_ContentType as Col7, a.FromProviderAddress as Col8, a.ToProviderAddress as Col9 From DBO.ACI_DEP_Inbound a with (nolock) Where a.ExternalId = '" + ExternalId + "'";
                cmd.CommandType = CommandType.Text;
                DataTable dt = new DataTable();
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
                int height = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    txtDescription.Text = dr["Col4"].ToString();
                    txtSubject.Text = dr["Col3"].ToString();
                    txtDepDate.Text = dr["Col2"].ToString();
                    txtToProviderAddress.Text = dr["Col8"].ToString();
                    txtFromProviderAddress.Text = dr["Col9"].ToString();

                    if (dr["Col5"].ToString() != "" && dr["Col6"].ToString() != "" && dr["Col7"].ToString() != "")
                    {
                        attachment = true;
                        Button btnAttachment = new Button();
                        btnAttachment.Size = new Size(pnlAttachments.Width - 1, 30);
                        btnAttachment.Tag = dr["Col6"].ToString() + "%" + dr["Col7"].ToString();
                        btnAttachment.Text = dr["Col5"].ToString();
                        btnAttachment.Location = new Point(1, height);
                        pnlAttachments.Controls.Add(btnAttachment);
                        btnAttachment.Click += new EventHandler(this.btnAttachment_Click);
                        height = height + 34;
                    }
                }
                pnlAttachments.Visible = attachment;
            }
        }

        private void btnAttachment_Click(object sender, EventArgs e)
        {
            string[] FileDetails = ((Button)sender).Tag.ToString().Split('%');
            if (FileDetails.Count() == 2)
            {
                string FilePath = FileDetails[0];
                string FileType = FileDetails[1].ToLower();
                if (File.Exists(FilePath))
                {
                    if (FileType.Contains("pdf") || FileType.Contains("jpeg") || FileType.Contains("jpg") || FileType.Contains("png") || FileType.Contains("txt") || FileType.Contains("text/plain"))
                    {
                        System.Diagnostics.Process.Start(FilePath);
                    }
                    else if (FileType.Contains("html"))
                    {
                        System.Diagnostics.Process.Start(FilePath);
                    }
                    else if (FileType.Contains("zip") || FileType.Contains("stream"))
                    {
                        string tempFileName = Path.GetDirectoryName(FilePath);
                        DirectoryInfo di = new DirectoryInfo(tempFileName);
                        foreach (DirectoryInfo d1 in di.GetDirectories())
                        {
                            foreach (DirectoryInfo d11 in d1.GetDirectories())
                            {
                                foreach (FileInfo fi in d11.GetFiles())
                                {
                                    if (fi.FullName.ToLower().Contains("ccda") && fi.Extension.ToLower().Contains("xml"))
                                    {
                                        string TempFilePath = fi.FullName;
                                        //MessageBox.Show(TempFilePath);
                                        string LinkedPath = AppDomain.CurrentDomain.BaseDirectory.ToString().Replace("\\Debug", "");
                                        string path2Exe = LinkedPath + @"\CCDAVIEWER\FileViewCCDA.exe";
                                        ProcessStartInfo startInfo = new ProcessStartInfo();
                                        startInfo.FileName = path2Exe;
                                        startInfo.Arguments = TempFilePath;
                                        Process.Start(startInfo);
                                    }
                                }
                            }
                        }
                    }
                    if (FileType.Contains("xml"))
                    {
                        string LinkedPath = AppDomain.CurrentDomain.BaseDirectory.ToString().Replace("\\Debug", "");
                        string path2Exe = LinkedPath + @"\CCDAVIEWER\FileViewCCDA.exe";
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = path2Exe;
                        startInfo.Arguments = FilePath;
                        Process.Start(startInfo);
                    }
                }
                else
                {
                    MessageBox.Show("File not found!");
                }
            }
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = dataGridView2.Rows[rowIndex];
                button2.Enabled = true;
                button2.Tag = (row.Cells[0].Value.ToString());
            }
        }

        private void dataGridView2_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            grid.ClearSelection();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            panel1.Hide();
        }
    }
}
