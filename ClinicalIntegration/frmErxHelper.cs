﻿using DBHelper;
using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmErxHelper : Form
    {
        DrfirstMedications med = null;
        public string PatientId { get; set; }
        public string PracticeToken { get; set; }
        public string SqlConString { get; set; }
        public string UserId { get; set; }
        public string SSOLaunchTime { get; set; }
        public string AppointmentId { get; set; }

        public Object connection;

        public Object DBObject
        {
            get
            {
                return connection;
            }
            set
            {
                connection = value;

            }
        }

        public frmErxHelper()
        {
            InitializeComponent();
        }

        private void frmErxHelper_Load(object sender, EventArgs e)
        {
            try
            {
                DBUtility db = new DBUtility();
                db.DbObject = DBObject;
                IDbConnection con = db.DbObject as IDbConnection;

                if (!ValidateUser(con))
                {
                    MessageBox.Show("You are not authorized for ERX, Please contact IO Support");
                    ErxComponent.RcopiaUserExternalId = "";
                    this.Close();
                }

                ErxComponent.InitializeErxComponent(con);
                string ssoURL = string.Empty, uploadUrl = string.Empty, downLoadUrl = string.Empty, portalHdr = string.Empty;
                ssoURL = ErxComponent.StagingSSOUrl;
                uploadUrl = ErxComponent.StagingUploadUrl;
                downLoadUrl = ErxComponent.StagingDownloadUrl;
                portalHdr = ErxComponent.StagingPortalHeader;

                var patDemo = new PatientDemographics();
                patDemo.SendPatientDemographics(PatientId, UserId, PracticeToken, AppointmentId, con);
                var medInfo = new DrfirstMedications();
                medInfo.SendMedications(PatientId, AppointmentId, UserId, PracticeToken, con);
                var alrg = new Allergies();
                alrg.SendAllergies(PatientId, AppointmentId, UserId, PracticeToken, con);

                var api = new DrFirstApi();
                string response = string.Empty;
                string rcopiaLaunchTime = string.Empty;
                var zone = TimeZone.CurrentTimeZone;

                var easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                var local = zone.ToUniversalTime(DateTime.Now);
                var easternDateTime = TimeZoneInfo.ConvertTimeFromUtc(local, easternTimeZone);
                rcopiaLaunchTime = local.ToString("MMddyyHHmmss");
                
                portalHdr = portalHdr.Replace("<patientId>", PatientId).Replace("<time>", rcopiaLaunchTime).Replace("<screencontext>", "patient").Replace("<n>", "n").Replace("<userExternalId>", ErxComponent.RcopiaUserExternalId);
                string hash = BitConverter.ToString(new System.Security.Cryptography.MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(portalHdr + ErxComponent.VendorPassword))).Replace("-", "").ToUpper();
                string URL = ssoURL + "?" + portalHdr + "&MAC=" + hash;
                object URLId = URL;

                if (!string.IsNullOrEmpty(URL))
                {
                    SSOLaunchTime = easternDateTime.ToString();// DateTime.Now.ToString();
                    bool apiStatus = true;
                    int apiType = (int)ApiActionType.LaunchSSOPatient;
                    new LogApiCall().LogErxActivity(URL, PatientId, UserId, DateTime.Now, string.Empty, AppointmentId, PracticeToken, apiType, con, apiStatus, false, string.Empty);
                    axWebBrowser1.Dock = DockStyle.Fill;
                    axWebBrowser1.Navigate(URL);
                }
            }
            catch
            {
                MessageBox.Show("There is some issue while Opening Erx screen, Please contact IO support!");
                this.Close();
            }
        }

        private bool ValidateUser(IDbConnection connection)
        {
            bool retStatus = false;
            if (connection.State.ToString() == "Closed")
                connection.Open();
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = "Select ExternalId From DrFirst.UserConfiguration with (nolock) Where ResourceId = '" + this.UserId + "' and IsActive = 1";
                cmd.CommandType = CommandType.Text;
                using (IDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        if (!String.IsNullOrEmpty(dr["ExternalId"].ToString()))
                        {
                            ErxComponent.RcopiaUserExternalId = dr["ExternalId"].ToString();
                            retStatus = true;
                        }
                    }
                }
            }
            return retStatus;
        }

        private void axWebBrowser1_NewWindow2(object sender, AxSHDocVw.DWebBrowserEvents2_NewWindow2Event e)
        {
            PopUp popUpForm = new PopUp();
            popUpForm.axWebBrowser1.RegisterAsBrowser = true;
            e.ppDisp = popUpForm.axWebBrowser1.Application;
            popUpForm.Visible = true;
        }

        private void frmErxHelper_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ErxComponent.RcopiaUserExternalId != "")
            {
                DBUtility db = new DBUtility();
                db.DbObject = DBObject;
                var con = db.DbObject as IDbConnection;
                string updtMedicationResponse = string.Empty;
                string updtAllergyResponse = string.Empty;
                med = new DrfirstMedications();
                updtMedicationResponse = med.GetUpdatedMedications(PatientId, AppointmentId, UserId, PracticeToken, con, false, string.Empty, SSOLaunchTime);
                med.SetMedicationsData(PatientId, AppointmentId, updtMedicationResponse, con);

                string prescriptionResponse = string.Empty;
                var prescription = new DrfirstPrescription();

                prescriptionResponse = prescription.GetPrescriptionResponse(PatientId, AppointmentId, UserId, PracticeToken, con, "pending", SSOLaunchTime);
                prescription.AddUpdatePrescriptionResponse(PatientId, AppointmentId, prescriptionResponse, con);

                prescriptionResponse = prescription.GetPrescriptionResponse(PatientId, AppointmentId, UserId, PracticeToken, con, "completed", SSOLaunchTime);
                prescription.AddUpdatePrescriptionResponse(PatientId, AppointmentId, prescriptionResponse, con);
            }
        }
    }
}
