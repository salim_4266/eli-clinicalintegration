﻿namespace ClinicalIntegration
{
    partial class frmFamilydetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHistoryCount = new System.Windows.Forms.Label();
            this.btnNewHistory = new System.Windows.Forms.Button();
            this.grdVwFamily = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.BtnEdit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdVwFamily)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHistoryCount
            // 
            this.lblHistoryCount.AutoSize = true;
            this.lblHistoryCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHistoryCount.Location = new System.Drawing.Point(16, 123);
            this.lblHistoryCount.Name = "lblHistoryCount";
            this.lblHistoryCount.Size = new System.Drawing.Size(93, 16);
            this.lblHistoryCount.TabIndex = 8;
            this.lblHistoryCount.Text = "Family History";
            // 
            // btnNewHistory
            // 
            this.btnNewHistory.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_new;
            this.btnNewHistory.Location = new System.Drawing.Point(685, 114);
            this.btnNewHistory.Name = "btnNewHistory";
            this.btnNewHistory.Size = new System.Drawing.Size(96, 36);
            this.btnNewHistory.TabIndex = 7;
            this.btnNewHistory.UseVisualStyleBackColor = true;
            this.btnNewHistory.Click += new System.EventHandler(this.btnNewHistory_Click);
            // 
            // grdVwFamily
            // 
            this.grdVwFamily.AllowUserToAddRows = false;
            this.grdVwFamily.AllowUserToDeleteRows = false;
            this.grdVwFamily.AllowUserToOrderColumns = true;
            this.grdVwFamily.AllowUserToResizeRows = false;
            this.grdVwFamily.BackgroundColor = System.Drawing.Color.White;
            this.grdVwFamily.Location = new System.Drawing.Point(12, 156);
            this.grdVwFamily.MultiSelect = false;
            this.grdVwFamily.Name = "grdVwFamily";
            this.grdVwFamily.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdVwFamily.Size = new System.Drawing.Size(769, 408);
            this.grdVwFamily.TabIndex = 6;
            this.grdVwFamily.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdVwFamily_CellContentClick);
            this.grdVwFamily.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdVwFamily_DataBindingComplete);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(266, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Filter Option";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "All",
            "Active",
            "Inactive"});
            this.comboBox1.Location = new System.Drawing.Point(340, 121);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(240, 21);
            this.comboBox1.TabIndex = 10;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // BtnEdit
            // 
            this.BtnEdit.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_edit;
            this.BtnEdit.Location = new System.Drawing.Point(588, 114);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(96, 36);
            this.BtnEdit.TabIndex = 11;
            this.BtnEdit.UseVisualStyleBackColor = true;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // frmFamilydetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 600);
            this.Controls.Add(this.BtnEdit);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblHistoryCount);
            this.Controls.Add(this.btnNewHistory);
            this.Controls.Add(this.grdVwFamily);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 79);
            this.Name = "frmFamilydetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmFamilydetails";
            this.Load += new System.EventHandler(this.frmFamilydetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdVwFamily)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHistoryCount;
        private System.Windows.Forms.Button btnNewHistory;
        private System.Windows.Forms.DataGridView grdVwFamily;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button BtnEdit;
    }
}