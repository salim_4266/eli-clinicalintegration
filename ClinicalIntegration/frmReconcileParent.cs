﻿using DBHelper;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmReconcileParent : Form
    {
        public string PatientId { get; set; }
        public string AppointmentId { get; set; }
        public string AppointmentDate { get; set; }
        public string UserId { get; set; }
        public string PatientName { get; set; }
        public Object DBObject { get; set; }

        public frmReconcileParent()
        {
            InitializeComponent();
        }

        private void frmReconcileParent_Load(object sender, EventArgs e)
        {
            try
            {
                DBUtility db = new DBUtility();
                db.DbObject = DBObject;
                ClinicalComponent._Connection = db.DbObject as IDbConnection;
                ClinicalComponent.PatientId = this.PatientId;
                ClinicalComponent.AppointmentId = this.AppointmentId;
                ClinicalComponent.UserId = this.UserId;
                ClinicalComponent.PatientName = this.PatientName;
                ClinicalComponent.AppointmentDate = this.AppointmentDate;

                label1.Text = "Patient Name - " + ClinicalComponent.PatientName;
                label2.Text = "Appointment Date - " + ClinicalComponent.AppointmentDate;
                label3.Text = "UserName - " + ClinicalComponent.UserName;
                DefaultButtonImages();
                DisableButtonBorder(button1);
                DisableButtonBorder(button2);
                DisableButtonBorder(button3);
                frmMedicationReconciliation frm = new frmMedicationReconciliation();
                frm.MdiParent = this;
                frm.ParentMDI = this;
                frm.Show();
                ActivateButtonImages(button1);
            }
            catch
            {
            }
        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }
        private void DefaultButtonImages()
        {
            button1.Image = Properties.Resources.btn_medication;
            button2.Image = Properties.Resources.btn_allergies;
            button3.Image = Properties.Resources.btn_problem;
        }
        private void ActivateButtonImages(Button btn)
        {
            DefaultButtonImages();
            switch (btn.Name)
            {
                case "button1":
                    button1.Image = Properties.Resources.btn_medication_active;
                    break;
                case "button2":
                    button2.Image = Properties.Resources.btn_allergies_active;
                    break;
                case "button3":
                    button3.Image = Properties.Resources.btn_problem_active;
                    break;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButtonImages(button);
            ValidateOpenForm();
            frmMedicationReconciliation frm = new frmMedicationReconciliation();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void ValidateOpenForm()
        {
            FormCollection fc = Application.OpenForms;
            if (fc != null && fc.Count > 0)
            {
                for (int i = 1; i < fc.Count; i++)
                {
                    if (fc[i] != null && fc[i].IsDisposed != true && !fc[i].IsMdiContainer)
                    {
                        fc[i].Dispose();
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButtonImages(button);
            ValidateOpenForm();
            frmAllergyReconciliation frm = new frmAllergyReconciliation();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ActivateButtonImages(button);
            ValidateOpenForm();
            frmReconcileProblems frm = new frmReconcileProblems();
            frm.MdiParent = this;
            frm.ParentMDI = this;
            frm.Show();
        }
    }
}
