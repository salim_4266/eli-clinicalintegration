﻿namespace ClinicalIntegration
{
    partial class frmProceduresList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEdit = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.CmbFilter = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.grdProcedure = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNew = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdProcedure)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEdit
            // 
            this.btnEdit.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_edit;
            this.btnEdit.Location = new System.Drawing.Point(886, 105);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(96, 36);
            this.btnEdit.TabIndex = 24;
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(569, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Filter Option";
            // 
            // CmbFilter
            // 
            this.CmbFilter.FormattingEnabled = true;
            this.CmbFilter.Items.AddRange(new object[] {
            "All",
            "Active",
            "Inactive"});
            this.CmbFilter.Location = new System.Drawing.Point(640, 114);
            this.CmbFilter.Name = "CmbFilter";
            this.CmbFilter.Size = new System.Drawing.Size(236, 21);
            this.CmbFilter.TabIndex = 22;
            this.CmbFilter.SelectedIndexChanged += new System.EventHandler(this.CmbFilter_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 21;
            this.label1.Text = "Procedures List";
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_quit;
            this.btnClose.Location = new System.Drawing.Point(12, 532);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(96, 36);
            this.btnClose.TabIndex = 20;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Visible = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grdProcedure
            // 
            this.grdProcedure.AllowUserToAddRows = false;
            this.grdProcedure.AllowUserToDeleteRows = false;
            this.grdProcedure.AllowUserToResizeColumns = false;
            this.grdProcedure.AllowUserToResizeRows = false;
            this.grdProcedure.BackgroundColor = System.Drawing.Color.White;
            this.grdProcedure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProcedure.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column9,
            this.Column4,
            this.Column5,
            this.Column8,
            this.Column10,
            this.Column11,
            this.Column3,
            this.Column6,
            this.Column7});
            this.grdProcedure.Location = new System.Drawing.Point(12, 145);
            this.grdProcedure.MultiSelect = false;
            this.grdProcedure.Name = "grdProcedure";
            this.grdProcedure.ReadOnly = true;
            this.grdProcedure.RowTemplate.ReadOnly = true;
            this.grdProcedure.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.grdProcedure.Size = new System.Drawing.Size(1070, 381);
            this.grdProcedure.TabIndex = 19;
            this.grdProcedure.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProcedure_CellClick);
            this.grdProcedure.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdProcedure_DataBindingComplete);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "id";
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "id";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "EnteredDate";
            this.Column2.Frozen = true;
            this.Column2.HeaderText = "Entered Date";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "AppointmentDate";
            this.Column9.Frozen = true;
            this.Column9.HeaderText = "Diagnosis Date";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "ConceptID";
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "SNOMED CT";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Term";
            this.Column5.Frozen = true;
            this.Column5.HeaderText = "SNOMED CT Description";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 175;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "ProcedureDetail";
            this.Column8.Frozen = true;
            this.Column8.HeaderText = "Procedure Detail";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 175;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "health";
            this.Column10.Frozen = true;
            this.Column10.HeaderText = "Health Concerns";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "reason";
            this.Column11.Frozen = true;
            this.Column11.HeaderText = "Reason /Referral";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Status";
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "Status";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 75;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "LastModifyDate";
            this.Column6.Frozen = true;
            this.Column6.HeaderText = "Modified Date";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "ResourceName";
            this.Column7.Frozen = true;
            this.Column7.HeaderText = "Modified By";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // btnNew
            // 
            this.btnNew.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_new;
            this.btnNew.Location = new System.Drawing.Point(985, 105);
            this.btnNew.Margin = new System.Windows.Forms.Padding(0);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(96, 36);
            this.btnNew.TabIndex = 18;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // frmProceduresList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 579);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CmbFilter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grdProcedure);
            this.Controls.Add(this.btnNew);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 79);
            this.Name = "frmProceduresList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmProceduresList";
            this.Load += new System.EventHandler(this.frmProceduresList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdProcedure)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CmbFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView grdProcedure;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
    }
}