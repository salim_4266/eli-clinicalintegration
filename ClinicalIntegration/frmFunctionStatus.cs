﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmFunctionStatus : Form
    {
        public Form ParentMDI { get; set; }
        public Int64 SnomedCode { get; set; }
        public string SnomedDesc { get; set; }
        public string Active { get; set; }
        public bool IsNew { get; set; }
        public Int64 FunctionalStatusId { get; set; }
        public string FunctionalValue { get; set; }



        public frmFunctionStatus()
        {
            InitializeComponent();
        }

        private void frmFunctionStatus_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillStatusCB();
            FillSnomedDetail();
            btnSave.TabStop = false;
            btnSave.FlatStyle = FlatStyle.Flat;
            btnSave.FlatAppearance.BorderSize = 0;
            btnCancel.TabStop = false;
            btnCancel.FlatStyle = FlatStyle.Flat;
            btnCancel.FlatAppearance.BorderSize = 0;
        }


        #region Button Event
        private void btnSnomedFinder_Click(object sender, EventArgs e)
        {


            frmSnomedFinder snomedfinder = new frmSnomedFinder();
            snomedfinder.isEdit = IsNew;
            snomedfinder.CallingFrom = "FunctionalStatus";
            snomedfinder.id = FunctionalStatusId;
            snomedfinder.FunctionalValue = cbFunction.SelectedItem.ToString();

            snomedfinder.editstatus = cmStatus.SelectedItem.ToString();
            snomedfinder.MdiParent = this.ParentMDI;
            snomedfinder.ParentMDI = this.ParentMDI;
            snomedfinder.Show();
            this.Close();



        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (GetFormValidate())
            {
                FunctionStatus.FunctionalModel objFM = new FunctionStatus.FunctionalModel()
                {
                    Id = FunctionalStatusId,
                    PatientId = Convert.ToInt64(ClinicalComponent.PatientId),
                    AppointmentID = Convert.ToInt64(ClinicalComponent.AppointmentId),
                    AppointmentDate = ClinicalComponent.AppointmentDate,
                    EnteredDate = DateTime.Now,
                    ConceptID = SnomedCode,
                    Term = SnomedDesc,
                    Status = cmStatus.SelectedItem.ToString(),
                    ResourceId = Convert.ToInt32(ClinicalComponent.UserId),
                    LastModofiedDate = DateTime.Now,
                    FunctionalValue = cbFunction.SelectedItem.ToString()

                };

                FunctionStatus.Functional objFunctional = new FunctionStatus.Functional();
                objFunctional.SaveFunctionalStatus(IsNew, objFM);

                frmFunctionalStatusList frmFSL = new frmFunctionalStatusList();
                frmFSL.MdiParent = this.ParentMDI;
                frmFSL.ParentMDI = this.ParentMDI;
                frmFSL.Show();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
            frmFunctionalStatusList frmp = new frmFunctionalStatusList();
            frmp.MdiParent = this.ParentMDI;
            frmp.ParentMDI = this.ParentMDI;
            frmp.Show();
        }
        #endregion

        private void FillSnomedDetail()
        {

            if (!string.IsNullOrEmpty(SnomedDesc))
            {
                txtDesc.Text = SnomedDesc;
                txtSnomedCode.Text = SnomedCode.ToString();
                //cmStatus.SelectedText = Active;
                cmStatus.SelectedIndex = cmStatus.Items.IndexOf(Active);
                cbFunction.SelectedItem = FunctionalValue;

            }
            else if (IsNew)
            {
                txtSnomedCode.Text = "";
                txtDesc.Text = "";
                cmStatus.SelectedIndex = cmStatus.Items.IndexOf("Active");
                cbFunction.SelectedItem = "Functional";
            }
        }

        private void FillStatusCB()
        {
            cmStatus.Items.Insert(0, "Active");
            cmStatus.Items.Insert(1, "Inactive");
            cmStatus.SelectedIndex = 0;
        }


        public void FillFunctionModel()
        {
            FunctionStatus.FunctionalModel objFM = new FunctionStatus.FunctionalModel()
            {
                Id = FunctionalStatusId,
                PatientId = Convert.ToInt64(ClinicalComponent.PatientId),
                AppointmentID = Convert.ToInt64(ClinicalComponent.AppointmentId),
                AppointmentDate = ClinicalComponent.AppointmentDate,
                EnteredDate = DateTime.Now,
                ConceptID = SnomedCode,
                Term = SnomedDesc,
                Status = cmStatus.SelectedItem.ToString(),
                ResourceId = Convert.ToInt32(ClinicalComponent.UserId),
                LastModofiedDate = DateTime.Now

            };
        }

        private bool GetFormValidate()
        {
            bool isFlag = true;
            if (string.IsNullOrEmpty(txtSnomedCode.Text))
            {
                isFlag = false;
                MessageBox.Show("Please select 'SNOMED Code/SNOMED Description'", "Error");

            }
            else if (string.IsNullOrEmpty(txtDesc.Text))
            {
                isFlag = false;
                MessageBox.Show("Please select 'SNOMED Code/SNOMED Description'", "Error");

            }
            return isFlag;
        }


    }
}
