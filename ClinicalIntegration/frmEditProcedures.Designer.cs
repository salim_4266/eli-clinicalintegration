﻿namespace ClinicalIntegration
{
    partial class frmEditProcedures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditProcedures));
            this.txtReason = new System.Windows.Forms.TextBox();
            this.txthealth = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmStatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSnomedCode = new System.Windows.Forms.TextBox();
            this.txtInstruction = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.btnFinder = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSnomedFinder = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtReason
            // 
            resources.ApplyResources(this.txtReason, "txtReason");
            this.txtReason.Name = "txtReason";
            // 
            // txthealth
            // 
            resources.ApplyResources(this.txthealth, "txthealth");
            this.txthealth.Name = "txthealth";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cmStatus
            // 
            this.cmStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cmStatus, "cmStatus");
            this.cmStatus.Name = "cmStatus";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // txtSnomedCode
            // 
            resources.ApplyResources(this.txtSnomedCode, "txtSnomedCode");
            this.txtSnomedCode.Name = "txtSnomedCode";
            this.txtSnomedCode.ReadOnly = true;
            // 
            // txtInstruction
            // 
            resources.ApplyResources(this.txtInstruction, "txtInstruction");
            this.txtInstruction.Name = "txtInstruction";
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_done;
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // Status
            // 
            resources.ApplyResources(this.Status, "Status");
            this.Status.Name = "Status";
            // 
            // txtDesc
            // 
            resources.ApplyResources(this.txtDesc, "txtDesc");
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.ReadOnly = true;
            // 
            // btnFinder
            // 
            resources.ApplyResources(this.btnFinder, "btnFinder");
            this.btnFinder.Name = "btnFinder";
            this.btnFinder.UseVisualStyleBackColor = true;
            this.btnFinder.Click += new System.EventHandler(this.btnSnomedFinder_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // btnSnomedFinder
            // 
            resources.ApplyResources(this.btnSnomedFinder, "btnSnomedFinder");
            this.btnSnomedFinder.Name = "btnSnomedFinder";
            this.btnSnomedFinder.UseVisualStyleBackColor = true;
            this.btnSnomedFinder.Click += new System.EventHandler(this.btnSnomedFinder_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_quit;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // frmEditProcedures
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtReason);
            this.Controls.Add(this.txthealth);
            this.Controls.Add(this.btnFinder);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSnomedFinder);
            this.Controls.Add(this.cmStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.txtSnomedCode);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.txtInstruction);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmEditProcedures";
            this.Load += new System.EventHandler(this.frmEditProcedures_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtReason;
        private System.Windows.Forms.TextBox txthealth;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSnomedCode;
        private System.Windows.Forms.TextBox txtInstruction;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Status;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Button btnFinder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSnomedFinder;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label6;
    }
}