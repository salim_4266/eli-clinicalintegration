﻿namespace ClinicalIntegration
{
    partial class EditFamilyDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtApptment = new System.Windows.Forms.DateTimePicker();
            this.btnSnomedMapping = new System.Windows.Forms.Button();
            this.txtSnomed = new System.Windows.Forms.TextBox();
            this.dtOnsetDate = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtICD10 = new System.Windows.Forms.TextBox();
            this.cmbRelations = new System.Windows.Forms.ComboBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.txtComments = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SnomedToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtApptment
            // 
            this.dtApptment.Location = new System.Drawing.Point(132, 108);
            this.dtApptment.Name = "dtApptment";
            this.dtApptment.Size = new System.Drawing.Size(231, 20);
            this.dtApptment.TabIndex = 34;
            // 
            // btnSnomedMapping
            // 
            this.btnSnomedMapping.Location = new System.Drawing.Point(323, 171);
            this.btnSnomedMapping.Name = "btnSnomedMapping";
            this.btnSnomedMapping.Size = new System.Drawing.Size(40, 22);
            this.btnSnomedMapping.TabIndex = 33;
            this.btnSnomedMapping.Text = "...";
            this.btnSnomedMapping.UseVisualStyleBackColor = true;
            this.btnSnomedMapping.Click += new System.EventHandler(this.btnSnomedMapping_Click);
            // 
            // txtSnomed
            // 
            this.txtSnomed.Enabled = false;
            this.txtSnomed.Location = new System.Drawing.Point(132, 172);
            this.txtSnomed.Name = "txtSnomed";
            this.txtSnomed.Size = new System.Drawing.Size(198, 20);
            this.txtSnomed.TabIndex = 32;
            // 
            // dtOnsetDate
            // 
            this.dtOnsetDate.Location = new System.Drawing.Point(132, 137);
            this.dtOnsetDate.Name = "dtOnsetDate";
            this.dtOnsetDate.Size = new System.Drawing.Size(231, 20);
            this.dtOnsetDate.TabIndex = 31;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_quit;
            this.button2.Location = new System.Drawing.Point(132, 424);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 36);
            this.button2.TabIndex = 30;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_done;
            this.button1.Location = new System.Drawing.Point(268, 424);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 36);
            this.button1.TabIndex = 29;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtICD10
            // 
            this.txtICD10.Enabled = false;
            this.txtICD10.Location = new System.Drawing.Point(132, 203);
            this.txtICD10.Name = "txtICD10";
            this.txtICD10.Size = new System.Drawing.Size(231, 20);
            this.txtICD10.TabIndex = 28;
            // 
            // cmbRelations
            // 
            this.cmbRelations.FormattingEnabled = true;
            this.cmbRelations.Location = new System.Drawing.Point(132, 237);
            this.cmbRelations.Name = "cmbRelations";
            this.cmbRelations.Size = new System.Drawing.Size(231, 21);
            this.cmbRelations.TabIndex = 27;
            // 
            // cmbStatus
            // 
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.cmbStatus.Location = new System.Drawing.Point(132, 272);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(231, 21);
            this.cmbStatus.TabIndex = 26;
            // 
            // txtComments
            // 
            this.txtComments.Location = new System.Drawing.Point(132, 313);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(231, 85);
            this.txtComments.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(60, 313);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Comments";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(76, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(51, 240);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Relationship";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(76, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "ICD 10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "SNOMED CT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(55, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Onset Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Diagnosis Date";
            // 
            // EditFamilyDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 549);
            this.Controls.Add(this.dtApptment);
            this.Controls.Add(this.btnSnomedMapping);
            this.Controls.Add(this.txtSnomed);
            this.Controls.Add(this.dtOnsetDate);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtICD10);
            this.Controls.Add(this.cmbRelations);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.txtComments);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 79);
            this.Name = "EditFamilyDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "EditFamilyDetail";
            this.Load += new System.EventHandler(this.EditFamilyDetail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtApptment;
        private System.Windows.Forms.Button btnSnomedMapping;
        private System.Windows.Forms.TextBox txtSnomed;
        private System.Windows.Forms.DateTimePicker dtOnsetDate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtICD10;
        private System.Windows.Forms.ComboBox cmbRelations;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.TextBox txtComments;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip SnomedToolTip;
        private System.Windows.Forms.Label label1;
    }
}