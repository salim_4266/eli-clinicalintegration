﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    class ReconcileList
    {
        public DataTable GetProbblemReconcileList(Int64 patientId, bool IsMerged)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "Select distinct PRP.Id,PRP.Patientid, ' ' AS DiagDesc , case  when ProblemStatus like '%complet%' then 'Resolved' else UPPER(LEFT(ProblemStatus,1))+LOWER(SUBSTRING(ProblemStatus,2,LEN(ProblemStatus)))  end  as [Status],PRP.SnomedCT as SnomedCode,' ' AS Term,' ' AS ICD10,' ' as problem, PRP.EffectiveDate as LastModefiedDate, 1 as ExistingProblem ";
                query += "FROM model.PatientReconcileProblems PRP with(nolock) where [Status] = " + Convert.ToInt16(IsMerged) + "  and  patientid=@patientid";

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {

            }
            return GetProblemDetailWithDt(dt);
        }

        public DataTable GetProblemDetailWithDt(DataTable dt)
        {
            try
            {
                String query = "";
                foreach (System.Data.DataColumn col in dt.Columns)
                {
                    dt.Columns["problem"].MaxLength = 300;
                    dt.Columns["ICD10"].MaxLength = 300;
                    dt.Columns["Term"].MaxLength = 300;
                    col.ReadOnly = false;
                }
                dt.AcceptChanges();
                foreach (DataRow dr in dt.Rows)
                {
                    //query = "SELECT TOP(1) SN.Term,SN.ICD10, SN.ICD10DESCR as problem FROM dbo.snomed_2016 SN  WHERE SN.ConceptId=@ConceptId";
                    query = "GetProblemDetailsBySnomedCode";
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        IDbDataParameter parConceptId = cmd.CreateParameter();
                        parConceptId.Direction = ParameterDirection.Input;
                        parConceptId.Value = Convert.ToString(dr["SnomedCode"]).TrimEnd().TrimStart();
                        parConceptId.ParameterName = "@ConceptId";
                        parConceptId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parConceptId);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = query;
                        try
                        {
                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    dr["problem"] = Convert.ToString(reader["problem"]);
                                    dr["ICD10"] = Convert.ToString(reader["ICD10"]);
                                    dr["Term"] = Convert.ToString(reader["Term"]);
                                }
                            }
                        }
                        catch
                        {
                            return new DataTable();
                        }
                    }
                }

            }
            catch
            {

            }
            return dt;
        }

        public void AddAllergyListchk(Int64 patientId, CheckedListBox objCheckedListBox, int index)
        {
            try
            {
                String query = "INSERT INTO [dbo].[PatientClinical]([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom],[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status],[DrawFileName],[Highlights],[FollowUp]";
                query += ",[PermanentCondition],[PostOpPeriod],[Surgery],[Activity],[LegacyClinicalDataSourceTypeId])";
                query += "VALUES(@AppointmentId,@PatientId,'H','OU','/ALLERGIES ALL',@FindingDetail,'','','A',@DrawFileName,'','','',0,'','',NULL)";
                string AlgCtrlId = string.Empty;
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@PatientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    IDbDataParameter parAppointmentId = cmd.CreateParameter();
                    parAppointmentId.Direction = ParameterDirection.Input;
                    parAppointmentId.Value = ClinicalComponent.AppointmentId;
                    parAppointmentId.ParameterName = "@AppointmentId";
                    parAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentId);

                    IDbDataParameter parFindingDetail = cmd.CreateParameter();
                    parFindingDetail.Direction = ParameterDirection.Input;
                    string[] allergyDesc = ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[1].ToString().Split(',').ToArray();
                    if (!string.IsNullOrEmpty(allergyDesc[0]))
                    {
                        if (allergyDesc[0].ToLower() == "none")
                        {
                            MessageBox.Show("Unable to Merge due to InValid Data.");
                            return;
                        }
                        AlgCtrlId = GetAllergiesInfo(allergyDesc[0].Trim());
                        if (string.IsNullOrEmpty(AlgCtrlId))
                        {
                            MessageBox.Show("Unable to Merge due to InValid Data.");
                            return;
                        }
                        //parFindingDetail.Value = allergyDesc[0].Trim().ToUpper() + "=" + AlgCtrlId + "<" + allergyDesc[0].Trim().ToUpper() + ">" + " /" + ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[2].ToString() + "/None";
                        parFindingDetail.Value = allergyDesc[0].Trim().ToUpper() + "=" + AlgCtrlId + "/" + ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[2].ToString().ToUpper() + "/NONE";
                    }
                    else
                    {
                        return;
                    }
                    parFindingDetail.ParameterName = "@FindingDetail";
                    parFindingDetail.DbType = DbType.String;
                    cmd.Parameters.Add(parFindingDetail);
                    IDbDataParameter parDrawFileName = cmd.CreateParameter();
                    parDrawFileName.Direction = ParameterDirection.Input;
                    parDrawFileName.Value = AlgCtrlId;
                    parDrawFileName.ParameterName = "@DrawFileName";
                    parDrawFileName.DbType = DbType.String;
                    cmd.Parameters.Add(parDrawFileName);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();

                    query = "update model.PatientReconcileAllergy set status =1 where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Convert.ToInt32(((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[0].ToString());
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }
        public void AddAllergyList(Int64 patientId, DataRow dr, int index, string clinicalId, Int64 appointmentId)
        {
            try
            {
                String query = "INSERT INTO [dbo].[PatientClinical]([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom],[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status],[DrawFileName],[Highlights],[FollowUp]";
                query += ",[PermanentCondition],[PostOpPeriod],[Surgery],[Activity],[LegacyClinicalDataSourceTypeId])";
                query += "VALUES(@AppointmentId,@PatientId,'H','OU','/ALLERGIES ALL',@FindingDetail,'','',@Status,@DrawFileName,'','','',0,'','',NULL)";
                string AlgCtrlId = string.Empty;
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@PatientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    IDbDataParameter parAppointmentId = cmd.CreateParameter();
                    parAppointmentId.Direction = ParameterDirection.Input;
                    parAppointmentId.Value = ClinicalComponent.AppointmentId;
                    parAppointmentId.ParameterName = "@AppointmentId";
                    parAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentId);

                    IDbDataParameter parFindingDetail = cmd.CreateParameter();
                    parFindingDetail.Direction = ParameterDirection.Input;
                    string allergyDesc = dr["allergy"].ToString();
                    if (!string.IsNullOrEmpty(allergyDesc))
                    {
                        if (allergyDesc.ToLower() == "none")
                        {
                            MessageBox.Show("Unable to merge due to insufficient Data");
                            return;
                        }
                        AlgCtrlId = GetAllergiesInfoExact(allergyDesc.Trim());
                        if (string.IsNullOrEmpty(AlgCtrlId))
                        {
                            AlgCtrlId = GetAllergiesInfo(allergyDesc.Trim());
                        }
                        if (string.IsNullOrEmpty(AlgCtrlId))
                        {
                            MessageBox.Show("Unable to merge due to insufficient Data");
                            return;
                        }
                        string aReaction = dr["Reaction"].ToString();
                        string aSeverity = dr["Severity"].ToString();

                        if (!string.IsNullOrEmpty(aReaction))
                        {
                        }
                        else
                        {
                            aReaction = "None";
                        }
                        if (!string.IsNullOrEmpty(aSeverity))
                        {
                        }
                        else
                        {
                            aSeverity = "None";
                        }
                        //parFindingDetail.Value = allergyDesc[0].Trim().ToUpper() + "=" + AlgCtrlId + "<" + allergyDesc[0].Trim().ToUpper() + ">" + " /" + ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[2].ToString() + "/None";
                        parFindingDetail.Value = allergyDesc.Trim().ToUpper() + "=" + AlgCtrlId + "/" + aReaction + "/" + aSeverity;
                    }
                    else
                    {
                        MessageBox.Show("Unable to merge due to insufficient Data");
                        return;
                    }
                    parFindingDetail.ParameterName = "@FindingDetail";
                    parFindingDetail.DbType = DbType.String;
                    cmd.Parameters.Add(parFindingDetail);
                    IDbDataParameter parDrawFileName = cmd.CreateParameter();
                    parDrawFileName.Direction = ParameterDirection.Input;
                    parDrawFileName.Value = AlgCtrlId;
                    parDrawFileName.ParameterName = "@DrawFileName";
                    parDrawFileName.DbType = DbType.String;
                    cmd.Parameters.Add(parDrawFileName);

                    IDbDataParameter parStatus = cmd.CreateParameter();
                    parStatus.Direction = ParameterDirection.Input;
                    if (Convert.ToString(dr["Status"]).ToLower().Contains("active"))
                    {
                        parStatus.Value = "A";
                    }
                    else
                    {
                        parStatus.Value = "D";
                    }
                    parStatus.ParameterName = "@Status";
                    parStatus.DbType = DbType.String;
                    cmd.Parameters.Add(parStatus);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();


                    if (Convert.ToString(clinicalId) != string.Empty)
                    {
                        query = "update dbo.patientclinical set status ='D'  where clinicalId=@clinicalId";
                        cmd.Parameters.Clear();
                        IDbDataParameter parclinicalId = cmd.CreateParameter();
                        parclinicalId.Direction = ParameterDirection.Input;
                        parclinicalId.Value = Convert.ToInt64(clinicalId);
                        parclinicalId.ParameterName = "@clinicalId";
                        parclinicalId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parclinicalId);
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }


                    query = "update model.PatientReconcileAllergy set status =1,MergedDate=Getdate(),AppointmentId=@AppointmentId where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Convert.ToInt32(dr["Id"].ToString());
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    IDbDataParameter parApId = cmd.CreateParameter();
                    parApId.Direction = ParameterDirection.Input;
                    parApId.Value = appointmentId;
                    parApId.ParameterName = "@AppointmentId";
                    parApId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parApId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                MessageBox.Show("Unable to merge due to insufficient Data");
            }
        }

        public void UpdateRcAllergy(Int64 Id, string clinicalId)
        {
            try
            {
                string AlgCtrlId = string.Empty;
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    string query = "update model.PatientReconcileAllergy set status =1,MergedDate=Getdate() where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Id;
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();

                    cmd.Parameters.Clear();
                    query = "update patientclinical set findingdetail =findingdetail where clinicalId=@clinicalId";
                    IDbDataParameter parclinicalId = cmd.CreateParameter();
                    parclinicalId.Direction = ParameterDirection.Input;
                    parclinicalId.Value = Convert.ToInt64(clinicalId);
                    parclinicalId.ParameterName = "@clinicalId";
                    parclinicalId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parclinicalId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();


                }
            }
            catch
            {
            }
        }

        public void UpdateRcMedications(Int64 Id)
        {
            try
            {
                string AlgCtrlId = string.Empty;
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    string query = "update model.PatientReconcileMedications set status =1,MergedDate=Getdate() where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Id;
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }
        public string GetAllergiesInfo(string AllergyDesc)
        {
            string AllergyControlId = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                String query = "select top(1) CompositeAllergyID from fdb.tblcompositeallergy where description  like '%" + AllergyDesc.Trim() + "%'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                if ((dt != null))
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int index = 0; index <= dt.Rows.Count - 1; index++)
                        {
                            if (!string.IsNullOrEmpty(dt.Rows[index]["CompositeAllergyID"].ToString().Trim()))
                            {
                                AllergyControlId = dt.Rows[index]["CompositeAllergyID"].ToString().Trim();
                            }
                        }
                    }
                }
                return AllergyControlId;
            }
            catch
            {
            }
            return AllergyControlId;
        }

        public string GetAllergiesInfoExact(string AllergyDesc)
        {
            string AllergyControlId = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                String query = "select top(1) CompositeAllergyID from fdb.tblcompositeallergy where description  ='" + AllergyDesc.Trim() + "'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                if ((dt != null))
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int index = 0; index <= dt.Rows.Count - 1; index++)
                        {
                            if (!string.IsNullOrEmpty(dt.Rows[index]["CompositeAllergyID"].ToString().Trim()))
                            {
                                AllergyControlId = dt.Rows[index]["CompositeAllergyID"].ToString().Trim();
                            }
                        }
                    }
                }
                return AllergyControlId;
            }
            catch
            {
            }
            return AllergyControlId;
        }

        public Medications GetMedicationInfo(string DrugName)
        {
            Medications objMedications = new Medications();
            try
            {
                DataTable dt = new DataTable();
                String query = "SELECT top(1) MED_ROUTED_DF_MED_ID_DESC,MED_STRENGTH+MED_STRENGTH_UOM AS MED_STRENGTH from fdb.tblcompositedrug  where MED_Routed_DF_MED_ID_DESC like '%" + DrugName.Trim() + "%'";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                if ((dt != null))
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int index = 0; index <= dt.Rows.Count - 1; index++)
                        {
                            if (!string.IsNullOrEmpty(dt.Rows[index]["MED_ROUTED_DF_MED_ID_DESC"].ToString().Trim()))
                            {
                                objMedications.MED_ROUTED_DF_MED_ID_DESC = dt.Rows[index]["MED_ROUTED_DF_MED_ID_DESC"].ToString().Trim();
                            }
                            if (!string.IsNullOrEmpty(dt.Rows[index]["MED_STRENGTH"].ToString().Trim()))
                            {
                                objMedications.MED_STRENGTH = dt.Rows[index]["MED_STRENGTH"].ToString().Trim();
                            }
                        }
                    }
                }
                return objMedications;
            }
            catch
            {
            }
            return objMedications;
        }
        public void AddProblemListchk(Int64 patientId, CheckedListBox objCheckedListBox, int index)
        {
            try
            {
                String query = "INSERT INTO [model].[PatientProblemList] ([PatientId],[AppointmentId],[AppointmentDate],[ConceptID],[Term],[ICD10],[ICD10DESCR],[COMMENTS],[ResourceName],[OnsetDate],[LastModifyDate],[Status],[ResourceId],[EnteredDate],[ResolvedDate])";
                query += "VALUES(@PatientId,@AppointmentId,@AppointmentDate,@ConceptID,@Term,@ICD10,@ICD10DESCR,NULL,NULL,GETDATE(),GETDATE(),'Active',@ResourceId,GETDATE(),NULL)";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    IDbDataParameter parAppointmentId = cmd.CreateParameter();
                    parAppointmentId.Direction = ParameterDirection.Input;
                    parAppointmentId.Value = ClinicalComponent.AppointmentId;
                    parAppointmentId.ParameterName = "@AppointmentId";
                    parAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentId);

                    IDbDataParameter parAppointmentDate = cmd.CreateParameter();
                    parAppointmentDate.Direction = ParameterDirection.Input;
                    parAppointmentDate.Value = ClinicalComponent.AppointmentDate;
                    parAppointmentDate.ParameterName = "@AppointmentDate";
                    parAppointmentDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parAppointmentDate);

                    IDbDataParameter parConceptId = cmd.CreateParameter();
                    parConceptId.Direction = ParameterDirection.Input;
                    parConceptId.Value = ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[4].ToString(); ;
                    parConceptId.ParameterName = "@ConceptId";
                    parConceptId.DbType = DbType.String;
                    cmd.Parameters.Add(parConceptId);

                    IDbDataParameter parTerm = cmd.CreateParameter();
                    parTerm.Direction = ParameterDirection.Input;
                    parTerm.Value = ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[5].ToString();
                    parTerm.ParameterName = "@Term";
                    parTerm.DbType = DbType.String;
                    cmd.Parameters.Add(parTerm);

                    IDbDataParameter parICD10 = cmd.CreateParameter();
                    parICD10.Direction = ParameterDirection.Input;
                    parICD10.Value = ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[6].ToString(); ;
                    parICD10.ParameterName = "@ICD10";
                    parICD10.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10);

                    IDbDataParameter parICD10DESCR = cmd.CreateParameter();
                    parICD10DESCR.Direction = ParameterDirection.Input;
                    parICD10DESCR.Value = ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[7].ToString(); ;
                    parICD10DESCR.ParameterName = "@ICD10DESCR";
                    parICD10DESCR.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10DESCR);

                    IDbDataParameter parResourceId = cmd.CreateParameter();
                    parResourceId.Direction = ParameterDirection.Input;
                    parResourceId.Value = ClinicalComponent.UserId;
                    parResourceId.ParameterName = "@ResourceId";
                    parResourceId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parResourceId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();

                    query = "update model.PatientReconcileProblems set status =1 where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Convert.ToInt32(((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[0].ToString());
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }

        private Boolean IsProblemExisting(string snomedCode)
        {
            try
            {
                String query = "select Conceptid from  [model].[PatientProblemList] where Conceptid=@Conceptid and  patientid=@patientid";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = ClinicalComponent.PatientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);


                    IDbDataParameter parConceptId = cmd.CreateParameter();
                    parConceptId.Direction = ParameterDirection.Input;
                    parConceptId.Value = snomedCode;
                    parConceptId.ParameterName = "@ConceptId";
                    parConceptId.DbType = DbType.String;
                    cmd.Parameters.Add(parConceptId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            return true;
                        }
                    }
                }
            }
            catch
            {
            }
            return false;
        }
        private void UpdateLastmodefiedDateProblem(string snomedCode, DataRow dr)
        {
            try
            {
                String query = "UPDATE [model].[PatientProblemList]  SET LastModifyDate =GETDATE(),Status=@Status where Conceptid=@Conceptid and  patientid=@patientid";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = ClinicalComponent.PatientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);


                    IDbDataParameter parConceptId = cmd.CreateParameter();
                    parConceptId.Direction = ParameterDirection.Input;
                    parConceptId.Value = snomedCode;
                    parConceptId.ParameterName = "@ConceptId";
                    parConceptId.DbType = DbType.String;
                    cmd.Parameters.Add(parConceptId);


                    IDbDataParameter parStatus = cmd.CreateParameter();
                    parStatus.Direction = ParameterDirection.Input;
                    if (Convert.ToString(dr["Status"]).ToLower().Contains("active"))
                    {
                        parStatus.Value = "Active";
                    }
                    else
                    {
                        parStatus.Value = "Resolved";
                    }
                    parStatus.ParameterName = "@Status";
                    parStatus.DbType = DbType.String;
                    cmd.Parameters.Add(parStatus);


                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }
        public void AddProblemList(Int64 patientId, DataRow objRow, int index, Int64 appointmentId)
        {
            try
            {
                String query = "";
                if (Convert.ToString(objRow["Status"]).ToLower().Contains("resolved") || (Convert.ToString(objRow["Status"]).ToLower().Contains("completed")))
                {
                    query = "INSERT INTO [model].[PatientProblemList] ([PatientId],[AppointmentId],[AppointmentDate],[ConceptID],[Term],[ICD10],[ICD10DESCR],[COMMENTS],[ResourceName],[OnsetDate],[LastModifyDate],[Status],[ResourceId],[EnteredDate],[ResolvedDate])";
                    query += "VALUES(@PatientId,@AppointmentId,@AppointmentDate,@ConceptID,@Term,@ICD10,@ICD10DESCR,NULL,NULL,GETDATE(),GETDATE(),@Status,@ResourceId,GETDATE(),@ResolvedDate)";
                }
                else
                {
                    query = "INSERT INTO [model].[PatientProblemList] ([PatientId],[AppointmentId],[AppointmentDate],[ConceptID],[Term],[ICD10],[ICD10DESCR],[COMMENTS],[ResourceName],[OnsetDate],[LastModifyDate],[Status],[ResourceId],[EnteredDate],[ResolvedDate])";
                    query += "VALUES(@PatientId,@AppointmentId,@AppointmentDate,@ConceptID,@Term,@ICD10,@ICD10DESCR,NULL,NULL,GETDATE(),GETDATE(),@Status,@ResourceId,GETDATE(),NULL)";
                }

                if (IsProblemExisting(objRow[4].ToString()))// snomed code
                {
                    UpdateLastmodefiedDateProblem(objRow[4].ToString(), objRow);
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        query = "update model.PatientReconcileProblems set MergedDate=Getdate(), status =1,AppointmentId=@AppointmentId where Id=@Id";
                        cmd.Parameters.Clear();
                        IDbDataParameter parId = cmd.CreateParameter();
                        parId.Direction = ParameterDirection.Input;
                        parId.Value = Convert.ToInt64(objRow[0].ToString());
                        parId.ParameterName = "@Id";
                        parId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parId);

                        IDbDataParameter parApId = cmd.CreateParameter();
                        parApId.Direction = ParameterDirection.Input;
                        parApId.Value = appointmentId;
                        parApId.ParameterName = "@AppointmentId";
                        parApId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parApId);

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                    return;
                }
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    IDbDataParameter parAppointmentId = cmd.CreateParameter();
                    parAppointmentId.Direction = ParameterDirection.Input;
                    parAppointmentId.Value = ClinicalComponent.AppointmentId;
                    parAppointmentId.ParameterName = "@AppointmentId";
                    parAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentId);

                    IDbDataParameter parAppointmentDate = cmd.CreateParameter();
                    parAppointmentDate.Direction = ParameterDirection.Input;
                    parAppointmentDate.Value = ClinicalComponent.AppointmentDate;
                    parAppointmentDate.ParameterName = "@AppointmentDate";
                    parAppointmentDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parAppointmentDate);

                    IDbDataParameter parConceptId = cmd.CreateParameter();
                    parConceptId.Direction = ParameterDirection.Input;
                    parConceptId.Value = objRow[4].ToString();//((System.Data.DataRowView)objRow[index]).Row.ItemArray[4].ToString(); ;
                    parConceptId.ParameterName = "@ConceptId";
                    parConceptId.DbType = DbType.String;
                    cmd.Parameters.Add(parConceptId);

                    IDbDataParameter parTerm = cmd.CreateParameter();
                    parTerm.Direction = ParameterDirection.Input;
                    parTerm.Value = objRow[5].ToString();
                    parTerm.ParameterName = "@Term";
                    parTerm.DbType = DbType.String;
                    cmd.Parameters.Add(parTerm);

                    IDbDataParameter parICD10 = cmd.CreateParameter();
                    parICD10.Direction = ParameterDirection.Input;
                    parICD10.Value = objRow[6].ToString();
                    parICD10.ParameterName = "@ICD10";
                    parICD10.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10);

                    IDbDataParameter parICD10DESCR = cmd.CreateParameter();
                    parICD10DESCR.Direction = ParameterDirection.Input;
                    parICD10DESCR.Value = objRow[7].ToString();
                    parICD10DESCR.ParameterName = "@ICD10DESCR";
                    parICD10DESCR.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10DESCR);

                    IDbDataParameter parResourceId = cmd.CreateParameter();
                    parResourceId.Direction = ParameterDirection.Input;
                    parResourceId.Value = ClinicalComponent.UserId;
                    parResourceId.ParameterName = "@ResourceId";
                    parResourceId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parResourceId);

                    IDbDataParameter parStatus = cmd.CreateParameter();
                    parStatus.Direction = ParameterDirection.Input;
                    if (Convert.ToString(objRow["Status"]).ToLower().Contains("active"))
                    {
                        parStatus.Value = "Active";
                    }
                    else
                    {
                        parStatus.Value = "Resolved";
                    }

                    parStatus.ParameterName = "@Status";
                    parStatus.DbType = DbType.String;

                    if (parStatus.Value.ToString() == "Resolved")
                    {
                        IDbDataParameter parResolvedDate = cmd.CreateParameter();
                        parResolvedDate.Direction = ParameterDirection.Input;
                        parResolvedDate.Value = DateTime.Now.ToString();
                        parResolvedDate.ParameterName = "@ResolvedDate";
                        parResolvedDate.DbType = DbType.String;
                        cmd.Parameters.Add(parResolvedDate);
                    }

                    cmd.Parameters.Add(parStatus);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();

                    query = "update model.PatientReconcileProblems set status =1,MergedDate=Getdate(),AppointmentId=@AppointmentId where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Convert.ToInt64(objRow[0].ToString());
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    IDbDataParameter parApId = cmd.CreateParameter();
                    parApId.Direction = ParameterDirection.Input;
                    parApId.Value = appointmentId;
                    parApId.ParameterName = "@AppointmentId";
                    parApId.DbType = DbType.Int64;

                    cmd.Parameters.Add(parApId);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }

        public void AddMedicationListchk(Int64 patientId, CheckedListBox objCheckedListBox, int index)
        {
            try
            {
                String query = "INSERT INTO [dbo].[PatientClinical]([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom],[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status],[DrawFileName],[Highlights],[FollowUp]";
                query += ",[PermanentCondition],[PostOpPeriod],[Surgery],[Activity],[LegacyClinicalDataSourceTypeId])";
                query += "VALUES(@AppointmentId,@PatientId,'A','OU','3',@FindingDetail,@ImageDescriptor,'','A','','','','',0,'','',NULL)";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@PatientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    IDbDataParameter parAppointmentId = cmd.CreateParameter();
                    parAppointmentId.Direction = ParameterDirection.Input;
                    parAppointmentId.Value = ClinicalComponent.AppointmentId;
                    parAppointmentId.ParameterName = "@AppointmentId";
                    parAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentId);


                    IDbDataParameter parFindingDetail = cmd.CreateParameter();
                    parFindingDetail.Direction = ParameterDirection.Input;
                    string[] medicationLst = ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[2].ToString().Split(',').ToArray();
                    if (!string.IsNullOrEmpty(medicationLst[0]))
                    {
                        Medications objMedications = GetMedicationInfo(medicationLst[0].Trim());
                        if (string.IsNullOrEmpty(objMedications.MED_ROUTED_DF_MED_ID_DESC))
                        {
                            return;
                        }
                        if (string.IsNullOrEmpty(objMedications.MED_STRENGTH))
                        {
                            objMedications.MED_STRENGTH = string.Empty;
                        }
                        // strng space mg 
                        parFindingDetail.Value = "RX-8/" + objMedications.MED_ROUTED_DF_MED_ID_DESC.ToUpper() + "-2/()(" + Convert.ToString(objMedications.MED_STRENGTH) + ")()-3/(" + ((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[3].ToString() + ")()()()()()-4/()()()()-5/()()-6/";
                    }
                    else
                    {
                        return;
                    }
                    parFindingDetail.ParameterName = "@FindingDetail";
                    parFindingDetail.DbType = DbType.String;
                    cmd.Parameters.Add(parFindingDetail);

                    IDbDataParameter parImageDescriptor = cmd.CreateParameter();
                    parImageDescriptor.Direction = ParameterDirection.Input;
                    parImageDescriptor.Value = "!" + Convert.ToDateTime(ClinicalComponent.AppointmentDate).ToString("MM/dd/yyyy");
                    parImageDescriptor.ParameterName = "@ImageDescriptor";
                    parImageDescriptor.DbType = DbType.String;
                    cmd.Parameters.Add(parImageDescriptor);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();

                    query = "update model.PatientReconcileMedications set status =1 where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Convert.ToInt32(((System.Data.DataRowView)objCheckedListBox.Items[index]).Row.ItemArray[0].ToString());
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }

        public void AddMedicationList(Int64 patientId, DataRow dr, int index, string clinicalId, Int64 appointmentId)
        {
            string drawFileName = string.Empty;
            try
            {
                String query = "INSERT INTO [dbo].[PatientClinical]([AppointmentId],[PatientId],[ClinicalType],[EyeContext],[Symptom],[FindingDetail],[ImageDescriptor],[ImageInstructions],[Status],[DrawFileName],[Highlights],[FollowUp]";
                query += ",[PermanentCondition],[PostOpPeriod],[Surgery],[Activity],[LegacyClinicalDataSourceTypeId])";
                query += "VALUES(@AppointmentId,@PatientId,'A','OU','3',@FindingDetail,@ImageDescriptor,'',@Status,@DrawFileName,'','','',0,'','',NULL)";

                if (Convert.ToString(dr[3]) == string.Empty)
                {
                    MessageBox.Show("Insufficient Data. Unable to Merge.");
                    return;
                }
                using (DataTable dtDrawFileName = GetMedicationDrawFileName(Convert.ToString(dr[3])))
                {
                    if (dtDrawFileName.Rows.Count > 0)
                    {
                        drawFileName = Convert.ToString(dtDrawFileName.Rows[0][0]);
                    }
                    else
                    {
                        MessageBox.Show("Insufficient Data. Unable to Merge.");
                        return;
                    }
                }

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@PatientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    IDbDataParameter parAppointmentId = cmd.CreateParameter();
                    parAppointmentId.Direction = ParameterDirection.Input;
                    parAppointmentId.Value = ClinicalComponent.AppointmentId;
                    parAppointmentId.ParameterName = "@AppointmentId";
                    parAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentId);


                    IDbDataParameter parFindingDetail = cmd.CreateParameter();
                    parFindingDetail.Direction = ParameterDirection.Input;
                    if (Convert.ToString(dr["Instructions"]).ToLower().Contains("0.09 MG/ACTUAT inhalant powder, 2 puffs".ToLower()))
                    {
                        dr["Instructions"] = "2 puffs once";
                    }
                    parFindingDetail.Value = "RX-8/" + Convert.ToString(dr["Medication"]).ToUpper() + "-2/()(" + Convert.ToString(dr["MED_STRENGTH"]) + ")()-3/(" + Convert.ToString(dr["Instructions"]) + ")()()()()()-4/()()()()-5/()(" + "" + ")-6/";

                    parFindingDetail.ParameterName = "@FindingDetail";
                    parFindingDetail.DbType = DbType.String;
                    cmd.Parameters.Add(parFindingDetail);

                    IDbDataParameter parImageDescriptor = cmd.CreateParameter();
                    parImageDescriptor.Direction = ParameterDirection.Input;
                    parImageDescriptor.Value = "!" + Convert.ToDateTime(ClinicalComponent.AppointmentDate).ToString("MM/dd/yyyy");
                    parImageDescriptor.ParameterName = "@ImageDescriptor";
                    parImageDescriptor.DbType = DbType.String;
                    cmd.Parameters.Add(parImageDescriptor);

                    IDbDataParameter parStatus = cmd.CreateParameter();
                    parStatus.Direction = ParameterDirection.Input;
                    if (Convert.ToString(dr["Status"]).ToLower().Contains("active"))
                    {
                        parStatus.Value = "A";
                    }
                    else
                    {
                        parStatus.Value = "D";
                    }
                    parStatus.ParameterName = "@Status";
                    parStatus.DbType = DbType.String;
                    cmd.Parameters.Add(parStatus);

                    IDbDataParameter parDrawFileName = cmd.CreateParameter();
                    parDrawFileName.Direction = ParameterDirection.Input;
                    parDrawFileName.Value = drawFileName.Trim();
                    parDrawFileName.ParameterName = "@DrawFileName";
                    parDrawFileName.DbType = DbType.String;
                    cmd.Parameters.Add(parDrawFileName);


                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();

                    if (Convert.ToString(clinicalId) != string.Empty)
                    {
                        query = "update dbo.patientclinical set status ='D'  where clinicalId=@clinicalId";
                        cmd.Parameters.Clear();
                        IDbDataParameter parclinicalId = cmd.CreateParameter();
                        parclinicalId.Direction = ParameterDirection.Input;
                        parclinicalId.Value = Convert.ToInt64(clinicalId);
                        parclinicalId.ParameterName = "@clinicalId";
                        parclinicalId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parclinicalId);
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }

                    query = "update model.PatientReconcileMedications set status =1,MergedDate=Getdate(),AppointmentId=@AppointmentId where Id=@Id";
                    cmd.Parameters.Clear();
                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = Convert.ToInt32(dr["Id"].ToString());
                    parId.ParameterName = "@Id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    IDbDataParameter parApId = cmd.CreateParameter();
                    parApId.Direction = ParameterDirection.Input;
                    parApId.Value = appointmentId;
                    parApId.ParameterName = "@AppointmentId";
                    parApId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parApId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                MessageBox.Show("Unable to merge due to insufficient Data");
            }
        }

        public DataTable GetMedicationReconcileList(Int64 patientId)
        {
            DataTable dt = new DataTable();
            try
            {
                // String query = "SELECT Id, frequency+ '  '+DrugName As Medication,DrugName,frequency,startDate as LastModefiedDate,RxNorm   from model.PatientReconcileMedications where status=0 and  patientid=@patientId";
                String query = "select distinct A.MEDID,A.MED_ROUTED_DF_MED_ID_DESC AS Medication,A.MED_STRENGTH + A.MED_STRENGTH_UOM AS MED_STRENGTH,B.RXNorm,B.frequency,B.Id, B.StartDate AS LastModefiedDate,B.Instructions,UPPER(LEFT(B.MedicationStatus ,1))+LOWER(SUBSTRING(B.MedicationStatus ,2,LEN(B.MedicationStatus))) as Status,'1' as ExistingMedication  from  model.PatientReconcilemedications B ";
                query += " INNER JOIN dbo.FDBtoRxNorm1 C ON C.RxNorm =B.RxNorm ";
                query += " INNER JOIN fdb.tblcompositedrug A  ON C.FDBId=A.MEDID ";
                query += "WHERE B.PatientId =@PatientId AND B.Status =0";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@PatientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {
            }
            return dt;
        }

        public DataTable GetMedicationDrawFileName(string rxnormId)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "SELECT TOP 1 FDBID FROM dbo.FDBtoRxNorm1 WHERE rxnorm=@rxnormId";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parRxnormId = cmd.CreateParameter();
                    parRxnormId.Direction = ParameterDirection.Input;
                    parRxnormId.Value = rxnormId.TrimEnd().TrimStart();
                    parRxnormId.ParameterName = "@rxnormId";
                    parRxnormId.DbType = DbType.String;
                    cmd.Parameters.Add(parRxnormId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {
            }
            return dt;
        }

        public DataTable GetAllergyReconcileList(Int64 patientId)
        {
            DataTable dt = new DataTable();
            try
            {
                //String query = "select Id,AllergySubstance +',  '+ AllergyReaction as  AllergySubstance,AllergyReaction from model.PatientReconcileAllergy WHERE AllergySubstance <> '' and AllergyReaction <> '' and    [Status]=0 and patientid = @patientId";
                String query = "select Id,AllergySubstance  as  AllergySubstance,AllergyReaction as Reaction,AllergySeverity as Severity, ReactionDate as LastModefiedDate,RxNorm,AllergySubstance as Allergy,  UPPER(LEFT(AllergyStatus,1))+LOWER(SUBSTRING(AllergyStatus,2,LEN(AllergyStatus))) as Status,'1' as ExistingAllergy  from model.PatientReconcileAllergy WHERE AllergySubstance <> '' and AllergyReaction <> '' and    [Status]=0 and patientid  = @patientId";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {
            }
            return dt;
        }

        public void CallAlert(string SnomedCPT, String AlertType)
        {
            try
            {
                ClsAlerts objalert = new ClsAlerts();
                objalert.DBObject = ClinicalComponent._Connection;
                objalert.UserId = Convert.ToInt32(ClinicalComponent.UserId);
                objalert.SnomedCPT = SnomedCPT;
                objalert.MedDesc = SnomedCPT;
                objalert.nEncounterID = Convert.ToInt64(ClinicalComponent.AppointmentId);
                objalert.m_PatientID = Convert.ToInt64(ClinicalComponent.PatientId);
                objalert.InitializeRuleParamers(AlertType);
            }
            catch
            {
            }
        }

        public void CallAllergyAlert(string AllergyDesc)
        {
            try
            {
                ClsAlerts objalert = new ClsAlerts();
                objalert.DBObject = ClinicalComponent._Connection;
                objalert.UserId = Convert.ToInt32(ClinicalComponent.UserId);
                objalert.AllergyDesc = AllergyDesc;
                objalert.nEncounterID = Convert.ToInt64(ClinicalComponent.AppointmentId);
                objalert.m_PatientID = Convert.ToInt64(ClinicalComponent.PatientId);
                objalert.InitializeRuleParamers("MEDALLERGIES");
            }
            catch
            {
            }
        }

        public DataTable GetProbblemList(Int64 patientId)
        {
            DataTable dt = new DataTable();
            try
            {

                String query = "SELECT Id,ICD10DESCR as Problem, ConceptID as SnomedCode, LastModifyDate,  Status  from model.PatientProblemList where patientid=@PatientId and Status not like '%Del%'  order by 1 desc ";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@PatientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {

            }
            return dt;
        }

        public DataTable GetAllergyList(Int64 patientId)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "SELECT ClinicalId,CASE SUBSTRING(findingdetail, 1, 1)  WHEN '*' THEN CASE WHEN FindingDetail like '%=%' THEN SUBSTRING(findingdetail, 2, CHARINDEX('=', findingdetail) - 2) ELSE SUBSTRING(findingdetail, 2, LEN(findingdetail)-1) END ";
                query += "ELSE CASE WHEN FindingDetail like '%=%' THEN SUBSTRING(findingdetail, 1, CHARINDEX('=', findingdetail) - 1) ELSE SUBSTRING(findingdetail, 1, LEN(findingdetail)) END ";
                query += " END + case CHARINDEX('/', findingdetail) when  0  then 'None None' else SUBSTRING(FindingDetail,CHARINDEX('/', findingdetail),len(findingdetail)-CHARINDEX('/', findingdetail)) end + '   '  AS AllergyName, '                                         ' as Reaction,'                                 ' as Severity,findingdetail,'Active' as Status,'              ' as RxNorm, (Select Top(1) AuditDateTime From dbo.AuditEntries Where KeyValueNumeric = ClinicalId and ObjectName = 'dbo.PatientClinical') as LastModefiedDate,symptom   From PatientClinical Where ClinicalType in ('h', 'c') and Symptom like '%allerg%' and Status = 'a' AND SYMPTOM NOT LIKE '%?%' AND PatientId=@patientId";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {
            }
            return dt;
        }

        public DataTable GetMedicationRxNorms(string DrugNames)
        {
            DataTable dt = new DataTable();
            //String query = "Select DISTINCT LOWER(B.MED_ROUTED_DF_MED_ID_DESC) AS DrugName,A.RxNorm ";
            //query += " from  dbo.FDBtoRxNorm1  A  INNER JOIN fdb.RXCUI2MEDID C ON C.RxCUI =A.RxNorm ";
            //query += " INNER JOIN  fdb.tblcompositedrug B on C.MEDID=B.MEDID WHERE B.MED_ROUTED_DF_MED_ID_DESC IN (" + DrugNames + ")";

            String query = "Select DISTINCT LOWER(B.MED_ROUTED_DF_MED_ID_DESC) AS DrugName,A.RxNorm ";
            query += " from  dbo.FDBtoRxNorm1  A ";// INNER JOIN fdb.RXCUI2MEDID C ON C.RxCUI =A.RxNorm ";
            query += " INNER JOIN  fdb.tblcompositedrug B on A.FDBId=B.MEDID WHERE B.MED_ROUTED_DF_MED_ID_DESC IN (" + DrugNames + ")";
            try
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {

                        dt.Load(reader);
                    }

                }
            }
            catch
            {
            }
            return dt;
        }

        public DataTable GetMedicationList(Int64 patientId)
        {
            DataTable dt = new DataTable();//' here 
            String query = "Select ClinicalId, CASE WHEN FindingDetail like 'RX-8/%-2/%' THEN SUBSTRING(findingdetail, 6, charindex('-2/', findingdetail) - 6) ELSE '' END +'   ' +" +
                "Substring(pc.findingdetail, Charindex('-3/',pc.findingdetail)+4,(charindex(')',(REPLACE(SUBSTRING(pc.findingdetail, CHARINDEX('-3/', pc.findingdetail), LEN(pc.findingdetail)), '-3/', '')))-2)) AS DrugName, FindingDetail, Case When Status like 'A' then 'Active' Else 'Completed' End AS Status, (Select Top(1) AuditDateTime From dbo.AuditEntries Where KeyValueNumeric = ClinicalId and ObjectName = 'dbo.PatientClinical') as LastModefiedDate, '              ' as RxNorm From PatientClinical pc Where Status = 'A' and ClinicalType = 'A' and substring(FindingDetail, 1, 2) = 'RX'  AND PatientId= @patientId";
            try
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parPatientId = cmd.CreateParameter();
                    parPatientId.Direction = ParameterDirection.Input;
                    parPatientId.Value = patientId;
                    parPatientId.ParameterName = "@patientId";
                    parPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatientId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {
            }
            return dt;
        }

        public Boolean CanDisplayAlert()
        {
            Boolean CanDisplay = false;
            String query = "SELECT R.Alerts FROM dbo.CDSROLES R  inner join dbo.Resources RS on R.ResourceType=RS.ResourceType where RS.ResourceID=@ResourceID";
            try
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parResourceID = cmd.CreateParameter();
                    parResourceID.Direction = ParameterDirection.Input;
                    parResourceID.Value = ClinicalComponent.UserId;
                    parResourceID.ParameterName = "@ResourceID";
                    parResourceID.DbType = DbType.Int64;
                    cmd.Parameters.Add(parResourceID);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return CanDisplay = Convert.ToBoolean(Convert.ToString(reader["Alerts"]));
                        }
                    }
                }
            }
            catch
            {
            }
            return CanDisplay;
        }
    }
}
