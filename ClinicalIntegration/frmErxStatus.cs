﻿using DBHelper;
using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmErxStatus : Form
    {
        public Object IDBCon { get; set; }
        public string PatientId { get; set; }
        public string SSOContext { get; set; }
        public frmErxStatus()
        {
            InitializeComponent();
        }

        private void axWebBrowser1_NewWindow2(object sender, AxSHDocVw.DWebBrowserEvents2_NewWindow2Event e)
        {
            PopUp popUpForm = new PopUp();
            popUpForm.axWebBrowser1.RegisterAsBrowser = true;
            e.ppDisp = popUpForm.axWebBrowser1.Application;
            popUpForm.Visible = true;
        }

        private void frmErxStatus_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = IDBCon;
            IDbConnection con = db.DbObject as IDbConnection;
            ErxComponent.InitializeErxComponent(con);
            string ssoURL = string.Empty, uploadUrl = string.Empty, downLoadUrl = string.Empty, portalHdr = string.Empty;
            ssoURL = ErxComponent.StagingSSOUrl;
            uploadUrl = ErxComponent.StagingUploadUrl;
            downLoadUrl = ErxComponent.StagingDownloadUrl;
            portalHdr = ErxComponent.StagingPortalHeader;
            int userId = 2;
            if (SSOContext == "message")
                this.Text = "Erx Message";
            else
                this.Text = "Erx Reports";
           
            DateTimeOffset utc_offset = DateTime.UtcNow.AddMinutes(03);
            portalHdr = portalHdr.Replace("<patientId>", string.Empty).Replace("<time>", utc_offset.ToString("MMddyyHHmmss")).Replace("<screencontext>", SSOContext).Replace("<n>", "y");    
            string hash = BitConverter.ToString(new System.Security.Cryptography.MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(portalHdr + ErxComponent.VendorPassword))).Replace("-", "").ToUpper();
            string URL = ssoURL + "?" + portalHdr + "&MAC=" + hash;
            if (!string.IsNullOrEmpty(URL))
            {
                axWebBrowser1.Dock = DockStyle.Fill;
                axWebBrowser1.Navigate(URL);
                int apiType = (int)ApiActionType.LaunchSSOReport;
                LogApiCall log = new LogApiCall();
                log.LogErxActivity(URL, PatientId, userId.ToString(), DateTime.UtcNow, string.Empty, string.Empty, string.Empty, apiType, con, false, false, ErxComponent.RCopiaPatientId);
            }
        }

        private void frmErxStatus_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
