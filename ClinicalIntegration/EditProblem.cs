﻿using System;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class EditProblem : Form
    {

        #region Properties
        public Form ParentMDI { get; set; }

        public ProblemModel objEditProb;
        public bool SetValueForAddUpdate { get; set; }        

        public bool IsNew { get; set; }

        #endregion

        #region  Form Load

        public EditProblem()
        {
            InitializeComponent();
        }

        private void EditProblem_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillStatusCB();
            FillIcdAndSnomedDeatils();
            btnSubmit.TabStop = false;
            btnSubmit.FlatStyle = FlatStyle.Flat;
            btnSubmit.FlatAppearance.BorderSize = 0;
            button2.TabStop = false;
            button2.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
        }

        #endregion

        #region  Events

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (GetFormValidate())
            {
                if (Convert.ToString(cmbStatus.SelectedValue)!=string.Empty ||Convert.ToString(cmbStatus.SelectedValue).ToUpper()!="SELECT")
                {
                    ProblemModel objDiagnosisList = new ProblemModel();

                    objDiagnosisList.Id = objEditProb.Id;
                    objDiagnosisList.AppointmentID = Convert.ToInt64(ClinicalComponent.AppointmentId);
                    objDiagnosisList.PatientId = Convert.ToInt64(ClinicalComponent.PatientId);
                    objDiagnosisList.AppointmentDate = dtp_diagnosis.Text;
                    objDiagnosisList.ICD10 = objEditProb.ICD10;
                    objDiagnosisList.ICD10DESCR = objEditProb.ICD10DESCR.Replace('"', ' ');
                    objDiagnosisList.ConceptID = objEditProb.ConceptID;
                    objDiagnosisList.Term = objEditProb.Term.Replace('"', ' ');
                    objDiagnosisList.ResourceId = Convert.ToInt32(ClinicalComponent.UserId);
                    objDiagnosisList.Status = cmbStatus.Text;
                    objDiagnosisList.EnteredDate = DateTime.Now;
                    objDiagnosisList.OnsetDate = Convert.ToDateTime(dtp_Onset.Text);
                    objDiagnosisList.LastModofiedDate = DateTime.Now;


                    Problems objdata = new Problems();
                    objdata.SaveProblemList(IsNew, objDiagnosisList);

                    frmProblemList frmGI = new frmProblemList();
                    frmGI.ParentMDI = ParentMDI;
                    frmGI.MdiParent = ParentMDI;
                    frmGI.Show();
                    this.Close();
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Please select 'Status'", "Error");
                }
            }
        }

        private void btnIcd_Click(object sender, EventArgs e)
        {           
            frmDiagnosisSnomed diagnosifinder = new frmDiagnosisSnomed();
            diagnosifinder.EditNew = IsNew;
            if(objEditProb!=null)
            diagnosifinder.PPLId = objEditProb.Id;
            diagnosifinder.OnsetDate = DateTime.Parse(dtp_Onset.Text);
            diagnosifinder.DiagDate = DateTime.Parse(dtp_diagnosis.Text);
            diagnosifinder.ProbStatus = cmbStatus.Text;
            diagnosifinder.MdiParent = this.ParentMDI;
            diagnosifinder.ParentMDI = this.ParentMDI;
            diagnosifinder.Show();
            this.Close();
                      

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
            frmProblemList frmp = new frmProblemList();
            frmp.MdiParent = this.ParentMDI;
            frmp.ParentMDI = this.ParentMDI;
            frmp.Show();
        }

        #endregion

        #region  Methods
        private void FillStatusCB()
        {
            cmbStatus.Items.Clear();
            //cmbStatus.Items.Insert(0, "Select");
            cmbStatus.Items.Insert(0, "Active");
            cmbStatus.Items.Insert(1, "Inactive");
            cmbStatus.Items.Insert(2, "Resolved");
            cmbStatus.SelectedIndex = 0;
        }

        private void FillIcdAndSnomedDeatils()
        {

            if(objEditProb != null)
            {
                txt_Icd10.Text = objEditProb.ICD10 + " /" + objEditProb.ICD10DESCR;
                txt_snomed.Text = objEditProb.ConceptID + " /" + objEditProb.Term;
                cmbStatus.SelectedItem = objEditProb.Status;
                dtp_diagnosis.Text = objEditProb.AppointmentDate;
                dtp_Onset.Text = objEditProb.OnsetDate.ToShortDateString();

            }
            else if (IsNew)
            {
                txt_Icd10.Text = "";
                txt_snomed.Text = "";
                dtp_diagnosis.Text = ClinicalComponent.AppointmentDate;
                //EditProblem eprob = new EditProblem();
                //IsNew = eprob.IsNew;
            }
        }

        private bool GetFormValidate()
        {
            bool isFlag = true;
            if (string.IsNullOrEmpty(txt_Icd10.Text))
            {
                isFlag = false;
                MessageBox.Show("Please select 'ICD 10/Snomed CT'", "Error");

            }
            else if (string.IsNullOrEmpty(txt_snomed.Text))
            {
                isFlag = false;
                MessageBox.Show("Please select 'ICD 10/Snomed CT'", "Error");

            }
            return isFlag;
        }

        #endregion

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
