﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmLabTestResults : Form
    {
        public Form ParentMDI { get; set; }

        public string OrderId { get; set; }

        public frmLabTestResults()
        {
            InitializeComponent();
        }

        private void LoadTestResultDetails(DataTable objDataTable)
        {
            bool alterColor = true;
            int CoorindateY = 29;
            foreach (DataRow dr in objDataTable.Rows)
            {
                Panel PanelOrder = new Panel();
                PanelOrder.Width = 948;
                PanelOrder.Height = 28;
                PanelOrder.Location = new Point(0, CoorindateY);
                PanelOrder.Size = new Size(921, 28);
                if (alterColor)
                {
                    PanelOrder.BackColor = Color.White;
                    alterColor = false;
                }
                else
                {
                    PanelOrder.BackColor = Color.WhiteSmoke;
                    alterColor = true;
                }

                Label LblResultName = new Label();
                LblResultName.Text = dr["Col1"].ToString();
                LblResultName.Name = "LblResultName" + Guid.NewGuid().ToString().Substring(0, 4);
                LblResultName.Tag = dr["Col2"].ToString();
                LblResultName.Location = new Point(11, 8);
                LblResultName.Size = new Size(327, 23);
                ToolTip tooltipResult = new ToolTip();
                tooltipResult.SetToolTip(LblResultName, dr["Col1"].ToString());
                PanelOrder.Controls.Add(LblResultName);

                TextBox txtResult = new TextBox();
                txtResult.Name = "txtResult" + Guid.NewGuid().ToString().Substring(0, 4);
                txtResult.Text = dr["Col3"].ToString();
                txtResult.Location = new Point(338, 7);
                txtResult.Size = new Size(150, 22);
                txtResult.MaxLength = 200;
                PanelOrder.Controls.Add(txtResult);

                TextBox txtUnit = new TextBox();
                txtUnit.Name = "txtUnit" + Guid.NewGuid().ToString().Substring(0, 4);
                txtUnit.Text = dr["Col4"].ToString();
                txtUnit.Location = new Point(521, 7);
                txtUnit.Size = new Size(70, 20);
                txtUnit.MaxLength = 200;
                PanelOrder.Controls.Add(txtUnit);

                TextBox txtMinRange = new TextBox();
                txtMinRange.Name = "txtMinRange" + Guid.NewGuid().ToString().Substring(0, 4);
                txtMinRange.Text = dr["Col5"].ToString();
                txtMinRange.Location = new Point(605, 7);
                txtMinRange.Size = new Size(70, 20);
                txtMinRange.MaxLength = 200;
                PanelOrder.Controls.Add(txtMinRange);

                TextBox txtMaxRange = new TextBox();
                txtMaxRange.Name = "txtMaxRange" + Guid.NewGuid().ToString().Substring(0, 4);
                txtMaxRange.Text = dr["Col6"].ToString();
                txtMaxRange.Location = new Point(697, 7);
                txtMaxRange.Size = new Size(70, 20);
                txtMaxRange.MaxLength = 200;
                PanelOrder.Controls.Add(txtMaxRange);

                TextBox txtRange = new TextBox();
                txtRange.Name = "txtRange" + Guid.NewGuid().ToString().Substring(0, 4);
                txtRange.Text = dr["Col7"].ToString();
                txtRange.Location = new Point(791, 7);
                txtRange.Size = new Size(70, 20);
                txtRange.MaxLength = 200;
                PanelOrder.Controls.Add(txtRange);

                panel1.Controls.Add(PanelOrder);
                CoorindateY += 29;
            }
        }

        private void frmLabTestResults_Load(object sender, EventArgs e)
        {
            LabOrder objLabOrder = new LabOrder();
            DataTable dtTestResult = objLabOrder.GetPatientLabOrderTestResults(this.OrderId);
            LoadTestResultDetails(dtTestResult);
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool excludeFirstPanel = false;
            foreach (Control panel in panel1.Controls)
            {
                string _recordId = "";
                string _resultContent = "";
                string _unit = "";
                string _minRange = "";
                string _maxRange = "";
                string _range = "";
                if (excludeFirstPanel)
                {
                    foreach (Control panelControls in panel.Controls)
                    {
                        if (panelControls.Name.Contains("LblResultName"))
                            _recordId = panelControls.Tag.ToString();
                        else if (panelControls.Name.Contains("txtResult"))
                            _resultContent = panelControls.Text.ToString();
                        else if (panelControls.Name.Contains("txtUnit"))
                            _unit = panelControls.Text.ToString();
                        else if (panelControls.Name.Contains("txtMinRange"))
                            _minRange = panelControls.Text.ToString();
                        else if (panelControls.Name.Contains("txtMaxRange"))
                            _maxRange = panelControls.Text.ToString();
                        else if (panelControls.Name.Contains("txtRange"))
                            _range = panelControls.Text.ToString();
                    }
                    LabOrderTestResultModel objLabOrderTestResultModel = new LabOrderTestResultModel()
                    {
                        RecordId = _recordId,
                        MaxRange = _maxRange,
                        MinRange = _minRange,
                        Unit = _unit,
                        Range = _range,
                        Result = _resultContent
                    };
                    objLabOrderTestResultModel.UpdateLabResultDetails();
                }
                excludeFirstPanel = true;
            }
            frmLabOrders frm = new frmLabOrders();
            frm.ParentMDI = this.ParentMDI;
            frm.MdiParent = this.ParentMDI;
            frm.Show();
            this.Close();
        }
    }
}
