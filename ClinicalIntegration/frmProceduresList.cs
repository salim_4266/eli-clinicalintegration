﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmProceduresList : Form
    {
        public Form ParentMDI { get; set; }
        bool IsEnabled { get; set; }

        public frmProceduresList()
        {
            InitializeComponent();
            btnEdit.Enabled = false;
        }
        private void grdProcedure_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void frmProceduresList_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            CmbFilter.SelectedItem = "Active";
            Procedures objProcedure = new Procedures();
            grdProcedure.DataSource = objProcedure.GetProceduresList("'Active'").DefaultView;
            grdProcedure.AutoGenerateColumns = false;
            grdProcedure.ReadOnly = true;
            grdProcedure.RowTemplate.Height = 32;

            grdProcedure.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            grdProcedure.EnableHeadersVisualStyles = false;
            grdProcedure.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            grdProcedure.EnableHeadersVisualStyles = false;
            grdProcedure.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            grdProcedure.ColumnHeadersHeight = 25;
            grdProcedure.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;

            btnEdit.TabStop = false;
            btnEdit.FlatStyle = FlatStyle.Flat;
            btnEdit.FlatAppearance.BorderSize = 0;
            btnNew.TabStop = false;
            btnNew.FlatStyle = FlatStyle.Flat;
            btnNew.FlatAppearance.BorderSize = 0;
            btnClose.TabStop = false;
            btnClose.FlatStyle = FlatStyle.Flat;
            btnClose.FlatAppearance.BorderSize = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (IsEnabled)
            {

                string[] ContainerList = (btnEdit).Tag.ToString().Split('#');
                if (ContainerList.Count() == 7)
                {
                    frmEditProcedures frmGI = new frmEditProcedures();
                    frmGI.GoalId = Convert.ToInt64(ContainerList[0]);
                    frmGI.SnomedCode = Convert.ToInt64(ContainerList[1]);
                    frmGI.SnomedDesc = ContainerList[2];
                    frmGI.ProcedureDesc = ContainerList[3];

                    frmGI.Health = ContainerList[4];
                    frmGI.Reason = ContainerList[5];
                    frmGI.Active = ContainerList[6];

                    frmGI.MdiParent = this.ParentMDI;
                    frmGI.ParentMDI = this.ParentMDI;
                    frmGI.Show();
                    this.Close();
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.Close();
            frmEditProcedures fs = new frmEditProcedures();
            fs.IsNew = true;
            fs.MdiParent = this.ParentMDI;
            fs.ParentMDI = this.ParentMDI;
            fs.Show();
        }

        private void grdProcedure_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            IsEnabled = true;
            btnEdit.Enabled = true;
            if (e.RowIndex > -1)
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = grdProcedure.Rows[rowIndex];
                btnEdit.Tag = int.Parse(row.Cells[0].Value.ToString()) + "#" + grdProcedure.Rows[rowIndex].Cells[3].Value.ToString() + "#" + grdProcedure.Rows[rowIndex].Cells[4].Value.ToString() + "#" + grdProcedure.Rows[rowIndex].Cells[5].Value.ToString() + "#" + grdProcedure.Rows[rowIndex].Cells[6].Value.ToString() + "#" + grdProcedure.Rows[rowIndex].Cells[7].Value.ToString() + "#" + grdProcedure.Rows[rowIndex].Cells[8].Value.ToString();
            }

        }

        private void CmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";
            if (CmbFilter.SelectedItem.ToString() == "Active")
                _Status = "'Active'";
            else if (CmbFilter.SelectedItem.ToString() == "Inactive")
                _Status = "'Inactive'";
            else
                _Status = "'Active','Inactive'";
            Procedures objprocFilter = new Procedures();
            grdProcedure.DataSource = objprocFilter.GetProceduresList(_Status).DefaultView;

        }
    }
}
