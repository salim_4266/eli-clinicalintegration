﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    internal class DrFirstConfiguration
    {
        public string ConfigurationId { get; set; }
        public string Version { get; set; }
        public string VendorName { get; set; }
        public string VendorPassword { get; set; }
        public string SystemName { get; set; }
        public string PracticeUserName { get; set; }
        public string UploadUrl { get; set; }
        public string DownloadUrl { get; set; }
        public string SSOUrl { get; set; }

        public DrFirstConfiguration()
        {
            DataTable dtTempTable = new DataTable();
            string selectQry = "Select Top(1) Id, Version, VendorName, VendorPassword, SystemName, PracticeUserName, UploadUrl, DownloadUrl, SSOUrl From DrFirst.PracticeConfiguration with(nolock) Where IsActive = '1'";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTempTable.Load(reader);
                    if (dtTempTable.Rows.Count > 0)
                    {
                        ConfigurationId = dtTempTable.Rows[0]["Id"].ToString();
                        Version = dtTempTable.Rows[0]["Version"].ToString();
                        VendorName = dtTempTable.Rows[0]["VendorName"].ToString();
                        VendorPassword = dtTempTable.Rows[0]["VendorPassword"].ToString();
                        SystemName = dtTempTable.Rows[0]["SystemName"].ToString();
                        PracticeUserName = dtTempTable.Rows[0]["PracticeUserName"].ToString();
                        UploadUrl = dtTempTable.Rows[0]["UploadUrl"].ToString();
                        DownloadUrl = dtTempTable.Rows[0]["DownloadUrl"].ToString();
                        SSOUrl = dtTempTable.Rows[0]["SSOUrl"].ToString();
                    }
                }
                ClinicalComponent._Connection.Close();
            }
        }

        public int Save()
        {
            int returnStatus = 0;
            string insertQry = "Update DrFirst.PracticeConfiguration Set Version = '" + this.Version + "', VendorName = '" + this.VendorName + "', VendorPassword = '" + this.VendorPassword + "', SystemName = '" + this.SystemName + "', PracticeUserName = '" + this.PracticeUserName + "', UploadUrl = '" + this.UploadUrl + "', DownloadUrl = '" + this.DownloadUrl + "', SSOUrl = '" + this.SSOUrl + "', ModifiedDate = GETDATE() Where Id = '" + ConfigurationId + "'";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandText = insertQry;
                cmd.CommandType = CommandType.Text;
                returnStatus = cmd.ExecuteNonQuery();
            }
            return returnStatus;
        }
    }

}
