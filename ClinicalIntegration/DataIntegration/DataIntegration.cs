﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    public class DataIntegration
    {
        public DataTable GetProviders(string userId)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "Select Id, DisplayName from model.users with (nolock) Where displayname not like '' and Displayname is not null and IsArchived = 0 and Id not in (" + userId.Trim() + ") and __EntityType__ ='Doctor' ";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {

            }
            return dt;
        }

        public DataTable GetHashcodeIntegrityById(Int64 Id)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "SELECT ID, FromProvider, ToProvider, FileName, Hashcode, FileNamexml, FileByteAry From [dbo].[HashcodeIntegrity] with (nolock) Where ID =" + Id;
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {

            }
            return dt;
        }

        public DataTable GetReceivedData(string userId)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "SELECT ID, FileName FROM [dbo].[HashcodeIntegrity] with (nolock) where ISNULL(IsArchieve,0) = 0 and ToProvider=" + userId.Trim() + " order by 1 desc";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch
            {

            }
            return dt;
        }

        public void SendData(string frmProviderId, string toProviderId, string plainText, String hashCode, string fileName, byte[] fileByteAry)
        {
            try
            {
                String query = "INSERT INTO [dbo].[HashcodeIntegrity] VALUES (@frmProviderId,@toProviderId,@fileName,@hashCode,@fileNameXml,GETDATE(),0,@FileByteAry)";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter parFromProviderId = cmd.CreateParameter();
                    parFromProviderId.Direction = ParameterDirection.Input;
                    parFromProviderId.Value = ClinicalComponent.UserId;
                    parFromProviderId.ParameterName = "@frmProviderId";
                    parFromProviderId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parFromProviderId);

                    IDbDataParameter parToProviderId = cmd.CreateParameter();
                    parToProviderId.Direction = ParameterDirection.Input;
                    parToProviderId.Value = toProviderId;
                    parToProviderId.ParameterName = "@toProviderId";
                    parToProviderId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parToProviderId);

                    IDbDataParameter parHashCode = cmd.CreateParameter();
                    parHashCode.Direction = ParameterDirection.Input;
                    parHashCode.Value = hashCode;
                    parHashCode.ParameterName = "@hashCode";
                    parHashCode.DbType = DbType.String;
                    cmd.Parameters.Add(parHashCode);

                    IDbDataParameter parFileNameXml = cmd.CreateParameter();
                    parFileNameXml.Direction = ParameterDirection.Input;
                    parFileNameXml.Value = plainText;
                    parFileNameXml.ParameterName = "@fileNameXml";
                    parFileNameXml.DbType = DbType.String;
                    cmd.Parameters.Add(parFileNameXml);

                    IDbDataParameter parFileName = cmd.CreateParameter();
                    parFileName.Direction = ParameterDirection.Input;
                    parFileName.Value = fileName;
                    parFileName.ParameterName = "@fileName";
                    parFileName.DbType = DbType.String;
                    cmd.Parameters.Add(parFileName);


                    IDbDataParameter parfileByteAry = cmd.CreateParameter();
                    parfileByteAry.Direction = ParameterDirection.Input;
                    parfileByteAry.Value = fileByteAry;
                    parfileByteAry.ParameterName = "@fileByteAry";
                    parfileByteAry.DbType = DbType.Binary;
                    cmd.Parameters.Add(parfileByteAry);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }

            }
            catch
            {

            }

        }
    }
}
