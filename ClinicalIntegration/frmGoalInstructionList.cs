﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmGoalInstructionList : Form
    {
        public Form ParentMDI { get; set; }
        bool IsEnabled { get; set; }
        public frmGoalInstructionList()
        {
            InitializeComponent();
            btnEdit.Enabled = false;
        }

        private void frmGoalInstructionList_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            CmbFilter.SelectedItem = "Active";
            Goal.Goal objGoal = new Goal.Goal();
            grdGoal.DataSource = objGoal.GetGoalList("'Active'").DefaultView;
            grdGoal.RowTemplate.Height = 32;
            grdGoal.AutoGenerateColumns = false;
            grdGoal.ReadOnly = true;

            grdGoal.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            grdGoal.EnableHeadersVisualStyles = false;
            grdGoal.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            grdGoal.EnableHeadersVisualStyles = false;
            grdGoal.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            grdGoal.ColumnHeadersHeight = 25;
            grdGoal.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
            grdGoal.ClearSelection();

            btnEdit.TabStop = false;
            btnEdit.FlatStyle = FlatStyle.Flat;
            btnEdit.FlatAppearance.BorderSize = 0;
            btnNew.TabStop = false;
            btnNew.FlatStyle = FlatStyle.Flat;
            btnNew.FlatAppearance.BorderSize = 0;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.Close();
            frmGoalInstruction fs = new frmGoalInstruction();
            fs.IsNew = true;
            fs.MdiParent = this.ParentMDI;
            fs.ParentMDI = this.ParentMDI;
            fs.Show();

        }

        private void grdGoal_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            IsEnabled = true;
            btnEdit.Enabled = true;
            if (e.RowIndex > -1)
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = grdGoal.Rows[rowIndex];
                btnEdit.Tag = int.Parse(row.Cells[0].Value.ToString()) + "#" + grdGoal.Rows[rowIndex].Cells[3].Value.ToString() + "#" + grdGoal.Rows[rowIndex].Cells[4].Value.ToString() + "#" + grdGoal.Rows[rowIndex].Cells[5].Value.ToString() + "#" + grdGoal.Rows[rowIndex].Cells[6].Value.ToString() + "#" + grdGoal.Rows[rowIndex].Cells[7].Value.ToString() + "#" + grdGoal.Rows[rowIndex].Cells[8].Value.ToString();
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void CmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";
            if (CmbFilter.SelectedItem.ToString() == "Active")
                _Status = "'Active'";
            else if (CmbFilter.SelectedItem.ToString() == "Inactive")
                _Status = "'Inactive'";
            else
                _Status = "'Active','Inactive'";
            Goal.Goal objGoalFilter = new Goal.Goal();
            grdGoal.DataSource = objGoalFilter.GetGoalList(_Status).DefaultView;

        }

        private void grdGoal_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (IsEnabled)
            {

                string[] ContainerList = (btnEdit).Tag.ToString().Split('#');
                if (ContainerList.Count() == 7)
                {
                    frmGoalInstruction frmGI = new frmGoalInstruction();
                    frmGI.GoalId = Convert.ToInt64(ContainerList[0]);
                    frmGI.SnomedCode = Convert.ToInt64(ContainerList[1]);
                    frmGI.SnomedDesc = ContainerList[2];
                    frmGI.GoalDesc = ContainerList[3];

                    frmGI.Health = ContainerList[4];
                    frmGI.Reason = ContainerList[5];
                    frmGI.Active = ContainerList[6];

                    frmGI.MdiParent = this.ParentMDI;
                    frmGI.ParentMDI = this.ParentMDI;
                    frmGI.Show();
                    this.Close();
                }
            }
        }
    }
}