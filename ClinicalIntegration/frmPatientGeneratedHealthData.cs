﻿using DBHelper;
using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmPatientGeneratedHealthData : Form
    {
        public string PatientId { get; set; }
        public string AppointmentId { get; set; }
        public Object DBObject { get; set; }
        public string UserId { get; set; }
        public string PracticeToken { get; set; }
        public string PinpointDirectory { get; set; }
        private string ExternalId { get; set; }
        private string ExternalLink { get; set; }
        private string FileId { get; set; }
        private string TypeId { get; set; }

        public frmPatientGeneratedHealthData()
        {
            InitializeComponent();
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex > -1)
            {
                button1.Enabled = true;
                button3.Enabled = true;
                button2.Enabled = false;
                DataGridViewRow row = dataGridView1.Rows[rowIndex];
                ExternalId = dataGridView1.Rows[rowIndex].Cells[0].Value.ToString();
                TypeId = dataGridView1.Rows[rowIndex].Cells[5].Value.ToString();
                FileId = dataGridView1.Rows[rowIndex].Cells[6].Value.ToString();
                ExternalLink = dataGridView1.Rows[rowIndex].Cells[1].Value.ToString();
                if (TypeId == "File")
                {
                    button2.Enabled = true;
                }
            }
        }

        private void frmPatientGeneratedHealthData_Load(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button3.Enabled = false;
            button2.Enabled = false;
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            ClinicalComponent.UserId = this.UserId;
            ClinicalComponent.PatientId = this.PatientId;
            PortalClient objPortalClient = new PortalClient();
            objPortalClient.GetPortalExternalLink(this.PatientId, this.PracticeToken, this.AppointmentId);
            objPortalClient.GetPortalExternalFile(this.PatientId, this.PracticeToken, this.AppointmentId);
            comboBox1.SelectedItem = "Incorporated";
            dataGridView1.DataSource = new PatientGeneratedHealthData().GetExternalLinks("'0'");
            dataGridView1.ClearSelection();
            dataGridView1.AutoGenerateColumns = false;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (ExternalId != "")
            {
                bool _retStatus = TypeId == "Link" ? new PatientGeneratedHealthData().Archive(ExternalId) : new PatientGeneratedHealthData().Archive(ExternalId, FileId, PatientId, PracticeToken);
                if (_retStatus)
                {
                    string _Status = "";
                    if (comboBox1.SelectedItem.ToString() == "All")
                        _Status = "'0','1'";

                    else if (comboBox1.SelectedItem.ToString() == "Deleted")
                        _Status = "'1'";

                    else if (comboBox1.SelectedItem.ToString() == "Incorporated")
                        _Status = "'0'";
                    dataGridView1.DataSource = new PatientGeneratedHealthData().GetExternalLinks(_Status);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ExternalLink != "")
            {
                string Path2Directory = ExternalLink;
                if (TypeId == "File")
                {
                    Path2Directory = PinpointDirectory + @"PGHD\" + this.PatientId + @"\" + ExternalLink;
                }
                PatientGeneratedHealthData obj = new PatientGeneratedHealthData();
                obj.UrlRead(ExternalId);
                frmExternalPortalLink frm = new frmExternalPortalLink();
                frm.ExternalLink = Path2Directory;
                frm.ShowDialog();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";
            if (comboBox1.SelectedItem.ToString() == "All")
                _Status = "'0','1'";

            else if (comboBox1.SelectedItem.ToString() == "Deleted")
                _Status = "'1'";

            else if (comboBox1.SelectedItem.ToString() == "Incorporated")
                _Status = "'0'";

            PatientGeneratedHealthData obj = new PatientGeneratedHealthData();
            dataGridView1.DataSource = obj.GetExternalLinks(_Status);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (TypeId != "Link")
            {
                if (new PatientGeneratedHealthData().DownloadFile(ExternalId, FileId, this.PatientId, this.PracticeToken, this.ExternalLink, this.PinpointDirectory))
                {
                    MessageBox.Show("File downloaded successfully");
                }
                else
                {
                    MessageBox.Show("There is some issue while downloading this file, Please contact IO Support");
                }
            }
        }
    }
}
