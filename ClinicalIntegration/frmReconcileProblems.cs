﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmReconcileProblems : Form
    {
        public Form ParentMDI { get; set; }
        private static DataTable dt;
        private static DataTable dtMerg;
        private static DataTable tempMerg;

        public frmReconcileProblems()
        {
            InitializeComponent();
        }

        private void ReconcileProblems_Load(object sender, EventArgs e)
        {
            try
            {
                this.Dock = DockStyle.Fill;
                DefaultGridUI(grdProblems);
                DefaultGridUI(dgReconcilation);
                DefaultGridUI(grdMergedProblem);
                dt = new DataTable();
                dtMerg = new DataTable();
                GetReconcileProblems();
                GetProblemsList();
                //DisableButtonBorder(btnMerge);
                grdProblems.AutoGenerateColumns = false;
                dgReconcilation.AutoGenerateColumns = false;

                grdProblems.RowTemplate.Height = 32;
                grdProblems.AutoGenerateColumns = false;
                grdProblems.ReadOnly = true;

                grdProblems.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
                grdProblems.EnableHeadersVisualStyles = false;
                grdProblems.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
                grdProblems.EnableHeadersVisualStyles = false;
                grdProblems.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                grdProblems.ColumnHeadersHeight = 25;
                grdProblems.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
                AlignGridUI(grdMergedProblem);
                AlignGridUI(dgReconcilation);
                // GetMergedProblems(new DataTable());

            }
            catch
            {
            }
        }

        private void AlignGridUI(DataGridView dg)
        {

            dg.RowTemplate.Height = 32;
            dg.AutoGenerateColumns = false;
            dg.ReadOnly = true;
            dg.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            dg.EnableHeadersVisualStyles = false;
            dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dg.EnableHeadersVisualStyles = false;
            dg.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dg.ColumnHeadersHeight = 25;
            dg.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
        }


        private void DefaultGridUI(DataGridView dg)
        {

            foreach (DataGridViewColumn col in dg.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font("Arial", 13F, FontStyle.Bold, GraphicsUnit.Pixel);

            }

        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }
        private void btnMerge_Click(object sender, EventArgs e)
        {
            ReconcileList objRconcil = new ReconcileList();
            dtMerg = dt.Clone();
            for (int i = 0; i < dgReconcilation.Rows.Count; i++)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[i].Cells[1];
                if (chk != null)
                {
                    if (!Convert.ToBoolean(chk.Value))
                        continue;
                    dtMerg.ImportRow(dt.Rows[i]);
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["LastModefiedDate"] = DateTime.Now.ToString();
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["ExistingProblem"] = "0";
                }
            }
            GetMergedProblems(dtMerg);
        }

        public void GetReconcileProblems()
        {
            ReconcileList objRconcil = new ReconcileList();
            //DataTable dt = new DataTable();
            dt = objRconcil.GetProbblemReconcileList(Convert.ToInt64(ClinicalComponent.PatientId), false);
            dgReconcilation.AutoGenerateColumns = false;
            dgReconcilation.DataSource = dt;
            dgReconcilation.AutoGenerateColumns = false;
        }

        public void GetMergedProblems(DataTable dtRecon)
        {
            ReconcileList objRconcil = new ReconcileList();
            DataTable dt = new DataTable();
            dt = dtRecon.Clone();
            foreach (DataRow dr in dtRecon.Rows)
            {
                dt.ImportRow(dr);
            }
            DataTable dt1 = objRconcil.GetProbblemList(Convert.ToInt64(ClinicalComponent.PatientId));
            if (dt.Rows.Count == 0)
            {
                grdMergedProblem.AutoGenerateColumns = false;
                grdMergedProblem.DataSource = dt1;
                grdMergedProblem.AutoGenerateColumns = false;
                return;
            }
            //dt = dt1.Clone();
            bool adddrdt = false;
            DataTable dtChild = new DataTable();
            dtChild = dt.Clone();
            foreach (DataRow dr in dt1.Rows)
            {
                adddrdt = true;
                foreach (DataRow drdtt in dt.Rows)
                {
                    if (Convert.ToString(drdtt["SnomedCode"]).ToLower() == Convert.ToString(dr["SnomedCode"]).ToLower())
                    {
                        adddrdt = false;
                        break;
                    }
                }

                if (adddrdt)
                {
                    DataRow drdt = dtChild.NewRow();
                    drdt["id"] = dr["Id"];
                    drdt["PatientId"] = 0;
                    drdt["Diagdesc"] = "";
                    drdt["Status"] = dr["Status"];

                    drdt["SnomedCode"] = dr["SnomedCode"];
                    drdt["Status"] = dr["Status"];

                    drdt["ExistingProblem"] = "1";
                    drdt["LastModefiedDate"] = dr["LastModifyDate"];
                    drdt["Term"] = "";
                    drdt["ICD10"] = "";
                    drdt["problem"] = dr["Problem"];
                    //drdt["id"] = dr["id"];
                    dtChild.Rows.Add(drdt);
                }

            }
            foreach (DataRow  dr in dtChild.Rows)
            {
                dt.ImportRow(dr);
            }
            grdMergedProblem.AutoGenerateColumns = false;
            grdMergedProblem.DataSource = dt;
            dtMerg = dt;
            grdMergedProblem.AutoGenerateColumns = false;
        }

        public void GetProblemsList()
        {
            ReconcileList objRconcil = new ReconcileList();
            DataTable dt = new DataTable();
            dt = objRconcil.GetProbblemList(Convert.ToInt64(ClinicalComponent.PatientId));
            grdProblems.DataSource = dt;
            tempMerg = dt;
        }

        private void chkboxlistReconcilList_Click(object sender, EventArgs e)
        {

        }

        private void chkboxlistReconcilList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string SnomedCPT = ((System.Data.DataRowView)chkboxlistReconcilList.SelectedItem).Row.ItemArray[4].ToString();
                //if (!chkboxlistReconcilList.GetItemChecked(chkboxlistReconcilList.SelectedIndex))
                //    return;

                //ReconcileList objRconcil = new ReconcileList();
                //if (objRconcil.CanDisplayAlert())
                //    objRconcil.CallAlert(SnomedCPT, "Problem");
            }
            catch
            {
                // throw;
            }
        }

        private void grdProblems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgReconcilation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                string SnmdCd = Convert.ToString(dgReconcilation.Rows[e.RowIndex].Cells[2].Value).Trim();
                SnmdCd = SnmdCd.Replace(@"\", "");
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[e.RowIndex].Cells[1];
                if (Convert.ToBoolean(chk.Value))
                {
                    chk.Value = false;
                }
                else
                {
                    chk.Value = true;
                    ReconcileList objRconcil = new ReconcileList();
                    if (objRconcil.CanDisplayAlert())
                        objRconcil.CallAlert(SnmdCd.TrimStart().TrimEnd(), "Problem");
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReconcileList objRconcil = new ReconcileList();
            for (int i = 0; i < grdMergedProblem.Rows.Count; i++)
            {
                if (Convert.ToInt16(dtMerg.Rows[i]["ExistingProblem"]) == 0)
                {
                    objRconcil.AddProblemList(Convert.ToInt64(ClinicalComponent.PatientId), dtMerg.Rows[i], i,Convert.ToInt64(ClinicalComponent.AppointmentId.Trim()));
                }
                else
                {

                }
            }
            grdMergedProblem.DataSource = new DataTable();
            GetReconcileProblems();
            GetProblemsList();
        }

        private void grdMergedProblem_SelectionChanged(object sender, EventArgs e)
        {
            grdMergedProblem.ClearSelection();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ReconcileList objRconcil = new ReconcileList();
            dtMerg = dt.Clone();
            for (int i = 0; i < dgReconcilation.Rows.Count; i++)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[i].Cells[1];
                if (chk != null)
                {
                    if (!Convert.ToBoolean(chk.Value))
                        continue;
                    dtMerg.ImportRow(dt.Rows[i]);
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["LastModefiedDate"] = DateTime.Now.ToString();
                    dtMerg.Rows[dtMerg.Rows.Count - 1]["ExistingProblem"] = "0";
                }
            }
            GetMergedProblems(dtMerg);
        }
    }
}
