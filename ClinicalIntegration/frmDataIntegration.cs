﻿using DBHelper;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;


namespace ClinicalIntegration
{
    public partial class frmDataIntegration : Form
    {
        public frmDataIntegration()
        {
            InitializeComponent();
        }

        public string ProviderId { get; set; }
        public Object DBObject { get; set; }
        public string FolderPath { get; set; }
        public string FileName { get; set; }
        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string extenssion = Path.GetExtension(openFileDialog1.FileName);
                string path = Path.GetFullPath(openFileDialog1.FileName);
                FileName = Path.GetFileName(openFileDialog1.FileName);
                if (extenssion.ToUpper() == ".XML" || extenssion.ToUpper() == ".TXT")
                {
                    txtPlainText.Text = System.IO.File.ReadAllText(openFileDialog1.FileName);
                    txtEncryptedText.Text = txtgenreatedhash.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show("Please choose only XML/TXT/PDF file");
                    return;
                }

                if (txtPlainText.Text != string.Empty)
                {
                    if (extenssion.ToUpper() == ".XML" || extenssion.ToUpper() == ".TXT" || extenssion.ToUpper() == ".PDF")
                    {
                        //txtEncryptedText.Text = ComputeHashd(txtPlainText.Text, "SHA512", null);
                        byte[] HashValue, MessageBytes = File.ReadAllBytes(path);
                        SHA512Managed SHhash = new SHA512Managed();
                        string strHex = "";

                        HashValue = SHhash.ComputeHash(MessageBytes);
                        foreach (byte b in HashValue)
                        {
                            strHex += String.Format("{0:x2}", b);
                        }
                        txtEncryptedText.Text = strHex;
                        //txtEncryptedText.Text = computeHash(txtPlainText.Text, "SHA512");
                    }
                    //else if (extenssion.ToUpper() == ".RTF" || extenssion.ToUpper() == ".TXT")
                    //{
                    //    txtEncryptedText.Text = computeHash(System.IO.File.ReadAllText(path, Encoding.UTF8), "SHA512");
                    //}
                }
            }
        }
        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        public string computeHash(String message, String algo)
        {

            byte[] sourceBytes = Encoding.Default.GetBytes(message);
            byte[] hashBytes = null;
            Console.WriteLine(algo);
            switch (algo.Trim().ToUpper())
            {
                case "MD5":
                    hashBytes = MD5CryptoServiceProvider.Create().ComputeHash(sourceBytes);
                    break;
                case "SHA1":
                    hashBytes = SHA1Managed.Create().ComputeHash(sourceBytes);
                    break;
                case "SHA256":
                    hashBytes = SHA256Managed.Create().ComputeHash(sourceBytes);
                    break;
                case "SHA384":
                    hashBytes = SHA384Managed.Create().ComputeHash(sourceBytes);
                    break;
                case "SHA512":
                    //SHA512 shaM = new SHA512Managed();
                    //hashBytes = shaM.ComputeHash(sourceBytes);
                    //return hashBytes.ToString();
                    hashBytes = SHA512Managed.Create().ComputeHash(sourceBytes);
                    break;
                default:
                    break;
            }
            StringBuilder hex = new StringBuilder(hashBytes.Length * 2);
            foreach (byte b in hashBytes)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }

        public static byte[] GetBytes(string text)
        {
            var bytes = new byte[text.Length * sizeof(char)];
            Buffer.BlockCopy(text.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private void frmDataIntegration_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            ClinicalComponent.UserId = ProviderId;
            BindProvider_To();
            BindCode_RecievedContent();
            txtEncryptedText.Clear();
            txtgenreatedhash.Clear();
            txtPlainText.Clear();
        }

        public void BindCode_RecievedContent()
        {
            try
            {
                DataIntegration ObjInt = new DataIntegration();
                DataTable dt = ObjInt.GetReceivedData(ClinicalComponent.UserId);
                foreach (DataRow dr in dt.Rows)
                {
                    ListViewItem listitem = new ListViewItem(dr[0].ToString());
                    listitem.SubItems.Add(dr[1].ToString());
                    lsvhashcode.Items.Add(listitem);
                }
            }
            catch
            {

            }
        }

        public void BindProvider_To()
        {
            try
            {
                DataIntegration objIntDta = new DataIntegration();
                DataTable dt = objIntDta.GetProviders(ClinicalComponent.UserId);
                DataRow dr = dt.NewRow();
                dr["displayname"] = "Select";
                dr["Id"] = 0;
                dt.Rows.InsertAt(dr, 0);
                cmbtoproivider.ValueMember = "Id";
                cmbtoproivider.DisplayMember = "displayname";
                cmbtoproivider.DataSource = dt;
                cmbtoproivider.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbtoproivider.SelectedIndex == 0)
                {
                    MessageBox.Show("Please select Provider");
                    return;
                }
                if (string.IsNullOrEmpty(txtPlainText.Text) || string.IsNullOrEmpty(txtEncryptedText.Text))
                {
                    MessageBox.Show("Unable to Send");
                    return;
                }
                DataIntegration objDta = new DataIntegration();
                objDta.SendData(ClinicalComponent.UserId, cmbtoproivider.SelectedValue.ToString(), txtPlainText.Text.Trim(), txtEncryptedText.Text.Trim(), FileName, File.ReadAllBytes(openFileDialog1.FileName));
                MessageBox.Show("Sent Succesfully");
                txtEncryptedText.Clear();
                txtgenreatedhash.Clear();
                txtPlainText.Clear();
                cmbtoproivider.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void lsvhashcode_DoubleClick_1(object sender, EventArgs e)
        {

            if (lsvhashcode.Items.Count > 0)
            {
                DataIntegration ObjInt = new DataIntegration();
                DataTable dt = ObjInt.GetHashcodeIntegrityById(Convert.ToInt64(lsvhashcode.SelectedItems[0].SubItems[0].Text));
                if (dt != null && dt.Rows.Count > 0)
                {
                    txtPlainText.Text = dt.Rows[0]["FileNamexml"].ToString();
                    txtEncryptedText.Text = dt.Rows[0]["Hashcode"].ToString();
                    byte[] HashValue, MessageBytes = (byte[])dt.Rows[0]["FileByteAry"];//dt.Rows[0]["FileByteAry"];
                    SHA512Managed SHhash = new SHA512Managed();
                    string strHex = "";

                    HashValue = SHhash.ComputeHash(MessageBytes);
                    foreach (byte b in HashValue)
                    {
                        strHex += String.Format("{0:x2}", b);
                    }
                    txtgenreatedhash.Text = strHex;
                }

                if (txtPlainText.Text != string.Empty)
                {
                    //txtgenreatedhash.Text = computeHash(txtPlainText.Text, "SHA512");
                    if (txtEncryptedText.Text == txtgenreatedhash.Text)
                    {
                        MessageBox.Show("Sent & Received HashCode are Matching");
                    }
                    else
                    {
                        MessageBox.Show("Sent & Received HashCode are Matching");
                    }
                }
            }
        }


    }
}

