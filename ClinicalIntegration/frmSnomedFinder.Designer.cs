﻿namespace ClinicalIntegration
{
    partial class frmSnomedFinder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnGo = new System.Windows.Forms.Button();
            this.txtSnomed = new System.Windows.Forms.TextBox();
            this.cbFindBy = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grdSnomed = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdSnomed)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSelect
            // 
            this.btnSelect.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_done;
            this.btnSelect.Location = new System.Drawing.Point(630, 486);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(0);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(96, 36);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_quit;
            this.btnClose.Location = new System.Drawing.Point(32, 486);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(96, 36);
            this.btnClose.TabIndex = 2;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGo
            // 
            this.btnGo.BackgroundImage = global::ClinicalIntegration.Properties.Resources.btn_go;
            this.btnGo.Location = new System.Drawing.Point(550, 150);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(51, 37);
            this.btnGo.TabIndex = 3;
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // txtSnomed
            // 
            this.txtSnomed.Location = new System.Drawing.Point(310, 167);
            this.txtSnomed.Name = "txtSnomed";
            this.txtSnomed.Size = new System.Drawing.Size(227, 20);
            this.txtSnomed.TabIndex = 4;
            // 
            // cbFindBy
            // 
            this.cbFindBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFindBy.FormattingEnabled = true;
            this.cbFindBy.Location = new System.Drawing.Point(93, 166);
            this.cbFindBy.Name = "cbFindBy";
            this.cbFindBy.Size = new System.Drawing.Size(199, 21);
            this.cbFindBy.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Search :";
            // 
            // grdSnomed
            // 
            this.grdSnomed.AllowUserToAddRows = false;
            this.grdSnomed.AllowUserToOrderColumns = true;
            this.grdSnomed.AllowUserToResizeColumns = false;
            this.grdSnomed.AllowUserToResizeRows = false;
            this.grdSnomed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSnomed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5,
            this.Column3});
            this.grdSnomed.Location = new System.Drawing.Point(32, 196);
            this.grdSnomed.Name = "grdSnomed";
            this.grdSnomed.Size = new System.Drawing.Size(691, 283);
            this.grdSnomed.TabIndex = 8;
            this.grdSnomed.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSnomed_CellClick);
            this.grdSnomed.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdSnomed_DataBindingComplete);
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Col1";
            this.Column4.Frozen = true;
            this.Column4.HeaderText = "SNOMED CT";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Col2";
            this.Column5.Frozen = true;
            this.Column5.HeaderText = "SNOMED CT Description";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 400;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Col3";
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "Status";
            this.Column3.Name = "Column3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "SNOMED Finder";
            // 
            // frmSnomedFinder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 590);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grdSnomed);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbFindBy);
            this.Controls.Add(this.txtSnomed);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSelect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 79);
            this.Name = "frmSnomedFinder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "SnomedFinder";
            this.Load += new System.EventHandler(this.frmSnomedFinder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdSnomed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.TextBox txtSnomed;
        private System.Windows.Forms.ComboBox cbFindBy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView grdSnomed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}