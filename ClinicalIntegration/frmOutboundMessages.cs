﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace ClinicalIntegration
{
    public partial class frmOutboundMessages : Form
    {
        public string StaffId { get; set; }
        public string ProviderId { get; set; }
        public string folderpath { get; set; }
        public string PatientId { get; set; }
        public string ChartRecordId { get; set; }
        public string ProviderEmailAddress { get; set; }
        public string PatientName { get; set; }
        public string ResourceName { get; set; }
        private string folderpath_json
        {
            get
            {
                return folderpath + @"\" + ProviderId + @"\JSONS\";
            }
        }

        private string DocumentPath
        {
            get
            {
                return folderpath.Replace("\\ProviderAttachments","") + @"\Document\";
            }
        }

        private string _selectedEmailAddress { get; set; }
        private string _selectedVendorId { get; set; }

        public frmOutboundMessages()
        {
            InitializeComponent();
        }

        private void frmOutboundMessages_Load(object sender, EventArgs e)
        {
            ClinicalComponent.ProviderId = this.ProviderId;
            ClinicalComponent.UserId = this.StaffId;
            LoadDefaults();
        }

        private void DefaultButtonImages()
        {
            browse_btn.Image = Properties.Resources.btn_browse;
            Send_btn.Image = Properties.Resources.btn_send;
            button1.Image = Properties.Resources.btn_remove;

        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }

        public void LoadDefaults()
        {
            try
            {
                comboBox2.DataSource = new RCPServices().LoadClinicalTypes();
                comboBox2.DisplayMember = "Col2";
                comboBox2.ValueMember = "Col1";
                From_txtbx.Text = this.ProviderEmailAddress;
                pnlReason4Referal.Hide();
                panel1.Hide();
                pnlBrowseAttachment.Hide();
                SetDefaultDocuments();
                DefaultButtonImages();
                DisableButtonBorder(browse_btn);
                DisableButtonBorder(Send_btn);
                DisableButtonBorder(button1);
                Subject_txtbx.Text = "";
                Message_txtbx.Text = "";
            }
            catch
            {

            }
        }

        private void browse_btn_Click(object sender, EventArgs e)
        {
            pnlBrowseAttachment.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            panel1.Show();
            pnlBrowseAttachment.Hide();
        }

        private void btnContact_Click(object sender, EventArgs e)
        {
            _selectedVendorId = ((Button)sender).Tag.ToString();
            string _selectedVendorDetail = ((Button)sender).Text.ToString();
            int _selectedVendorlength = _selectedVendorDetail.IndexOf(')') - _selectedVendorDetail.IndexOf('(');
            _selectedEmailAddress = _selectedVendorDetail.Substring(_selectedVendorDetail.IndexOf('(') + 1, _selectedVendorlength - 1);
            if (_selectedEmailAddress != "")
            {
                To_txtbx.Text = _selectedEmailAddress;
            }
            panel1.Hide();
            pnlBrowseAttachment.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (txtAddressSearchBox.Text.Trim() != "")
            {
                int _xLocation = 10;
                int _yLocation = 10;
                int _btnCount = 0;
                pnlAddressContent.Controls.Clear();
                DataTable dtTemp = new AddressBook().FindExternalContacts(txtAddressSearchBox.Text.Trim());
                foreach (DataRow dr in dtTemp.Rows)
                {
                    _btnCount += 1;
                    Button btnContact = new Button();
                    btnContact.Text = dr["Col2"].ToString() + " \n\n (" + dr["Col3"].ToString() + ")";
                    btnContact.Tag = dr["Col1"].ToString();
                    btnContact.Click += new EventHandler(btnContact_Click);
                    btnContact.ForeColor = System.Drawing.Color.White;
                    btnContact.BackColor = System.Drawing.Color.Teal;
                    btnContact.Location = new Point(_xLocation, _yLocation);
                    btnContact.Size = new Size(210, 70);
                    _xLocation = _xLocation + 220;
                    if (_btnCount == 3)
                    {
                        _xLocation = 10;
                        _yLocation = _yLocation + 80;
                        _btnCount = 0;
                    }
                    pnlAddressContent.Controls.Add(btnContact);
                }
            }
        }

        private void Send_btn_Click_1(object sender, EventArgs e)
        {
            Send_btn.Enabled = false;
            bool retStatus = false;
            string _fromAddress = From_txtbx.Text;
            string _toAddress = To_txtbx.Text;
            string _messageContent = Message_txtbx.Text;
            string _subjectContent = Subject_txtbx.Text;
            string _clinicalDocumentType = comboBox2.SelectedValue.ToString();
            string _reason4Referal = textBox1.Text;
           
            if (_fromAddress == "" || _toAddress == "" || _messageContent == "" || _subjectContent == "0" || _clinicalDocumentType == "" || _reason4Referal == "")
            {
                MessageBox.Show("Please provide all required details");
                Send_btn.Enabled = true;
            }

            if (_fromAddress != "" && _toAddress != "" && _messageContent != "" && _subjectContent != "0" && _clinicalDocumentType != "" && _reason4Referal != "")
            {
                try
                {
                    SendMessageEntity objSME = new SendMessageEntity()
                    {
                        _ClinicalDocumentType = _clinicalDocumentType,
                        _ExternalContactId = _selectedVendorId,
                        _FromAddress = _fromAddress,
                        _MessageBody = _messageContent,
                        _PathDirectory = folderpath_json,
                        _Reason4Referal = _reason4Referal,
                        _ResourceId = this.ProviderId,
                        _ToAddress = _toAddress,
                        _MessageSubject = _subjectContent,
                        _PatientId = this.PatientId,
                        _AppointmentId = this.ChartRecordId,
                        _StaffId = this.StaffId
                    };

                    List<AttachmentEntity> objTempAttachements = new List<AttachmentEntity>();
                    if (listBox1.Items.Count > 0)
                    {
                        foreach (String file in listBox1.Items)
                        {
                            string _fileType = "";
                            string _fileName = "";
                            string data = RCPServices.ConvertToBase64String(file);
                            string extension = Path.GetExtension(file);
                            if (extension == ".txt" || extension == ".TXT")
                            {
                                _fileType = "txt";
                                _fileName = Path.GetFileName(file);
                            }
                            if (extension == ".pdf" || extension == ".PDF")
                            {
                                _fileType = "pdf";
                                _fileName = Path.GetFileName(file);
                            }
                            if (extension == ".xml" || extension == ".XML")
                            {
                                _fileType = "xml";
                                _fileName = Path.GetFileName(file);
                            }
                            if (extension == ".jpg" || extension == ".JPG")
                            {
                                _fileType = "jpg";  //"img";
                                _fileName = Path.GetFileName(file);
                            }
                            if (extension == ".png" || extension == ".PNG")
                            {
                                _fileType = "png";  //"img";
                                _fileName = Path.GetFileName(file);
                            }
                            if (extension == ".zip" || extension == ".ZIP")
                            {
                                _fileType = "zip";
                                _fileName = Path.GetFileName(file);
                            }
                            AttachmentEntity objAE = new AttachmentEntity()
                            {
                                _fileType = _fileType,
                                _fileName = _fileName,
                                _fileContent = data,
                                _file = file
                            };
                            objTempAttachements.Add(objAE);
                        }
                    }

                    foreach (Button btn in pnlLetters.Controls)
                    {
                        if (btn.AccessibleDescription.ToString() == "1")
                        {
                            string _fileType = "";
                            string _fileName = "";
                            string _path2PDF = RCPServices.ConvertDoc2PDF(btn.Tag.ToString());
                            string data = RCPServices.ConvertToBase64String(_path2PDF);
                            string extension = Path.GetExtension(_path2PDF);
                            if (extension == ".pdf" || extension == ".PDF")
                            {
                                _fileType = "pdf";
                                _fileName = Path.GetFileName(_path2PDF);
                            }
                            AttachmentEntity objAE = new AttachmentEntity()
                            {
                                _fileType = _fileType,
                                _fileName = _fileName,
                                _fileContent = data,
                                _file = _path2PDF
                            };
                            objTempAttachements.Add(objAE);
                        }
                    }
                    objSME._attachmentList = objTempAttachements;
                    retStatus = new DEPMessages().SendMessage(objSME);
                }
                catch
                {
                    MessageBox.Show("There is some issue while sendning DEP message, Pleas try again");
                    Send_btn.Enabled = true;
                }
                if (retStatus)
                {
                    MessageBox.Show("Message has been sent successfully");
                    To_txtbx.Text = "";
                    Message_txtbx.Text = "";
                    Subject_txtbx.Text = "";
                    listBox1.Items.Clear();
                    _selectedEmailAddress = "";
                    _selectedVendorId = "";
                    textBox1.Text = "";
                    comboBox2.SelectedValue = "0";
                    Send_btn.Enabled = true;
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            panel1.Hide();
            textBox1.Text = "";
            pnlAddressContent.Controls.Clear();
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() != "")
            {
                panel1.Hide();
                pnlReason4Referal.Hide();
            }
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedValue.ToString() != "0")
            {
                panel1.Hide();
                pnlBrowseAttachment.Hide();
                pnlReason4Referal.Show();
                Subject_txtbx.Text = comboBox2.Text.ToString() + " of " + this.PatientName;
                Message_txtbx.Text = "Hi, Please find PHI of " + this.PatientName + "\n \n Thanks"; 
            }
            else
            {
                Subject_txtbx.Text = "";
                textBox1.Text = "";
            }
        }
        
        private void button7_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Title = "Browse Files";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|Images(*.BMP; *.JPG; *.GIF,*.PNG,*.TIFF)|*.BMP; *.JPG; *.GIF; *.PNG; *.TIFF|Html files (*.html)|*.html|Pdf files (*.pdf)|*.pdf|Xml files (*.xml)|*.xml";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in openFileDialog1.FileNames)
                {
                    try
                    {
                        if ((myStream = openFileDialog1.OpenFile()) != null)
                        {
                            using (myStream)
                            {
                                listBox1.Items.Add(file);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Could not read file from disk: " + ex.Message);
                    }
                }
                pnlBrowseAttachment.Hide();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pnlBrowseAttachment.Hide();
        }

        private void btnDefaultAttachment_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn.AccessibleDescription == "0")
            {
                btn.AccessibleDescription = "1";
                btn.BackColor = System.Drawing.Color.OrangeRed;
            }
            else
            {
                btn.AccessibleDescription = "0";
                btn.BackColor = System.Drawing.Color.Teal;
            }
        }

        private void btnDefaultAttachment_DoubleClick(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            System.Diagnostics.Process.Start(btn.Tag.ToString());
        }
        private void SetDefaultDocuments()
        {
            pnlLetters.Controls.Clear();
            DataTable dtLetters = new DEPMessages().FindAvaiableDocument(ChartRecordId);
            if (dtLetters.Rows.Count > 0)
            {
                string _path2File = "";
                string _letterName = "";
                int _xLocation = 0;
                int _yLocation = 0;
                int _btnCount = 0;
                foreach (DataRow dr in dtLetters.Rows)
                {
                    _letterName = dr["Name"].ToString() + ".doc";
                    _path2File = DocumentPath + _letterName;
                    if (File.Exists(_path2File))
                    {
                        Button btnDefaultAttachment = new Button();
                        btnDefaultAttachment.Text = _letterName;
                        btnDefaultAttachment.Tag = _path2File;
                        btnDefaultAttachment.AccessibleDescription = "0";
                        btnDefaultAttachment.BackColor = System.Drawing.Color.Teal;
                        btnDefaultAttachment.ForeColor = System.Drawing.Color.White;
                        btnDefaultAttachment.Location = new Point(_xLocation, _yLocation);
                        btnDefaultAttachment.Size = new Size(190, 70);
                        btnDefaultAttachment.Click += new EventHandler(btnDefaultAttachment_Click);
                        btnDefaultAttachment.DoubleClick += new EventHandler(btnDefaultAttachment_DoubleClick);
                        _xLocation = _xLocation + 210;
                        _btnCount += 1; 
                        if (_btnCount == 3)
                        {
                            _xLocation = 0;
                            _yLocation = _yLocation + 80;
                            _btnCount = 0;
                        }
                        pnlLetters.Controls.Add(btnDefaultAttachment);
                    }
                }
            }
        }
    }
}
