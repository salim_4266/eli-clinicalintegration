﻿using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmDiagnosisSnomed : Form
    {
        public Form ParentMDI { get; set; }
        public Object DBAccess { get; set; }
        public string PatientId { get; set; }
        public bool DiagnosisFlag { get; set; }
        public bool snomedFlag { get; set; }
        public bool EditNew { get; set; }
        public bool IsEdit { get; set; }
        public bool IsNew { get; set; }
        public bool IsFamilyHistoryEdit { get; set; }
        public Int64 PPLId { get; set; }
        public EditFamilyDetail family { get; set; }
        public bool IsFamilyRequest { get; set; }
        public DateTime familyDetDate { get; set; }
        public DateTime familyDetOnsetDate { get; set; }
        public string familyRelation { get; set; }
        public string familyComments { get; set; }
        public string familyStatus { get; set; }
        public string ProbStatus { get; set; }
        public string FamilyHistoryId { get; set; }
        public DateTime DiagDate { get; set; }
        public DateTime OnsetDate { get; set; }

        ProblemModel obj;

        public string SetValueForIcdCode1 = "";

        public frmDiagnosisSnomed()
        {
            InitializeComponent();
        }

        private void grdVwDiagnosis_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }

        private void grdVw_snomed_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }

        private void frmDiagnosisFinder_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillComboBoxIcd();

            Problems objprob = new Problems();
            FillDiagnosisList(objprob.DiagnosisFinder(parFlag: "all"));

            obj = new ProblemModel();
            btnSelect.TabStop = false;
            btnSelect.FlatStyle = FlatStyle.Flat;
            btnSelect.FlatAppearance.BorderSize = 0;
            btnClose.TabStop = false;
            btnClose.FlatStyle = FlatStyle.Flat;
            btnClose.FlatAppearance.BorderSize = 0;
            btn_Go.TabStop = false;
            btn_Go.FlatStyle = FlatStyle.Flat;
            btn_Go.FlatAppearance.BorderSize = 0;
        }

        private void FillComboBoxIcd()
        {
            cb_icd.Items.Insert(0, "Select");
            cb_icd.Items.Insert(1, "ICD10"); cb_icd.Items.Insert(2, "Diagnosis");
            cb_icd.Items.Insert(3, "SNOMED CT Code");
            cb_icd.SelectedIndex = 0;
        }

        private void FillDiagnosisList(DataTable dt)
        {
            DataGridViewCheckBoxColumn colCB = new DataGridViewCheckBoxColumn();
            grdVwDiagnosis.DataSource = dt.DefaultView;

        }
        private void FillSnomedList(DataTable dt)
        {
            grdVw_snomed.DataSource = dt.DefaultView;
        }

        private void btnNewHistory_Click(object sender, EventArgs e)
        {
            this.Hide();
            EditProblem frmEdit = new EditProblem();
            frmEdit.ShowDialog();
            this.Close();
        }

        private void grdVw_snomed_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex > -1)
            {
                snomedFlag = true;
                DataGridViewRow row = grdVw_snomed.Rows[rowIndex];

                obj.ConceptID = Convert.ToInt64(grdVw_snomed.Rows[rowIndex].Cells[0].Value.ToString());
                obj.Term = grdVw_snomed.Rows[rowIndex].Cells[1].Value.ToString();
            }
        }
        private void grdVwDiagnosis_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex > -1)
            {
                DataGridViewRow row = grdVwDiagnosis.Rows[rowIndex];
                var icdCode = grdVwDiagnosis.Rows[rowIndex].Cells[0].Value.ToString();// row.Cells[1].Value;
                obj.ICD10 = icdCode.ToString();
                obj.ICD10DESCR = grdVwDiagnosis.Rows[rowIndex].Cells[1].Value.ToString();
                //obj.Status = grdVwDiagnosis.Rows[rowIndex].Cells[2].Value.ToString();

                DiagnosisFlag = true;
                Problems objdata = new Problems();
                FillSnomedList(objdata.DiagnosisFinder(parIcd10Id: icdCode.ToString()));
            }

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (IsFamilyRequest)
            {
                EditFamilyDetail family = new EditFamilyDetail();

                if (string.IsNullOrEmpty(obj.ICD10) || string.IsNullOrEmpty(obj.ICD10DESCR) || string.IsNullOrEmpty(obj.Term))
                {
                    if (string.IsNullOrEmpty(obj.ICD10) || string.IsNullOrEmpty(obj.ICD10DESCR))
                        if (grdVw_snomed.Rows.Count > 0)
                        {
                            obj.ConceptID = Convert.ToInt64(grdVw_snomed.Rows[0].Cells[0].Value.ToString());
                            obj.Term = grdVw_snomed.Rows[0].Cells[1].Value.ToString();
                        }
                        else
                            return;
                    if (grdVw_snomed.Rows.Count > 0)
                    {
                        obj.ICD10 = Convert.ToString(grdVwDiagnosis.Rows[0].Cells[0].Value.ToString());
                        obj.ICD10DESCR = Convert.ToString(grdVwDiagnosis.Rows[0].Cells[1].Value.ToString());
                        obj.ConceptID = Convert.ToInt64(grdVw_snomed.Rows[0].Cells[0].Value.ToString());
                        obj.Term = grdVw_snomed.Rows[0].Cells[1].Value.ToString();
                    }
                }

                family.IsNew = this.IsNew;
                family.IsEdit = this.IsEdit;
                family.ICD10 = obj.ICD10;
                family.ICD10Desc = obj.ICD10DESCR;
                family.SnomedDesc = obj.Term;
                family.AppointmentDt = familyDetDate;
                family.IsfromSnomed = true;
                family.OnSetdate = familyDetOnsetDate;
                family.Comments = familyComments;
                family.Status = familyStatus;
                family.Relation = familyRelation;
                family.Id = this.FamilyHistoryId != "" ? Convert.ToInt16(this.FamilyHistoryId) : 0;
                family.SnomedID = obj.ConceptID.ToString();
                family.MdiParent = this.ParentMDI;
                family.ParentMDI = this.ParentMDI;

                family.Show();
                this.Close();
            }
            else
            {
                if (!snomedFlag)
                {
                    if (obj.ICD10 == null)
                    {
                        MessageBox.Show("Please select Icd10 code and snomed code");
                        return;
                    }

                    obj.ConceptID = Convert.ToInt64(grdVw_snomed.Rows[0].Cells[0].Value.ToString());
                    obj.Term = grdVw_snomed.Rows[0].Cells[1].Value.ToString();
                    snomedFlag = false;


                }
                if (!DiagnosisFlag)
                {
                    obj.ICD10 = grdVwDiagnosis.Rows[0].Cells[0].Value.ToString();
                    obj.ICD10DESCR = grdVwDiagnosis.Rows[0].Cells[1].Value.ToString();

                    DiagnosisFlag = false;
                }

                obj.OnsetDate = OnsetDate;
                obj.AppointmentDate = DiagDate.ToShortDateString();
                obj.Status = ProbStatus;
                obj.Id = PPLId;

                EditProblem objDiag = new EditProblem();
                objDiag.IsNew = EditNew;
                objDiag.SetValueForAddUpdate = false;
                objDiag.objEditProb = obj;
                obj.OnsetDate = OnsetDate;
                obj.AppointmentDate = DiagDate.ToShortDateString();
                obj.Status = ProbStatus;
                objDiag.MdiParent = this.ParentMDI;
                objDiag.ParentMDI = this.ParentMDI;
                objDiag.Show();
                this.Close();
            }
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            if (cb_icd.SelectedIndex > 0)
            {
                Problems objdata = new Problems();

                if (cb_icd.Text.Equals("SNOMED CT Code"))
                {
                    if (ValidateSnomedID())
                    {
                        FillDiagnosisList(objdata.DiagnosisFinder("snomeddiag", "", txt_icdDiag.Text));
                        FillSnomedList(objdata.DiagnosisFinder("snomedbyid", "", txt_icdDiag.Text));
                    }
                    else { MessageBox.Show("Please fill numeric value for snomed code", "Error"); }
                }
                else
                {
                    FillDiagnosisList(objdata.DiagnosisFinder(cb_icd.Text.ToLower(), txt_icdDiag.Text, ""));
                    FillSnomedList(new DataTable());
                }
            }
            else
            {
                MessageBox.Show("Please select the finder option from dropdown", "Error");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (IsFamilyRequest)
            {
                frmFamilydetails frm = new frmFamilydetails();
                frm.MdiParent = this.ParentMDI;
                frm.ParentMDI = this.ParentMDI;
                frm.Show();
                Close();
            }
            else
            {
                frmProblemList frmp = new frmProblemList();
                frmp.MdiParent = this.ParentMDI;
                frmp.ParentMDI = this.ParentMDI;
                frmp.Show();
                Close();
            }
        }

        private bool ValidateSnomedID()
        {
            Int64 val;
            if (Int64.TryParse(txt_icdDiag.Text, out val))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
