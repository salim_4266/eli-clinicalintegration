﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    public class LabOrderModel
    {
        public string OrderId { get; set; }
        public string OrderType { get; set; }
        public string OrderDate { get; set; }
        public string LOINCNumber { get; set; }
        public string Status { get; set; }
        public string LastModifiedBy { get; set; }
        public string LastModifiedDate { get; set; }
        public string OrderForReason { get; set; }
        
    }


    public class LabOrderTestResultModel
    {
        public string RecordId { get; set; }
        public string Unit { get; set; }
        public string Result { get; set; }
        public string MinRange { get; set; }
        public string MaxRange { get; set; }
        public string Range { get; set; }

        public bool UpdateLabResultDetails()
        {
            bool retStatus = true;
            string _error = "";
            try
            {
                string _UpdateQry = "Update dbo.PatientLabTestResults Set Result = @Result, Unit = @Unit, MinRange = @MinRange, MaxRange = @MaxRange, Range = @Range Where Id = @RecordId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter idbResult = cmd.CreateParameter();
                    idbResult.Direction = ParameterDirection.Input;
                    idbResult.Value = this.Result == null ? "" : this.Result;
                    idbResult.ParameterName = "@Result";
                    idbResult.DbType = DbType.String;
                    cmd.Parameters.Add(idbResult);

                    IDbDataParameter idbUnit = cmd.CreateParameter();
                    idbUnit.Direction = ParameterDirection.Input;
                    idbUnit.Value = this.Unit == null ? "" : this.Unit;
                    idbUnit.ParameterName = "@Unit";
                    idbUnit.DbType = DbType.String;
                    cmd.Parameters.Add(idbUnit);

                    IDbDataParameter idbMinRange = cmd.CreateParameter();
                    idbMinRange.Direction = ParameterDirection.Input;
                    idbMinRange.Value = this.MinRange == null ? "" : this.MinRange;
                    idbMinRange.ParameterName = "@MinRange";
                    idbMinRange.DbType = DbType.String;
                    cmd.Parameters.Add(idbMinRange);

                    IDbDataParameter idbMaxRange = cmd.CreateParameter();
                    idbMaxRange.Direction = ParameterDirection.Input;
                    idbMaxRange.Value = this.MaxRange == null ? "" : this.MaxRange;
                    idbMaxRange.ParameterName = "@MaxRange";
                    idbMaxRange.DbType = DbType.String;
                    cmd.Parameters.Add(idbMaxRange);

                    IDbDataParameter idbRange = cmd.CreateParameter();
                    idbRange.Direction = ParameterDirection.Input;
                    idbRange.Value = this.Range == null ? "" : this.Range;
                    idbRange.ParameterName = "@Range";
                    idbRange.DbType = DbType.String;
                    cmd.Parameters.Add(idbRange);

                    IDbDataParameter idbRecordId = cmd.CreateParameter();
                    idbRecordId.Direction = ParameterDirection.Input;
                    idbRecordId.Value = this.RecordId;
                    idbRecordId.ParameterName = "@RecordId";
                    idbRecordId.DbType = DbType.String;
                    cmd.Parameters.Add(idbRecordId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _UpdateQry;
                    cmd.ExecuteNonQuery();
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }
    }



}
