﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    public class LabOrder
    {
        public DataTable GetDataSet_LOINCNumber(string _SerachContent, string _Category)
        {
            DataTable dtLOINCTable = new DataTable();
            string _error = "";
            try
            {
                string _selectQry = "Select LOINC_NUM as Col1, COMPONENT as Col2, LONG_COMMON_NAME as Col3, PROPERTY as Col4, SHORTNAME as Col5 From dbo.Loinc_2017 with(nolock) Where " + _Category + " Like '%" + _SerachContent + "%'";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtLOINCTable.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return dtLOINCTable;
        }

        public DataTable GetPatientLabOrder(string _Status)
        {
            DataTable dtPatientLabOrder = new DataTable();
            string _error = "";
            try
            {
                string _selectQry = "Select distinct OrderId as Col1,OrderDate as Col2, b.Loinc_num as Col3, b.LONG_COMMON_NAME as Col4, Case(a.Status) When 1 then 'Pending' else 'Completed' End as Col5, LastModifiedDate as Col6, c.ResourceName as Col7, a.OrderReason as Col8 From dbo.PatientLabOrders a inner join dbo.Loinc_2017 b on a.LoincNumber = b.Loinc_num inner join dbo.Resources c on c.ResourceId = a.LastModifiedBy Where a.PatientId = @PatientId and a.Status in   (" + _Status + ") Order By OrderDate DESC ";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter idbPatientId = cmd.CreateParameter();
                    idbPatientId.Direction = ParameterDirection.Input;
                    idbPatientId.Value = ClinicalComponent.PatientId;
                    idbPatientId.ParameterName = "@PatientId";
                    idbPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(idbPatientId);

                    //IDbDataParameter idbAppointmentId = cmd.CreateParameter();
                    //idbAppointmentId.Direction = ParameterDirection.Input;
                    //idbAppointmentId.Value = ClinicalComponent.AppointmentId;
                    //idbAppointmentId.ParameterName = "@AppointmentId";
                    //idbAppointmentId.DbType = DbType.Int64;
                    //cmd.Parameters.Add(idbAppointmentId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtPatientLabOrder.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return dtPatientLabOrder;
        }

        public bool PlaceNewOrder(LabOrderModel objLOM)
        {
            bool retStatus = true;
            string _error = "";
            try
            {
                string _insertQry = "Insert into dbo.PatientLabOrders(PatientId, AppId, LoincNumber, Status, LastModifiedDate, OrderType, OrderDate, LastModifiedBy, IsArchieved, OrderReason) Values (@PatientId, @AppointmentId, @Loinc_Num, @Status, GETDATE(), @OrderType, @OrderDate, @LastModifiedBy, 0, @OrderReason); Select Scope_Identity() as OrderId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter PatientId = cmd.CreateParameter();
                    PatientId.Direction = ParameterDirection.Input;
                    PatientId.Value = ClinicalComponent.PatientId;
                    PatientId.ParameterName = "@PatientId";
                    PatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(PatientId);

                    IDbDataParameter AppointmentId = cmd.CreateParameter();
                    AppointmentId.Direction = ParameterDirection.Input;
                    AppointmentId.Value = ClinicalComponent.AppointmentId;
                    AppointmentId.ParameterName = "@AppointmentId";
                    AppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(AppointmentId);

                    IDbDataParameter Loinc_Num = cmd.CreateParameter();
                    Loinc_Num.Direction = ParameterDirection.Input;
                    Loinc_Num.Value = objLOM.LOINCNumber;
                    Loinc_Num.ParameterName = "@Loinc_Num";
                    Loinc_Num.DbType = DbType.String;
                    cmd.Parameters.Add(Loinc_Num);

                    IDbDataParameter Status = cmd.CreateParameter();
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = objLOM.Status == "Pending" ? 1 : 0;
                    Status.ParameterName = "@Status";
                    Status.DbType = DbType.Int64;
                    cmd.Parameters.Add(Status);

                    IDbDataParameter OrderType = cmd.CreateParameter();
                    OrderType.Direction = ParameterDirection.Input;
                    OrderType.Value = objLOM.OrderType;
                    OrderType.ParameterName = "@OrderType";
                    OrderType.DbType = DbType.String;
                    cmd.Parameters.Add(OrderType);

                    IDbDataParameter OrderDate = cmd.CreateParameter();
                    OrderDate.Direction = ParameterDirection.Input;
                    OrderDate.Value = objLOM.OrderDate;
                    OrderDate.ParameterName = "@OrderDate";
                    OrderDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(OrderDate);

                    IDbDataParameter LastModifiedBy = cmd.CreateParameter();
                    LastModifiedBy.Direction = ParameterDirection.Input;
                    LastModifiedBy.Value = int.Parse(objLOM.LastModifiedBy);
                    LastModifiedBy.ParameterName = "@LastModifiedBy";
                    LastModifiedBy.DbType = DbType.Int64;
                    cmd.Parameters.Add(LastModifiedBy);

                    IDbDataParameter OrderReason = cmd.CreateParameter();
                    OrderReason.Direction = ParameterDirection.Input;
                    OrderReason.Value = objLOM.OrderForReason;
                    OrderReason.ParameterName = "@OrderReason";
                    OrderReason.DbType = DbType.String;
                    cmd.Parameters.Add(OrderReason);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    string _orderId = cmd.ExecuteScalar().ToString();
                    if (_orderId != "")
                    {
                        _insertQry = "Insert into dbo.PatientLabTestResults Select a.PatientId, a.AppId ,OrderId, Id as TestResultId, '' as Result, '' as Unit, '' as MinRange, '' as MaxRange, '' as Range, '' as Comment ,0 as IsArchieved From dbo.PatientLabOrders a with(nolock) inner join dbo.LoincTestResults b on a.LoincNumber = b.ParentLoincNumber Where a.PatientId = '" + ClinicalComponent.PatientId + "' and a.AppId = '" + ClinicalComponent.AppointmentId + "' and a.LoincNumber = '" + objLOM.LOINCNumber + "' and a.OrderId = '" + _orderId + "'";
                        using (IDbCommand cmd1 = ClinicalComponent._Connection.CreateCommand())
                        {
                            cmd1.CommandType = CommandType.Text;
                            cmd1.CommandText = _insertQry;
                            cmd1.ExecuteNonQuery();
                        }
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }
        public bool UpdateExistingOrderDetail(LabOrderModel objLOM, bool isLoincChanged)
        {
            bool retStatus = true;
            string _error = "";
            try
            {
                string _UpdateQry = "Update dbo.PatientLabOrders Set Status = @Status, OrderDate = @OrderDate, LastModifiedDate = GETDATE(), LastModifiedBy = @LastModifiedBy, OrderReason = @OrderReason, LoincNumber = @LoincNumber Where OrderId = @OrderId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter Status = cmd.CreateParameter();
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = objLOM.Status;
                    Status.ParameterName = "@Status";
                    Status.DbType = DbType.Int16;
                    cmd.Parameters.Add(Status);

                    IDbDataParameter OrderDate = cmd.CreateParameter();
                    OrderDate.Direction = ParameterDirection.Input;
                    OrderDate.Value = objLOM.OrderDate;
                    OrderDate.ParameterName = "@OrderDate";
                    OrderDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(OrderDate);

                    IDbDataParameter LastModifiedBy = cmd.CreateParameter();
                    LastModifiedBy.Direction = ParameterDirection.Input;
                    LastModifiedBy.Value = objLOM.LastModifiedBy;
                    LastModifiedBy.ParameterName = "@LastModifiedBy";
                    LastModifiedBy.DbType = DbType.String;
                    cmd.Parameters.Add(LastModifiedBy);

                    IDbDataParameter OrderId = cmd.CreateParameter();
                    OrderId.Direction = ParameterDirection.Input;
                    OrderId.Value = objLOM.OrderId;
                    OrderId.ParameterName = "@OrderId";
                    OrderId.DbType = DbType.Int64;
                    cmd.Parameters.Add(OrderId);

                    IDbDataParameter OrderReason = cmd.CreateParameter();
                    OrderReason.Direction = ParameterDirection.Input;
                    OrderReason.Value = objLOM.OrderForReason;
                    OrderReason.ParameterName = "@OrderReason";
                    OrderReason.DbType = DbType.String;
                    cmd.Parameters.Add(OrderReason);

                    IDbDataParameter LoincNumber = cmd.CreateParameter();
                    LoincNumber.Direction = ParameterDirection.Input;
                    LoincNumber.Value = objLOM.LOINCNumber;
                    LoincNumber.ParameterName = "@LoincNumber";
                    LoincNumber.DbType = DbType.String;
                    cmd.Parameters.Add(LoincNumber);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _UpdateQry;
                    if (isLoincChanged)
                    {
                        int id = cmd.ExecuteNonQuery();
                        if (id > 0)
                        {
                            string _deleteQry = "Delete from PatientLabTestResults where LabOrderId = '" + objLOM.OrderId + "'";
                            string _insertQry = "Insert into dbo.PatientLabTestResults Select a.PatientId, a.AppId ,OrderId, Id as TestResultId, '' as Result, '' as Unit, '' as MinRange, '' as MaxRange, '' as Range, '' as Comment ,0 as IsArchieved From dbo.PatientLabOrders a with(nolock) inner join dbo.LoincTestResults b on a.LoincNumber = b.ParentLoincNumber Where a.PatientId = '" + ClinicalComponent.PatientId + "' and a.AppId = '" + ClinicalComponent.AppointmentId + "' and a.LoincNumber = '" + objLOM.LOINCNumber + "' and a.OrderId = '" + objLOM.OrderId + "'";
                            using (IDbCommand cmd1 = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd1.CommandType = CommandType.Text;
                                cmd1.CommandText = _deleteQry;
                                cmd1.ExecuteNonQuery();

                                cmd1.CommandType = CommandType.Text;
                                cmd1.CommandText = _insertQry;
                                cmd1.ExecuteNonQuery();
                            }
                        }
                    }
                    else
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }

        public LabOrderModel ReteriveOrderDetail(string _orderId)
        {
            LabOrderModel objLMO = new LabOrderModel();
            string _error = "";
            try
            {
                DataTable dtLabOrder = new DataTable();
                string _SelectQry = "Select a.OrderId, a.OrderDate, a.LoincNumber, a.Status, a.OrderReason, c.ResourceName as ProviderName, d.ResourceName as AssignedName, a.LastModifiedDate, a.OrderType From dbo.PatientLabOrders a inner join dbo.Appointments b on a.AppId = b.AppointmentId inner join dbo.Resources c on c.ResourceId = b.ResourceId1 inner join dbo.Resources d on a.LastModifiedBy = d.ResourceId Where OrderId = @OrderId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter idbOrderId = cmd.CreateParameter();
                    idbOrderId.Direction = ParameterDirection.Input;
                    idbOrderId.Value = _orderId;
                    idbOrderId.ParameterName = "@OrderId";
                    idbOrderId.DbType = DbType.Int64;
                    cmd.Parameters.Add(idbOrderId);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _SelectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtLabOrder.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
                if (dtLabOrder.Rows.Count == 1)
                {
                    objLMO.OrderId = dtLabOrder.Rows[0]["OrderId"].ToString();
                    objLMO.OrderDate = dtLabOrder.Rows[0]["OrderDate"].ToString();
                    objLMO.LOINCNumber = dtLabOrder.Rows[0]["LoincNumber"].ToString();
                    objLMO.Status = dtLabOrder.Rows[0]["Status"].ToString() == "1" ? "Pending" : "Completed";
                    objLMO.OrderType = dtLabOrder.Rows[0]["OrderType"].ToString();
                    objLMO.OrderForReason = dtLabOrder.Rows[0]["OrderReason"].ToString();
                }
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return objLMO;
        }

        public DataTable GetPatientLabOrderTestResults(string _orderId)
        {
            DataTable dtPatientLabTestResult = new DataTable();
            string _error = "";
            try
            {
                string _selectQry = "Select b.CompanyName as Col1,a.Id as Col2, Result as Col3, Unit as Col4, MinRange as Col5, MaxRange as Col6, Range as Col7  From PatientLabTestResults a inner join dbo.LoincTestResults b on a.LabTestResultId = b.Id Where LabOrderId = @OrderId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    //IDbDataParameter idbPatientId = cmd.CreateParameter();
                    //idbPatientId.Direction = ParameterDirection.Input;
                    //idbPatientId.Value = ClinicalComponent.PatientId;
                    //idbPatientId.ParameterName = "@PatientId";
                    //idbPatientId.DbType = DbType.Int64;
                    //cmd.Parameters.Add(idbPatientId);

                    //IDbDataParameter idbAppointmentId = cmd.CreateParameter();
                    //idbAppointmentId.Direction = ParameterDirection.Input;
                    //idbAppointmentId.Value = ClinicalComponent.AppointmentId;
                    //idbAppointmentId.ParameterName = "@AppointmentId";
                    //idbAppointmentId.DbType = DbType.Int64;
                    //cmd.Parameters.Add(idbAppointmentId);

                    IDbDataParameter idbOrderId = cmd.CreateParameter();
                    idbOrderId.Direction = ParameterDirection.Input;
                    idbOrderId.Value = _orderId;
                    idbOrderId.ParameterName = "@OrderId";
                    idbOrderId.DbType = DbType.Int64;
                    cmd.Parameters.Add(idbOrderId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtPatientLabTestResult.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return dtPatientLabTestResult;
        }
    }
}
