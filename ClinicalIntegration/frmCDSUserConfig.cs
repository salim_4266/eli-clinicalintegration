﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmCDSUserConfig : Form
    {
        public string ResourceType { get; set; }
        public Boolean enableIntervention { get; set; }
        public Boolean enableInfo { get; set; }
        public Boolean enableConfig { get; set; }
        public frmCDSUserConfig()
        {
            InitializeComponent();
        }
        public static List<CDSUserRoles> lstUserRoles { get; set; }
        private void frmCDSUserConfig_Load(object sender, EventArgs e)
        {
            try
            {
                CDSConfig objCDSConfig = new CDSConfig();
                lstUserRoles = objCDSConfig.GetCDSUserRoles(ResourceType);
                grdProcedure.AutoGenerateColumns = false;
                grdProcedure.DataSource = lstUserRoles;
                updateColumnStatus();
                UpdateInformationStatus();
                grdProcedure.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
                grdProcedure.EnableHeadersVisualStyles = false;
                grdProcedure.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
                grdProcedure.EnableHeadersVisualStyles = false;
                grdProcedure.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                grdProcedure.ColumnHeadersHeight = 34;
                grdProcedure.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
                button2.TabStop = false;
                button2.FlatStyle = FlatStyle.Flat;
                button2.FlatAppearance.BorderSize = 0;
            }
            catch (Exception)
            {

            }
        }

        private void updateColumnStatus()
        {

            for (int i = 0; i < lstUserRoles.Count; i++)
            {
                if (enableConfig)
                {

                }
                else

                {
                    DataGridViewCheckBoxCell chkCell = (DataGridViewCheckBoxCell)grdProcedure.Rows[i].Cells[1];
                    chkCell.Value = false;
                    chkCell.FlatStyle = FlatStyle.Flat;
                    chkCell.Style.ForeColor = Color.DarkGray;
                    chkCell.ReadOnly = true;
                }
                if (enableIntervention )
                {
                }
                else
                {
                    DataGridViewCheckBoxCell chkCell = (DataGridViewCheckBoxCell)grdProcedure.Rows[i].Cells[2];
                    chkCell.Value = false;
                    chkCell.FlatStyle = FlatStyle.Flat;
                    chkCell.Style.ForeColor = Color.DarkGray;
                    chkCell.ReadOnly = true;
                }
                if (enableInfo)
                {
                }
                else
                {
                    DataGridViewCheckBoxCell chkCell = (DataGridViewCheckBoxCell)grdProcedure.Rows[i].Cells[3];
                    chkCell.Value = false;
                    chkCell.FlatStyle = FlatStyle.Flat;
                    chkCell.Style.ForeColor = Color.DarkGray;
                    chkCell.ReadOnly = true;
                }

            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateInformationStatus()
        {
            for (int i = 0; i < lstUserRoles.Count; i++)
            {
                if (lstUserRoles[i].Alerts)
                {
                }
                else
                {
                    DataGridViewCheckBoxCell chkCell = (DataGridViewCheckBoxCell)grdProcedure.Rows[i].Cells[3];
                    chkCell.Value = false;
                    chkCell.FlatStyle = FlatStyle.Flat;
                    chkCell.Style.ForeColor = Color.DarkGray;
                    chkCell.ReadOnly = true;
                }
            }
        }
        private void grdProcedure_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)grdProcedure.Rows[e.RowIndex].Cells[e.ColumnIndex];
                if (chk.ReadOnly == true && chk.Style.ForeColor == Color.DarkGray)
                {
                    return;
                }
                string _updateQry = "";
                int columnIndex = e.ColumnIndex;
                switch (columnIndex)
                {
                    case 1:
                        _updateQry = "UpdATE CDS_UserPermissions sET CDSConfiguration = CASE(CDSConfiguration) When 0 Then 1 Else 0 End wHERE ResourceId = '" + lstUserRoles[e.RowIndex].Id + "'";
                        break;
                    case 2:
                        _updateQry = "UpdATE CDS_UserPermissions sET Alerts = CASE(Alerts) When 0 Then 1 Else 0  End,Info = CASE(Alerts) When 0 Then 1 Else 0 end wHERE ResourceId = '" + lstUserRoles[e.RowIndex].Id + "'";
                        break;
                    case 3:
                        _updateQry = "UpdATE CDS_UserPermissions sET Info = CASE(Info) When 0 Then 1 Else 0 End wHERE ResourceId = '" + lstUserRoles[e.RowIndex].Id + "'";
                        break;
                    default:
                        break;
                }
                CDSConfig objCDS = new CDSConfig();
                objCDS.updateCDS(_updateQry);
                lstUserRoles = objCDS.GetCDSUserRoles(ResourceType);
                grdProcedure.DataSource = lstUserRoles;
                updateColumnStatus();
                UpdateInformationStatus();
            }

            catch (Exception)
            {
            }
        }
    }
}
