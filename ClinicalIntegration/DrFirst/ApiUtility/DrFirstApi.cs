﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Data;

namespace ClinicalIntegration
{
    public class DrFirstApi
    {
        /// <summary>
        /// Invoke API
        /// </summary>
        /// <param name="demoGraphicsXml">api xml</param>
        public void InvokeSendPatientApi(string action, string demoGraphicsXml, out bool error, out string responseXml, bool uploadUrl, string patientId, string userid, DateTime dateTime, string encounterId, string PracticeToken, int AppTypeId, IDbConnection connection, bool apiStatus, bool IsError, int maxTry, string rcopia = "")
        {

            
            string url = string.Empty;
            responseXml = string.Empty;
            error = false;
            if (maxTry == 0)
            {
                LogApiCall log = new LogApiCall();
                int appType = (int)ApiActionType.ApiRetry;
                log.LogErxActivity(demoGraphicsXml, patientId, userid, DateTime.Now, responseXml, encounterId, PracticeToken, appType, connection, apiStatus, error, rcopia);
            }


            do
            {
                --maxTry;
                DateTime apiInvokeBeginningTime = DateTime.Now;

                try
                {

                    if (uploadUrl && (!String.IsNullOrEmpty(ErxComponent.StagingUploadUrl)))
                        url = ErxComponent.StagingUploadUrl;
                    else
                        url = ErxComponent.StagingDownloadUrl;

                    demoGraphicsXml = demoGraphicsXml.Trim('?').Replace("&", " and ").Replace("%", " percent");

                    WebRequest request = WebRequest.Create(url);
                    request.Method = "POST";
                    byte[] byteArray = Encoding.UTF8.GetBytes(demoGraphicsXml);
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = byteArray.Length;
                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    using (WebResponse response = request.GetResponse())
                    {
                        dataStream = response.GetResponseStream();
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            string xml = reader.ReadToEnd();
                            responseXml = xml;
                            if (xml.IndexOf("<Error>") > 0)
                            {
                                int pos1 = xml.IndexOf("<Text>", xml.IndexOf("<Error>")) + 6;
                                int pos2 = xml.IndexOf("</Text>", pos1);
                                xml = xml.Substring(pos1, pos2 - pos1);
                                responseXml = xml;
                                error = true;
                            }
                            else error = false;
                            reader.Close();
                            dataStream.Close();
                        }
                        response.Close();
                    }
                    break;
                }
                catch (Exception)
                {
                    DateTime apiInvokeEndTime = DateTime.Now;
                    var miliSeconds = ((apiInvokeEndTime - apiInvokeBeginningTime).TotalMilliseconds);
                    if (miliSeconds < 20000)
                    {
                        Thread.Sleep(int.Parse(miliSeconds.ToString()));
                    }
                }
            } while (maxTry > 0);
        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("  :{0}", logMessage);
            w.WriteLine("-------------------------------");
        }
    }

    public enum ApiAction
    {
        created,
        updated,
        deleted
    }

    public enum ApiActionType
    {
        GetNotification = 1,
        SendPatient = 2,
        SendMedications = 3,
        SendAllergy = 4,
        LaunchSSOPatient = 5,
        UpdateMedications = 6,
        UpdatePrescriptionPatientLevelPending = 7,
        UpdatePrescriptionPatientLevelComplete = 8,
        LaunchSSOMessage = 9,
        LaunchSSOReport = 10,
        UpdatePrescriptionPracticeLevelCompleted = 11,
        UpdateMedicationPracticeLevelCompleted = 12,
        ApiRetry = 13,
        //SendProblems = 3, //UpdateAllergy = 6,
    }
}
