﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public class Diagnoses
    {
        string connection = string.Empty;
        public void SendDiagnoses(string patientId, string appointmentId, string userId, string PracticeToken, IDbConnection sqlConString)
        {
            try
            {
                
                ErxComponent.InitializeErxComponent(sqlConString);
                string action;
                string DiagnosesXml = GetDiagnosesData(patientId, appointmentId, out action, sqlConString);

                if (!string.IsNullOrEmpty(DiagnosesXml))
                {
                    string diagnosesXml = "xml=" + DiagnosesXml;
                    DrFirstApi api = new DrFirstApi();
                    bool error;
                    int apiType = -3;// (int)ApiActionType.SendProblems;
                    bool apiStatus = false;
                    string response = string.Empty;
                    string command = action;
                    int maxTry = 3;
                    api.InvokeSendPatientApi(command, diagnosesXml, out error, out response, true, patientId, userId, DateTime.Now, appointmentId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);
                    DateTime dateTime = DateTime.Now;
                    string xmlResponse = response;


                    LogApiCall log = new LogApiCall();
                    log.LogErxActivity(diagnosesXml, patientId, userId, dateTime, xmlResponse, appointmentId, PracticeToken, apiType, sqlConString, apiStatus, error);

                }
            }
            catch (Exception ee)
            {
                System.Windows.Forms.MessageBox.Show(ee.InnerException.ToString());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        private string GetDiagnosesData(string patientId, string appointmentId, out string action, IDbConnection con)
        {
            action = string.Empty;
            DataTable dt = new DataTable();
            string patientDignosesXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;

            //query = "[DrFirst].[GetPatientDiagnosis]";
            query = "[DrFirst].[GetPatientProblemDiagnosis]";
            if (con.State.ToString() == "Closed")
                con.Open();
            using (IDbCommand cmd = con.CreateCommand())
            {
                IDbDataParameter parPatient = cmd.CreateParameter();
                parPatient.Direction = ParameterDirection.Input;
                parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                parPatient.ParameterName = "@PatientId";
                parPatient.DbType = DbType.Int64;
                cmd.Parameters.Add(parPatient);

                IDbDataParameter parAppointment = cmd.CreateParameter();
                parAppointment.Direction = ParameterDirection.Input;
                parAppointment.Value = int.Parse(appointmentId);
                parAppointment.ParameterName = "@appointmentID";
                parAppointment.DbType = DbType.Int64;
                cmd.Parameters.Add(parAppointment);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
                if (dt.Rows.Count > 0)
                {
                    patientDignosesXml = GenerateXmlForDiagnoses(dt, out action);
                }
                return patientDignosesXml;
            }
        }

        private string GenerateXmlForDiagnoses(DataTable dt, out string action)
        {
            action = string.Empty;
            XmlDocument doc = new XmlDocument();
            var xmlDocumentElement = doc.DocumentElement;

            var xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = doc.CreateElement("RCExtRequest");
            doc.AppendChild(rootNode);

            XmlAttribute version = doc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = doc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = doc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = doc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = doc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = doc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = doc.CreateElement("Request");
            rootNode.AppendChild(request);

            var checkEligibility = doc.CreateElement("CheckEligibility");
            request.AppendChild(checkEligibility);

            var command = doc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "send_problem";
            action = "send_problem";

            XmlNode problemList = doc.CreateElement("ProblemList");
            request.AppendChild(problemList);

            foreach (DataRow diag in dt.Rows)
            {
                var problem = doc.CreateElement("Problem");
                problemList.AppendChild(problem);

                var EncounterList = doc.CreateElement("EncounterList");
                problem.AppendChild(EncounterList);

                var Encounter = doc.CreateElement("Encounter");
                EncounterList.AppendChild(Encounter);

                var ExternalID = doc.CreateElement("ExternalID");
                Encounter.AppendChild(ExternalID);
                ExternalID.InnerText = diag["AppointmentId"].ToString();

                var EncounterDate = doc.CreateElement("EncounterDate");
                Encounter.AppendChild(EncounterDate);
                EncounterDate.InnerText = diag["AppointmentDate"].ToString();

                var deleted = doc.CreateElement("Deleted");
                problem.AppendChild(deleted);
                deleted.InnerText = diag["Status"].ToString() == "Active" ? "n" : "y";

                var status = doc.CreateElement("Status");
                if (diag["Status"].ToString() == "Active")
                {
                    problem.AppendChild(status);
                    status.InnerXml = "<Active/>";
                }
                else
                {
                    problem.AppendChild(status);
                    status.InnerXml = "<InActive/>";
                }

                var probExternalID = doc.CreateElement("ExternalID");
                problem.AppendChild(probExternalID);
                probExternalID.InnerText = diag["id"].ToString();

                var patient = doc.CreateElement("Patient");
                problem.AppendChild(patient);

                var patExternalID = doc.CreateElement("ExternalID");
                patient.AppendChild(patExternalID);
                patExternalID.InnerText = diag["Patientid"].ToString();

                var SNOMED = doc.CreateElement("SNOMED");
                problem.AppendChild(SNOMED);

                var conceptID = doc.CreateElement("ConceptID");
                SNOMED.AppendChild(conceptID);
                conceptID.InnerText = diag["ConceptID"].ToString();

                var description = doc.CreateElement("Description");
                SNOMED.AppendChild(description);
                description.InnerText = diag["Term"].ToString();


                var ICD9 = doc.CreateElement("ICD10");
                problem.AppendChild(ICD9);

                var code = doc.CreateElement("Code");
                ICD9.AppendChild(code);
                code.InnerText = diag["ICD10"].ToString();

                var descriptionICD9 = doc.CreateElement("Description");
                ICD9.AppendChild(descriptionICD9);
                descriptionICD9.InnerText = diag["ICD10DESCR"].ToString();

                var OnsetDate = doc.CreateElement("OnsetDate");
                problem.AppendChild(deleted);
                OnsetDate.InnerText = diag["OnsetDate"].ToString();


            }

            return doc.InnerXml;
        }
        private string GenerateXmlForDiagnoses_old(DataTable dt, out string action)
        {
            action = string.Empty;
            XmlDocument doc = new XmlDocument();
            var xmlDocumentElement = doc.DocumentElement;

            var xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = doc.CreateElement("RCExtRequest");
            doc.AppendChild(rootNode);

            XmlAttribute version = doc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = doc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = doc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = doc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = doc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = doc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = doc.CreateElement("Request");
            rootNode.AppendChild(request);

            var checkEligibility = doc.CreateElement("CheckEligibility");
            request.AppendChild(checkEligibility);

            var command = doc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "send_problem";
            action = "send_problem";

            XmlNode problemList = doc.CreateElement("ProblemList");
            request.AppendChild(problemList);

            foreach (DataRow diag in dt.Rows)
            {
                var problem = doc.CreateElement("Problem");
                problemList.AppendChild(problem);

                var deleted = doc.CreateElement("Deleted");
                problem.AppendChild(deleted);
                deleted.InnerText = diag["ImageDescriptor"].ToString() == "D" ? "y" : "n";

                var status = doc.CreateElement("Status");
                if (diag["ImageDescriptor"].ToString() == "D")
                {
                    problem.AppendChild(status);
                    status.InnerXml = "<InActive/>";
                }
                else
                {
                    problem.AppendChild(status);
                    status.InnerXml = "<Active/>";
                }

                var probExternalID = doc.CreateElement("ExternalID");
                problem.AppendChild(probExternalID);
                probExternalID.InnerText = diag["ClinicalId"].ToString();

                var patient = doc.CreateElement("Patient");
                problem.AppendChild(patient);
 
                var patExternalID = doc.CreateElement("ExternalID");
                patient.AppendChild(patExternalID);
                patExternalID.InnerText = diag["Patientid"].ToString();

                var SNOMED = doc.CreateElement("SNOMED");
                problem.AppendChild(patient);

                var conceptID = doc.CreateElement("ConceptID");
                SNOMED.AppendChild(conceptID);

                var description = doc.CreateElement("Description");
                SNOMED.AppendChild(description);

                var ICD9 = doc.CreateElement("ICD9");
                problem.AppendChild(ICD9);

                var code = doc.CreateElement("Code");
                ICD9.AppendChild(code);
                code.InnerText = diag["Code"].ToString();

                var descriptionICD9 = doc.CreateElement("Description");
                ICD9.AppendChild(descriptionICD9);
                descriptionICD9.InnerText = diag["Description"].ToString();
            }

            return doc.InnerXml;
        }
    }
}
