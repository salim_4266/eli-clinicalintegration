﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace EPLib
{
    [ComVisible(true)]
    public class SyncIODrFirst
    {
        string patientId = string.Empty;
        
        public void SyncDrFirstData(string parPatientId, string parPracticeToken, string parSqlConString)
        {
        }

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="updtMedicationResponse"></param>
        /// <param name="updtAllergyResponse"></param>
        /// <param name="sqlConString"></param>
        private void SyncData(string patientId,string updtMedicationResponse, string updtAllergyResponse, string sqlConString)
        {
            string patientDemographicsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string queryErxLogs = string.Empty;
            string queryErxResponseLogs = string.Empty;
            Guid Id = Guid.NewGuid();
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = sqlConString;

                queryErxLogs = "INSERT INTO [DrFirst].[ERxLogs](Id, DateTime, EncounterId,UserId,IsActive,TypeId) VALUES (@Id, @dateTime, @encounterId,@userid,@IsActive,@AppTypeId)";
                con.Open();
                using (SqlCommand cmd = new SqlCommand(queryErxLogs, con))
                {
                    // Add the parameters for the InsertCommand.
                    cmd.Parameters.AddWithValue("@patientId", patientId);
                    cmd.Parameters.AddWithValue("@updtMedicationResponse", updtMedicationResponse);
                    cmd.Parameters.AddWithValue("@updtAllergyResponse", updtAllergyResponse);
                    
                    cmd.CommandType = CommandType.Text;
                    adapter.InsertCommand = cmd;
                    adapter.InsertCommand.ExecuteNonQuery();
                }
            }
        }
    }
}
