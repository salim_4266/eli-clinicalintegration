﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public class ErxComponent
    {
        public static string StagingUploadUrl;
        public static string StagingDownloadUrl;
        public static string StagingSSOUrl;
        public static string StagingPortalHeader;
        public static string RequestCredentials;

        public static string VendorName;
        public static string SystemName;
        public static string RcopiaPracticeUsername;
        public static string VendorPassword;
        public static string APIVersion;

        public static string RCopiaPatientId;
        public static string RCopiaLastUpdated;

        public static string RcopiaUserExternalId;

        public static void InitializeErxComponent(IDbConnection con)
        {
            string qryStagingPortalHeader = string.Empty; 
            string qryStagingSSOUrl = string.Empty;
            string qryStagingDownloadUrl = string.Empty;
            string qryStagingUploadUrl = string.Empty;
            string qryCredentials = string.Empty;
            string qryInitialData = string.Empty;
            qryInitialData = "Select top 1 * from [DRFIRST].[PracticeConfiguration] Where IsActive = 1";
            GetQueryResult(con, qryInitialData);
        }

        private static void GetCredentials(string RequestCredentials)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(RequestCredentials);

            string xpathVersion = "Erx";
            var childNdVersion = doc.SelectNodes(xpathVersion);
            foreach (XmlNode childrenNode in childNdVersion)
            {
                APIVersion = childrenNode.SelectSingleNode("Version").InnerText;
            }

            string xpathSysName = "Erx";
            var childNdErx1 = doc.SelectNodes(xpathSysName);
            foreach (XmlNode childrenNode in childNdErx1)
            {
                SystemName = childrenNode.SelectSingleNode("SystemName").InnerText;
            }

            string xpathPracUsrName = "Erx";
            var childNdErx2 = doc.SelectNodes(xpathPracUsrName);
            foreach (XmlNode childrenNode in childNdErx2)
            {
                RcopiaPracticeUsername = childrenNode.SelectSingleNode("RcopiaPracticeUsername").InnerText;
            }

            string xpathVendorName = "Erx/Caller";
            var childNdCaller1 = doc.SelectNodes(xpathVendorName);
            foreach (XmlNode childrenNode in childNdCaller1)
            {
                VendorName = childrenNode.SelectSingleNode("VendorName").InnerText;
            }

            string xpathVendorPassword = "Erx/Caller";
            var childNdCaller2 = doc.SelectNodes(xpathVendorPassword);
            foreach (XmlNode childrenNode in childNdCaller2)
            {
                VendorPassword = childrenNode.SelectSingleNode("VendorPassword").InnerText;
            }
        }

        private static void GetQueryResult(IDbConnection connection, string query)
        {
            if (connection.State.ToString() == "Closed")
                connection.Open();
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;

                using (IDataReader dr = cmd.ExecuteReader())
                {
                    if(dr.Read())
                    {
                        if (!String.IsNullOrEmpty(dr["SSOUrl"].ToString()))
                            StagingSSOUrl = dr["SSOUrl"].ToString();
                        if (!String.IsNullOrEmpty(dr["DownloadUrl"].ToString()))
                            StagingDownloadUrl = dr["DownloadUrl"].ToString();
                        if (!String.IsNullOrEmpty(dr["UploadUrl"].ToString()))
                            StagingUploadUrl = dr["UploadUrl"].ToString();
                        if (!String.IsNullOrEmpty(dr["Version"].ToString()))
                            APIVersion = dr["Version"].ToString();
                        if (!String.IsNullOrEmpty(dr["SystemName"].ToString()))
                            SystemName = dr["SystemName"].ToString();
                        if (!String.IsNullOrEmpty(dr["PracticeUserName"].ToString()))
                            RcopiaPracticeUsername = dr["PracticeUserName"].ToString();
                        if (!String.IsNullOrEmpty(dr["VendorName"].ToString()))
                            VendorName = dr["VendorName"].ToString();
                        if (!String.IsNullOrEmpty(dr["VendorPassword"].ToString()))
                            VendorPassword = dr["VendorPassword"].ToString();
                        StagingPortalHeader = "service=rcopia&action=login&rcopia_user_external_id=&rcopia_user_id=<userExternalId>&rcopia_patient_external_id=<patientId>&rcopia_portal_system_name=" + SystemName + "&close_window=n&logout_url=Close&time=<time>&rcopia_practice_user_name=" + RcopiaPracticeUsername + "&startup_screen=<screencontext>&limp_mode=<n>";
                    }
                }
            }
        }
    }
}
