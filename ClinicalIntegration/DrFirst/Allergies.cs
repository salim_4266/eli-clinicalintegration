﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Xml;

namespace ClinicalIntegration
{

    public class Allergies
    {
        string connection = string.Empty;
        string userid = string.Empty;
        string encounterId = string.Empty;

        public void SendAllergies(string patientId, string appointmentId, string userId, string PracticeToken, IDbConnection sqlConString)
        {
            ErxComponent.InitializeErxComponent(sqlConString);
            string action;
            string AllergiesXml = GetAllergiesData(patientId, appointmentId, out action, sqlConString);
            if (!string.IsNullOrEmpty(AllergiesXml))
            {
                string allergiesXml = "xml=" + AllergiesXml;
                string command = action;
                DrFirstApi api = new DrFirstApi();
                bool apiStatus = false;
                bool error;
                int apiType = (int)ApiActionType.SendAllergy;
                string response = string.Empty;
                int maxTry = 3;
                api.InvokeSendPatientApi(command, allergiesXml, out error, out response, true, patientId, userId, DateTime.Now, appointmentId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);
                DateTime dateTime = DateTime.Now;
                LogApiCall log = new LogApiCall();
                log.LogErxActivity(allergiesXml, patientId, userId, dateTime, response, appointmentId, PracticeToken, apiType, sqlConString, apiStatus, error);
            }
        }

        /// <summary>
        /// Retrieve Allergies data.
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        private string GetAllergiesData(string patientId, string appointmentId, out string action, IDbConnection con)
        {
            action = string.Empty;
            DataTable dt = new DataTable();
            string patientAllergiessXml = string.Empty;
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                string query = string.Empty;
                query = "DrFirst.GetPatientAllergy";
                if (con.State.ToString() == "Closed")
                    con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);

                    IDbDataParameter parAppointment = cmd.CreateParameter();
                    parAppointment.Direction = ParameterDirection.Input;
                    parAppointment.Value = int.Parse(appointmentId);
                    parAppointment.ParameterName = "@appointmentID";
                    parAppointment.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointment);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    patientAllergiessXml = GenerateXmlForAllergies(dt, out action);
                }
            }
            catch
            { }
            return patientAllergiessXml;
        }

        private string GenerateXmlForAllergies(DataTable dt, out string action)
        {
            action = string.Empty;
            var xmlDoc = new XmlDocument();
            var documentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "send_allergy";
            action = "send_allergy";

            var allergyList = xmlDoc.CreateElement("AllergyList");
            request.AppendChild(allergyList);

            string RXNormId = string.Empty;
            string RXNormIdDb = string.Empty;
            string NDCID = string.Empty;
            foreach (DataRow item in dt.Rows)
            {
                var allergy = xmlDoc.CreateElement("Allergy");
                allergyList.AppendChild(allergy);

                var deleted = xmlDoc.CreateElement("Deleted");
                allergy.AppendChild(deleted);
                deleted.InnerText = item["AllergyStatus"].ToString() == "D" ? "y" : "n";

                var status = xmlDoc.CreateElement("Status");
                if (item["AllergyStatus"].ToString() == "D")
                {
                    allergy.AppendChild(status);
                    status.InnerXml = "<Deleted/>";
                }
                else
                {
                    allergy.AppendChild(status);
                    status.InnerXml = "<Active/>";
                }

                var allergyExternalID = xmlDoc.CreateElement("ExternalID");
                allergy.AppendChild(allergyExternalID);
                allergyExternalID.InnerText = item["clinicalID"].ToString();

                var allergyPatient = xmlDoc.CreateElement("Patient");
                allergy.AppendChild(allergyPatient);

                var patExternalID = xmlDoc.CreateElement("ExternalID");
                allergyPatient.AppendChild(patExternalID);
                patExternalID.InnerText = item["PatientId"].ToString();

                var alrgAllergen = xmlDoc.CreateElement("Allergen");
                allergy.AppendChild(alrgAllergen);

                var alrgnName = xmlDoc.CreateElement("Name");
                alrgAllergen.AppendChild(alrgnName);
                alrgnName.InnerText = item["AllergyName"].ToString();

                string ConceptType = item["ConceptType"].ToString();

                if (!ConceptType.Equals("GROUP"))
                {
                    var Drug = xmlDoc.CreateElement("Drug");
                    alrgAllergen.AppendChild(Drug);

                    RXNormId = string.Empty;
                    RXNormIdDb = string.Empty;

                    if (!string.IsNullOrEmpty(item["AllergyName"].ToString()))
                    {
                        RXNormId = Getrxnormid(item["AllergyName"].ToString());

                        RXNormIdDb = item["RxnormID"] != null ? item["RxnormID"].ToString() : string.Empty;

                        if (RXNormId != "" || RXNormIdDb != "")
                        {
                            if (RXNormId != "")
                                NDCID = GetNdcocde(RXNormId);

                            if (NDCID == "")
                            {
                                if (RXNormIdDb != "")
                                    NDCID = GetNdcocde(RXNormIdDb);
                            }

                            //if (!string.IsNullOrEmpty(RXNormId) && !string.IsNullOrEmpty(NDCID))
                            //{
                            var NDCId = xmlDoc.CreateElement("NDCID");
                            Drug.AppendChild(NDCId);
                            NDCId.InnerText = NDCID;

                            var RxnormID = xmlDoc.CreateElement("RxnormID");
                            Drug.AppendChild(RxnormID);
                            RxnormID.InnerText = RXNormIdDb == "" ? RXNormId : RXNormIdDb;

                            //}// item["RxnormID"] != null ? item["RxnormID"].ToString() : string.Empty;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(ConceptType) && ConceptType == "GROUP")
                {
                    var Group = xmlDoc.CreateElement("Group");
                    alrgAllergen.AppendChild(Group);

                    var GroupName = xmlDoc.CreateElement("Name");
                    Group.AppendChild(GroupName);
                    GroupName.InnerText = item["AllergyName"].ToString();


                    var GroupID = xmlDoc.CreateElement("GroupID");
                    Group.AppendChild(GroupID);

                    var FDBSpecificGroupID = xmlDoc.CreateElement("FDBSpecificGroupID");
                    Group.AppendChild(FDBSpecificGroupID);
                    FDBSpecificGroupID.InnerText = item["FDBCONCEPTID"].ToString();

                }

                var OnsetDate = xmlDoc.CreateElement("OnsetDate");
                allergy.AppendChild(OnsetDate);
                OnsetDate.InnerText = DateTime.Now.ToString();

                var Reaction = xmlDoc.CreateElement("Reaction");
                allergy.AppendChild(Reaction);
                Reaction.InnerText = item["Reaction"] != null ? item["Reaction"].ToString() : string.Empty;
            }
            return xmlDoc.InnerXml;
        }

        private string Getrxnormid(string allergyname)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            string url = "https://rxnav.nlm.nih.gov/REST/rxcui?name=" + allergyname + "";
            var client = new WebClient();
            var UDIjson = client.DownloadString(url);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(UDIjson);
            string finalxml = doc.InnerXml;
            finalxml = finalxml.Replace(doc.FirstChild.OuterXml, "");
            XmlDocument docnew = new XmlDocument();
            docnew.LoadXml(finalxml);

            if (docnew.ChildNodes.Count > 0)
            {
                XmlNode node = docnew.SelectSingleNode("//idGroup");
                if (node.ChildNodes.Count > 1)
                {
                    return node.ChildNodes[1].InnerText;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        private string GetNdcocde(string Rxcui)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            string url = "https://rxnav.nlm.nih.gov/REST/rxcui/" + Rxcui + "/ndcs.json";
            var client = new WebClient();
            var UDIjson = client.DownloadString(url);
            RootObject returnobj = JsonConvert.DeserializeObject<RootObject>(UDIjson);
            string ndccodes = "";

            if (returnobj.ndcGroup.ndcList == null)
            {
                return ndccodes;
            }
            foreach (var ndc in returnobj.ndcGroup.ndcList.ndc)
            {
                if (ndccodes == "")
                {
                    ndccodes = ndc.ToString();
                }
                else
                {
                    ndccodes = ndccodes + "," + ndc.ToString();
                }
            }

            return ndccodes;
        }
        public string GetUpdatedAllergies(string patientId, string parEncounterId, string parUserId, string PracticeToken, IDbConnection sqlConString)
        {
            string action;
            string medicationDwnldReq = "xml=" + GenerateDownloadRequestXml(patientId, out action);
            bool apiStatus = false;
            int apiType = -1;// (int)ApiActionType.UpdateAllergy;
            DrFirstApi api = new DrFirstApi();
            bool error;
            string response = string.Empty;
            ErxComponent.InitializeErxComponent(sqlConString);
            string command = action;
            int maxTry = 3;
            api.InvokeSendPatientApi(command, medicationDwnldReq, out error, out response, false, patientId, parUserId, DateTime.Now, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);

            LogApiCall log = new LogApiCall();
            log.LogErxActivity(medicationDwnldReq, patientId, parUserId, DateTime.Now, response, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error);
            return response;
        }

        private string GenerateDownloadRequestXml(string patientId, out string action)
        {
            action = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            var xmlDocumentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "update_allergy";
            action = "update_allergy";

            string LastUpdateDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss", CultureInfo.InvariantCulture);

            var lastUpdateDate = xmlDoc.CreateElement("LastUpdateDate");
            request.AppendChild(lastUpdateDate);
            lastUpdateDate.InnerText = LastUpdateDate;

            var returnAllNDCIDs = xmlDoc.CreateElement("ReturnAllNDCIDs");
            request.AppendChild(lastUpdateDate);
            returnAllNDCIDs.InnerText = "y";

            XmlNode Patient = xmlDoc.CreateElement("Patient");
            request.AppendChild(Patient);

            XmlNode patRcopiaID = xmlDoc.CreateElement("RcopiaID");
            Patient.AppendChild(patRcopiaID);
            patRcopiaID.InnerText = ErxComponent.RCopiaPatientId;

            XmlNode patExternalID = xmlDoc.CreateElement("ExternalID");
            Patient.AppendChild(patExternalID);
            patExternalID.InnerText = patientId;

            return xmlDoc.InnerXml;
        }

    }
}

