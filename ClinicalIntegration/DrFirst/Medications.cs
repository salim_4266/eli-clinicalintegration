﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Xml;

namespace ClinicalIntegration
{

    public class DrfirstMedications
    {
        string userid = string.Empty;
        string encounterId = string.Empty;

        public void SendMedications(string patientId, string appointmentId, string userId, string PracticeToken, IDbConnection sqlConString)
        {

            bool apiStatus = false;
            ErxComponent.InitializeErxComponent(sqlConString);
            string action;
            string MedicationsXml = GetMedicationsData(patientId, out action, sqlConString);
            string command = action;
            if (!string.IsNullOrEmpty(MedicationsXml))
            {

                string medicationXml = "xml=" + MedicationsXml;
                DrFirstApi api = new DrFirstApi();
                bool error;
                string response = string.Empty;
                int apiType = (int)ApiActionType.SendMedications;
                int maxTry = 3;
                api.InvokeSendPatientApi(command, medicationXml, out error, out response, true, patientId, userId, DateTime.Now, appointmentId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);
                DateTime dateTime = DateTime.Now;
                string xmlResponse = response;

                XmlDocument xmlDoc = new XmlDocument();
                if (!string.IsNullOrEmpty(xmlResponse) && (xmlResponse.Contains("RCExtResponse")))
                {
                    xmlDoc.LoadXml(xmlResponse);
                    string RCExtResponse = "RCExtResponse";
                    var ResponseEndTime = xmlDoc.SelectNodes(RCExtResponse);
                    string ResponseEndTimestamp = string.Empty;

                    foreach (XmlNode childrenNode in ResponseEndTime)
                    {
                        ResponseEndTimestamp = childrenNode.SelectSingleNode("ResponseEndTimestamp").FirstChild.Value;
                        break;
                    }
                    ErxComponent.RCopiaLastUpdated = ResponseEndTimestamp;

                    LogApiCall log = new LogApiCall();
                    log.LogErxActivity(medicationXml, patientId, userId, DateTime.Now, xmlResponse, appointmentId, PracticeToken, apiType, sqlConString, apiStatus, error);
                }


            }
        }

        private string GetMedicationsData(string patientId, out string action, IDbConnection con)
        {
            string patientMedicationsXml = string.Empty;
            action = string.Empty;
            try
            {

                DataTable dt = new DataTable();

                SqlDataAdapter adapter = new SqlDataAdapter();
                string query = string.Empty;

                query = "[DrFirst].[GetPatientMedications]";

                if (con.State.ToString() == "Closed")
                    con.Open();

                using (IDbCommand cmd = con.CreateCommand())
                {
                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    patientMedicationsXml = GenerateXmlForMedications(dt, out action);
                }
            }
            catch(Exception ee)
            {

            }
            return patientMedicationsXml;
        }
        public void SetMedicationsData(string patientId, string appointmentId, string xmlResponse, IDbConnection SqlConString)
        {
            DrFirstApi api = new DrFirstApi();
            LogApiCall log = new LogApiCall();
            bool error = true;
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;
            int apiType = 0;
            if (SqlConString.State.ToString() == "Closed")
                SqlConString.Open();
            using (IDbCommand cmd = SqlConString.CreateCommand())
            {
                try
                {
                    if (string.IsNullOrEmpty(patientId) && string.IsNullOrEmpty(appointmentId))
                    {
                        query = "[DrFirst].[MedicationResponsePracticeLevel]";
                        apiType = (int)ApiActionType.UpdateMedicationPracticeLevelCompleted;
                    }
                    else
                    {
                        apiType = (int)ApiActionType.UpdateMedications;
                        query = "[DrFirst].[UpdateMedicationResponse]";
                        IDbDataParameter parPatient = cmd.CreateParameter();
                        parPatient.Direction = ParameterDirection.Input;
                        parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                        parPatient.ParameterName = "@PatientId";
                        parPatient.DbType = DbType.Int64;
                        cmd.Parameters.Add(parPatient);

                        IDbDataParameter parAppointmentId = cmd.CreateParameter();
                        parAppointmentId.Direction = ParameterDirection.Input;
                        parAppointmentId.Value = int.Parse(appointmentId);
                        parAppointmentId.ParameterName = "@appointmentId";
                        parAppointmentId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parAppointmentId);
                    }

                    IDbDataParameter parXmlData = cmd.CreateParameter();
                    parXmlData.Direction = ParameterDirection.Input;
                    parXmlData.Value = xmlResponse;
                    parXmlData.ParameterName = "@xmlStringData";
                    parXmlData.DbType = DbType.String;
                    cmd.Parameters.Add(parXmlData);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = true;
                    log.LogErxActivity(xmlResponse, patientId, "", DateTime.Now, ex.InnerException.ToString(), "", "", apiType, SqlConString, false, error, string.Empty);
                    System.Windows.Forms.MessageBox.Show("Error occured. Please contact administrator");

                }
            }
        }
        public void SetPrescriptionData(string patientId, string appointmentId, string xmlResponse, IDbConnection SqlConString)
        {
            DrFirstApi api = new DrFirstApi();
            LogApiCall log = new LogApiCall();
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;
            bool error = true;

            query = "[DrFirst].[AddUpdatePrescriptionResponsePracticeLevel]";
            if (SqlConString.State.ToString() == "Closed")
                SqlConString.Open();
            using (IDbCommand cmd = SqlConString.CreateCommand())
            {
                try
                {
                    IDbDataParameter parXmlData = cmd.CreateParameter();
                    parXmlData.Direction = ParameterDirection.Input;
                    parXmlData.Value = xmlResponse;
                    parXmlData.ParameterName = "@xmlStringData";
                    parXmlData.DbType = DbType.String;
                    cmd.Parameters.Add(parXmlData);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = true;
                    log.LogErxActivity(xmlResponse, patientId, "", DateTime.Now, ex.InnerException.ToString(), "", "", (int)ApiActionType.UpdatePrescriptionPatientLevelComplete, SqlConString, false, error, string.Empty);
                    System.Windows.Forms.MessageBox.Show("Error occured. Please contact administrator");
                }
            }
        }

        private string Getrxnormid(string drugname)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            string url = "https://rxnav.nlm.nih.gov/REST/rxcui?name=" + drugname + "";
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            var client = new WebClient();
            var UDIjson = client.DownloadString(url);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(UDIjson);

            string finalxml = doc.InnerXml;
            finalxml = finalxml.Replace(doc.FirstChild.OuterXml, "");
            XmlDocument docnew = new XmlDocument();
            docnew.LoadXml(finalxml);

            if (docnew.ChildNodes.Count > 0)
            {
                XmlNode node = docnew.SelectSingleNode("//idGroup");
                if (node.ChildNodes.Count > 1)
                {
                    return node.ChildNodes[1].InnerText;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        private string GetNdcocde(string Rxcui)
        {
            string ndccodes = "";
            if (!string.IsNullOrEmpty(Rxcui))
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                string url = "https://rxnav.nlm.nih.gov/REST/rxcui/" + Rxcui + "/ndcs.json";
                var client = new WebClient();
                var UDIjson = client.DownloadString(url);
                RootObject returnobj = JsonConvert.DeserializeObject<RootObject>(UDIjson);
                if (returnobj.ndcGroup.ndcList == null)
                {
                    return ndccodes;
                }
                foreach (var ndc in returnobj.ndcGroup.ndcList.ndc)
                {
                    if (ndccodes == "")
                    {
                        ndccodes = ndc.ToString();
                    }
                    else
                    {
                        ndccodes = ndccodes + "," + ndc.ToString();
                    }
                }
            }
            return ndccodes;
        }
        private string GenerateXmlForMedications(DataTable dt, out string action)
        {
            action = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            var xmlDocumentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var checkEligibility = xmlDoc.CreateElement("CheckEligibility");
            request.AppendChild(checkEligibility);
            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "send_medication";
            action = "send_medication";

            XmlNode medicationList = xmlDoc.CreateElement("MedicationList");
            request.AppendChild(medicationList);

            string medsRoute = string.Empty;
            string RxNormId = string.Empty;
            string NDCID = string.Empty;

            string[] route = new string[] { "apply", "instill", "administer", "use", "take", "drink", "inject", "spray", "dissolve", "drink", "inhale", "insert", "chew" };

            foreach (DataRow drMed in dt.Rows)
            {
                if (string.IsNullOrEmpty(userid))
                    userid = drMed["LoginId"].ToString();
                encounterId = drMed["AppointmentId"].ToString();

                XmlNode medication = xmlDoc.CreateElement("Medication");
                medicationList.AppendChild(medication);

                XmlNode deleted = xmlDoc.CreateElement("Deleted");
                medication.AppendChild(deleted);
                deleted.InnerText = drMed["Status"].ToString();

                if (drMed["RcopiyaID"].ToString().Trim() != "")
                {
                    XmlNode RcopiyaID = xmlDoc.CreateElement("RcopiaID");
                    medication.AppendChild(RcopiyaID);
                    RcopiyaID.InnerText = drMed["RcopiyaID"].ToString();
                }

                XmlNode medExternalID = xmlDoc.CreateElement("ExternalID");
                medExternalID.InnerText = drMed["ClinicalIds"].ToString(); ;
                medication.AppendChild(medExternalID);

                XmlNode Patient = xmlDoc.CreateElement("Patient");
                medication.AppendChild(Patient);

                XmlNode patExternalID = xmlDoc.CreateElement("ExternalID");
                Patient.AppendChild(patExternalID);
                patExternalID.InnerText = drMed["Patientid"].ToString();

                XmlNode Provider = xmlDoc.CreateElement("Provider");
                medication.AppendChild(Provider);

                XmlNode provusername = xmlDoc.CreateElement("Username");
                Provider.AppendChild(provusername);
                provusername.InnerText = "iprovider35";

                XmlNode Sig = xmlDoc.CreateElement("Sig");
                medication.AppendChild(Sig);

                XmlNode Drug = xmlDoc.CreateElement("Drug");
                Sig.AppendChild(Drug);

                if (!string.IsNullOrEmpty(drMed["BrandName"].ToString()))
                {//drMed["BrandName"].ToString()
                    RxNormId = Getrxnormid(drMed["BrandName"].ToString());
                    NDCID = GetNdcocde(RxNormId);
                    //NDCID = "";   
                }

                if (string.IsNullOrEmpty(RxNormId) || string.IsNullOrEmpty(NDCID))
                {
                    if (drMed["NDC"].ToString().Trim() != "")
                    {
                        NDCID = drMed["NDC"].ToString();
                    }
                    if (drMed["RxnormID"].ToString().Trim() != "")
                    {
                        RxNormId = drMed["RxnormID"].ToString();
                    }
                }
                XmlNode drugNDCID = xmlDoc.CreateElement("NDCID");
                Drug.AppendChild(drugNDCID);
                drugNDCID.InnerText = NDCID;// drMed["NDC"].ToString();
                                            //}



                XmlNode drugRxnormID = xmlDoc.CreateElement("RxnormID");
                Drug.AppendChild(drugRxnormID);
                drugRxnormID.InnerText = RxNormId;// drMed["RxnormID"].ToString();

                XmlNode drugBrandName = xmlDoc.CreateElement("BrandName");
                Drug.AppendChild(drugBrandName);
                drugBrandName.InnerText = drMed["BrandName"].ToString();

                XmlNode drugGenericName = xmlDoc.CreateElement("GenericName");
                Drug.AppendChild(drugGenericName);
                drugGenericName.InnerText = drMed["GenericDrugName"].ToString();


                XmlNode drugFormD = xmlDoc.CreateElement("Form");
                Drug.AppendChild(drugFormD);
                drugFormD.InnerText = drMed["Form"].ToString();


                XmlNode drugStrength = xmlDoc.CreateElement("Strength");
                Drug.AppendChild(drugStrength);
                drugStrength.InnerText = drMed["Strength"].ToString();

                XmlNode drugSupply = xmlDoc.CreateElement("Supply");
                Drug.AppendChild(drugSupply);
                drugSupply.InnerText = drMed["Supply"].ToString();

                XmlNode SigAction = xmlDoc.CreateElement("Action");
                Sig.AppendChild(SigAction);

                medsRoute = drMed["PO"].ToString().ToUpper();

                if (medsRoute == "PO" || medsRoute == "BY MOUTH")
                {
                    SigAction.InnerText = "Take";
                }
                else if (medsRoute == "INSTILL IN EYES")
                {
                    SigAction.InnerText = "INSTILL";
                }
                else if (medsRoute == "APPLY TO LIDS")
                {
                    SigAction.InnerText = "Apply";
                }

                string[] actionValue = medsRoute.Split(' ');
                if (string.IsNullOrEmpty(SigAction.InnerText))
                {
                    if (Array.IndexOf(route, actionValue[0].ToString().ToLower()) > 0)
                        SigAction.InnerText = actionValue[0];
                }


                //string DosageUnit = drMed["Dosage"].ToString();
                string[] DosUnit = drMed["Dosage"].ToString().Split(' ');
                if (DosUnit.Count() > 1)
                {
                    // string[] DosUnit = DosageUnit.Split(' ');

                    XmlNode SigDose = xmlDoc.CreateElement("Dose");
                    Sig.AppendChild(SigDose);
                    SigDose.InnerText = DosUnit[0];

                    XmlNode SigDoseUnit = xmlDoc.CreateElement("DoseUnit");
                    Sig.AppendChild(SigDoseUnit);
                    //SigDoseUnit.InnerText = DosUnit[1];
                    SigDoseUnit.InnerText = string.Join(" ", DosUnit.Skip(1));
                }
                else
                {
                    XmlNode SigDose = xmlDoc.CreateElement("Dose");
                    Sig.AppendChild(SigDose);

                    XmlNode SigDoseUnit = xmlDoc.CreateElement("DoseUnit");
                    Sig.AppendChild(SigDoseUnit);
                }

                XmlNode SigRoute = xmlDoc.CreateElement("Route");
                Sig.AppendChild(SigRoute);

                if (medsRoute == "PO" || medsRoute == "BY MOUTH")
                {
                    SigRoute.InnerText = "by mouth";
                }
                else
                {
                    if (drMed["OSODOU"].ToString().ToLower().Equals("os"))
                        SigRoute.InnerText = "in left eye";
                    else if (drMed["OSODOU"].ToString().ToLower().Equals("od"))
                        SigRoute.InnerText = "in right eye";
                    else if (drMed["OSODOU"].ToString().ToLower().Equals("ou"))
                        SigRoute.InnerText = "in both eye";
                    else
                        SigRoute.InnerText = string.Join(" ", actionValue.Skip(1)); ;
                }

                XmlNode SigDoseTiming = xmlDoc.CreateElement("DoseTiming");
                Sig.AppendChild(SigDoseTiming);
                SigDoseTiming.InnerText = drMed["Frequency"].ToString();

                XmlNode SigDuration = xmlDoc.CreateElement("Duration");
                Sig.AppendChild(SigDuration);
                SigDuration.InnerText = drMed["Duration"].ToString();

                //string QuantityUnit = drMed["QtyUnit"].ToString();
                string[] QtyUnit = drMed["QtyUnit"].ToString().Split(' ');

                if (QtyUnit.Count() > 1)
                {
                    //string[] QtyUnit = QuantityUnit.Split(' ');

                    XmlNode SigQuantity = xmlDoc.CreateElement("Quantity");
                    Sig.AppendChild(SigQuantity);
                    SigQuantity.InnerText = QtyUnit[0];

                    XmlNode SigQuantityUnit = xmlDoc.CreateElement("QuantityUnit");
                    Sig.AppendChild(SigQuantityUnit);
                    SigQuantityUnit.InnerText = QtyUnit[1];
                }
                else
                {
                    XmlNode SigQuantity = xmlDoc.CreateElement("Quantity");
                    Sig.AppendChild(SigQuantity);

                    XmlNode SigQuantityUnit = xmlDoc.CreateElement("QuantityUnit");
                    Sig.AppendChild(SigQuantityUnit);
                }

                XmlNode SigRefills = xmlDoc.CreateElement("Refills");
                Sig.AppendChild(SigRefills);
                if (drMed["PRN"].ToString().ToLower() == "prn")
                    SigRefills.InnerText = drMed["PRN"].ToString();
                else if (drMed["PRN"].ToString().ToLower() == "" && drMed["Refills"].ToString().ToLower() != "")
                    if (drMed["Refills"].ToString().Contains("."))
                        SigRefills.InnerText = Math.Round(Convert.ToDecimal(drMed["Refills"])).ToString();
                    else
                        SigRefills.InnerText = drMed["Refills"].ToString();
                else
                    SigRefills.InnerText = "";

                XmlNode SigSubstitutionPermitted = xmlDoc.CreateElement("SubstitutionPermitted");
                Sig.AppendChild(SigSubstitutionPermitted);

                XmlNode SigOtherNotest = xmlDoc.CreateElement("OtherNotes");
                Sig.AppendChild(SigOtherNotest);
                SigOtherNotest.InnerText = drMed["OtherNote"].ToString();

                XmlNode SigPatientNotes = xmlDoc.CreateElement("PatientNotes");
                Sig.AppendChild(SigPatientNotes);
                SigPatientNotes.InnerText = drMed["PatientNote"].ToString();

                XmlNode SigComments = xmlDoc.CreateElement("Comments");
                Sig.AppendChild(SigComments);

                XmlNode StartDate = xmlDoc.CreateElement("StartDate");
                medication.AppendChild(StartDate);
                StartDate.InnerText = drMed["StartDate"].ToString();

                XmlNode StopDate = xmlDoc.CreateElement("StopDate");
                medication.AppendChild(StopDate);
                StopDate.InnerText = drMed["StopDate"].ToString();

                XmlNode FillDate = xmlDoc.CreateElement("FillDate");
                medication.AppendChild(FillDate);

                XmlNode StopReason = xmlDoc.CreateElement("StopReason");
                medication.AppendChild(StopReason);
            }
            return xmlDoc.InnerXml;
        }

        //private string GenerateXmlForMedications(DataTable dt, out string action)
        //{
        //    action = string.Empty;
        //    XmlDocument xmlDoc = new XmlDocument();
        //    var xmlDocumentElement = xmlDoc.DocumentElement;

        //    var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
        //    xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

        //    var rootNode = xmlDoc.CreateElement("RCExtRequest");
        //    xmlDoc.AppendChild(rootNode);

        //    XmlAttribute version = xmlDoc.CreateAttribute("version");
        //    version.Value = ErxComponent.APIVersion;
        //    rootNode.Attributes.Append(version);

        //    var caller = xmlDoc.CreateElement("Caller");
        //    rootNode.AppendChild(caller);

        //    var systemName = xmlDoc.CreateElement("SystemName");
        //    rootNode.AppendChild(systemName);
        //    systemName.InnerText = ErxComponent.VendorName;

        //    var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
        //    rootNode.AppendChild(rcopiaPracticeUsername);
        //    rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

        //    var vendorName = xmlDoc.CreateElement("VendorName");
        //    caller.AppendChild(vendorName);
        //    vendorName.InnerText = ErxComponent.VendorName;

        //    var vendorPassword = xmlDoc.CreateElement("VendorPassword");
        //    caller.AppendChild(vendorPassword);
        //    vendorPassword.InnerText = ErxComponent.VendorPassword;

        //    var request = xmlDoc.CreateElement("Request");
        //    rootNode.AppendChild(request);

        //    var checkEligibility = xmlDoc.CreateElement("CheckEligibility");
        //    request.AppendChild(checkEligibility);
        //    var command = xmlDoc.CreateElement("Command");
        //    request.AppendChild(command);
        //    command.InnerText = "send_medication";
        //    action = "send_medication";

        //    XmlNode medicationList = xmlDoc.CreateElement("MedicationList");
        //    request.AppendChild(medicationList);

        //    foreach (DataRow drMed in dt.Rows)
        //    {
        //        if (string.IsNullOrEmpty(userid))
        //            userid = drMed["LoginId"].ToString();
        //        encounterId = drMed["AppointmentId"].ToString();

        //        XmlNode medication = xmlDoc.CreateElement("Medication");
        //        medicationList.AppendChild(medication);

        //        XmlNode deleted = xmlDoc.CreateElement("Deleted");
        //        medication.AppendChild(deleted);
        //        deleted.InnerText = drMed["Status"].ToString();

        //        XmlNode medExternalID = xmlDoc.CreateElement("ExternalID");
        //        medExternalID.InnerText = drMed["ClinicalIds"].ToString(); ;
        //        medication.AppendChild(medExternalID);

        //        XmlNode Patient = xmlDoc.CreateElement("Patient");
        //        medication.AppendChild(Patient);

        //        XmlNode patExternalID = xmlDoc.CreateElement("ExternalID");
        //        Patient.AppendChild(patExternalID);
        //        patExternalID.InnerText = drMed["Patientid"].ToString();

        //        XmlNode Provider = xmlDoc.CreateElement("Provider");
        //        medication.AppendChild(Provider);

        //        XmlNode provusername = xmlDoc.CreateElement("Username");
        //        Provider.AppendChild(provusername);
        //        provusername.InnerText = "iprovider35";

        //        XmlNode Sig = xmlDoc.CreateElement("Sig");
        //        medication.AppendChild(Sig);

        //        XmlNode Drug = xmlDoc.CreateElement("Drug");
        //        Sig.AppendChild(Drug);

        //        XmlNode drugNDCID = xmlDoc.CreateElement("NDCID");
        //        Drug.AppendChild(drugNDCID);
        //        drugNDCID.InnerText = drMed["NDC"].ToString();

        //        XmlNode drugBrandName = xmlDoc.CreateElement("BrandName");
        //        Drug.AppendChild(drugBrandName);
        //        drugBrandName.InnerText = drMed["BrandName"].ToString();

        //        XmlNode drugGenericName = xmlDoc.CreateElement("GenericName");
        //        Drug.AppendChild(drugGenericName);
        //        drugGenericName.InnerText = drMed["GenericDrugName"].ToString();


        //        XmlNode drugFormD = xmlDoc.CreateElement("Form");
        //        Drug.AppendChild(drugFormD);
        //        drugFormD.InnerText = drMed["Form"].ToString();


        //        XmlNode drugStrength = xmlDoc.CreateElement("Strength");
        //        Drug.AppendChild(drugStrength);
        //        drugStrength.InnerText = drMed["Strength"].ToString();

        //        XmlNode drugSupply = xmlDoc.CreateElement("Supply");
        //        Drug.AppendChild(drugSupply);
        //        drugSupply.InnerText = drMed["Supply"].ToString();

        //        XmlNode SigAction = xmlDoc.CreateElement("Action");
        //        Sig.AppendChild(SigAction);

        //        if (drMed["PO"].ToString() == "PO")
        //        {
        //            SigAction.InnerText = "Take";
        //        }
        //        else if (drMed["PO"].ToString() == "INSTILL IN EYES")
        //        {
        //            SigAction.InnerText = "INSTILL";
        //        }
        //        else if (drMed["PO"].ToString() == "APPLY TO LIDS")
        //        {
        //            SigAction.InnerText = "Apply";
        //        }

        //        string DosageUnit = drMed["Dosage"].ToString();

        //        if (DosageUnit.Trim().Length > 0)
        //        {
        //            string[] DosUnit = DosageUnit.Split(' ');

        //            XmlNode SigDose = xmlDoc.CreateElement("Dose");
        //            Sig.AppendChild(SigDose);
        //            SigDose.InnerText = DosUnit[0];

        //            XmlNode SigDoseUnit = xmlDoc.CreateElement("DoseUnit");
        //            Sig.AppendChild(SigDoseUnit);
        //            SigDoseUnit.InnerText = DosUnit[1];
        //        }
        //        else
        //        {
        //            XmlNode SigDose = xmlDoc.CreateElement("Dose");
        //            Sig.AppendChild(SigDose);                   

        //            XmlNode SigDoseUnit = xmlDoc.CreateElement("DoseUnit");
        //            Sig.AppendChild(SigDoseUnit);
        //        }

        //        XmlNode SigRoute = xmlDoc.CreateElement("Route");
        //        Sig.AppendChild(SigRoute);

        //        if (drMed["PO"].ToString().Equals("PO"))
        //        {
        //            SigRoute.InnerText = "by Mouth";
        //        }
        //        else
        //        {
        //            if (drMed["OSODOU"].ToString().Equals("OS"))
        //                SigRoute.InnerText = "in left eye";
        //            else if (drMed["OSODOU"].ToString().Equals("OD"))
        //                SigRoute.InnerText = "in right eye";
        //            else if (drMed["OSODOU"].ToString().Equals("OU"))
        //                SigRoute.InnerText = "in both eye";
        //            else
        //                SigRoute.InnerText = "";
        //        }

        //        XmlNode SigDoseTiming = xmlDoc.CreateElement("DoseTiming");
        //        Sig.AppendChild(SigDoseTiming);
        //        SigDoseTiming.InnerText = drMed["Frequency"].ToString();

        //        XmlNode SigDuration = xmlDoc.CreateElement("Duration");
        //        Sig.AppendChild(SigDuration);
        //        SigDuration.InnerText = drMed["Duration"].ToString();

        //        string QuantityUnit = drMed["QtyUnit"].ToString();

        //        if (QuantityUnit.Trim().Length > 0)
        //        {
        //            string[] QtyUnit = QuantityUnit.Split(' ');

        //            XmlNode SigQuantity = xmlDoc.CreateElement("Quantity");
        //            Sig.AppendChild(SigQuantity);
        //            SigQuantity.InnerText = QtyUnit[0];

        //            XmlNode SigQuantityUnit = xmlDoc.CreateElement("QuantityUnit");
        //            Sig.AppendChild(SigQuantityUnit);
        //            SigQuantityUnit.InnerText = QtyUnit[1];
        //        }
        //        else
        //        {
        //            XmlNode SigQuantity = xmlDoc.CreateElement("Quantity");
        //            Sig.AppendChild(SigQuantity);

        //            XmlNode SigQuantityUnit = xmlDoc.CreateElement("QuantityUnit");
        //            Sig.AppendChild(SigQuantityUnit);
        //        }

        //        XmlNode SigRefills = xmlDoc.CreateElement("Refills");
        //        Sig.AppendChild(SigRefills);
        //        if (drMed["PRN"].ToString().ToLower() == "prn")
        //            SigRefills.InnerText = drMed["PRN"].ToString();
        //        else if (drMed["PRN"].ToString().ToLower() == "" && drMed["Refills"].ToString().ToLower() != "")
        //            if (drMed["Refills"].ToString().Contains("."))
        //                SigRefills.InnerText = Math.Round(Convert.ToDecimal(drMed["Refills"])).ToString();
        //            else
        //                SigRefills.InnerText = drMed["Refills"].ToString();
        //        else
        //            SigRefills.InnerText = "";

        //        XmlNode SigSubstitutionPermitted = xmlDoc.CreateElement("SubstitutionPermitted");
        //        Sig.AppendChild(SigSubstitutionPermitted);

        //        XmlNode SigOtherNotest = xmlDoc.CreateElement("OtherNotes");
        //        Sig.AppendChild(SigOtherNotest);
        //        SigOtherNotest.InnerText = drMed["OtherNote"].ToString();

        //        XmlNode SigPatientNotes = xmlDoc.CreateElement("PatientNotes");
        //        Sig.AppendChild(SigPatientNotes);
        //        SigPatientNotes.InnerText = drMed["PatientNote"].ToString();

        //        XmlNode SigComments = xmlDoc.CreateElement("Comments");
        //        Sig.AppendChild(SigComments);

        //        XmlNode StartDate = xmlDoc.CreateElement("StartDate");
        //        medication.AppendChild(StartDate);
        //        StartDate.InnerText = drMed["StartDate"].ToString();

        //        XmlNode StopDate = xmlDoc.CreateElement("StopDate");
        //        medication.AppendChild(StopDate);
        //        StopDate.InnerText = drMed["StopDate"].ToString();

        //        XmlNode FillDate = xmlDoc.CreateElement("FillDate");
        //        medication.AppendChild(FillDate);

        //        XmlNode StopReason = xmlDoc.CreateElement("StopReason");
        //        medication.AppendChild(StopReason);
        //    }
        //    return xmlDoc.InnerXml;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="PracticeToken"></param>
        /// <param name="sqlConString"></param>
        public string GetUpdatedMedications(string patientId, string parEncounterId, string parUserId, string PracticeToken, IDbConnection sqlConString, bool IsPracticeLevel, string lastUpdateTime, string SSOLaunchTime)
        {
            string action;
            DrFirstApi api = new DrFirstApi();
            LogApiCall log = new LogApiCall();
            bool error = false;
            string response = string.Empty;
            bool apiStatus = false;
            int apiType = 0;
            string lastUpDt = string.Empty;
            string medicationDwnldReq = string.Empty;
            try
            {
                if (IsPracticeLevel)
                    apiType = (int)ApiActionType.UpdateMedicationPracticeLevelCompleted;
                else
                    apiType = (int)ApiActionType.UpdateMedications;

                lastUpDt = log.GetLastUpdateDate(sqlConString, patientId, SSOLaunchTime, apiType);
                medicationDwnldReq = "xml=" + GenerateDownloadRequestXml(patientId, out action, IsPracticeLevel, lastUpdateTime, lastUpDt);
                string command = action;

                //if (IsPracticeLevel)
                //    apiType = (int)ApiActionType.UpdateMedicationPracticeLevelCompleted;
                //else
                //    apiType = (int)ApiActionType.UpdateMedications;

                ErxComponent.InitializeErxComponent(sqlConString);
                int maxTry = 3;

                api.InvokeSendPatientApi(command, medicationDwnldReq, out error, out response, false, patientId, parUserId, DateTime.Now, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);


                string xmlResponse = response;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlResponse);
                string xpathVendorName = "RCExtResponse/Response";
                var childNdCaller1 = doc.SelectNodes(xpathVendorName);

                foreach (XmlNode childrenNode in childNdCaller1)
                {
                    xmlResponse = childrenNode.SelectSingleNode("LastUpdateDate").FirstChild.Value;
                    break;
                }
                log.LogErxActivity(medicationDwnldReq, patientId, parUserId, DateTime.Parse(xmlResponse), response, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error, string.Empty);
            }
            catch (Exception ex)
            {
                error = true;
                log.LogErxActivity(medicationDwnldReq, patientId, parUserId, DateTime.Parse(lastUpDt), ex.InnerException.ToString(), parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error, string.Empty);
                System.Windows.Forms.MessageBox.Show("Error occured. Please contact administrator");
            }

            return response;
        }

        private string GenerateDownloadRequestXml(string patientId, out string action, bool IsPracticeLevel, string lastupdate, string date)
        {
            action = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            var xmlDocumentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            //systemName.InnerText = ErxComponent.SystemName;
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "update_medication";
            action = "update_medication";

            var lastUpdateDt = xmlDoc.CreateElement("LastUpdateDate");
            request.AppendChild(lastUpdateDt);
            lastUpdateDt.InnerText = date;

            if (!IsPracticeLevel)
            {
                XmlNode Patient = xmlDoc.CreateElement("Patient");
                request.AppendChild(Patient);
                XmlNode patRcopiaID = xmlDoc.CreateElement("RcopiaID");
                Patient.AppendChild(patRcopiaID);
                patRcopiaID.InnerText = ErxComponent.RCopiaPatientId;

                XmlNode patExternalID = xmlDoc.CreateElement("ExternalID");
                Patient.AppendChild(patExternalID);
                patExternalID.InnerText = patientId;
            }
            else
            {
                XmlNode MaximumUpdateDate = xmlDoc.CreateElement("MaximumUpdateDate");
                request.AppendChild(MaximumUpdateDate);
                MaximumUpdateDate.InnerText = DateTime.Now.ToString();
            }
            return xmlDoc.InnerXml;
        }

        public string GetUpdatedPrescriptions(string patientId, string parEncounterId, string parUserId, string PracticeToken, IDbConnection sqlConString, bool IsPracticeLevel, string SSOLaunchTime)
        {
            string action;
            LogApiCall log = new LogApiCall();
            DrFirstApi api = new DrFirstApi();
            bool error;
            string response = string.Empty;
            bool apiStatus = false;
            int apiType = 0;
            string medicationDwnldReq = string.Empty;
            string lastUpDt = string.Empty;
            try
            {

                lastUpDt = log.GetLastUpdateDate(sqlConString, patientId, SSOLaunchTime, (int)ApiActionType.UpdateMedications);
                medicationDwnldReq = "xml=" + GenerateDownloadRequestPrescriptionXml(patientId, out action, IsPracticeLevel, lastUpDt);
                string command = action;

                if (IsPracticeLevel)
                    apiType = (int)ApiActionType.UpdatePrescriptionPracticeLevelCompleted;
                //else
                //    apiType = (int)ApiActionType.UpdatePrescriptionPatientLevelPending;


                ErxComponent.InitializeErxComponent(sqlConString);
                int maxTry = 3;
                api.InvokeSendPatientApi(command, medicationDwnldReq, out error, out response, false, patientId, parUserId, DateTime.Now, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);


                string xmlResponse = response;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlResponse);
                string xpathVendorName = "RCExtResponse/Response";
                var childNdCaller1 = doc.SelectNodes(xpathVendorName);

                foreach (XmlNode childrenNode in childNdCaller1)
                {
                    xmlResponse = childrenNode.SelectSingleNode("LastUpdateDate").FirstChild.Value;
                    break;
                }
                log.LogErxActivity(medicationDwnldReq, patientId, parUserId, DateTime.Parse(xmlResponse), response, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error, string.Empty);
            }
            catch (Exception ex)
            {
                error = true;
                log.LogErxActivity(medicationDwnldReq, patientId, parUserId, DateTime.Parse(lastUpDt), ex.InnerException.ToString(), parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error, string.Empty);
                System.Windows.Forms.MessageBox.Show("Error occured. Please contact administrator");
            }

            return response;
        }

        private string GenerateDownloadRequestPrescriptionXml(string patientId, out string action, bool IsPracticeLevel, string lastUpdateTime)
        {
            action = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            var xmlDocumentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            //systemName.InnerText = ErxComponent.SystemName;
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "update_prescription";
            action = "update_prescription";

            var lastUpdateDate = xmlDoc.CreateElement("LastUpdateDate");
            request.AppendChild(lastUpdateDate);
            lastUpdateDate.InnerText = lastUpdateTime;

            if (!IsPracticeLevel)
            {
                XmlNode Patient = xmlDoc.CreateElement("Patient");
                request.AppendChild(Patient);
                XmlNode patRcopiaID = xmlDoc.CreateElement("RcopiaID");
                Patient.AppendChild(patRcopiaID);
                patRcopiaID.InnerText = ErxComponent.RCopiaPatientId;

                XmlNode patExternalID = xmlDoc.CreateElement("ExternalID");
                Patient.AppendChild(patExternalID);
                patExternalID.InnerText = patientId;
            }
            else
            {
                XmlNode MaximumUpdateDate = xmlDoc.CreateElement("MaximumUpdateDate");
                request.AppendChild(MaximumUpdateDate);
                MaximumUpdateDate.InnerText = DateTime.Now.ToString();
                XmlNode Status = xmlDoc.CreateElement("Status");
                request.AppendChild(Status);
                Status.InnerText = "completed";
            }
            return xmlDoc.InnerXml;
        }
    }
}
