﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace ClinicalIntegration
{
    
    public class DrfirstPrescription
    {          
        public void AddUpdatePrescriptionResponse(string patientId, string appointmentId, string xmlResponse, IDbConnection SqlConString)
        {
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;

            query = "[DrFirst].[AddUpdatePrescriptionResponse]";
            if (SqlConString.State.ToString() == "Closed")              
            SqlConString.Open();
            using (IDbCommand cmd = SqlConString.CreateCommand())
            {
                try
                {
                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);

                    IDbDataParameter parAppointmentId = cmd.CreateParameter();
                    parAppointmentId.Direction = ParameterDirection.Input;
                    parAppointmentId.Value = int.Parse(appointmentId);
                    parAppointmentId.ParameterName = "@appointmentId";
                    parAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentId);

                    IDbDataParameter parXmlData = cmd.CreateParameter();
                    parXmlData.Direction = ParameterDirection.Input;
                    parXmlData.Value = xmlResponse;
                    parXmlData.ParameterName = "@xmlStringData";
                    parXmlData.DbType = DbType.String;
                    cmd.Parameters.Add(parXmlData);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
                catch
                {

                }
            }
        }
        
        /// <summary>
        /// GetPrescriptionResponse
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="PracticeToken"></param>
        /// <param name="sqlConString"></param>
        public string GetPrescriptionResponse(string patientId, string parEncounterId, string parUserId, string PracticeToken, IDbConnection sqlConString, string status, string SSOLaunchTime)
        {
            string action;
            LogApiCall log = new LogApiCall();
            string lastUpDt = string.Empty;
            DrFirstApi api = new DrFirstApi();
            bool error;
            int apiType = -2;
            string response = string.Empty;
            bool apiStatus = false;
            string prescriptionReqXml = string.Empty;
            try
            {

                lastUpDt = log.GetLastUpdateDate(sqlConString, patientId, SSOLaunchTime, (int)ApiActionType.UpdatePrescriptionPatientLevelComplete);
                prescriptionReqXml  = "xml=" + GenerateDownloadPresRequestXml(patientId, out action, status, lastUpDt);
                string command = action;

                if (status == "pending")
                    apiType = (int)ApiActionType.UpdatePrescriptionPatientLevelPending;
                if (status == "completed")
                    apiType = (int)ApiActionType.UpdatePrescriptionPatientLevelComplete;
                ErxComponent.InitializeErxComponent(sqlConString);
                int maxTry = 3;
                api.InvokeSendPatientApi(command, prescriptionReqXml, out error, out response, false, patientId, parUserId, DateTime.Now, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);

                string xmlResponse = response;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlResponse);
                string xpathVendorName = "RCExtResponse/Response";
                var childNdCaller1 = doc.SelectNodes(xpathVendorName);

                foreach (XmlNode childrenNode in childNdCaller1)
                {
                    xmlResponse = childrenNode.SelectSingleNode("LastUpdateDate").FirstChild.Value;
                    break;
                }
                
                log.LogErxActivity(prescriptionReqXml, patientId, parUserId, DateTime.Parse(xmlResponse), response, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error, string.Empty);
            }
            catch (Exception ex)
            {
                error = true;
                log.LogErxActivity(prescriptionReqXml, patientId, parUserId, DateTime.Parse(lastUpDt), ex.InnerException.ToString(), parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error, string.Empty);

            }

            return response;
        }

        private string GenerateDownloadPresRequestXml(string patientId, out string action, string status,string lastUpDt)
        {
            action = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            var xmlDocumentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);           
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "update_prescription";
            action = "update_prescription";

            var lastUpdateDate = xmlDoc.CreateElement("LastUpdateDate");
            request.AppendChild(lastUpdateDate);
            lastUpdateDate.InnerText = lastUpDt;
            
            XmlNode Patient = xmlDoc.CreateElement("Patient");
            request.AppendChild(Patient);

            XmlNode patRcopiaID = xmlDoc.CreateElement("RcopiaID");
            Patient.AppendChild(patRcopiaID);
            patRcopiaID.InnerText = ErxComponent.RCopiaPatientId;

            XmlNode patExternalID = xmlDoc.CreateElement("ExternalID");
            Patient.AppendChild(patExternalID);
            patExternalID.InnerText = patientId;

            XmlNode statusNode = xmlDoc.CreateElement("Status");
            request.AppendChild(statusNode);
            statusNode.InnerText = status;

            return xmlDoc.InnerXml;
        }


        public string GetUpdatedPrescriptionsPracticelevel(string patientId, string parEncounterId, string parUserId, string PracticeToken, IDbConnection sqlConString, bool IsPracticeLevel, string lastUpdateTime)
        {
            string action;
            DrFirstApi api = new DrFirstApi();
            bool error = false;
            string response = string.Empty;
            bool apiStatus = false;
            int apiType = 0;
            string medicationDwnldReq = string.Empty;

            medicationDwnldReq = "xml=" + GenerateDownloadRequestPrescriptionXmlPracticeLevel(patientId, out action, IsPracticeLevel, lastUpdateTime);
            string command = action;
            
           
            apiType = (int)ApiActionType.UpdatePrescriptionPracticeLevelCompleted;
           
            ErxComponent.InitializeErxComponent(sqlConString);
            int maxTry = 3;

            api.InvokeSendPatientApi(command, medicationDwnldReq, out error, out response, false, patientId, parUserId, DateTime.Now, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, false, maxTry);

            LogApiCall log = new LogApiCall();
            log.LogErxActivity(medicationDwnldReq, patientId, parUserId, DateTime.Now, response, parEncounterId, PracticeToken, apiType, sqlConString, apiStatus, error, string.Empty);

            return response;
        }

        public void AddUpdatePrescriptionResponsePracticeLevel(string patientId, string appointmentId, string xmlResponse, IDbConnection SqlConString)
        {
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;
            LogApiCall log = new LogApiCall();
            query = "[DrFirst].[AddUpdatePrescriptionResponse]";
            if (SqlConString.State.ToString() == "Closed")
                SqlConString.Open();
            using (IDbCommand cmd = SqlConString.CreateCommand())
            {
                try
                {                  

                    IDbDataParameter parXmlData = cmd.CreateParameter();
                    parXmlData.Direction = ParameterDirection.Input;
                    parXmlData.Value = xmlResponse;
                    parXmlData.ParameterName = "@xmlStringData";
                    parXmlData.DbType = DbType.String;
                    cmd.Parameters.Add(parXmlData);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    log.LogErxActivity(xmlResponse, patientId, string.Empty, DateTime.Now, ex.InnerException.ToString(), string.Empty, string.Empty, (int)ApiActionType.UpdatePrescriptionPracticeLevelCompleted, SqlConString, false, true, string.Empty);
                    System.Windows.Forms.MessageBox.Show("Error occured. Please contact administrator");
                }
            }
        }

        private string GenerateDownloadRequestPrescriptionXmlPracticeLevel(string patientId, out string action, bool IsPracticeLevel, string lastUpdateTime)
        {
            action = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            var xmlDocumentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            //systemName.InnerText = ErxComponent.SystemName;
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "update_prescription";
            action = "update_prescription";

            var lastUpdateDate = xmlDoc.CreateElement("LastUpdateDate");
            request.AppendChild(lastUpdateDate);
            lastUpdateDate.InnerText = lastUpdateTime;

            //if (!IsPracticeLevel)
            //{
            //    XmlNode Patient = xmlDoc.CreateElement("Patient");
            //    request.AppendChild(Patient);
            //    XmlNode patRcopiaID = xmlDoc.CreateElement("RcopiaID");
            //    Patient.AppendChild(patRcopiaID);
            //    patRcopiaID.InnerText = ErxComponent.RCopiaPatientId;

            //    XmlNode patExternalID = xmlDoc.CreateElement("ExternalID");
            //    Patient.AppendChild(patExternalID);
            //    patExternalID.InnerText = patientId;
            //}
            //else
            //{
            XmlNode MaximumUpdateDate = xmlDoc.CreateElement("MaximumUpdateDate");
            request.AppendChild(MaximumUpdateDate);
            MaximumUpdateDate.InnerText = DateTime.Now.ToString();
            XmlNode Status = xmlDoc.CreateElement("Status");
            request.AppendChild(Status);
            Status.InnerText = "completed";
            //}
            return xmlDoc.InnerXml;
        }
    }
}
