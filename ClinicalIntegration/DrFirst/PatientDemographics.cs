﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Xml;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public class PatientDemographics
    {
        string PatientRcopiaID = string.Empty;
        public void SendPatientDemographics(string patientId,string userid,string PracticeToken,string appointmentId, IDbConnection sqlConString)
        {
            bool apiStatus = false;
            LogApiCall log = new LogApiCall();
            string demoGraphicsXml = string.Empty;
            int apiType = 0;
            string xmlResponse = string.Empty;
            bool error = false;
            try
            {              
               
                ErxComponent.InitializeErxComponent(sqlConString);
                string action;
                demoGraphicsXml = "xml=" + GetDemoGraphicsData(patientId,out action, sqlConString, userid);
                DrFirstApi api = new DrFirstApi();
                
                apiType=(int)ApiActionType.SendPatient;
                string response = string.Empty;
                string command = action;
                int maxTry = 3;
                
                api.InvokeSendPatientApi(command,demoGraphicsXml, out error, out response, true, patientId, userid, DateTime.Now, appointmentId, PracticeToken, apiType, sqlConString, apiStatus,false, maxTry);
                
                DateTime dateTime = DateTime.Now;
                xmlResponse= response;

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlResponse);
                string xpathVendorName = "RCExtResponse/Response/PatientList/Patient";
                var childNdCaller1 = doc.SelectNodes(xpathVendorName);
                
                foreach (XmlNode childrenNode in childNdCaller1)
                {
                    PatientRcopiaID = childrenNode.SelectSingleNode("RcopiaID").FirstChild.Value;
                    break;
                }
                //ErxComponent.RCopiaPatientId = RcopiaID;
                
                log.LogErxActivity(demoGraphicsXml, patientId, userid, DateTime.Now, xmlResponse, appointmentId, PracticeToken, apiType, sqlConString, apiStatus, error, PatientRcopiaID);
                
            }
            catch (Exception ex)
            {
                error = true;
                log.LogErxActivity(demoGraphicsXml, patientId, userid, DateTime.Now, ex.InnerException.ToString(), appointmentId, PracticeToken, apiType, sqlConString, apiStatus, error, PatientRcopiaID);
            }
        }

        private string GenerateUrlXml(string patientId)
        {
            XmlDocument xmlDoc = new XmlDocument();
            var xmlDocumentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, xmlDocumentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion; 
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            systemName.InnerText = ErxComponent.VendorName;

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername; 

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName; 

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            command.InnerText = "get_url";

            return xmlDoc.InnerXml;
        }
        
        
        /// <summary>
        /// Retrieve Patient Demographics information
        /// </summary>
        /// <param name="patientId">patient Id</param>
        /// <returns>Patients Demographics xml</returns>
        private string GetDemoGraphicsData(string patientId, out string action,IDbConnection con,string userId)
        {
            action = string.Empty;
            DataTable dt = new DataTable();
            string patientDemographicsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;
            try
            {
                query = "DrFirst.GetPatient";
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (IDbCommand cmd = con.CreateCommand())
                {
                    // Add the parameters for the SelectCommand.
                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }

                }

                if (dt.Rows.Count > 0)
                {
                    patientDemographicsXml = GenerateXmlForPatientDemographics(dt, out action, con, userId, patientId);
                }
                
            }
            catch
            {
               
            }
            return patientDemographicsXml;
        }

        private DataTable GetEncountersData(string enconterId,string patientId,IDbConnection con)
        {
            DataTable dt = new DataTable();
            string patientEncounterXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;
               
                query = "DrFirst.GetPatientEncounter";

            if (con.State.ToString() == "Closed")
                con.Open();
            using (IDbCommand cmd = con.CreateCommand())
                {
                // Add the parameters for the SelectCommand.
                IDbDataParameter parPatient = cmd.CreateParameter();
                parPatient.Direction = ParameterDirection.Input;
                parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                parPatient.ParameterName = "@PatineiId";
                parPatient.DbType = DbType.Int64;
                cmd.Parameters.Add(parPatient);

                IDbDataParameter parAppointmentid = cmd.CreateParameter();
                parAppointmentid.Direction = ParameterDirection.Input;
                parAppointmentid.Value = int.Parse(enconterId);
                parAppointmentid.ParameterName = "@Appointmentid";
                parAppointmentid.DbType = DbType.Int64;
                cmd.Parameters.Add(parAppointmentid);
                      
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            
            return dt;
        }

        private string GetPatientByPatientRCopiaID(string patRcopiaId,string patientId, IDbConnection con)
        {

            DataTable dt = new DataTable();
            string patRcopiaID = string.Empty;
            string query = "select top 1 *  from drfirst.xmlqueueresponse with (nolock) where patientid=@patientId and actiontypeid=" + (int)ApiActionType.SendPatient +" order by 1 desc";
            using (IDbCommand cmd = con.CreateCommand())
            {
                IDbDataParameter parPatient = cmd.CreateParameter();
                parPatient.Direction = ParameterDirection.Input;
                parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                parPatient.ParameterName = "@PatientId";
                parPatient.DbType = DbType.Int64;
                cmd.Parameters.Add(parPatient);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                    if (dt.Rows.Count > 0)
                    {
                       patRcopiaID = dt.Rows[0]["PatientRcopiaId"].ToString()+"~"+ dt.Rows[0]["RecordDateTime"].ToString();
                    }
                }
            }

            return patRcopiaID;
        }
        private string GenerateXmlForPatientDemographics(DataTable dt,out string action,IDbConnection sqlConString,string userId,string patientId)
        {
            action = string.Empty;
            string RCopiaID = GetPatientByPatientRCopiaID(PatientRcopiaID, patientId, sqlConString);
            string[] values = !string.IsNullOrEmpty(RCopiaID)? RCopiaID.Split('~'):null;
            var xmlDoc = new XmlDocument();
            var documentElement = xmlDoc.DocumentElement;

            var xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.InsertBefore(xmlDeclaration, documentElement);

            var rootNode = xmlDoc.CreateElement("RCExtRequest");
            xmlDoc.AppendChild(rootNode);

            XmlAttribute version = xmlDoc.CreateAttribute("version");
            version.Value = ErxComponent.APIVersion;
            rootNode.Attributes.Append(version);

            var caller = xmlDoc.CreateElement("Caller");
            rootNode.AppendChild(caller);

            var systemName = xmlDoc.CreateElement("SystemName");
            rootNode.AppendChild(systemName);
            systemName.InnerText = ErxComponent.VendorName; 

            var rcopiaPracticeUsername = xmlDoc.CreateElement("RcopiaPracticeUsername");
            rootNode.AppendChild(rcopiaPracticeUsername);
            rcopiaPracticeUsername.InnerText = ErxComponent.RcopiaPracticeUsername;

            var vendorName = xmlDoc.CreateElement("VendorName");
            caller.AppendChild(vendorName);
            vendorName.InnerText = ErxComponent.VendorName;

            var vendorPassword = xmlDoc.CreateElement("VendorPassword");
            caller.AppendChild(vendorPassword);
            vendorPassword.InnerText = ErxComponent.VendorPassword;

            var request = xmlDoc.CreateElement("Request");
            rootNode.AppendChild(request);

            //var checkEligibility = xmlDoc.CreateElement("CheckEligibility");
            //request.AppendChild(checkEligibility);

            var command = xmlDoc.CreateElement("Command");
            request.AppendChild(command);
            //if (string.IsNullOrEmpty(RCopiaID))
            //{
                command.InnerText = "send_patient";
                action = "send_patient";
            //}
            //else if (!string.IsNullOrEmpty(RCopiaID))
            //{
            //    command.InnerText = "update_patient";
            //    action = "update_patient";
            //    //var lastUpdate = xmlDoc.CreateElement("LastUpdateDate");
            //    //request.AppendChild(lastUpdate);
            //    //if(values!=null)
            //    //lastUpdate.InnerText = values[1].ToString();
            //}
            var patientList = xmlDoc.CreateElement("PatientList");
            request.AppendChild(patientList);

            foreach (DataRow item in dt.Rows)
            {
                var patient = xmlDoc.CreateElement("Patient");
                patientList.AppendChild(patient);

                //var encounterList = xmlDoc.CreateElement("EncounterList");

                //patient.AppendChild(encounterList);

                //DataTable dtEnctrs = GetEncountersData(item["AppointmentId"].ToString(), item["PatientId"].ToString(), sqlConString);
                //if (dtEnctrs != null && dtEnctrs.Rows.Count > 0)
                //{
                //    foreach (DataRow enctr in dtEnctrs.Rows)
                //    {
                //        var encounter = xmlDoc.CreateElement("Encounter");
                //        encounterList.AppendChild(encounter);

                //        var externalID = xmlDoc.CreateElement("ExternalID");
                //        encounter.AppendChild(externalID);
                //        externalID.InnerText = enctr["AppointmentId"].ToString();

                //        var user = xmlDoc.CreateElement("User");
                //        encounter.AppendChild(user);

                //        var rCopiaID = xmlDoc.CreateElement("RcopiaID");
                //        user.AppendChild(rCopiaID);

                //        var userName = xmlDoc.CreateElement("Username");
                //        user.AppendChild(userName);
                //        userName.InnerText = "icstaff2";

                //        var userExternalID = xmlDoc.CreateElement("ExternalID");
                //        user.AppendChild(userExternalID);
                //        userExternalID.InnerText = enctr["PatientId"].ToString();

                //        var encounterDate = xmlDoc.CreateElement("EncounterDate");
                //        encounter.AppendChild(encounterDate);
                //        encounterDate.InnerText = enctr["AppDate"].ToString();

                //        var actionTaken = xmlDoc.CreateElement("ActionTaken");
                //        encounter.AppendChild(actionTaken);
                //        actionTaken.InnerText = ApiAction.created.ToString();
                //    }
                //}

                //var deleted = xmlDoc.CreateElement("Deleted");
                //patient.AppendChild(deleted);
                //deleted.InnerText = "n";  

                //var medHxStatus = xmlDoc.CreateElement("MedHxStatus");
                //patient.AppendChild(medHxStatus);

                //var patRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //patient.AppendChild(patRcopiaID);

                var patExternalID = xmlDoc.CreateElement("ExternalID");
                patient.AppendChild(patExternalID);
                patExternalID.InnerText = item["PatientId"].ToString();// userId;

                if (!string.IsNullOrEmpty(RCopiaID))
                {
                    var patRCopiaID = xmlDoc.CreateElement("RcopiaID");
                    patient.AppendChild(patRCopiaID);
                   
                    if (values != null)
                        patRCopiaID.InnerText = values[0].ToString();
                   
                }
                
                var firstName = xmlDoc.CreateElement("FirstName");
                patient.AppendChild(firstName);
                firstName.InnerText = item["FirstName"].ToString();

                //var middleName = xmlDoc.CreateElement("MiddleName");
                //patient.AppendChild(middleName);
                //middleName.InnerText = item["MiddleName"].ToString();

                var lastName = xmlDoc.CreateElement("LastName");
                patient.AppendChild(lastName);
                lastName.InnerText = item["LastName"].ToString();

                //var suffix = xmlDoc.CreateElement("Suffix");
                //patient.AppendChild(suffix);
                //suffix.InnerText = item["Suffix"].ToString();

                //var prefix = xmlDoc.CreateElement("Prefix");
                //patient.AppendChild(prefix);
                //prefix.InnerText = item["Prefix"].ToString();

                var dob = xmlDoc.CreateElement("DOB");
                patient.AppendChild(dob);
                dob.InnerText = item["DateOfBirth"].ToString();

                var sex = xmlDoc.CreateElement("Sex");
                patient.AppendChild(sex);
                sex.InnerText = item["GenderId"].ToString();

                //var race = xmlDoc.CreateElement("Race");
                //patient.AppendChild(race);
                //race.InnerText = item["Race"].ToString();

                //var codeList = xmlDoc.CreateElement("CodeList");
                //race.AppendChild(codeList);

                //var code = xmlDoc.CreateElement("Code");
                //codeList.AppendChild(code);

                //var ethinicity = xmlDoc.CreateElement("Ethnicity");
                //patient.AppendChild(ethinicity);
                //ethinicity.InnerText = item["Ethnicity"].ToString();

                //var preferredLanguage = xmlDoc.CreateElement("PreferredLanguage");
                //patient.AppendChild(preferredLanguage);
                ////preferredLanguage.InnerText = item["preferredLanguage"].ToString();

                //var smokingStatus = xmlDoc.CreateElement("SmokingStatus");
                //patient.AppendChild(smokingStatus);

                //var currentStatus = xmlDoc.CreateElement("CurrentStatus");
                //smokingStatus.AppendChild(currentStatus);

                //var usrSmoking = xmlDoc.CreateElement("User");
                //smokingStatus.AppendChild(usrSmoking);

                //var rcopiaID = xmlDoc.CreateElement("RcopiaID");
                //usrSmoking.AppendChild(rcopiaID);

                //var username = xmlDoc.CreateElement("Username");
                //usrSmoking.AppendChild(username);

                //var smokeUsrExternalId = xmlDoc.CreateElement("ExternalID");
                //usrSmoking.AppendChild(smokeUsrExternalId);

                //var modifiedDate = xmlDoc.CreateElement("ModifiedDate");
                //smokingStatus.AppendChild(modifiedDate);

                //var statusHistoryList = xmlDoc.CreateElement("StatusHistoryList");
                //smokingStatus.AppendChild(statusHistoryList);

                //var statusHistory = xmlDoc.CreateElement("StatusHistory");
                //statusHistoryList.AppendChild(statusHistory);
                //var Status = xmlDoc.CreateElement("Status");
                //statusHistory.AppendChild(Status);

                //var statusUser = xmlDoc.CreateElement("User");
                //statusHistory.AppendChild(statusUser);

                //var usrRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //statusUser.AppendChild(usrRcopiaID);
                //var usrUsername = xmlDoc.CreateElement("Username");
                //statusUser.AppendChild(usrUsername);
                //var usrExternalID = xmlDoc.CreateElement("ExternalID");
                //statusUser.AppendChild(usrExternalID);

                //var hisModifiedDate = xmlDoc.CreateElement("ModifiedDate");
                //statusHistory.AppendChild(hisModifiedDate);


                //var emailAddress = xmlDoc.CreateElement("EmailAddress");
                //patient.AppendChild(emailAddress);
                //emailAddress.InnerText = item["Email"].ToString(); 

                //var weight = xmlDoc.CreateElement("Weight");
                //patient.AppendChild(weight);
                //var weightUnit = xmlDoc.CreateElement("WeightUnit");
                //patient.AppendChild(weightUnit);
                //var height = xmlDoc.CreateElement("Height");
                //patient.AppendChild(height);
                //var heightUnit = xmlDoc.CreateElement("HeightUnit");
                //patient.AppendChild(hisModifiedDate);
                //var bsa = xmlDoc.CreateElement("BSA");
                //patient.AppendChild(bsa);
                //var bsaUnit = xmlDoc.CreateElement("BSAUnit");
                //patient.AppendChild(bsaUnit);

                var homePhone = xmlDoc.CreateElement("HomePhone");
                patient.AppendChild(homePhone);
                homePhone.InnerText = item["homePhone"].ToString();

                //var mobilePhone = xmlDoc.CreateElement("MobilePhone");
                //patient.AppendChild(mobilePhone);

                //var workPhone = xmlDoc.CreateElement("WorkPhone");
                //patient.AppendChild(workPhone);

                //var otherPhone = xmlDoc.CreateElement("OtherPhone");
                //patient.AppendChild(otherPhone);

                var address1 = xmlDoc.CreateElement("Address1");
                patient.AppendChild(address1);
                address1.InnerText = item["Address"].ToString();

                var address2 = xmlDoc.CreateElement("Address2");
                patient.AppendChild(address2);

                var city = xmlDoc.CreateElement("City");
                patient.AppendChild(city);
                city.InnerText = item["city"].ToString();

                var state = xmlDoc.CreateElement("State");
                patient.AppendChild(state);
                state.InnerText = item["State"].ToString();

                var zip = xmlDoc.CreateElement("Zip");
                patient.AppendChild(zip);
                zip.InnerText = item["zip"].ToString();

                //var pregnant = xmlDoc.CreateElement("Pregnant");
                //patient.AppendChild(pregnant);

                //var breastfeeding = xmlDoc.CreateElement("Breastfeeding");
                //patient.AppendChild(breastfeeding);

                //var defaultPharmacy = xmlDoc.CreateElement("DefaultPharmacy");
                //patient.AppendChild(defaultPharmacy);
                //var pharmacyRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //defaultPharmacy.AppendChild(pharmacyRcopiaID);

                //var pharmacyCardNumber = xmlDoc.CreateElement("PharmacyCardNumber");
                //patient.AppendChild(pharmacyRcopiaID);

                //var lastVisitDate = xmlDoc.CreateElement("LastVisitDate");
                //patient.AppendChild(lastVisitDate);

                //var location = xmlDoc.CreateElement("Location");
                //patient.AppendChild(location);

                //var primaryCareProvider = xmlDoc.CreateElement("PrimaryCareProvider");
                //patient.AppendChild(primaryCareProvider);

                //var cPvdRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //primaryCareProvider.AppendChild(cPvdRcopiaID);

                //var nPI = xmlDoc.CreateElement("NPI");
                //primaryCareProvider.AppendChild(nPI);

                //var primaryCareUsername = xmlDoc.CreateElement("Username");
                //primaryCareProvider.AppendChild(primaryCareUsername);

                //var primaryCarefirstName = xmlDoc.CreateElement("FirstName");
                //primaryCareProvider.AppendChild(primaryCarefirstName);

                //var primaryCareLastName = xmlDoc.CreateElement("LastName");
                //primaryCareProvider.AppendChild(primaryCareLastName);

                //var guarantor = xmlDoc.CreateElement("Guarantor");
                //patient.AppendChild(guarantor);

                //var grRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //guarantor.AppendChild(grRcopiaID);

                //var grExternalID = xmlDoc.CreateElement("ExternalID");
                //guarantor.AppendChild(grExternalID);

                //var grRelationship = xmlDoc.CreateElement("Relationship");
                //guarantor.AppendChild(grRelationship);

                //var insuranceList = xmlDoc.CreateElement("InsuranceList");
                //patient.AppendChild(insuranceList);

                //var insurance = xmlDoc.CreateElement("Insurance");
                //insuranceList.AppendChild(insurance);

                //var insDeleted = xmlDoc.CreateElement("Deleted");
                //insurance.AppendChild(insDeleted);

                //var insRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //insurance.AppendChild(insRcopiaID);

                //var insFormulary = xmlDoc.CreateElement("Formulary");
                //insurance.AppendChild(insFormulary);

                //var forRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //insurance.AppendChild(forRcopiaID);

                //var fromEligibility = xmlDoc.CreateElement("FromEligibility");
                //insurance.AppendChild(fromEligibility);

                //var pbmName = xmlDoc.CreateElement("PbmName");
                //insurance.AppendChild(pbmName);

                //var planName = xmlDoc.CreateElement("PlanName");
                //insurance.AppendChild(planName);
                //var productName = xmlDoc.CreateElement("ProductName");
                //insurance.AppendChild(productName);

                //var groupNumber = xmlDoc.CreateElement("GroupNumber");
                //insurance.AppendChild(groupNumber);

                //var policyNumber = xmlDoc.CreateElement("PolicyNumber");
                //insurance.AppendChild(policyNumber);

                //var insAddress1 = xmlDoc.CreateElement("Address1");
                //insurance.AppendChild(insAddress1);

                //var insAddress2 = xmlDoc.CreateElement("Address2");
                //insurance.AppendChild(insAddress2);

                //var insCity = xmlDoc.CreateElement("City");
                //insurance.AppendChild(insCity);

                //var insState = xmlDoc.CreateElement("State");
                //insurance.AppendChild(insState);

                //var insZip = xmlDoc.CreateElement("Zip");
                //insurance.AppendChild(insZip);

                //var insPhone = xmlDoc.CreateElement("Phone");
                //insurance.AppendChild(insPhone);

                //var insFax = xmlDoc.CreateElement("Fax");
                //insurance.AppendChild(insFax);

                //var insGuarantor = xmlDoc.CreateElement("Guarantor");
                //insurance.AppendChild(insGuarantor);

                //var guaRcopiaID = xmlDoc.CreateElement("RcopiaID");
                //insGuarantor.AppendChild(guaRcopiaID);

                //var guaExternalID = xmlDoc.CreateElement("ExternalID");
                //insGuarantor.AppendChild(guaExternalID);

                //var guaRelationship = xmlDoc.CreateElement("Relationship");
                //insGuarantor.AppendChild(guaRelationship);
            }
            
            return xmlDoc.InnerXml;
        }
    }
}
