﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace ClinicalIntegration
{
    public class LogApiCall
    {
        /// <summary>
        /// Log Patient Demographics
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="userid"></param>
        /// <param name="dateTime"></param>
        /// <param name="xmlResponse"></param>
        /// <param name="encounterId"></param>
        /// <param name="PracticeToken"></param>
        /// <param name="AppTypeId"></param>
        public void LogErxActivity(string reqXml, string patientId, string userid, DateTime dateTime, string xmlResponse, string encounterId, string PracticeToken, int AppTypeId, IDbConnection connection, bool apiStatus, bool IsError, string rcopia = "")
        {
            string patientDemographicsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string queryQueueLogs = string.Empty;
            string queryErxResponseLogs = string.Empty;
            string queryErxExceptionLogs = string.Empty;
            object queueId = 0;
            try
            {
                queryQueueLogs = "INSERT INTO [DrFirst].[XmlQueue]( PatientId, EncounterId,XmlValue,UserId,ActionTypeId,RecordDatetime) VALUES ( @PatientId, @EncounterId,@XmlValue,@UserId,@ActionTypeId,@RecordDatetime); SELECT SCOPE_IDENTITY() as queueId";
                queryErxResponseLogs = "INSERT INTO [DrFirst].[XmlQueueResponse](XmlQueueId,PatientId, EncounterId,XmlResponse,UserId,RecordDateTime,PatientRcopiaId,ActionTypeId) VALUES (@XmlQueueId,@PatientId, @EncounterId,@XmlResponse,@UserId,@RecordDateTime,@PatientRcopiaId,@ActionTypeId)";
                queryErxExceptionLogs = "INSERT INTO [DrFirst].[XmlQueueException](XmlQueueId,PatientId, EncounterId,XmlException,UserId,RecordDateTime) VALUES ( @XmlQueueId,@PatientId, @EncounterId,@XmlException,@UserId,@RecordDateTime)";
                
                if(ConnectionState.Closed==connection.State)
                connection.Open();

                using (IDbCommand cmd = connection.CreateCommand())
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = queryQueueLogs;
                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);

                    IDbDataParameter parEncounterId = cmd.CreateParameter();
                    parEncounterId.Direction = ParameterDirection.Input;
                    parEncounterId.Value = encounterId != "" && encounterId != null ? Int64.Parse(encounterId) : 0;
                    parEncounterId.ParameterName = "@EncounterId";
                    parEncounterId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parEncounterId);

                    IDbDataParameter parXml = cmd.CreateParameter();
                    parXml.Direction = ParameterDirection.Input;
                    parXml.Value = reqXml != null ? reqXml : "";
                    parXml.ParameterName = "@XmlValue";
                    parXml.DbType = DbType.String;
                    cmd.Parameters.Add(parXml);

                    IDbDataParameter parUserId = cmd.CreateParameter();
                    parUserId.Direction = ParameterDirection.Input;
                    parUserId.Value = userid != "" && userid != null ? Int64.Parse(userid) : 0;
                    parUserId.ParameterName = "@UserId";
                    parUserId.DbType = DbType.Int32;
                    cmd.Parameters.Add(parUserId);

                    IDbDataParameter parActionTypeId = cmd.CreateParameter();
                    parActionTypeId.Direction = ParameterDirection.Input;
                    parActionTypeId.Value = AppTypeId;
                    parActionTypeId.ParameterName = "@ActionTypeId";
                    parActionTypeId.DbType = DbType.Int32;
                    cmd.Parameters.Add(parActionTypeId);

                    IDbDataParameter parRecDt = cmd.CreateParameter();
                    parRecDt.Direction = ParameterDirection.Input;
                    parRecDt.Value = dateTime;
                    parRecDt.ParameterName = "@RecordDatetime";
                    parRecDt.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parRecDt);

                    cmd.CommandType = CommandType.Text;
                    queueId = cmd.ExecuteScalar();
                    
                }

                if (!IsError)
                {
                    using (IDbCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = queryErxResponseLogs;

                        IDbDataParameter parQueueId = cmd.CreateParameter();
                        parQueueId.Direction = ParameterDirection.Input;
                        parQueueId.Value = Int64.Parse(queueId.ToString());
                        parQueueId.ParameterName = "@XmlQueueId";
                        parQueueId.DbType = DbType.Int32;
                        cmd.Parameters.Add(parQueueId);

                        IDbDataParameter parPatient = cmd.CreateParameter();
                        parPatient.Direction = ParameterDirection.Input;
                        parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                        parPatient.ParameterName = "@PatientId";
                        parPatient.DbType = DbType.Int64;
                        cmd.Parameters.Add(parPatient);

                        IDbDataParameter parEncounterId = cmd.CreateParameter();
                        parEncounterId.Direction = ParameterDirection.Input;
                        parEncounterId.Value = encounterId != "" && encounterId != null ? Int64.Parse(encounterId) : 0;
                        parEncounterId.ParameterName = "@EncounterId";
                        parEncounterId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parEncounterId);

                        IDbDataParameter parXmlResponse = cmd.CreateParameter();
                        parXmlResponse.Direction = ParameterDirection.Input;
                        //if (AppTypeId == 7 || AppTypeId == 8 || AppTypeId == 11)
                        //    parXmlResponse.Value = reqXml;
                        //else
                            parXmlResponse.Value = xmlResponse;
                        parXmlResponse.ParameterName = "@XmlResponse";
                        parXmlResponse.DbType = DbType.String;
                        cmd.Parameters.Add(parXmlResponse);

                        IDbDataParameter parUserId = cmd.CreateParameter();
                        parUserId.Direction = ParameterDirection.Input;
                        parUserId.Value = userid != null && userid!="" ? Int64.Parse(userid) : 0;
                        parUserId.ParameterName = "@UserId";
                        parUserId.DbType = DbType.Int32;
                        cmd.Parameters.Add(parUserId);

                        IDbDataParameter parRecDt = cmd.CreateParameter();
                        parRecDt.Direction = ParameterDirection.Input;
                        parRecDt.Value = dateTime;
                        parRecDt.ParameterName = "@RecordDatetime";
                        parRecDt.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parRecDt);

                        IDbDataParameter parActionTypeId = cmd.CreateParameter();
                        parActionTypeId.Direction = ParameterDirection.Input;
                        parActionTypeId.Value = AppTypeId;
                        parActionTypeId.ParameterName = "@ActionTypeId";
                        parActionTypeId.DbType = DbType.Int32;
                        cmd.Parameters.Add(parActionTypeId);

                        IDbDataParameter parRcopiaId = cmd.CreateParameter();
                        parRcopiaId.Direction = ParameterDirection.Input;
                        parRcopiaId.Value = rcopia != null ? rcopia : "0";
                        parRcopiaId.ParameterName = "@PatientRcopiaId";
                        parRcopiaId.DbType = DbType.String;
                        cmd.Parameters.Add(parRcopiaId);

                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                     }
                }
                else if (IsError)
                {
                    using (IDbCommand cmd = connection.CreateCommand())
                    {
                        IDbDataParameter parQueueId = cmd.CreateParameter();
                        parQueueId.Direction = ParameterDirection.Input;
                        parQueueId.Value = Int64.Parse(queueId.ToString());
                        parQueueId.ParameterName = "@XmlQueueId";
                        parQueueId.DbType = DbType.Int32;
                        cmd.Parameters.Add(parQueueId);

                        IDbDataParameter parPatient = cmd.CreateParameter();
                        parPatient.Direction = ParameterDirection.Input;
                        parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                        parPatient.ParameterName = "@PatientId";
                        parPatient.DbType = DbType.Int64;
                        cmd.Parameters.Add(parPatient);

                        IDbDataParameter parEncounterId = cmd.CreateParameter();
                        parEncounterId.Direction = ParameterDirection.Input;
                        parEncounterId.Value = encounterId != "" && encounterId != null ? Int64.Parse(encounterId) : 0;
                        parEncounterId.ParameterName = "@EncounterId";
                        parEncounterId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parEncounterId);

                        IDbDataParameter parXmlResponse = cmd.CreateParameter();
                        parXmlResponse.Direction = ParameterDirection.Input;
                        parXmlResponse.Value = xmlResponse;
                        parXmlResponse.ParameterName = "@XmlException";
                        parXmlResponse.DbType = DbType.String;
                        cmd.Parameters.Add(parXmlResponse);


                        IDbDataParameter parUserId = cmd.CreateParameter();
                        parUserId.Direction = ParameterDirection.Input;
                        parUserId.Value = userid != null && userid!="" ? Int64.Parse(userid) : 0;
                        parUserId.ParameterName = "@UserId";
                        parUserId.DbType = DbType.Int32;
                        cmd.Parameters.Add(parUserId);

                        IDbDataParameter parRecDt = cmd.CreateParameter();
                        parRecDt.Direction = ParameterDirection.Input;
                        parRecDt.Value = dateTime;
                        parRecDt.ParameterName = "@RecordDatetime";
                        parRecDt.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parRecDt);

                        IDbDataParameter parActionTypeId = cmd.CreateParameter();
                        parActionTypeId.Direction = ParameterDirection.Input;
                        parActionTypeId.Value = AppTypeId;
                        parActionTypeId.ParameterName = "@ActionTypeId";
                        parActionTypeId.DbType = DbType.Int32;
                        cmd.Parameters.Add(parActionTypeId);

                        cmd.CommandText = queryErxExceptionLogs;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch
            {

            }
            //finally
            //{
            //    connection.Close();
            //    connection.Dispose();
            //}
        }


        public string GetLastUpdateDate(IDbConnection con, string patientId, string SSOLaunchTime, int actionType)
        {
            string action = string.Empty;
            DataTable dt = new DataTable();
            string lastUpdateDate = string.Empty;
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;
            using (IDbCommand cmd = con.CreateCommand())
            {
                if (actionType == (int)ApiActionType.UpdatePrescriptionPracticeLevelCompleted ||
                    actionType == (int)ApiActionType.UpdateMedicationPracticeLevelCompleted)
                {
                    query = "Select top 1 RecordDateTime as lastupdatedate from [DrFirst].[XmlQueueResponse] with (nolock) where actiontypeid=" + actionType + " " +
                        "Order By RecordId Desc";
                }
                else
                {
                    query = "Select top 1 RecordDateTime as lastupdatedate from [DrFirst].[XmlQueueResponse] with (nolock) where actiontypeid=" + actionType + " " +
                        " AND Patientid = @PatientId order by recordid desc";
                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = patientId != null && patientId != "" ? Int64.Parse(patientId) : 0;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);
                }

                if (con.State.ToString() == "Closed")
                    con.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if (dt.Rows.Count > 0 )
            {
                lastUpdateDate = Convert.ToDateTime(dt.Rows[0]["lastupdatedate"]).ToString("MM/dd/yyyy HH:mm:ss");
            }
            else
            {
                //if (!string.IsNullOrEmpty(SSOLaunchTime) && SSOLaunchTime.Length == 12)// MMddyyHHmmss
                //        lastUpdateDate = SSOLaunchTime.Substring(0, 2) +"/"+ SSOLaunchTime.Substring(2, 2)+"/20" + SSOLaunchTime.Substring(4, 2) + " " +                        SSOLaunchTime.Substring(6, 2) + ":" + SSOLaunchTime.Substring(8, 2) + ":" + SSOLaunchTime.Substring(10, 2);
                    lastUpdateDate = Convert.ToDateTime(SSOLaunchTime).ToString("MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
            return lastUpdateDate;
        }
    }
}
