﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ClinicalIntegration.FunctionStatus
{
    class Functional
    {
        public DataTable GetSnomedDetails(string parFlag = "", string parSnomed = "")
        {
            DataTable dt = new DataTable();            
            SqlDataAdapter adapter = new SqlDataAdapter();            

            String  query = "[Model].[USP_GetSnomedDetails]";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {

                IDbDataParameter Snomed = cmd.CreateParameter();
                Snomed.Direction = ParameterDirection.Input;
                Snomed.Value = parSnomed;
                Snomed.ParameterName = "@snomed";
                Snomed.DbType = DbType.String;
                cmd.Parameters.Add(Snomed);

                IDbDataParameter Flag = cmd.CreateParameter();
                Flag.Direction = ParameterDirection.Input;
                Flag.Value = parFlag;
                Flag.ParameterName = "@Flag";
                Flag.DbType = DbType.String;
                cmd.Parameters.Add(Flag);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }
        public DataTable GetFunctionalStatusList(string _Status)
        {          
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;            
            query = "SELECT  [Id],CONVERT(VARCHAR(20),EnteredDate ,101) AS [EnteredDate],CONVERT(VARCHAR(10), cast(AppointmentDate AS DATE),101) AS AppointmentDate,[ConceptID],[Term]";
            query += ",PL.[Status],LastModifyDate ,RS.[ResourceName],FunctionalValue FROM [model].[FunctionalStatus] PL with (nolock) INNER JOIN Dbo.resources RS with (nolock) ON RS.ResourceId=PL.ResourceId Where PatientId = @PatientId and PL.Status in (" + _Status + ") Order By  EnteredDate Desc";

            if (ClinicalComponent._Connection.State.ToString()== "Closed")
            ClinicalComponent._Connection.Open();

            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter idbPatientId = cmd.CreateParameter();
                idbPatientId.Direction = ParameterDirection.Input;
                idbPatientId.Value = ClinicalComponent.PatientId;
                idbPatientId.ParameterName = "@PatientId";
                idbPatientId.DbType = DbType.Int64;
                cmd.Parameters.Add(idbPatientId);

                //IDbDataParameter idbAppointmentId = cmd.CreateParameter();
                //idbAppointmentId.Direction = ParameterDirection.Input;
                //idbAppointmentId.Value = ClinicalComponent.AppointmentId;
                //idbAppointmentId.ParameterName = "@AppointmentId";
                //idbAppointmentId.DbType = DbType.Int64;
                //cmd.Parameters.Add(idbAppointmentId);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            return dt;
        }
        
        public void SaveFunctionalStatus(bool isNew, FunctionalModel objFunctionModel)
        {          
            string _error = "";

            string query = string.Empty;
            if (isNew)
            {
                try
                {
                    query = "INSERT INTO [model].[FunctionalStatus] ([PatientId],[AppointmentID],[AppointmentDate],";
                    query += "[EnteredDate],[ConceptID],[Term],[ResourceID],[Status],[LastModifyDate],FunctionalValue)";
                    query += "VALUES(@PatientId,@AppointmentID,@AppointmentDate,@EnteredDate,@ConceptID,@Term,";
                    query += "@ResourceID,@Status,@LastModifyDate,@functionalValue)";

                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.Parameters.Clear();

                        IDbDataParameter parPatient = cmd.CreateParameter();
                        parPatient.Direction = ParameterDirection.Input;
                        parPatient.Value = objFunctionModel.PatientId;
                        parPatient.ParameterName = "@PatientId";
                        parPatient.DbType = DbType.Int64;
                        cmd.Parameters.Add(parPatient);

                        IDbDataParameter parAppointmentID = cmd.CreateParameter();
                        parAppointmentID.Direction = ParameterDirection.Input;
                        parAppointmentID.Value = objFunctionModel.AppointmentID;
                        parAppointmentID.ParameterName = "@AppointmentID";
                        parAppointmentID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parAppointmentID);

                        IDbDataParameter parAppDate= cmd.CreateParameter();
                        parAppDate.Direction = ParameterDirection.Input;
                        parAppDate.Value = objFunctionModel.AppointmentDate;
                        parAppDate.ParameterName = "@AppointmentDate";
                        parAppDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parAppDate);

                        IDbDataParameter parEnteredDate = cmd.CreateParameter();
                        parEnteredDate.Direction = ParameterDirection.Input;
                        parEnteredDate.Value = objFunctionModel.EnteredDate;
                        parEnteredDate.ParameterName = "@EnteredDate";
                        parEnteredDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parEnteredDate);

                        IDbDataParameter parConceptID = cmd.CreateParameter();
                        parConceptID.Direction = ParameterDirection.Input;
                        parConceptID.Value = objFunctionModel.ConceptID;
                        parConceptID.ParameterName = "@ConceptID";
                        parConceptID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parConceptID);

                        IDbDataParameter parTerm = cmd.CreateParameter();
                        parTerm.Direction = ParameterDirection.Input;
                        parTerm.Value = objFunctionModel.Term;
                        parTerm.ParameterName = "@Term";
                        parTerm.DbType = DbType.String;
                        cmd.Parameters.Add(parTerm);

                        IDbDataParameter parResourceID = cmd.CreateParameter();
                        parResourceID.Direction = ParameterDirection.Input;
                        parResourceID.Value = objFunctionModel.ResourceId;
                        parResourceID.ParameterName = "@ResourceID";
                        parResourceID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parResourceID);

                        IDbDataParameter parStatus = cmd.CreateParameter();
                        parStatus.Direction = ParameterDirection.Input;
                        parStatus.Value = objFunctionModel.Status;
                        parStatus.ParameterName = "@Status";
                        parStatus.DbType = DbType.String;
                        cmd.Parameters.Add(parStatus);

                        IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                        parLastModifiedDate.Direction = ParameterDirection.Input;
                        parLastModifiedDate.Value = objFunctionModel.LastModofiedDate;
                        parLastModifiedDate.ParameterName = "@LastModifyDate";
                        parLastModifiedDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parLastModifiedDate);

                        IDbDataParameter parFS = cmd.CreateParameter();
                        parFS.Direction = ParameterDirection.Input;
                        parFS.Value = objFunctionModel.FunctionalValue;
                        parFS.ParameterName = "@functionalValue";
                        parFS.DbType = DbType.String;
                        cmd.Parameters.Add(parFS);


                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }

                catch (Exception ee)
                {
                    _error = ee.Message;
                }
            }
            else
            {
                try
                {
                    query = "UPDATE [model].[FunctionalStatus]  SET [ConceptID] = @ConceptID,[Term] =@Term,FunctionalValue=@FunctionalValue ";
                    query += ",[LastModifyDate] =@LastModifyDate ,[Status] = @Status , [ResourceId]=@ResourceId WHERE Id=@id";
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.Parameters.Clear();                        

                        IDbDataParameter parConceptID = cmd.CreateParameter();
                        parConceptID.Direction = ParameterDirection.Input;
                        parConceptID.Value = objFunctionModel.ConceptID;
                        parConceptID.ParameterName = "@ConceptID";
                        parConceptID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parConceptID);

                        IDbDataParameter parId = cmd.CreateParameter();
                        parId.Direction = ParameterDirection.Input;
                        parId.Value = objFunctionModel.Id;
                        parId.ParameterName = "@id";
                        parId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parId);

                        IDbDataParameter parTerm = cmd.CreateParameter();
                        parTerm.Direction = ParameterDirection.Input;
                        parTerm.Value = objFunctionModel.Term;
                        parTerm.ParameterName = "@Term";
                        parTerm.DbType = DbType.String;
                        cmd.Parameters.Add(parTerm);

                        IDbDataParameter parStatus = cmd.CreateParameter();
                        parStatus.Direction = ParameterDirection.Input;
                        parStatus.Value = objFunctionModel.Status;
                        parStatus.ParameterName = "@Status";
                        parStatus.DbType = DbType.String;
                        cmd.Parameters.Add(parStatus);

                        IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                        parLastModifiedDate.Direction = ParameterDirection.Input;
                        parLastModifiedDate.Value = objFunctionModel.LastModofiedDate;
                        parLastModifiedDate.ParameterName = "@LastModifyDate";
                        parLastModifiedDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parLastModifiedDate);

                        IDbDataParameter parResourceId = cmd.CreateParameter();
                        parResourceId.Direction = ParameterDirection.Input;
                        parResourceId.Value = objFunctionModel.ResourceId;
                        parResourceId.ParameterName = "@ResourceId";
                        parResourceId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parResourceId);

                        IDbDataParameter parFS = cmd.CreateParameter();
                        parFS.Direction = ParameterDirection.Input;
                        parFS.Value = objFunctionModel.FunctionalValue;
                        parFS.ParameterName = "@functionalValue";
                        parFS.DbType = DbType.String;
                        cmd.Parameters.Add(parFS);

                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();

                    }
                }
                catch (Exception ee)
                {
                    _error = ee.Message;
                }
            }
        }

    }
}
