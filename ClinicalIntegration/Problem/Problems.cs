﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ClinicalIntegration
{
    public class Problems
    {
        public DataTable DiagnosisFinder(string parFlag = "", string parDiagnosis = "", string parIcd10Id = "")
        {
            DataTable dt = new DataTable();
            string patientMedicationsXml = string.Empty;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;

            query = "[Model].[USP_GetICD10_SnomedCT]";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter Diagnosis = cmd.CreateParameter();
                Diagnosis.Direction = ParameterDirection.Input;
                Diagnosis.Value = parDiagnosis;
                Diagnosis.ParameterName = "@IcdOrDiagnosis";
                Diagnosis.DbType = DbType.String;
                cmd.Parameters.Add(Diagnosis);

                IDbDataParameter Flag = cmd.CreateParameter();
                Flag.Direction = ParameterDirection.Input;
                Flag.Value = parFlag;
                Flag.ParameterName = "@Flag";
                Flag.DbType = DbType.String;
                cmd.Parameters.Add(Flag);


                IDbDataParameter Icd10Id = cmd.CreateParameter();
                Icd10Id.Direction = ParameterDirection.Input;
                Icd10Id.Value = parIcd10Id;
                Icd10Id.ParameterName = "@Icd10Id";
                Icd10Id.DbType = DbType.String;
                cmd.Parameters.Add(Icd10Id);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            return dt;
        }



        public DataTable GetProblemList(string _Status)
        {
            DataTable dt = new DataTable();
            try
            {
                string patientMedicationsXml = string.Empty;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string query = string.Empty;
                query = "Select [Id], pl.EnteredDate , pl.AppointmentDate, pl.ConceptID, pl.Term, pl.ICD10, pl.ICD10DESCR, pl.Status, pl.LastModifyDate, RS.[ResourceName], pl.onsetdate, pl.ResolvedDate From model.PatientProblemList PL with(nolock) INNER JOIN dbo.Appointments A with(nolock) on A.PatientId = PL.PatientId and PL.PatientId = @PatientId and Pl.AppointmentId = A.AppointmentId inner join dbo.Resources RS with(nolock) ON RS.ResourceId = PL.ResourceId Where A.AppDate <= (Select AppDate From dbo.Appointments with(nolock) Where AppointmentId = @AppointmentId) and PL.Status in (" + _Status + ") Order by EnteredDate DESC";

                //System.Windows.Forms.MessageBox.Show(query);

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();

                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter idbPatientId = cmd.CreateParameter();
                    idbPatientId.Direction = ParameterDirection.Input;
                    idbPatientId.Value = ClinicalComponent.PatientId;
                    idbPatientId.ParameterName = "@PatientId";
                    idbPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(idbPatientId);

                    IDbDataParameter idbAppointmentId = cmd.CreateParameter();
                    idbAppointmentId.Direction = ParameterDirection.Input;
                    idbAppointmentId.Value = ClinicalComponent.AppointmentId;
                    idbAppointmentId.ParameterName = "@AppointmentId";
                    idbAppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(idbAppointmentId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                return dt;
            }
            catch
            {

            }
            return dt;
        }

        public bool IsNewActivate()
        {
            bool isNewActivate = false;
            try
            {
                string patientMedicationsXml = string.Empty;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string query = string.Empty;
                query = "SELECT Value from model.ApplicationSettings with(nolock) WHERE Name ='IsNewProblemListActivate' ";

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();

                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            isNewActivate = Convert.ToString(reader["Value"]).ToLower().Contains("true") ? true : false;
                        }
                    }
                }
                return isNewActivate;
            }
            catch
            {

            }
            return isNewActivate;
        }
        public void SaveProblemList(bool isNew, ProblemModel objDiagnosis)
        {
            string query = string.Empty;
            if (isNew)
            {
                query = "INSERT INTO [model].[PatientProblemList] ([PatientId],[AppointmentID],[AppointmentDate],[EnteredDate],[OnsetDate],";
                query += "[ConceptID],[Term],[ICD10],[ICD10DESCR],[ResourceId],[Status],[LastModifyDate],ResolvedDate)";
                query += "VALUES(@PatientId,@AppointmentID, @AppointmentDate,@EnteredDate,@OnsetDate, @ConceptID,@Term,@ICD10,@ICD10DESCR,@ResourceId,";
                query += "@Status,@LastModifyDate,@ResolvedDate)";

                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {

                    cmd.Parameters.Clear();
                    cmd.CommandText = query;

                    IDbDataParameter parPatient = cmd.CreateParameter();
                    parPatient.Direction = ParameterDirection.Input;
                    parPatient.Value = objDiagnosis.PatientId;
                    parPatient.ParameterName = "@PatientId";
                    parPatient.DbType = DbType.Int64;
                    cmd.Parameters.Add(parPatient);

                    IDbDataParameter parAppointmentID = cmd.CreateParameter();
                    parAppointmentID.Direction = ParameterDirection.Input;
                    parAppointmentID.Value = objDiagnosis.AppointmentID;
                    parAppointmentID.ParameterName = "@AppointmentID";
                    parAppointmentID.DbType = DbType.Int64;
                    cmd.Parameters.Add(parAppointmentID);

                    IDbDataParameter parAppDate = cmd.CreateParameter();
                    parAppDate.Direction = ParameterDirection.Input;
                    parAppDate.Value = objDiagnosis.AppointmentDate;
                    parAppDate.ParameterName = "@AppointmentDate";
                    parAppDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parAppDate);


                    IDbDataParameter parEnteredDate = cmd.CreateParameter();
                    parEnteredDate.Direction = ParameterDirection.Input;
                    parEnteredDate.Value = objDiagnosis.EnteredDate;
                    parEnteredDate.ParameterName = "@EnteredDate";
                    parEnteredDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parEnteredDate);

                    IDbDataParameter parOnsetDate = cmd.CreateParameter();
                    parOnsetDate.Direction = ParameterDirection.Input;
                    parOnsetDate.Value = objDiagnosis.OnsetDate;
                    parOnsetDate.ParameterName = "@OnsetDate";
                    parOnsetDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parOnsetDate);


                    IDbDataParameter parConceptID = cmd.CreateParameter();
                    parConceptID.Direction = ParameterDirection.Input;
                    parConceptID.Value = objDiagnosis.ConceptID;
                    parConceptID.ParameterName = "@ConceptID";
                    parConceptID.DbType = DbType.Int64;
                    cmd.Parameters.Add(parConceptID);

                    IDbDataParameter parTerm = cmd.CreateParameter();
                    parTerm.Direction = ParameterDirection.Input;
                    parTerm.Value = objDiagnosis.Term;
                    parTerm.ParameterName = "@Term";
                    parTerm.DbType = DbType.String;
                    cmd.Parameters.Add(parTerm);

                    IDbDataParameter parICD10 = cmd.CreateParameter();
                    parICD10.Direction = ParameterDirection.Input;
                    parICD10.Value = objDiagnosis.ICD10;
                    parICD10.ParameterName = "@ICD10";
                    parICD10.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10);

                    IDbDataParameter parICD10DESCR = cmd.CreateParameter();
                    parICD10DESCR.Direction = ParameterDirection.Input;
                    parICD10DESCR.Value = objDiagnosis.ICD10DESCR;
                    parICD10DESCR.ParameterName = "@ICD10DESCR";
                    parICD10DESCR.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10DESCR);

                    IDbDataParameter parResourceID = cmd.CreateParameter();
                    parResourceID.Direction = ParameterDirection.Input;
                    parResourceID.Value = objDiagnosis.ResourceId;
                    parResourceID.ParameterName = "@ResourceId";
                    parResourceID.DbType = DbType.Int64;
                    cmd.Parameters.Add(parResourceID);

                    IDbDataParameter parStatus = cmd.CreateParameter();
                    parStatus.Direction = ParameterDirection.Input;
                    parStatus.Value = objDiagnosis.Status;
                    parStatus.ParameterName = "@Status";
                    parStatus.DbType = DbType.String;
                    cmd.Parameters.Add(parStatus);



                    IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                    parLastModifiedDate.Direction = ParameterDirection.Input;
                    parLastModifiedDate.Value = objDiagnosis.LastModofiedDate;
                    parLastModifiedDate.ParameterName = "@LastModifyDate";
                    parLastModifiedDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parLastModifiedDate);


                    IDbDataParameter parResolved = cmd.CreateParameter();
                    parResolved.Direction = ParameterDirection.Input;
                    if (objDiagnosis.Status.Equals("Resolved"))
                        parResolved.Value = DateTime.Now;
                    else
                        parResolved.Value = DBNull.Value;
                    parResolved.ParameterName = "@ResolvedDate";
                    parResolved.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parResolved);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();

                }
            }
            else
            {

                query = "UPDATE [model].[PatientProblemList]  SET [ConceptID] = @ConceptID,[Term] =@Term  ,AppointmentDate=@AppointmentDate, OnsetDate=@OnsetDate";
                query += ",[ICD10] = @ICD10 ,[ICD10DESCR] =@ICD10DESCR ,[LastModifyDate] =@LastModifyDate ,[Status] = @Status,[ResourceId] =@ResourceId,ResolvedDate=@ResolvedDate WHERE Id=@id";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {

                    cmd.Parameters.Clear();
                    cmd.CommandText = query;

                    IDbDataParameter parConceptID = cmd.CreateParameter();
                    parConceptID.Direction = ParameterDirection.Input;
                    parConceptID.Value = objDiagnosis.ConceptID;
                    parConceptID.ParameterName = "@ConceptID";
                    parConceptID.DbType = DbType.Int64;
                    cmd.Parameters.Add(parConceptID);

                    IDbDataParameter parId = cmd.CreateParameter();
                    parId.Direction = ParameterDirection.Input;
                    parId.Value = objDiagnosis.Id;
                    parId.ParameterName = "@id";
                    parId.DbType = DbType.Int64;
                    cmd.Parameters.Add(parId);

                    IDbDataParameter parTerm = cmd.CreateParameter();
                    parTerm.Direction = ParameterDirection.Input;
                    parTerm.Value = objDiagnosis.Term;
                    parTerm.ParameterName = "@Term";
                    parTerm.DbType = DbType.String;
                    cmd.Parameters.Add(parTerm);


                    IDbDataParameter parAppointmentDate = cmd.CreateParameter();
                    parAppointmentDate.Direction = ParameterDirection.Input;
                    parAppointmentDate.Value = objDiagnosis.AppointmentDate;
                    parAppointmentDate.ParameterName = "@AppointmentDate";
                    parAppointmentDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parAppointmentDate);


                    IDbDataParameter parOnsetDate = cmd.CreateParameter();
                    parOnsetDate.Direction = ParameterDirection.Input;
                    parOnsetDate.Value = objDiagnosis.OnsetDate;
                    parOnsetDate.ParameterName = "@OnsetDate";
                    parOnsetDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parOnsetDate);


                    IDbDataParameter parICD10 = cmd.CreateParameter();
                    parICD10.Direction = ParameterDirection.Input;
                    parICD10.Value = objDiagnosis.ICD10;
                    parICD10.ParameterName = "@ICD10";
                    parICD10.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10);

                    IDbDataParameter parICD10DESCR = cmd.CreateParameter();
                    parICD10DESCR.Direction = ParameterDirection.Input;
                    parICD10DESCR.Value = objDiagnosis.ICD10DESCR;
                    parICD10DESCR.ParameterName = "@ICD10DESCR";
                    parICD10DESCR.DbType = DbType.String;
                    cmd.Parameters.Add(parICD10DESCR);

                    IDbDataParameter parStatus = cmd.CreateParameter();
                    parStatus.Direction = ParameterDirection.Input;
                    parStatus.Value = objDiagnosis.Status;
                    parStatus.ParameterName = "@Status";
                    parStatus.DbType = DbType.String;
                    cmd.Parameters.Add(parStatus);

                    IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                    parLastModifiedDate.Direction = ParameterDirection.Input;
                    parLastModifiedDate.Value = objDiagnosis.LastModofiedDate;
                    parLastModifiedDate.ParameterName = "@LastModifyDate";
                    parLastModifiedDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parLastModifiedDate);

                    IDbDataParameter parResourceID = cmd.CreateParameter();
                    parResourceID.Direction = ParameterDirection.Input;
                    parResourceID.Value = objDiagnosis.ResourceId;
                    parResourceID.ParameterName = "@ResourceId";
                    parResourceID.DbType = DbType.Int64;
                    cmd.Parameters.Add(parResourceID);

                    IDbDataParameter parResolved = cmd.CreateParameter();
                    parResolved.Direction = ParameterDirection.Input;
                    if (objDiagnosis.Status.Equals("Resolved"))
                        parResolved.Value = DateTime.Now;
                    else
                        parResolved.Value = DBNull.Value;
                    parResolved.ParameterName = "@ResolvedDate";
                    parResolved.DbType = DbType.DateTime;
                    cmd.Parameters.Add(parResolved);




                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                }
            }
        }
    }
}
