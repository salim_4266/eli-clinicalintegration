﻿using System;

namespace ClinicalIntegration
{
    public class ProblemModel
    {
        public Int64 PatientId { get; set; }
        public Int64 Id { get; set; }
        public Int64 AppointmentID { get; set; }       
        public string AppointmentDate { get; set; }
        public DateTime EnteredDate { get; set; }
        public DateTime OnsetDate { get; set; }
        public Int64 ConceptID { get; set; }
        public string Term { get; set; }
        public string ICD10 { get; set; }
        public string ICD10DESCR { get; set; }
        public string ResourceName { get; set; }
        public Int32 ResourceId { get; set; }
        public string Status { get; set; }
        public string COMMENTS { get; set; }
        public DateTime LastModofiedBy { get; set; }
        public DateTime LastModofiedDate { get; set; }

        public DateTime ResolvedDate { get; set; }
    }
}
