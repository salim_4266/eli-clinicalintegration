﻿using System;

namespace ClinicalIntegration
{
    class ProceduresModel
    {
        public Int64 PatientId { get; set; }
        public Int64 Id { get; set; }
        public Int64 AppointmentID { get; set; }
        public DateTime EnteredDate { get; set; }
        public string AppointmentDate { get; set; }
        public Int64 ConceptID { get; set; }
        public string Term { get; set; }
        public string Proceduredetail { get; set; }
        public Int32 ResourceId { get; set; }
        public string Status { get; set; }
        public DateTime LastModofiedDate { get; set; }
        public string Health { get; set; }
        public string Reason { get; set; }
    }
}
