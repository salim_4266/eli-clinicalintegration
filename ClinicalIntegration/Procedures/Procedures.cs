﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ClinicalIntegration
{
    class Procedures
    {      
        public DataTable GetProceduresList(string _Status)
        {
            DataTable dt = new DataTable();

            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = string.Empty;
            query = "SELECT  [Id],CONVERT(VARCHAR(20),EnteredDate ,101) AS [EnteredDate],CONVERT(VARCHAR(10), CAST(AppointmentDate AS DATE),101) AS AppointmentDate ,[ConceptID],[Term],[Proceduredetail],[Health],[Reason] ";
            query += ",PL.[Status],RS.[ResourceName],CONVERT(VARCHAR(20),LastModifyDate ,101) [LastModifyDate] FROM [model].[ProceduresDetail] PL with(nolock) INNER JOIN Dbo.resources RS with(nolock) ON RS.ResourceId=PL.ResourceId Where PatientId = @PatientId and PL.Status in (" + _Status + ") Order By EnteredDate Desc";

            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter idbPatientId = cmd.CreateParameter();
                idbPatientId.Direction = ParameterDirection.Input;
                idbPatientId.Value = ClinicalComponent.PatientId;
                idbPatientId.ParameterName = "@PatientId";
                idbPatientId.DbType = DbType.Int64;
                cmd.Parameters.Add(idbPatientId);

                //IDbDataParameter idbAppointmentId = cmd.CreateParameter();
                //idbAppointmentId.Direction = ParameterDirection.Input;
                //idbAppointmentId.Value = ClinicalComponent.AppointmentId;
                //idbAppointmentId.ParameterName = "@AppointmentId";
                //idbAppointmentId.DbType = DbType.Int64;
                //cmd.Parameters.Add(idbAppointmentId);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            return dt;
        }

        public void SaveProcedures(bool isNew, ProceduresModel objGoalModel)
        {
            string _error = "";

            string query = string.Empty;
            if (isNew)
            {
                try
                {
                    query = "INSERT INTO [model].[ProceduresDetail] ([PatientId],[AppointmentID],[AppointmentDate],";
                    query += "[EnteredDate],[ConceptID],[Term], [Proceduredetail],[ResourceID],[Status],[LastModifyDate],[Health],[Reason])";
                    query += "VALUES(@PatientId,@AppointmentID,@AppointmentDate,@EnteredDate,@ConceptID,@Term,@Proceduredetail,";
                    query += "@ResourceID,@Status,@LastModifyDate,@health,@reason)";

                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.Parameters.Clear();

                        IDbDataParameter parPatient = cmd.CreateParameter();
                        parPatient.Direction = ParameterDirection.Input;
                        parPatient.Value = objGoalModel.PatientId;
                        parPatient.ParameterName = "@PatientId";
                        parPatient.DbType = DbType.Int64;
                        cmd.Parameters.Add(parPatient);

                        IDbDataParameter parAppointmentID = cmd.CreateParameter();
                        parAppointmentID.Direction = ParameterDirection.Input;
                        parAppointmentID.Value = objGoalModel.AppointmentID;
                        parAppointmentID.ParameterName = "@AppointmentID";
                        parAppointmentID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parAppointmentID);

                        IDbDataParameter parAppDate = cmd.CreateParameter();
                        parAppDate.Direction = ParameterDirection.Input;
                        parAppDate.Value = objGoalModel.AppointmentDate;
                        parAppDate.ParameterName = "@AppointmentDate";
                        parAppDate.DbType = DbType.String;
                        cmd.Parameters.Add(parAppDate);



                        IDbDataParameter parEnteredDate = cmd.CreateParameter();
                        parEnteredDate.Direction = ParameterDirection.Input;
                        parEnteredDate.Value = objGoalModel.EnteredDate;
                        parEnteredDate.ParameterName = "@EnteredDate";
                        parEnteredDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parEnteredDate);

                        IDbDataParameter parConceptID = cmd.CreateParameter();
                        parConceptID.Direction = ParameterDirection.Input;
                        parConceptID.Value = objGoalModel.ConceptID;
                        parConceptID.ParameterName = "@ConceptID";
                        parConceptID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parConceptID);

                        IDbDataParameter parTerm = cmd.CreateParameter();
                        parTerm.Direction = ParameterDirection.Input;
                        parTerm.Value = objGoalModel.Term;
                        parTerm.ParameterName = "@Term";
                        parTerm.DbType = DbType.String;
                        cmd.Parameters.Add(parTerm);

                        IDbDataParameter parProceduredetail = cmd.CreateParameter();
                        parProceduredetail.Direction = ParameterDirection.Input;
                        parProceduredetail.Value = objGoalModel.Proceduredetail;
                        parProceduredetail.ParameterName = "@Proceduredetail";
                        parProceduredetail.DbType = DbType.String;
                        cmd.Parameters.Add(parProceduredetail);

                        IDbDataParameter parResourceID = cmd.CreateParameter();
                        parResourceID.Direction = ParameterDirection.Input;
                        parResourceID.Value = objGoalModel.ResourceId;
                        parResourceID.ParameterName = "@ResourceID";
                        parResourceID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parResourceID);

                        IDbDataParameter parStatus = cmd.CreateParameter();
                        parStatus.Direction = ParameterDirection.Input;
                        parStatus.Value = objGoalModel.Status;
                        parStatus.ParameterName = "@Status";
                        parStatus.DbType = DbType.String;
                        cmd.Parameters.Add(parStatus);

                        IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                        parLastModifiedDate.Direction = ParameterDirection.Input;
                        parLastModifiedDate.Value = objGoalModel.LastModofiedDate;
                        parLastModifiedDate.ParameterName = "@LastModifyDate";
                        parLastModifiedDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parLastModifiedDate);

                        IDbDataParameter parHealth = cmd.CreateParameter();
                        parHealth.Direction = ParameterDirection.Input;
                        parHealth.Value = objGoalModel.Health;
                        parHealth.ParameterName = "@health";
                        parHealth.DbType = DbType.String;
                        cmd.Parameters.Add(parHealth);

                        IDbDataParameter parReason = cmd.CreateParameter();
                        parReason.Direction = ParameterDirection.Input;
                        parReason.Value = objGoalModel.Reason;
                        parReason.ParameterName = "@Reason";
                        parReason.DbType = DbType.String;
                        cmd.Parameters.Add(parReason);


                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }

                catch (Exception ee)
                {
                    _error = ee.Message;
                }
            }
            else
            {
                try
                {
                    query = "UPDATE [model].[ProceduresDetail]  SET [ConceptID] = @ConceptID,[Term] =@Term ";
                    query += ",[Proceduredetail]=@Proceduredetail, [LastModifyDate] =@LastModifyDate ,[Status] = @Status, Health=@Health, Reason=@Reason  WHERE Id=@id";
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.Parameters.Clear();

                        IDbDataParameter parConceptID = cmd.CreateParameter();
                        parConceptID.Direction = ParameterDirection.Input;
                        parConceptID.Value = objGoalModel.ConceptID;
                        parConceptID.ParameterName = "@ConceptID";
                        parConceptID.DbType = DbType.Int64;
                        cmd.Parameters.Add(parConceptID);

                        IDbDataParameter parId = cmd.CreateParameter();
                        parId.Direction = ParameterDirection.Input;
                        parId.Value = objGoalModel.Id;
                        parId.ParameterName = "@id";
                        parId.DbType = DbType.Int64;
                        cmd.Parameters.Add(parId);

                        IDbDataParameter parTerm = cmd.CreateParameter();
                        parTerm.Direction = ParameterDirection.Input;
                        parTerm.Value = objGoalModel.Term;
                        parTerm.ParameterName = "@Term";
                        parTerm.DbType = DbType.String;
                        cmd.Parameters.Add(parTerm);

                        IDbDataParameter parProceduredetail = cmd.CreateParameter();
                        parProceduredetail.Direction = ParameterDirection.Input;
                        parProceduredetail.Value = objGoalModel.Proceduredetail;
                        parProceduredetail.ParameterName = "@Proceduredetail";
                        parProceduredetail.DbType = DbType.String;
                        cmd.Parameters.Add(parProceduredetail);

                        IDbDataParameter parStatus = cmd.CreateParameter();
                        parStatus.Direction = ParameterDirection.Input;
                        parStatus.Value = objGoalModel.Status;
                        parStatus.ParameterName = "@Status";
                        parStatus.DbType = DbType.String;
                        cmd.Parameters.Add(parStatus);

                        IDbDataParameter parLastModifiedDate = cmd.CreateParameter();
                        parLastModifiedDate.Direction = ParameterDirection.Input;
                        parLastModifiedDate.Value = objGoalModel.LastModofiedDate;
                        parLastModifiedDate.ParameterName = "@LastModifyDate";
                        parLastModifiedDate.DbType = DbType.DateTime;
                        cmd.Parameters.Add(parLastModifiedDate);

                        IDbDataParameter parHealth = cmd.CreateParameter();
                        parHealth.Direction = ParameterDirection.Input;
                        parHealth.Value = objGoalModel.Health;
                        parHealth.ParameterName = "@health";
                        parHealth.DbType = DbType.String;
                        cmd.Parameters.Add(parHealth);

                        IDbDataParameter parReason = cmd.CreateParameter();
                        parReason.Direction = ParameterDirection.Input;
                        parReason.Value = objGoalModel.Reason;
                        parReason.ParameterName = "@Reason";
                        parReason.DbType = DbType.String;
                        cmd.Parameters.Add(parReason);



                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ee)
                {
                    _error = ee.Message;
                }
            }
        }
    }
}
