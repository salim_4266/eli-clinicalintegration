﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace ClinicalIntegration
{
    public partial class frmSnomedFinder : Form
    {
        #region Properties
        public Form ParentMDI { get; set; }
        public Int64 ConceptID { get; set; }
        public string Description { get; set; }
        public string status { get; set; }

        public bool isEdit { get; set; }
        private bool isSnomedTaken { get; set; }
        public Int64 id { get; set; }

        public string editstatus { get; set; }
        public string CallingFrom { get; set; }
        public string GoalDesc { get; set; }
        public string ProcedureDesc { get; set; }
        public string Health { get; set; }
        public string Reason { get; set; }
        public string FunctionalValue { get; set; }


        #endregion

        #region Form Load
        public frmSnomedFinder()
        {
            InitializeComponent();
        }

        private void frmSnomedFinder_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            FillFindByCB();
            FillSnomedGrid("all", "");
            btnGo.TabStop = false;
            btnGo.FlatStyle = FlatStyle.Flat;
            btnGo.FlatAppearance.BorderSize = 0;
            btnSelect.TabStop = false;
            btnSelect.FlatStyle = FlatStyle.Flat;
            btnSelect.FlatAppearance.BorderSize = 0;
            btnClose.TabStop = false;
            btnClose.FlatStyle = FlatStyle.Flat;
            btnClose.FlatAppearance.BorderSize = 0;
        }

        #endregion

        #region Button Event
        private void btnGo_Click(object sender, EventArgs e)
        {
            btnGo.FlatAppearance.BorderColor = Color.Red;
            if (cbFindBy.SelectedIndex == 0)
            {
                if (ValidateSnomedID())
                {
                    FillSnomedGrid("snomed", txtSnomed.Text);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Please Enter correct SnomedID", "Error");
                }
            }
            else
            {
                FillSnomedGrid("snomeddesc", txtSnomed.Text);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (CallingFrom.Equals("FunctionalStatus"))
            {
                EditFunctionStatus();
            }
            else if (CallingFrom.Equals("Goal"))
            {
                EditGoalInstruction();
            }
            else if (CallingFrom.Equals("Procedure"))
            {
                EditProcedureDetails();
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (CallingFrom.Equals("FunctionalStatus"))
            {
                frmFunctionalStatusList frmp = new frmFunctionalStatusList();
                frmp.MdiParent = this.ParentMDI;
                frmp.ParentMDI = this.ParentMDI;
                frmp.Show();
            }
            else if (CallingFrom.Equals("Goal"))
            {
                frmGoalInstructionList frmp = new frmGoalInstructionList();
                frmp.MdiParent = this.ParentMDI;
                frmp.ParentMDI = this.ParentMDI;
                frmp.Show();
            }
            else if (CallingFrom.Equals("Procedure"))
            {
                frmProceduresList frmp = new frmProceduresList();
                frmp.MdiParent = this.ParentMDI;
                frmp.ParentMDI = this.ParentMDI;
                frmp.Show();
            }
            Close();
        }
        private void grdSnomed_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }

        private void grdSnomed_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex > -1)
            {
                DataGridViewRow row = grdSnomed.Rows[rowIndex];
                isSnomedTaken = true;
                ConceptID = Convert.ToInt64(grdSnomed.Rows[rowIndex].Cells[0].Value.ToString());
                Description = grdSnomed.Rows[rowIndex].Cells[1].Value.ToString();
                //status = grdSnomed.Rows[rowIndex].Cells[2].Value.ToString();
            }
        }

        #endregion

        #region Methods

        private void FillSnomedGrid(string flag, string searchText)
        {
            FunctionStatus.Functional objFun = new FunctionStatus.Functional();

            grdSnomed.DataSource = objFun.GetSnomedDetails(flag, searchText);
            grdSnomed.AutoGenerateColumns = false;
        }


        private void FillFindByCB()
        {
            cbFindBy.Items.Insert(0, "SNOMED CT");
            cbFindBy.Items.Insert(1, "SNOMED CT Description");
            cbFindBy.SelectedIndex = 0;

        }

        /// <summary>
        /// Is text box for SnomedID
        /// </summary>
        /// <returns></returns>
        private bool ValidateSnomedID()
        {
            Int64 val;
            if (Int64.TryParse(txtSnomed.Text, out val))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void EditFunctionStatus()
        {
            frmFunctionStatus frmFS = new frmFunctionStatus();

            if (isSnomedTaken)
            {
                frmFS.SnomedCode = ConceptID;
                frmFS.SnomedDesc = Description;
                frmFS.FunctionalStatusId = id;
                frmFS.Active = editstatus;

            }
            else
            {
                if (grdSnomed.Rows.Count > 0)
                    frmFS.SnomedCode = Convert.ToInt64(grdSnomed.Rows[0].Cells[0].Value.ToString());
                if (grdSnomed.Rows.Count > 0)
                    frmFS.SnomedDesc = grdSnomed.Rows[0].Cells[1].Value.ToString();
                frmFS.Active = editstatus;
                //frmFS.Active = grdSnomed.Rows[0].Cells[2].Value.ToString();
            }

            frmFS.FunctionalValue = FunctionalValue;
            frmFS.IsNew = isEdit;
            frmFS.MdiParent = this.ParentMDI;
            frmFS.ParentMDI = this.ParentMDI;
            frmFS.Show();
            this.Close();
        }

        private void EditGoalInstruction()
        {
            frmGoalInstruction frmFS = new frmGoalInstruction();

            if (isSnomedTaken)
            {
                frmFS.SnomedCode = ConceptID;
                frmFS.SnomedDesc = Description;
                frmFS.GoalId = id;
                frmFS.Active = editstatus;
            }
            else
            {
                if (grdSnomed.Rows.Count > 0)
                    frmFS.SnomedCode = Convert.ToInt64(grdSnomed.Rows[0].Cells[0].Value.ToString());
                if (grdSnomed.Rows.Count > 0)
                    frmFS.SnomedDesc = grdSnomed.Rows[0].Cells[1].Value.ToString();
                frmFS.Active = editstatus;
                //frmFS.Active = grdSnomed.Rows[0].Cells[2].Value.ToString();
            }
            frmFS.GoalDesc = GoalDesc;
            frmFS.Health = Health;
            frmFS.Reason = Reason;
            frmFS.IsNew = isEdit;
            frmFS.MdiParent = this.ParentMDI;
            frmFS.ParentMDI = this.ParentMDI;
            frmFS.Show();
            this.Close();
        }

        private void EditProcedureDetails()
        {
            frmEditProcedures frmFS = new frmEditProcedures();

            if (isSnomedTaken)
            {
                frmFS.SnomedCode = ConceptID;
                frmFS.SnomedDesc = Description;
                frmFS.GoalId = id;
                frmFS.Active = editstatus;
            }
            else
            {
                if (grdSnomed.Rows.Count > 0)
                    frmFS.SnomedCode = Convert.ToInt64(grdSnomed.Rows[0].Cells[0].Value.ToString());
                if (grdSnomed.Rows.Count > 0)
                    frmFS.SnomedDesc = grdSnomed.Rows[0].Cells[1].Value.ToString();
                frmFS.Active = editstatus;
                //frmFS.Active = grdSnomed.Rows[0].Cells[2].Value.ToString();
            }
            frmFS.ProcedureDesc = ProcedureDesc;
            frmFS.Health = Health;
            frmFS.Reason = Reason;
            frmFS.IsNew = isEdit;
            frmFS.MdiParent = this.ParentMDI;
            frmFS.ParentMDI = this.ParentMDI;
            frmFS.Show();
            this.Close();
        }


        #endregion

    }
}
