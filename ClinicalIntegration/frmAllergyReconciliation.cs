﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace ClinicalIntegration
{
    public partial class frmAllergyReconciliation : Form
    {
        public Form ParentMDI { get; set; }

        private void DefaultGridUI(DataGridView dg)
        {

            foreach (DataGridViewColumn col in dg.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font("Arial", 13F, FontStyle.Bold, GraphicsUnit.Pixel);

            }

        }
        public string Filter { get; set; }
        public frmAllergyReconciliation()
        {
            InitializeComponent();
        }
        private static DataTable dtRecon;
        private static DataTable dtMerge;
        private static DataTable dtEAlg;
        private void btnReconcile_Click(object sender, EventArgs e)
        {
            ReconcileList objRconcil = new ReconcileList();
            dtMerge = dtRecon.Clone();
            foreach (System.Data.DataColumn col in dtMerge.Columns) col.ReadOnly = false;
            for (int i = 0; i < dgReconcilation.Rows.Count; i++)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[i].Cells[1];
                if (chk != null)
                {
                    if (!Convert.ToBoolean(chk.Value))
                        continue;

                    dtMerge.ImportRow(dtRecon.Rows[i]);
                    dtMerge.Rows[dtMerge.Rows.Count - 1]["LastModefiedDate"] = DateTime.Now.ToString();
                    dtMerge.Rows[dtMerge.Rows.Count - 1]["ExistingAllergy"] = "0";
                }
            }
            GetMergedAllergies(dtMerge);
            //gdMergedList.DataSource = dtMerge;
        }

        private void frmAllergyReconciliation_Load(object sender, EventArgs e)
        {
            try
            {
                this.Dock = DockStyle.Fill;
                DefaultGridUI(dgReconcilation);
                DefaultGridUI(grdAllergy);
                DefaultGridUI(gdMergedList);
                dtRecon = new DataTable();
                ReconcileList objRconcil = new ReconcileList();
                FillAllergies();
                FillAllergiesReconciliation();
                grdAllergy.RowTemplate.Height = 32;
                grdAllergy.AutoGenerateColumns = false;
                grdAllergy.ReadOnly = true;

                grdAllergy.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
                grdAllergy.EnableHeadersVisualStyles = false;
                grdAllergy.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
                grdAllergy.EnableHeadersVisualStyles = false;
                grdAllergy.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                grdAllergy.ColumnHeadersHeight = 25;
                grdAllergy.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;

                AlignGridUI(dgReconcilation);
                AlignGridUI(gdMergedList);
            }
            catch
            {

            }
        }
        private void AlignGridUI(DataGridView dg)
        {
            dg.RowTemplate.Height = 32;
            dg.AutoGenerateColumns = false;
            dg.ReadOnly = true;
            dg.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            dg.EnableHeadersVisualStyles = false;
            dg.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dg.EnableHeadersVisualStyles = false;
            dg.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            dg.ColumnHeadersHeight = 25;
            dg.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }

        public void FillAllergiesReconciliation()
        {
            ReconcileList objRconcil = new ReconcileList();
            //DataTable dt = new DataTable();
            dtRecon = objRconcil.GetAllergyReconcileList(Convert.ToInt64(ClinicalComponent.PatientId));
            dgReconcilation.AutoGenerateColumns = false;
            dgReconcilation.DataSource = dtRecon;
            dgReconcilation.AutoGenerateColumns = false;
        }

        public void FillAllergies()
        {
            try
            {
                ReconcileList objRconcil = new ReconcileList();
                DataTable dt = new DataTable();
                dt = objRconcil.GetAllergyList(Convert.ToInt64(ClinicalComponent.PatientId));
                foreach (System.Data.DataColumn col in dt.Columns) col.ReadOnly = false;
                foreach (DataRow dr in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dr["LastModefiedDate"])))
                    {
                        dr["LastModefiedDate"] = Convert.ToDateTime(dr["LastModefiedDate"].ToString()).ToLocalTime();
                    }
                    string[] Allg = Convert.ToString(dr[4]).Split('/').ToArray();
                    string[] AllgName = Convert.ToString(dr[1]).Split('/').ToArray();
                    if (Allg.Length > 2)
                    {
                        try
                        {
                            dr[1] = (AllgName[0] == null) ? "" : AllgName[0];
                            dr[2] = (Allg[1] == null) ? "" : Allg[1];
                            dr[3] = (Allg[2] == null) ? "" : Allg[2];
                        }
                        catch
                        {
                        }
                    }
                }
                grdAllergy.AutoGenerateColumns = false;
                dt = RemoveDuplicateRows(dt, "AllergyName");
                dtEAlg = dt;
                grdAllergy.DataSource = dt;
                grdAllergy.AutoGenerateColumns = false;
            }
            catch
            {
            }

        }

        public void GetMergedAllergies(DataTable dtRecon)
        {
            ReconcileList objRconcil = new ReconcileList();
            DataTable dt = new DataTable();
            dt = dtRecon.Clone();
            foreach (DataRow dr in dtRecon.Rows)
            {
                dt.ImportRow(dr);
            }
            DataTable dt1 = new DataTable();
            dt1 = dtEAlg;
            if (dt.Rows.Count == 0)
            {
                gdMergedList.AutoGenerateColumns = false;
                gdMergedList.DataSource = dt1;
                gdMergedList.AutoGenerateColumns = false;
                return;
            }
            //dt = dt1.Clone();
            DataTable dtChild = new DataTable();
            dtChild = dt.Clone();
            bool adddrdt = true;
            foreach (DataRow dr in dt1.Rows)
            {
                adddrdt = true;
                foreach (DataRow drdtt in dt.Rows)
                {
                    if (Convert.ToString(drdtt["AllergySubstance"]).ToLower() == Convert.ToString(dr["AllergyName"]).ToLower())
                    {
                        adddrdt = false;
                        break;
                    }
                }

                if (adddrdt)
                {
                    DataRow drdt = dtChild.NewRow();
                    drdt["AllergySubstance"] = dr["AllergyName"];
                    drdt["Reaction"] = dr["Reaction"];
                    drdt["Severity"] = dr["Severity"];
                    drdt["RxNorm"] = dr["RxNorm"];
                    drdt["Status"] = dr["Status"];

                    drdt["Allergy"] = "";
                    drdt["Id"] = 0;
                    drdt["ExistingAllergy"] = "1";
                    drdt["LastModefiedDate"] = dr["LastModefiedDate"];
                    dtChild.Rows.Add(drdt);
                }


            }
            foreach (DataRow dr in dtChild.Rows)
            {
                dt.ImportRow(dr);
            }
            gdMergedList.AutoGenerateColumns = false;
            gdMergedList.DataSource = dt;
            dtMerge = dt;
            gdMergedList.AutoGenerateColumns = false;
        }
        public DataTable RemoveDuplicateRows(DataTable table, string DistinctColumn)
        {
            try
            {
                ArrayList UniqueRecords = new ArrayList();
                ArrayList DuplicateRecords = new ArrayList();
                foreach (DataRow dRow in table.Rows)
                {
                    if (UniqueRecords.Contains(dRow[DistinctColumn]))
                        DuplicateRecords.Add(dRow);
                    else
                        UniqueRecords.Add(dRow[DistinctColumn]);
                }
                foreach (DataRow dRow in DuplicateRecords)
                {
                    table.Rows.Remove(dRow);
                }
                return table;
            }
            catch
            {
                return null;
            }
        }
        private void chkboxlistReconcilList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string[] allergyDesc = ((System.Data.DataRowView)chkboxlistReconcilList.SelectedItem).Row.ItemArray[1].ToString().Split(',').ToArray();
                //if (!chkboxlistReconcilList.GetItemChecked(chkboxlistReconcilList.SelectedIndex))
                //    return;
                //ReconcileList objRconcil = new ReconcileList();
                //if (objRconcil.CanDisplayAlert())
                //    objRconcil.CallAllergyAlert(allergyDesc[0]);
            }
            catch
            {

                // throw;
            }
        }

        private void dgReconcilation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                string medication = dtRecon.Rows[e.RowIndex]["Allergy"].ToString();
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[e.RowIndex].Cells[1];
                if (Convert.ToBoolean(chk.Value))
                {
                    chk.Value = false;
                }
                else
                {
                    chk.Value = true;
                    ReconcileList objRconcil = new ReconcileList();
                    if (objRconcil.CanDisplayAlert())
                        objRconcil.CallAllergyAlert(medication.TrimStart().TrimEnd());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string clinicalId = string.Empty;
            ReconcileList objRconcil = new ReconcileList();
            for (int i = 0; i < gdMergedList.Rows.Count; i++)
            {
                if (Convert.ToString(dtMerge.Rows[i]["ExistingAllergy"]) != "0")
                {
                    continue;
                }

                for (int j = 0; j < grdAllergy.Rows.Count; j++)
                {
                    if ((Convert.ToString(grdAllergy.Rows[j].Cells[1].Value).TrimEnd().TrimStart().ToUpper()) == Convert.ToString(gdMergedList.Rows[i].Cells[2].Value).TrimEnd().TrimStart().ToUpper())
                    {
                        clinicalId = Convert.ToString(dtEAlg.Rows[j]["ClinicalId"]);
                        break;
                    }
                }
                objRconcil.AddAllergyList(Convert.ToInt64(ClinicalComponent.PatientId), dtMerge.Rows[i], i, clinicalId, Convert.ToInt64(ClinicalComponent.AppointmentId.Trim()));
                FillAllergies();
            }

            gdMergedList.DataSource = new DataTable();
            FillAllergies();
            FillAllergiesReconciliation();
        }

        private void gdMergedList_SelectionChanged(object sender, EventArgs e)
        {
            gdMergedList.ClearSelection();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ReconcileList objRconcil = new ReconcileList();
            dtMerge = dtRecon.Clone();
            foreach (System.Data.DataColumn col in dtMerge.Columns) col.ReadOnly = false;
            for (int i = 0; i < dgReconcilation.Rows.Count; i++)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dgReconcilation.Rows[i].Cells[1];
                if (chk != null)
                {
                    if (!Convert.ToBoolean(chk.Value))
                        continue;

                    dtMerge.ImportRow(dtRecon.Rows[i]);
                    dtMerge.Rows[dtMerge.Rows.Count - 1]["LastModefiedDate"] = DateTime.Now.ToString();
                    dtMerge.Rows[dtMerge.Rows.Count - 1]["ExistingAllergy"] = "0";
                }
            }
            GetMergedAllergies(dtMerge);
            //gdMergedList.DataSource = dtMerge;
        }
    }
}
