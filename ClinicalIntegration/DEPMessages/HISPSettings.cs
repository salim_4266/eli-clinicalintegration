﻿using System.Data;

namespace ClinicalIntegration
{
    public class HISPSettings
    {
        public string ApplicationId { get; set; }
        public string ApplicationPassword { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }

        public HISPSettings(string ProviderId)
        {
            DataTable dtTemp = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            string _selectQry = "Select ApplicationId, DBO.getDecryptPassword(ApplicationPassword) as ApplicationPassword, AccountId From DBO.UpdoxConfigurationDetails with(nolock) Where IsActive = 1";
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                    if (dtTemp.Rows.Count > 0)
                    {
                        ApplicationId = dtTemp.Rows[0]["ApplicationId"].ToString();
                        ApplicationPassword = dtTemp.Rows[0]["ApplicationPassword"].ToString();
                        AccountId = dtTemp.Rows[0]["AccountId"].ToString();
                        UserId = ProviderId;
                    }
                }
                ClinicalComponent._Connection.Close();
            }
        }
    }
}
