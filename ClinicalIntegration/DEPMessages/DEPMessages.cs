﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace ClinicalIntegration
{
    public class DEPMessages
    {
        public DataTable FindAvaiableDocument(string _appointmentId)
        {
            DataTable dtTemp = new DataTable();
            string _selectQry = "Select 'Ltrs-S' + Cast(TransactionId as nvarchar(10)) as Name From Dbo.PracticeTransactionJournal with(nolock) Where TransactionTypeId = '" + _appointmentId + "' and TransactionType = 'S' and(TransactionRemark Like '%Referral%' or TransactionRemark Like '%Consult%')";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                }
            }
            return dtTemp;
        }


        public DataTable LoadInboxMessages(string ProviderId)
        {
            DataTable dt = new DataTable();


            return dt;
        }

        public DataTable LoadOutBoxMessages(string ProviderId)
        {
            DataTable dtTemp = new DataTable();
            string _selectQry = "Select a.AciDepKey as Col1, Dep_RequestDate as Col2, MessageSubject as Col4, a.ToAddress as Col3, a.FromAddress as Col7, ACI_MessageStatus as Col6 From DBO.ACI_DEP a with(nolock) inner join DBO.UpdoxProviders b with(nolock) on a.FromAddress = b.DirectAddress and b.ResourceId = '" + ProviderId + "' Order by Dep_RequestDate Desc";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                }
            }
            return dtTemp;
        }

        public void DownloadNewMessages(string ProviderId)
        {

        }

        public bool SendMessage(SendMessageEntity objSME)
        {
            int _depId = 0;
            bool retStatus = false;

            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            using (IDbCommand cmd_save = ClinicalComponent._Connection.CreateCommand())
            {
                cmd_save.CommandText = "INSERT INTO ACI_DEP(PatientKey, ChartrecordKey, ProviderKey, CreatedBy, FromAddress, ToAddress, Dep_RequestDate, Message, HISP_Type, PtOutboundReferralKey, MessageSubject, ClinicalDocumentType, ReasonContent) VALUES('" + objSME._PatientId + "', '" + objSME._AppointmentId + "', '" + objSME._ResourceId + "', '" + objSME._StaffId + "',  '" + objSME._FromAddress + "', '" + objSME._ToAddress + "', '" + DateTime.Now + "', '" + objSME._MessageBody + "', 'Updox', '" + objSME._ExternalContactId + "', '" + objSME._MessageSubject + "', '" + objSME._ClinicalDocumentType + "', '" + objSME._Reason4Referal + "');";
                cmd_save.CommandType = CommandType.Text;
                cmd_save.ExecuteNonQuery();
            }

            using (IDbCommand cmd_getaci_depkey = ClinicalComponent._Connection.CreateCommand())
            {
                cmd_getaci_depkey.CommandText = "SELECT IDENT_CURRENT('ACI_DEP') AS Id";
                cmd_getaci_depkey.CommandType = CommandType.Text;
                _depId = Convert.ToInt16(cmd_getaci_depkey.ExecuteScalar());
            }

            if (objSME._attachmentList != null)
            {
                foreach (AttachmentEntity AE in objSME._attachmentList)
                {
                    using (IDbCommand cmd_insertattachment = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd_insertattachment.CommandType = CommandType.Text;
                        cmd_insertattachment.CommandText = "INSERT INTO ACI_DEP_OutBound_Attachments(ACI_DepKey, FileType, FileName1, FilePath, FileContent) VALUES ('" + _depId + "', '" + AE._fileType + "', '" + AE._fileName + "', '" + AE._file + "' , '" + AE._fileContent + "') ";
                        cmd_insertattachment.ExecuteNonQuery();
                    }
                }
            }
            ClinicalComponent._Connection.Close();
            retStatus = CreateOutBoundMessage(_depId, objSME._PathDirectory, objSME._ResourceId);
            return retStatus;
        }

        private bool CreateOutBoundMessage(int _depId, string _path2Directory, string _resourceId)
        {
            bool retStatus = false;
            string _bindReference = "";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Exec DBO.ACI_JSON29_DEP_GETReferenceNumber '" + _depId + "'";
                _bindReference = cmd.ExecuteScalar().ToString();
            }
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select * from DBO.VIEW_ACI_JSON29_DEPMessagingData with (nolock) where AciDepKey = '" + _depId + "'";
                DataTable dt = new DataTable();
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
                if (dt.Rows.Count > 0)
                {
                    Outbound outbound = new Outbound();
                    outbound.ResourceType = dt.Rows[0]["ResourceType"].ToString();
                    outbound.ExternalId = dt.Rows[0]["ExternalId"].ToString();
                    outbound.ReferenceNumber = _bindReference.ToString();
                    outbound.Source = dt.Rows[0]["Source"].ToString();
                    outbound.ExternalReferenceId = dt.Rows[0]["ExternalReferenceId"].ToString();
                    outbound.PatientAccountNumber = dt.Rows[0]["PatientAccountNumber"].ToString();
                    outbound.EncounterId = dt.Rows[0]["EncounterId"].ToString();
                    outbound.ClinicalDocumentType = dt.Rows[0]["ClinicalDocumentType"].ToString();
                    outbound.DEPRequestDate = Convert.ToDateTime(dt.Rows[0]["DEPRequestDate"]).ToUniversalTime().ToString("MM/dd/yyyy hh:mm:ss tt");
                    outbound.OrderingECNPI = dt.Rows[0]["OrderingECNPI"].ToString();

                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd1 = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd1.CommandType = CommandType.Text;
                        cmd1.CommandText = "Select FromProviderAddress, FromAddressPassword, ToProviderAddress, TransactionType, MessageSubject, MessageBody from VIEW_ACI_JSON29_DEPMessagingData_DirectAddress with (nolock) where AciDepKey = '" + _depId + "'";
                        DataTable dt1 = new DataTable();
                        using (var reader = cmd1.ExecuteReader())
                        {
                            dt1.Load(reader);
                        }
                        outbound.DirectAddress = new DirectAddress();
                        if (dt1.Rows.Count > 0)
                        {
                            outbound.DirectAddress.FromProviderAddress = dt1.Rows[0]["FromProviderAddress"].ToString();
                            outbound.DirectAddress.FromAddressPassword = dt1.Rows[0]["FromAddressPassword"].ToString();
                            outbound.DirectAddress.ToProviderAddress = dt1.Rows[0]["ToProviderAddress"].ToString();
                            outbound.DirectAddress.CCProviderAddress = "";
                            outbound.DirectAddress.BCCProviderAddress = "";
                            outbound.DirectAddress.HISP_Type = "Updox";
                            outbound.DirectAddress.TransactionType = dt1.Rows[0]["TransactionType"].ToString();
                            outbound.DirectAddress.MessageSubject = dt1.Rows[0]["MessageSubject"].ToString();
                            outbound.DirectAddress.MessageBody = dt1.Rows[0]["MessageBody"].ToString();
                        }
                    }

                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd_createjson2 = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd_createjson2.CommandText = "Select FileName1, FileType, FileContent from DBO.Aci_Dep_Outbound_Attachments with (nolock) Where ACI_DepKey = '" + _depId + "'";
                        cmd_createjson2.CommandType = CommandType.Text;
                        DataTable dt2 = new DataTable();
                        using (var reader = cmd_createjson2.ExecuteReader())
                        {
                            dt2.Load(reader);
                        }
                        List<Attachment> Attachments = new List<Attachment>();
                        foreach (DataRow dr in dt2.Rows)
                        {
                            Attachments.Add(new Attachment { Content = dr["FileContent"].ToString(), FileName = dr["FileName1"].ToString(), ContentType = dr["FileType"].ToString(), ContentDisposition = "Base64" });
                        }
                        if (Attachments.Count >= 1)
                        {
                            outbound.Attachments = Attachments;
                        }
                        outbound.HISPSetting = new List<HISPSettings>() { new HISPSettings(_resourceId) };
                        string outboundjson = JsonConvert.SerializeObject(outbound);
                        RCPServices.CreateFile(_path2Directory, outboundjson, _depId.ToString());
                    }
                    retStatus = new ACIOperation().SendNewOutBound(_path2Directory, _depId.ToString());
                }
            }
            return retStatus;
        }
    }
}
