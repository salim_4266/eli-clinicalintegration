﻿using System.Data;

namespace ClinicalIntegration
{
    public class AddressBook
    {
        public DataTable FindExternalContacts(string _searchContent)
        {
            DataTable dtExternalContact = new DataTable();
            if (ClinicalComponent._Connection.State == ConnectionState.Closed)
                ClinicalComponent._Connection.Open();
            string _searchQry = "Select a.Id as Col1, a.DisplayName as Col2, b.Value as Col3 From model.ExternalContacts a with(nolock) inner join model.ExternalContactEmailAddresses b with(nolock) on a.Id = b.ExternalContactId Where b.OrdinalId = 1 and (a.FirstName Like '%" + _searchContent + "%' OR a.LastNameOrEntityName Like '%" + _searchContent + "%')";
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _searchQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtExternalContact.Load(reader);
                }
            }
            return dtExternalContact;
        }
    }
}
