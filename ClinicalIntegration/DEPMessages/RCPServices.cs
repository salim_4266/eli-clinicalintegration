﻿using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.IO;

namespace ClinicalIntegration
{
    public class RCPServices
    {
        public DataTable LoadResourceDetail(string _providerId)
        {
            DataTable dsResourceTable = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                string query = string.Empty;
                query = "Select * FROM DBO.UpdoxProviders With (nolock) where ResourceId = '" + _providerId + "'";
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                using (var reader = cmd.ExecuteReader())
                {
                    dsResourceTable.Load(reader);
                }
            }
            return dsResourceTable;
        }

        public DataTable LoadClinicalTypes()
        {
            DataTable dtDropDown1 = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd1 = ClinicalComponent._Connection.CreateCommand())
            {
                cmd1.CommandText = "Select '0' as Col1, 'Select Document Type' as Col2  Union Select DATA as Col1, DESCRIPTION1 as Col2 from DBO.DROPDOWN (NOLOCK) where TABLENAME = 'ACI_DEP' and FIELDNAME = 'ClinicalDocumentType'";
                cmd1.CommandType = CommandType.Text;
                using (var reader = cmd1.ExecuteReader())
                {
                    dtDropDown1.Load(reader);
                }
            }
            return dtDropDown1;
        }

        public static void CreateFile(string FolderPath, string Json, string filename)
        {
            try
            {
                string filenamewithext = filename + ".json";
                string path = FolderPath + "\\" + filenamewithext;
                StreamWriter sr = new StreamWriter(path, true);
                sr.Write(Json);
                sr.Close();
            }
            catch
            {

            }
        }

        public static string ConvertDoc2PDF(string filepath2Document)
        {
            string returnPath2PDF = "";
            try
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(filepath2Document);
                returnPath2PDF = filepath2Document.Replace(".doc", ".pdf");
                doc.Save(returnPath2PDF, Aspose.Words.SaveFormat.Pdf);
            }
            catch
            {
                returnPath2PDF = "";
            }
            return returnPath2PDF;
        }

        public static string ConvertToBase64String(string filePath)
        {
            Byte[] bytes = File.ReadAllBytes(filePath);
            return Convert.ToBase64String(bytes);
        }

        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || (strInput.StartsWith("[") && strInput.EndsWith("]")))
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
