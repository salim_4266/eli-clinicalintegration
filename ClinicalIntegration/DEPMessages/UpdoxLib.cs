﻿using System;
using System.IO;
using System.Net;
using System.Data;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ClinicalIntegration
{
    public class UpdoxProviderDetail
    {
        public string UserId { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string TimeZone { get; set; }
        public string DirectAddress { get; set; }
        public string isActive { get; set; }
        public string isProvider { get; set; }
        public string isAdmin { get; set; }
    }


    public class UpdoxLib
    {
        private string applicationId { get; set; }
        private string applicationPassword { get; set; }
        private string accountid { get; set; }
        private string environment { get; set; }

        public UpdoxLib()
        {
            string _selectQry = "Select ApplicationId, dbo.getDecryptPassword(ApplicationPassword) as ApplicationPassword, AccountId, Environment From dbo.UpdoxConfigurationDetails with(nolock) Where IsActive = 1";
            ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                DataTable dtUpdoxDetail = new DataTable();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtUpdoxDetail.Load(reader);
                }
                if (dtUpdoxDetail.Rows.Count > 0)
                {
                    applicationId = dtUpdoxDetail.Rows[0]["ApplicationId"].ToString();
                    applicationPassword = dtUpdoxDetail.Rows[0]["ApplicationPassword"].ToString();
                    accountid = dtUpdoxDetail.Rows[0]["AccountId"].ToString();
                    environment = dtUpdoxDetail.Rows[0]["Environment"].ToString();
                }
            }
            ClinicalComponent._Connection.Close();
        }

        #region "Request"
        public int Updox_UserCreate(ResourceDetail objResource)
        {
            int retStatus = 0;
            UserCreateRequest userCreateRequest = new UserCreateRequest();
            string methodName = "UserCreate";

            //set up credentials
            userCreateRequest.auth.applicationId = applicationId;
            userCreateRequest.auth.applicationPassword = applicationPassword;
            userCreateRequest.auth.accountId = accountid;
            userCreateRequest.auth.userId = "";

            //set up request specific parameters
            userCreateRequest.userId = objResource.UserId;
            userCreateRequest.loginId = objResource.UpdoxDetail.LoginId;
            userCreateRequest.loginPassword = objResource.UpdoxDetail.Password;
            userCreateRequest.firstName = objResource.FirstName;
            userCreateRequest.middleName = objResource.MiddleName;
            userCreateRequest.lastName = objResource.LastName;
            userCreateRequest.address1 = objResource.AddressLine1;
            userCreateRequest.address2 = objResource.AddressLine2 == null ? "" : objResource.AddressLine2;
            userCreateRequest.city = objResource.AddressCity;
            userCreateRequest.state = objResource.AddressState;
            userCreateRequest.postal = objResource.AddressZip5Chars;
            userCreateRequest.timeZone = objResource.UpdoxDetail.TimeZone;
            userCreateRequest.active = objResource.UpdoxDetail.isActive == "True" ? true : false;
            userCreateRequest.provider = objResource.UpdoxDetail.isProvider == "True" ? true : false;
            userCreateRequest.admin = objResource.UpdoxDetail.isAdmin == "True" ? true : false;
            userCreateRequest.directAddress = objResource.UpdoxDetail.DirectAddress;
            userCreateRequest.searchOptOut = false;
            userCreateRequest.metadata = "";

            //create the request
            string json = Utilities.buildRequestJson(userCreateRequest);
            //send the request and get the response
            string response = Utilities.callApi(environment, methodName, json);
            DataTable dt = ConvertJSONToDataTable(response);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["successful"].ToString().ToUpper() == "TRUE")
                {
                    objResource = GetProviderDetail_Updox(objResource);
                    retStatus = objResource.SaveUpdoxDetailsinDB(objResource);
                }
            }
            return retStatus;
        }

        public ResourceDetail GetProviderDetail_Updox(ResourceDetail objResource)
        {
            UserGetRequest userGetRequest = new UserGetRequest();
            string methodName = "UserGet";

            //set up credentials
            userGetRequest.auth.applicationId = applicationId;
            userGetRequest.auth.applicationPassword = applicationPassword;
            userGetRequest.auth.accountId = accountid;
            userGetRequest.auth.userId = "";

            //set up request specific parameters
            userGetRequest.userId = objResource.UserId;

            //create the request
            string json = Utilities.buildRequestJson(userGetRequest);
            //send the request and get the response
            string response = Utilities.callApi(environment, methodName, json);
            System.Data.DataTable dt = ConvertJSONToDataTable(response);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["successful"].ToString().ToUpper() == "TRUE")
                {
                    if (dt.Rows[0]["directaddress"].ToString() != "null")
                    {
                        objResource.UpdoxDetail.DirectAddress = dt.Rows[0]["directaddress"].ToString();
                    }
                    if (dt.Rows[0]["loginId"].ToString() != "null")
                    {
                        objResource.UpdoxDetail.LoginId = dt.Rows[0]["loginId"].ToString();
                    }
                    if (dt.Rows[0]["timeZone"].ToString() != "null")
                    {
                        objResource.UpdoxDetail.TimeZone = dt.Rows[0]["timeZone"].ToString();
                    }
                }
            }
            return objResource;
        }

        public int Updox_UserUpdate(ResourceDetail objResource)
        {
            int retStatus = 0;
            UserCreateRequest userUpdateRequest = new UserCreateRequest();
            string methodName = "UserUpdate";

            //set up credentials
            userUpdateRequest.auth.applicationId = applicationId;
            userUpdateRequest.auth.applicationPassword = applicationPassword;
            userUpdateRequest.auth.accountId = accountid;
            userUpdateRequest.auth.userId = "";

            //set up request specific parameters
            userUpdateRequest.userId = objResource.UserId;
            userUpdateRequest.loginId = objResource.UpdoxDetail.LoginId;
            userUpdateRequest.loginPassword = objResource.UpdoxDetail.Password;
            userUpdateRequest.firstName = objResource.FirstName;
            userUpdateRequest.middleName = objResource.MiddleName;
            userUpdateRequest.lastName = objResource.LastName;
            userUpdateRequest.address1 = objResource.AddressLine1;
            userUpdateRequest.address2 = objResource.AddressLine2 == null ? "" : objResource.AddressLine2;
            userUpdateRequest.city = objResource.AddressCity;
            userUpdateRequest.state = objResource.AddressState;
            userUpdateRequest.postal = objResource.AddressZip5Chars;
            userUpdateRequest.timeZone = objResource.UpdoxDetail.TimeZone;
            userUpdateRequest.active = objResource.UpdoxDetail.isActive == "True" ? true : false;
            userUpdateRequest.provider = objResource.UpdoxDetail.isProvider == "True" ? true : false;
            userUpdateRequest.admin = objResource.UpdoxDetail.isAdmin == "True" ? true : false;
            userUpdateRequest.directAddress = objResource.UpdoxDetail.DirectAddress;
            userUpdateRequest.searchOptOut = false;
            userUpdateRequest.metadata = "";

            //create the request
            string json = Utilities.buildRequestJson(userUpdateRequest);
            //send the request and get the response
            string response = Utilities.callApi(environment, methodName, json);
            DataTable dt = ConvertJSONToDataTable(response);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["successful"].ToString().ToUpper() == "TRUE")
                {
                    objResource = GetProviderDetail_Updox(objResource);
                    retStatus = objResource.SaveUpdoxDetailsinDB(objResource);
                }
            }
            return retStatus;
        }
        #endregion
        #region functions


        private DataTable ConvertJSONToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            //strip out bad characters
            string[] jsonParts = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");

            //hold column names
            List<string> dtColumns = new List<string>();

            //get columns
            foreach (string jp in jsonParts)
            {
                //only loop thru once to get column names
                string[] propData = Regex.Split(jp.Replace("{", "").Replace("}", ""), ",");
                foreach (string rowData in propData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        if (idx > 0)
                        {
                            string n = rowData.Substring(0, idx - 1);
                            string v = rowData.Substring(idx + 1);
                            if (!dtColumns.Contains(n))
                            {
                                dtColumns.Add(n.Replace("\"", ""));
                            }
                        }
                    }
                    catch
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", rowData));
                    }

                }
                break; // TODO: might not be correct. Was : Exit For
            }

            //build dt
            foreach (string c in dtColumns)
            {
                dt.Columns.Add(c);
            }
            //get table data
            foreach (string jp in jsonParts)
            {
                string[] propData = Regex.Split(jp.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in propData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string n = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string v = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[n] = v;
                    }
                    catch
                    {
                        continue;
                    }

                }
                dt.Rows.Add(nr);
            }
            return dt;
        }

        #endregion

        #region "Requestandresponse"

        private class UserGetRequest : UpdoxRequest
        {
            public string userId { get; set; }
        }
        private class UserCreateRequest : UpdoxRequest
        {
            public string userId { get; set; }
            public string loginId { get; set; }
            public string loginPassword { get; set; }
            public string firstName { get; set; }
            public string middleName { get; set; }
            public string lastName { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string postal { get; set; }
            public string timeZone { get; set; }
            public bool active { get; set; }
            public bool provider { get; set; }
            public bool admin { get; set; }
            public string directAddress { get; set; }
            public bool searchOptOut { get; set; }
            public string npi { get; set; }
            public string providerTaxonomyCode { get; set; }
            public string metadata { get; set; }
        }
        private class UserCreateResponse : UpdoxResponse
        {
            public string userId { get; set; }
            public string action { get; set; }
        }

        private class UpdoxRequest
        {
            public UpdoxAuth auth { get; set; }
            public UpdoxRequest()
            {
                this.auth = new UpdoxAuth();
            }
        }
        private class UpdoxAuth
        {
            public string applicationId { get; set; }
            public string applicationPassword { get; set; }
            public string accountId { get; set; }
            public string userId { get; set; }
        }
        private class UpdoxResponse
        {
            public bool successful { get; set; }
            public string responseMessage { get; set; }
            public long responseCode { get; set; }
        }

        #endregion

        #region "utils"
        private static class Utilities
        {
            public static string buildRequestJson(UpdoxRequest request)
            {
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(request);
                return json;
            }

            public static HttpWebResponse sendRequest(string json, string url)
            {
                var httpWebRequest = HttpWebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    return httpResponse;
                }
            }

            public static string callApi(string environment, string methodName, string json)
            {
                string url = environment + methodName;
                HttpWebResponse httpResponse = sendRequest(json, url);

                if (httpResponse == null)
                {
                    throw new ArgumentNullException("response");
                }
                Stream dataStream = null;
                StreamReader reader = null;
                string responseFromServer = null;
                try
                {
                    dataStream = httpResponse.GetResponseStream();
                    reader = new StreamReader(dataStream);
                    responseFromServer = reader.ReadToEnd();
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                    if (dataStream != null)
                    {
                        dataStream.Close();
                    }
                    httpResponse.Close();
                }
                return responseFromServer;
            }
        }


        #endregion
    }

    public class ResourceDetail
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressState { get; set; }
        public string AddressCountry { get; set; }
        public string AddressZip5Chars
        {
            get
            {
                if (AddressZip.Length <= 5)
                    return AddressZip;
                else
                    return AddressZip.Substring(0, 5);
            }
        }
        public string AddressZip { get; set; }
        public string IsUserExists { get; set; }
        public UpdoxProviderDetail UpdoxDetail { get; set; }

        public ResourceDetail(string ResourceId)
        {
            string _selectQry = "Select R.ResourceId, ResourceFirstName, ResourceLastName, ResourceAddress, ResourceCity, ResourceState, ResourceZip, ISNULL(UP.LoginId,'') as LoginId, ISNULL(UP.Password,'') as Password, ISNULL(UP.TimeZone, '') as TimeZone, ISNULL(UP.DirectAddress, '') as DirectAddress, ISNULL(UP.IsActive,'false') as IsActive, Case When R.ResourceType = 'D' Then 'True' Else 'False' End as IsProvider, Case When R.ResourceType = 'D' Then 'True' When R.ResourceType = 'O' Then 'True' Else 'False' End as IsAdmin From dbo.Resources R with(nolock) Left join dbo.UpdoxProviders UP with(nolock) on R.ResourceId = UP.ResourceId Where R.ResourceId = '" + ResourceId + "'";
            ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                DataTable dtResource = new DataTable();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtResource.Load(reader);
                }
                if (dtResource.Rows.Count > 0)
                {
                    this.UserId = ResourceId;
                    this.FirstName = dtResource.Rows[0]["ResourceFirstName"].ToString();
                    this.LastName = dtResource.Rows[0]["ResourceLastName"].ToString();
                    this.AddressLine1 = dtResource.Rows[0]["ResourceAddress"].ToString();
                    this.AddressCity = dtResource.Rows[0]["ResourceCity"].ToString();
                    this.AddressState = dtResource.Rows[0]["ResourceState"].ToString();
                    this.AddressZip = dtResource.Rows[0]["ResourceZip"].ToString();
                    this.IsUserExists = dtResource.Rows[0]["LoginId"].ToString() != "" ? "true" : "false";
                    this.UpdoxDetail = new UpdoxProviderDetail();
                    this.UpdoxDetail.LoginId = dtResource.Rows[0]["LoginId"].ToString() != "" ? dtResource.Rows[0]["LoginId"].ToString() : "";
                    this.UpdoxDetail.Password = "";
                    this.UpdoxDetail.DirectAddress = dtResource.Rows[0]["DirectAddress"].ToString() != "" ? dtResource.Rows[0]["DirectAddress"].ToString() : "";
                    this.UpdoxDetail.TimeZone = dtResource.Rows[0]["TimeZone"].ToString() != "" ? dtResource.Rows[0]["TimeZone"].ToString() : "";
                    this.UpdoxDetail.isActive = dtResource.Rows[0]["IsActive"].ToString();
                    this.UpdoxDetail.isAdmin = dtResource.Rows[0]["IsAdmin"].ToString();
                    this.UpdoxDetail.isProvider = dtResource.Rows[0]["IsProvider"].ToString();

                }
            }
            ClinicalComponent._Connection.Close();
        }

        public int SaveUpdoxDetailsinDB(ResourceDetail objResource)
        {
            int retStatus = 0;
            int isActive = objResource.UpdoxDetail.isActive == "False" ? 0 : 1;
            int isProvider = objResource.UpdoxDetail.isProvider == "False" ? 0 : 1;
            int isAdmin = objResource.UpdoxDetail.isAdmin == "False" ? 0 : 1;
            // To insert new record
            string _selectQry = "Insert into dbo.UpdoxProviders(ResourceId, LoginId, Password, TimeZone, DirectAddress, IsActive, IsProvider, IsAdmin) Values ('" + objResource.UserId + "', '" + objResource.UpdoxDetail.LoginId + "', '" + objResource.UpdoxDetail.Password + "', '" + objResource.UpdoxDetail.TimeZone + "','" + objResource.UpdoxDetail.DirectAddress + "', '" + isActive + "', '" + isProvider + "', '" + isAdmin + "')";
            // To update existing record
            string _updateQry = "Update dbo.UpdoxProviders Set TimeZone = '" + objResource.UpdoxDetail.TimeZone + "', DirectAddress = '" + objResource.UpdoxDetail.DirectAddress + "', IsActive = '" + objResource.UpdoxDetail.isActive + "', IsProvider = '" + objResource.UpdoxDetail.isProvider + "', IsAdmin = '" + objResource.UpdoxDetail.isAdmin + "' Where ResourceId = '" + objResource.UserId + "'";
            ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                if (Convert.ToBoolean(objResource.IsUserExists))
                    cmd.CommandText = _updateQry;
                else
                    cmd.CommandText = _selectQry;
                retStatus = cmd.ExecuteNonQuery();
            }
            ClinicalComponent._Connection.Close();
            return retStatus;
        }
    }

}