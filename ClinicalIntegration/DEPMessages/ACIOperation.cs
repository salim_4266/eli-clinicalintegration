﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace ClinicalIntegration
{
    public class ACIOperation
    {
        internal string GetSentStatus(string externalId, string authToken, ACI_Defaults objACIDefaults, int seconds2Wait)
        {
            string returnStatus = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(seconds2Wait);
                string serviceUrl = objACIDefaults.baseUrl2 + objACIDefaults.productkey + "/" + externalId;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Add("ProductKey", objACIDefaults.productkey);
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + authToken);
                    HttpResponseMessage response = client.GetAsync(serviceUrl).Result;
                    string result = response.Content.ReadAsStringAsync().Result;
                    string jsonString = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1] { '"' });
                    inbounderrorresponse resultResponse = JsonConvert.DeserializeObject<inbounderrorresponse>(jsonString);
                    returnStatus = resultResponse.Status + ", " +  resultResponse.Description;
                    if (ClinicalComponent._Connection.State.ToString() == "Closed")
                        ClinicalComponent._Connection.Open();
                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "Update ACI_DEP Set ACI_MessageStatus = '" + resultResponse.Status + "' Where AciDepKey = '" + externalId + "'";
                        cmd.ExecuteNonQuery();
                    }
                    ClinicalComponent._Connection.Close();
                }
            }
            catch
            {
            }
            return returnStatus;
        }

        internal string GetDeliveryStatus(string ExternalId, string Token, ACI_Defaults objACIDefaults)
        {
            string returnMessage = string.Empty;
            try
            {
                string serviceUrl = objACIDefaults.baseurl3 + objACIDefaults.productkey + "/" + ExternalId;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                    client.DefaultRequestHeaders.Add("ProductKey", objACIDefaults.productkey);
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
                    HttpResponseMessage response = client.GetAsync(serviceUrl).Result;
                    string result = response.Content.ReadAsStringAsync().Result;
                    string jsonString = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1] { '"' });
                    inbounderrorresponse resultResponse = JsonConvert.DeserializeObject<inbounderrorresponse>(jsonString);
                    returnMessage = resultResponse.Status + ", " + resultResponse.Description;
                }
            }
            catch
            {
            }
            return returnMessage;
        }

        public bool PostBatch(string _jsonNumber, string _externalId, string _path2Directory, ResponseTokenEntity objJWTToken, ACI_Defaults objACIDefaults)
        {
            string _batchNumber = "";
            string _readyJson2Process = "";
            string _readyJson = "";
            bool retStatus = false;
            if (_jsonNumber == "21" && _externalId != "")
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd_createJson = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd_createJson.CommandType = CommandType.Text;
                    cmd_createJson.CommandText = "Exec ACI_INSERT_ACI_JSON_MASTER_WithParams 'JSON21_CARECOORDINATIONORDER', '" + _externalId + "'";
                    cmd_createJson.ExecuteNonQuery();
                }

                DataTable dtTemp = new DataTable();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Exec ACI_Check_ISVALIDJSON_WithParam 'JSON21_CARECOORDINATIONORDER', '" + _externalId + "'";
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtTemp.Load(reader);
                    }
                }
                if (dtTemp.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtTemp.Rows.Count; i++)
                    {
                        string _aciMasterkey = dtTemp.Rows[i]["ACIJSONMASTERKEY"].ToString();
                        string _aciJsonstring = dtTemp.Rows[i]["JSONNEW"].ToString();
                        _batchNumber = dtTemp.Rows[i]["BATCHNO"].ToString();
                        bool test = RCPServices.IsValidJson(_aciJsonstring);
                        if (test)
                        {
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "Update ACI_JSON_MASTER set ISVALIDJSON = 'T', BATCHNO = " + _batchNumber + " where ACIJSONMASTERKEY ='" + _aciMasterkey + "'";
                                cmd.ExecuteNonQuery();
                            }

                            if (_readyJson == "")
                            {
                                _readyJson += _aciJsonstring;
                            }
                            else
                            {
                                _readyJson += "," + _aciJsonstring;
                            }
                        }
                        else
                        {
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "Update ACI_JSON_MASTER set ISVALIDJSON = 'F', BATCHNO = " + _batchNumber + " where ACIJSONMASTERKEY ='" + _aciMasterkey + "'";
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }

                if (_readyJson != "")
                {
                    _readyJson2Process = "[" + _readyJson + "]";
                    RCPServices.CreateFile(_path2Directory, _readyJson, _batchNumber + ".json");
                    string date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.ffffff");
                    string _serviceUrl = objACIDefaults.serviceUrl + objACIDefaults.batchId + "&RefBatchID=" + objACIDefaults.batchId + "&Priority=N&BatchCreateDate=" + date;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(objACIDefaults.batchUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                        client.DefaultRequestHeaders.Add("ProductKey", objACIDefaults.productkey);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objJWTToken.TokenDetail);
                        var content = new StringContent(_readyJson2Process.ToString(), Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(_serviceUrl, content).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            string responseString = response.Content.ReadAsStringAsync().Result;
                            ResponseJson json = JsonConvert.DeserializeObject<ResponseJson>(responseString);
                            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                                ClinicalComponent._Connection.Open();
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandText = "ACI_Insert_JSON_Response";

                                IDbDataParameter IdbParam1 = cmd.CreateParameter();
                                IdbParam1.Direction = ParameterDirection.Input;
                                IdbParam1.Value = _batchNumber + ".json";
                                IdbParam1.ParameterName = "@JSONFileName";
                                IdbParam1.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam1);

                                IDbDataParameter IdbParam2 = cmd.CreateParameter();
                                IdbParam2.Direction = ParameterDirection.Input;
                                IdbParam2.Value = json.responseCode;
                                IdbParam2.ParameterName = "@ResponseCode";
                                IdbParam2.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam2);

                                IDbDataParameter IdbParam3 = cmd.CreateParameter();
                                IdbParam3.Direction = ParameterDirection.Input;
                                IdbParam3.Value = json.responseMessage;
                                IdbParam3.ParameterName = "@ResponseMessage";
                                IdbParam3.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam3);

                                IDbDataParameter IdbParam4 = cmd.CreateParameter();
                                IdbParam4.Direction = ParameterDirection.Input;
                                IdbParam4.Value = responseString;
                                IdbParam4.ParameterName = "@CompleteResponse";
                                IdbParam4.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam4);

                                IDbDataParameter IdbParam5 = cmd.CreateParameter();
                                IdbParam5.Direction = ParameterDirection.Input;
                                IdbParam5.Value = objACIDefaults.batchId;
                                IdbParam5.ParameterName = "@BatchID";
                                IdbParam5.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam5);

                                cmd.ExecuteNonQuery();
                            }
                            ClinicalComponent._Connection.Close();
                            retStatus = true;
                            System.Threading.Thread.Sleep(20000);
                        }
                    }
                }
            }

            if (_jsonNumber == "07" && _externalId != "")
            {
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd_createJson = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd_createJson.CommandType = CommandType.Text;
                    cmd_createJson.CommandText = "Exec ACI_INSERT_ACI_JSON_MASTER_WithParams 'PATIENTEXAM', '" + _externalId + "'";
                    cmd_createJson.ExecuteNonQuery();
                }

                DataTable dtTemp = new DataTable();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "Exec ACI_Check_ISVALIDJSON_WithParam 'PATIENTEXAM', '" + _externalId + "'";
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtTemp.Load(reader);
                    }
                }
                if (dtTemp.Rows.Count >= 1)
                {
                    for (int i = 0; i < dtTemp.Rows.Count; i++)
                    {
                        string _aciMasterkey = dtTemp.Rows[i]["ACIJSONMASTERKEY"].ToString();
                        string _aciJsonstring = dtTemp.Rows[i]["JSONNEW"].ToString();
                        _batchNumber = dtTemp.Rows[i]["BATCHNO"].ToString();
                        bool test = RCPServices.IsValidJson(_aciJsonstring);
                        if (test)
                        {
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "Update ACI_JSON_MASTER set ISVALIDJSON = 'T', BATCHNO = " + _batchNumber + " where ACIJSONMASTERKEY ='" + _aciMasterkey + "'";
                                cmd.ExecuteNonQuery();
                            }

                            if (_readyJson == "")
                            {
                                _readyJson += _aciJsonstring;
                            }
                            else
                            {
                                _readyJson += "," + _aciJsonstring;
                            }
                        }
                        else
                        {
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "Update ACI_JSON_MASTER set ISVALIDJSON = 'F', BATCHNO = " + _batchNumber + " where ACIJSONMASTERKEY ='" + _aciMasterkey + "'";
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }

                if (_readyJson != "")
                {
                    _readyJson2Process = "[" + _readyJson + "]";
                    RCPServices.CreateFile(_path2Directory, _readyJson, _batchNumber + ".json");
                    string date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.ffffff");
                    string _serviceUrl = objACIDefaults.serviceUrl + objACIDefaults.batchId + "&RefBatchID=" + objACIDefaults.batchId + "&Priority=N&BatchCreateDate=" + date;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(objACIDefaults.batchUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                        client.DefaultRequestHeaders.Add("ProductKey", objACIDefaults.productkey);
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objJWTToken.TokenDetail);
                        var content = new StringContent(_readyJson2Process.ToString(), Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(_serviceUrl, content).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            string responseString = response.Content.ReadAsStringAsync().Result;
                            ResponseJson json = JsonConvert.DeserializeObject<ResponseJson>(responseString);
                            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                                ClinicalComponent._Connection.Open();
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.CommandText = "ACI_Insert_JSON_Response";

                                IDbDataParameter IdbParam1 = cmd.CreateParameter();
                                IdbParam1.Direction = ParameterDirection.Input;
                                IdbParam1.Value = _batchNumber + ".json";
                                IdbParam1.ParameterName = "@JSONFileName";
                                IdbParam1.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam1);

                                IDbDataParameter IdbParam2 = cmd.CreateParameter();
                                IdbParam2.Direction = ParameterDirection.Input;
                                IdbParam2.Value = json.responseCode;
                                IdbParam2.ParameterName = "@ResponseCode";
                                IdbParam2.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam2);

                                IDbDataParameter IdbParam3 = cmd.CreateParameter();
                                IdbParam3.Direction = ParameterDirection.Input;
                                IdbParam3.Value = json.responseMessage;
                                IdbParam3.ParameterName = "@ResponseMessage";
                                IdbParam3.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam3);

                                IDbDataParameter IdbParam4 = cmd.CreateParameter();
                                IdbParam4.Direction = ParameterDirection.Input;
                                IdbParam4.Value = responseString;
                                IdbParam4.ParameterName = "@CompleteResponse";
                                IdbParam4.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam4);

                                IDbDataParameter IdbParam5 = cmd.CreateParameter();
                                IdbParam5.Direction = ParameterDirection.Input;
                                IdbParam5.Value = objACIDefaults.batchId;
                                IdbParam5.ParameterName = "@BatchID";
                                IdbParam5.DbType = DbType.String;
                                cmd.Parameters.Add(IdbParam5);

                                cmd.ExecuteNonQuery();
                            }
                            ClinicalComponent._Connection.Close();
                            retStatus = true;
                            System.Threading.Thread.Sleep(15000);
                        }
                    }
                }
            }
            return retStatus;
        }

        public bool SendNewOutBound(string _path2Directory, string _depId)
        {
            bool retStatus = false;
            try
            {
                ACI_Defaults objACIDefaults = new ACI_Defaults();
                ResponseTokenEntity objJWTToken = new RequestTokenEntity().GeneratJWTToken();
                retStatus = PostBatch("21", _depId, _path2Directory, objJWTToken, objACIDefaults);
                System.Threading.Thread.Sleep(5000);
                if (retStatus)
                {
                    string _jsonFilePath = _path2Directory + "\\" + _depId + ".json";
                    if (File.Exists(_jsonFilePath))
                    {
                        string aci_dep_json = File.ReadAllText(_jsonFilePath);
                        bool test = RCPServices.IsValidJson(aci_dep_json);
                        if (test == true)
                        {
                            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                                ClinicalComponent._Connection.Open();
                            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = "Update ACI_DEP set ISVALIDJSON = 'T' Where AciDepKey ='" + _depId + "'";
                                cmd.ExecuteNonQuery();
                            }

                            string finalurl = objACIDefaults.baseUrl + objACIDefaults.productkey;
                            string authorizationToken = "bearer " + objJWTToken.TokenDetail;
                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri(objACIDefaults.baseUrl);
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                                client.DefaultRequestHeaders.Add("ProductKey", objACIDefaults.productkey);
                                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objJWTToken.TokenDetail);
                                var content = new StringContent(aci_dep_json.ToString(), Encoding.UTF8, "application/json");
                                HttpResponseMessage response = client.PostAsync(finalurl, content).Result; // getting response from api
                                if (response.IsSuccessStatusCode)
                                {
                                    string responseString = response.Content.ReadAsStringAsync().Result;  // response string from the api
                                    DEPResponseJson json = JsonConvert.DeserializeObject<DEPResponseJson>(responseString);
                                    using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                                    {
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.CommandText = "ACI_INSERT_DEP_JSON_RESPONSE";

                                        IDbDataParameter IdbParam1 = cmd.CreateParameter();
                                        IdbParam1.Direction = ParameterDirection.Input;
                                        IdbParam1.Value = _depId;
                                        IdbParam1.ParameterName = "@ACIDEPKEY";
                                        IdbParam1.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam1);

                                        IDbDataParameter IdbParam2 = cmd.CreateParameter();
                                        IdbParam2.Direction = ParameterDirection.Input;
                                        IdbParam2.Value = json.status;
                                        IdbParam2.ParameterName = "@Status";
                                        IdbParam2.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam2);

                                        IDbDataParameter IdbParam20 = cmd.CreateParameter();
                                        IdbParam20.Direction = ParameterDirection.Input;
                                        IdbParam20.Value = json.auditTrackingID;
                                        IdbParam20.ParameterName = "@auditTrackingID";
                                        IdbParam20.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam20);

                                        IDbDataParameter IdbParam21 = cmd.CreateParameter();
                                        IdbParam21.Direction = ParameterDirection.Input;
                                        IdbParam21.Value = json.statusCode;
                                        IdbParam21.ParameterName = "@STATUSCODE";
                                        IdbParam21.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam21);

                                        IDbDataParameter IdbParam22 = cmd.CreateParameter();
                                        IdbParam22.Direction = ParameterDirection.Input;
                                        IdbParam22.Value = json.description;
                                        IdbParam22.ParameterName = "@DESCRIPTION";
                                        IdbParam22.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam22);

                                        IDbDataParameter IdbParam3 = cmd.CreateParameter();
                                        IdbParam3.Direction = ParameterDirection.Input;
                                        IdbParam3.Value = responseString;
                                        IdbParam3.ParameterName = "@Response";
                                        IdbParam3.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam3);

                                        IDbDataParameter IdbParam4 = cmd.CreateParameter();
                                        IdbParam4.Direction = ParameterDirection.Input;
                                        IdbParam4.Value = "1";
                                        IdbParam4.ParameterName = "@StatusofResponse";
                                        IdbParam4.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam4);

                                        IDbDataParameter IdbParam5 = cmd.CreateParameter();
                                        IdbParam5.Direction = ParameterDirection.Input;
                                        IdbParam5.Value = "1";
                                        IdbParam5.ParameterName = "@APINO";
                                        IdbParam5.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam5);

                                        IDbDataParameter IdbParam6 = cmd.CreateParameter();
                                        IdbParam6.Direction = ParameterDirection.Input;
                                        IdbParam6.Value = "Response From API-1";
                                        IdbParam6.ParameterName = "@APICOMMENTS";
                                        IdbParam6.DbType = DbType.String;
                                        cmd.Parameters.Add(IdbParam6);

                                        cmd.ExecuteNonQuery();
                                        retStatus = true;
                                    }
                                    ClinicalComponent._Connection.Close();
                                }
                            }
                        }
                        if (retStatus)
                        {
                            GetSentStatus(_depId, objJWTToken.TokenDetail, objACIDefaults, 30000);
                        }
                    }
                }
            }
            catch
            {
                retStatus = false;
            }
            return retStatus;
        }
    }
}
