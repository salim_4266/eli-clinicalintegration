﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;

namespace ClinicalIntegration
{
    public class SendMessageEntity
    {
        public string _FromAddress { get; set; }
        public string _ToAddress { get; set; }
        public string _ClinicalDocumentType { get; set; }
        public string _MessageSubject { get; set; }
        public string _MessageBody { get; set; }
        public string _Reason4Referal { get; set; }
        public List<AttachmentEntity> _attachmentList { get; set; }
        public string _ExternalContactId { get; set; }
        public string _ResourceId { get; set; }
        public string _PatientId { get; set; }
        public string _AppointmentId { get; set; }
        public string _PathDirectory { get; set; }
        public string _StaffId { get; set; }
    }

    public class AttachmentEntity
    {
        public string _fileType { get; set; }
        public string _fileName { get; set; }
        public string _fileContent { get; set; }
        public string _file { get; set; }
    }

    public class Outbound
    {
        public string ResourceType { get; set; }
        public string ExternalId { get; set; }
        public string ReferenceNumber { get; set; }
        public string Source { get; set; }
        public string ExternalReferenceId { get; set; }
        public string PatientAccountNumber { get; set; }
        public string EncounterId { get; set; }
        public string ClinicalDocumentType { get; set; }
        public string DEPRequestDate { get; set; }
        public string OrderingECNPI { get; set; }
        public List<Attachment> Attachments { get; set; }
        public DirectAddress DirectAddress { get; set; }
        public List<HISPSettings> HISPSetting { get; set; }
    }
    public class Attachment
    {
        public string ContentDisposition { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public string Content { get; set; }
    }

    public class DirectAddress
    {
        public string FromProviderAddress { get; set; }
        public string FromAddressPassword { get; set; }
        public string ToProviderAddress { get; set; }
        public string CCProviderAddress { get; set; }
        public string BCCProviderAddress { get; set; }
        public string HISP_Type { get; set; }
        public string TransactionType { get; set; }
        public string MessageSubject { get; set; }
        public string MessageBody { get; set; }
    }

    public class InboundProviderModel
    {
        public string ProviderAddress { get; set; }
        public string ProviderAddressinEMR { get; set; }
        public string ProviderPassword { get; set; }
        public string ApplicationId { get; set; }
        public string ApplicationPassword { get; set; }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public InboundProviderModel()
        {

        }

        public InboundProviderModel(string providerId, string providerAddress)
        {
            HISPSettings objHISPSettings = new HISPSettings(providerId);
            string _endTime = "";
            string _selectQry = "Select Top(1) EndTime From DBO.ACI_DEP_RequestLogs with (nolock) where UserId = '" + providerId + "' Order By Id Desc";
            try
            {
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    _endTime = cmd.ExecuteScalar().ToString();
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {

            }
            ProviderAddress = providerAddress;
            ProviderAddressinEMR = providerAddress;
            ProviderPassword = Guid.NewGuid().ToString().Substring(0, 5);
            ApplicationId = objHISPSettings.ApplicationId;
            ApplicationPassword = objHISPSettings.ApplicationPassword;
            AccountId = objHISPSettings.AccountId;
            UserId = objHISPSettings.UserId;
            if (_endTime != "")
            {
                FromDate = Convert.ToDateTime(_endTime).ToUniversalTime().AddDays(-1).ToString("yyyy-MM-dd hh:mm:ss tt");
                ToDate = DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd hh:mm:ss tt");
            }
            else
            {
                FromDate = DateTime.UtcNow.AddDays(-1).ToString("yyyy-MM-dd hh:mm:ss tt");
                ToDate = DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd hh:mm:ss tt");
            }
        }

        public bool SaveRequestLog(string startTime, string endTime)
        {
            bool retStatus = false;
            DateTime dtFrom = DateTime.Parse(this.FromDate);
            DateTime dtTo = DateTime.Parse(this.ToDate);
            try
            {
                string _insertQry = "Insert into DBO.ACI_DEP_RequestLogs(UserId, StartTime, EndTime, CreatedDateTime, JSONDetails) Values ('" + this.UserId + "', '" + startTime + "', '" + endTime + "', GETDATE(), '')";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;
                    cmd.ExecuteNonQuery();
                    retStatus = true;
                }
                ClinicalComponent._Connection.Close();
            }
            catch
            {
                retStatus = false;
            }
            return retStatus;
        }
    }

    public class inbounderrorresponse
    {
        public string Status { get; set; }
        public int? AuditTrackingID { get; set; }
        public int? StatusCode { get; set; }
        public string Description { get; set; }
    }

    public class InboundJson
    {
        public string ResourceType { get; set; }
        public string ExternalId { get; set; }
        public string ReferenceNumber { get; set; }
        public string Source { get; set; }
        public string ExternalReferenceId { get; set; }
        public string PatientAccountNumber { get; set; }
        public string EncounterId { get; set; }
        public string ClinicalDocumentType { get; set; }
        public string DEPRequestDate { get; set; }
        public string OrderingECNPI { get; set; }
        public List<Attachment> attachments { get; set; }
        public DirectAddress DirectAddress { get; set; }
        public string Description { get; set; }
    }

    public class MessageStatusRequest
    {
        public string FromAddress { get; set; }
        public string ExternalId { get; set; }
    }

    public class MessageStatusReponse
    {
        public string MdnConfirmationDte { get; set; }
        public string MessageId { get; set; }
        public string MessageStatus { get; set; }
        public string fromAddress { get; set; }
        public string toAddress { get; set; }
    }

    public class DEPResponseJson
    {
        public string status { get; set; }
        public int auditTrackingID { get; set; }
        public int statusCode { get; set; }
        public string description { get; set; }

    }

    public class ResponseJson
    {
        public int? responseCode { get; set; }
        public string responseMessage { get; set; }
        public List<Result> result { get; set; }
    }

    public class ResourcesStatus
    {
        public string resourceId { get; set; }
        public int statusCode { get; set; }
        public string message { get; set; }
    }

    public class Result
    {
        public int batchId { get; set; }
        public int responseCode { get; set; }
        public string responseMessage { get; set; }
        public List<ResourcesStatus> resourcesStatus { get; set; }
    }

    public class RequestTokenEntity
    {
        public string UserName { get; set; }
        public string BaseURL { get; set; }
        public string Role { get; set; }
        public string ProductKey { get; set; }
        public string PairKey { get; set; }

        public RequestTokenEntity()
        {
            DataTable dtTemp = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            string _selectQry = "ACI_GET_JSON_DEFAULTS";
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                    if (dtTemp.Rows.Count > 0)
                    {
                        UserName = dtTemp.Rows[0]["ACI_UserName"].ToString(); ;
                        BaseURL = dtTemp.Rows[0]["ACI_Dashboard_BaseUrl"].ToString();
                        Role = dtTemp.Rows[0]["ACI_ROLE"].ToString();
                        ProductKey = dtTemp.Rows[0]["ACI_ProductKey"].ToString();
                        PairKey = dtTemp.Rows[0]["ACI_PairKey"].ToString();
                    }
                }

                ClinicalComponent._Connection.Close();
            }
        }

        public ResponseTokenEntity GeneratJWTToken()
        {
            RequestTokenEntity objToken = new RequestTokenEntity();
            ResponseTokenEntity objResponseEntity = new ResponseTokenEntity();
            if (objToken.UserName != "")
            {
                string result = string.Empty;
                string apiUrl = "https://portal.iopracticeware.com/api/ACI/GenerateToken/";
                string jsonToBeSent = Newtonsoft.Json.JsonConvert.SerializeObject(objToken);
                var webclient = new WebClient();
                webclient.Headers["Content-type"] = "application/json";
                webclient.Encoding = System.Text.Encoding.UTF8;
                try
                {
                    result = webclient.UploadString(apiUrl, "POST", jsonToBeSent);
                    objResponseEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseTokenEntity>(result);
                }
                catch
                {
                }
            }
            return objResponseEntity;
        }
    }

    public class ResponseTokenEntity
    {
        public string TokenStatus { get; set; }
        public string TokenDetail { get; set; }
    }

}
