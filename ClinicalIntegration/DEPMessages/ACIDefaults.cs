﻿using System.Data;

namespace ClinicalIntegration
{
    public class ACI_Defaults
    {
        public string batchUrl { get; set; }
        public string baseUrl { get; set; }
        public string baseUrl2 { get; set; }
        public string baseurl3 { get; set; }
        public string baseurl4 { get; set; }
        public string productkey { get; set; }
        public string username { get; set; }
        public string role { get; set; }
        public string aci_issuer { get; set; }
        public string expiretime { get; set; }
        public string pairkey { get; set; }
        public string aci_audience { get; set; }
        public string serviceUrl { get; set; }
        public string batchId { get; set; }
        public ACI_Defaults()
        {
            DataTable dtTemp = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();

            string _selectQry = "ACI_GET_JSON_DEFAULTS";
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = _selectQry;
                using (var reader = cmd.ExecuteReader())
                {
                    dtTemp.Load(reader);
                    if (dtTemp.Rows.Count > 0)
                    {
                        batchUrl = dtTemp.Rows[0]["ACI_BaseURL"].ToString();
                        baseUrl = dtTemp.Rows[0]["ACI_Baseurl_PublishDEP"].ToString();
                        baseUrl2 = dtTemp.Rows[0]["ACI_Baseurl_GetPublishDEPResult"].ToString();
                        baseurl3 = dtTemp.Rows[0]["ACI_Baseurl_GetMessageDeliveryNotification"].ToString();
                        baseurl4 = dtTemp.Rows[0]["ACI_Baseurl_RetriveInboundDEPData"].ToString();
                        productkey = dtTemp.Rows[0]["ACI_ProductKey"].ToString();
                        username = dtTemp.Rows[0]["ACI_USERNAME"].ToString();
                        role = dtTemp.Rows[0]["ACI_ROLE"].ToString();
                        pairkey = dtTemp.Rows[0]["ACI_PairKey"].ToString();
                        aci_issuer = dtTemp.Rows[0]["ACI_Issuer"].ToString();
                        aci_audience = dtTemp.Rows[0]["ACI_Audience"].ToString();
                        expiretime = dtTemp.Rows[0]["ACI_Expires"].ToString();
                        serviceUrl = dtTemp.Rows[0]["ACI_ServiceURL"].ToString();
                        batchId = dtTemp.Rows[0]["ACI_BatchID"].ToString();
                    }
                }
                ClinicalComponent._Connection.Close();
            }
        }
    }
}
