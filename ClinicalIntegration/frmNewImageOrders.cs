﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmNewImageOrders : Form
    {
        public Form ParentMDI { get; set; }
        public bool IsEditMode { get; set; }
        public string ORDERTESTKEY { get; set; }
        public ImageOrderModel objImageModel { get; set; }

        public frmNewImageOrders()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ImageOrderModel objImageModel = new ImageOrderModel();
            objImageModel.OrderId = objImageModel.OrderId == "" ? "" : this.objImageModel.OrderId;
            objImageModel.OrderDate = dateTimePicker1.Value.ToString();
            objImageModel.OrderType = "Imaging";
            objImageModel.Status = comboBox2.SelectedItem == null ? "1" : comboBox2.SelectedItem.ToString() == "Pending" ? "1" : "0";
            objImageModel.OrderForReason = textBox3.Text;
            objImageModel.OrderForm = comboBox1.SelectedItem != null ? comboBox1.SelectedValue.ToString() : "0";
            frmCptCodes frm = new frmCptCodes();
            frm.objImageModel = objImageModel;
            frm.MdiParent = this.ParentMDI;
            frm.ParentMDI = this.ParentMDI;
            frm.IsEditMode = this.IsEditMode;
            frm.Show();
            this.Close();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (IsEditMode)
            {
                if (comboBox1.SelectedItem == null)
                {
                    MessageBox.Show("Please select order form");
                    return;
                }

                if (comboBox2.SelectedItem == null)
                {
                    MessageBox.Show("Please select status");
                    return;
                }

                if (comboBox1.SelectedValue.ToString() == "0")
                {
                    MessageBox.Show("Please select order form");
                    return;
                }

                ImageOrderModel objIMO = new ImageOrderModel()
                {
                    OrderId = objImageModel.OrderId,
                    CptCode = textBox2.Text,
                    OrderForm = comboBox1.SelectedValue.ToString(),
                    OrderType = objImageModel.OrderType,
                    //Status = comboBox2.SelectedItem.ToString() == "Active" ? "1" : "0",
                    Status = comboBox2.SelectedItem == null ? "Pending" : comboBox2.SelectedItem.ToString(),
                    OrderDate = dateTimePicker1.Value.ToString(),
                    LastModifiedBy = ClinicalComponent.UserId,
                    LastModifiedDate = DateTime.Now.ToString(),
                    OrderForReason = textBox3.Text
                };

                ImageOrder objIO = new ImageOrder();
                bool retStatus = objIO.UpdateExistingOrderDetail(objIMO);
                if (retStatus)
                {
                    frmImageOrders frm = new frmImageOrders();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue while adding new order, Please try again");
                }
            }
            else
            {
                //string OrderForm = comboBox1.SelectedItem.ToString();
                //string OrderForm = comboBox1.SelectedValue.ToString();

                if (comboBox1.SelectedValue.ToString() == "0")
                {
                    MessageBox.Show("Please select order form");
                    return;
                }

                if (textBox2.Text == "")
                {
                    MessageBox.Show("Please select Cpt Code");
                    return;
                }

                if (comboBox2.SelectedItem == null)
                {
                    MessageBox.Show("Please select status");
                    return;
                }

                ImageOrderModel objIMO = new ImageOrderModel()
                {
                    CptCode = textBox2.Text,
                    OrderForm = comboBox1.SelectedValue.ToString(),
                    OrderType = objImageModel.OrderType,
                    Status = comboBox2.SelectedItem.ToString(),
                    OrderDate = dateTimePicker1.Value.ToString(),
                    LastModifiedBy = ClinicalComponent.UserId,
                    LastModifiedDate = DateTime.Now.ToString(),
                    OrderForReason = textBox3.Text
                };

                ImageOrder objIO = new ImageOrder();
                bool retStatus = objIO.PlaceNewOrder(objIMO);
                if (retStatus)
                {
                    this.Close();
                    frmImageOrders frm = new frmImageOrders();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                }
                else
                {
                    MessageBox.Show("There is some issue while adding new order, Please try again");
                }
            }
        }

        private void frmNewImageOrders_Load_1(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            LoadOrderFormTypes();
            if (objImageModel.OrderForm != null)
            {
                comboBox1.SelectedValue = objImageModel.OrderForm;
            }
            textBox3.Text = objImageModel.OrderForReason != null ? objImageModel.OrderForReason.ToString() : "";
            textBox2.Text = objImageModel.CptCode == null ? "" : objImageModel.CptCode.ToString();
            dateTimePicker1.Value = objImageModel.OrderDate == null ? Convert.ToDateTime(ClinicalComponent.AppointmentDate).Date : Convert.ToDateTime(objImageModel.OrderDate).Date;
            comboBox2.SelectedItem = objImageModel.Status == null ? "Pending" : objImageModel.Status.ToString() == "1" ? "Pending" : "Completed";
            button3.TabStop = false;
            button3.FlatStyle = FlatStyle.Flat;
            button3.FlatAppearance.BorderSize = 0;
            button2.TabStop = false;
            button2.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
        }

        private void LoadOrderFormTypes()
        {
            ImageOrder ObjIOF = new ImageOrder();
            DataTable dt = ObjIOF.GetOrderForms();
            comboBox1.DataSource = dt;
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Id";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            frmImageOrders frm = new frmImageOrders();
            frm.MdiParent = this.ParentMDI;
            frm.ParentMDI = this.ParentMDI;
            frm.Show();
        }
    }
}