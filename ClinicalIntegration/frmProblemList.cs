﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmProblemList : Form
    {
        public Form ParentMDI { get; set; }
        private string snomedId { get; set; }
        private string IcdCode { get; set; }
        private bool IsEnabled { get; set; }
        ProblemModel objDiag = new ProblemModel();
        public frmProblemList()
        {
            InitializeComponent();
            btnInfo.Enabled = false;
            btnEdit.Enabled = false;
            btnNew.Enabled = false;
        }
        private void grdProblems_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void frmProblemList_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Problems objp = new Problems();
            grdProblems.DataSource = objp.GetProblemList("'Active'");
            grdProblems.RowTemplate.Height = 32;
            comboBox1.SelectedItem = "Active";
            grdProblems.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            grdProblems.EnableHeadersVisualStyles = false;
            grdProblems.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            grdProblems.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
            grdProblems.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            grdProblems.ColumnHeadersHeight = 25;
            grdProblems.ClearSelection();
            btnEdit.TabStop = false;
            btnEdit.FlatStyle = FlatStyle.Flat;
            btnEdit.FlatAppearance.BorderSize = 0;
            btnNew.TabStop = false;
            btnNew.FlatStyle = FlatStyle.Flat;
            btnNew.FlatAppearance.BorderSize = 0;
            btnInfo.TabStop = false;
            btnInfo.FlatStyle = FlatStyle.Flat;
            btnInfo.FlatAppearance.BorderSize = 0;
            if (!objp.IsNewActivate())
            {
                btnNew.Enabled = false;
                btnNew.BackgroundImage = Properties.Resources.btn_new_Inactive;
            }

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            EditProblem editProb = new EditProblem();
            editProb.IsNew = true;
            editProb.MdiParent = this.ParentMDI;
            editProb.ParentMDI = this.ParentMDI;
            editProb.Show();
            this.Close();
        }

        private void grdProblems_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex > -1)
            {
                IsEnabled = true;
                btnInfo.Enabled = true;
                btnEdit.Enabled = true;
                DataGridViewRow row = grdProblems.Rows[rowIndex];
                objDiag.Id = Convert.ToInt64(grdProblems.Rows[rowIndex].Cells[0].Value.ToString());
                objDiag.EnteredDate = DateTime.Parse(grdProblems.Rows[rowIndex].Cells[1].Value.ToString());
                objDiag.AppointmentDate = grdProblems.Rows[rowIndex].Cells[2].Value.ToString();
                objDiag.OnsetDate = DateTime.Parse(grdProblems.Rows[rowIndex].Cells[10].Value.ToString());
                objDiag.ConceptID = Convert.ToInt64(grdProblems.Rows[rowIndex].Cells[3].Value.ToString());
                snomedId = objDiag.ConceptID.ToString();//
                objDiag.Term = grdProblems.Rows[rowIndex].Cells[4].Value.ToString();
                objDiag.ICD10 = grdProblems.Rows[rowIndex].Cells[5].Value.ToString();
                objDiag.ICD10DESCR = grdProblems.Rows[rowIndex].Cells[6].Value.ToString();
                objDiag.Status = grdProblems.Rows[rowIndex].Cells[7].Value.ToString();
                IcdCode = objDiag.ICD10.ToString();
            }
        }
        private void Info_Click(object sender, EventArgs e)
        {
            if (IsEnabled)
            {
                frmInfo pop = new frmInfo();
                pop.IsMedList = false;
                pop.IsProblemList = true;
                pop.IcdCode = IcdCode;
                pop.CodeType = "ICD10";
                pop.PatientId = ClinicalComponent.PatientId;
                pop.ExternalId = objDiag.Id.ToString();
                pop.ResourceId = ClinicalComponent.UserId;
                pop.AppointmentId = ClinicalComponent.AppointmentId;
                pop.ShowDialog();
            }
            else
            {
                btnInfo.Enabled = false;
            }
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            if (IsEnabled)
            {
                Close();
                EditProblem editProb = new EditProblem();
                editProb.IsNew = false;
                editProb.objEditProb = objDiag;
                editProb.MdiParent = this.ParentMDI;
                editProb.ParentMDI = this.ParentMDI;
                editProb.Show();
            }
            else
            {
                btnEdit.Enabled = false;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";
            if (comboBox1.SelectedItem.ToString() == "Active")
                _Status = "'Active'";
            else if (comboBox1.SelectedItem.ToString() == "Inactive")
                _Status = "'Inactive'";
            else
                _Status = "'Active','Inactive','Resolved'";

            if (comboBox1.SelectedItem.ToString() == "Resolved")
                _Status = "'Resolved'";

            Problems objpf = new Problems();

            grdProblems.DataSource = objpf.GetProblemList(_Status);
        }
    }
}
