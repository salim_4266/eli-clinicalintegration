﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace ClinicalIntegration
{
    public partial class frmFunctionalStatusList : Form
    {
        public Form ParentMDI { get; set; }

        bool IsEnabled { get; set; }

        public frmFunctionalStatusList()
        {
            InitializeComponent();
            btnEdit.Enabled = false;
        }

        private void FunctionalStatusList_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            CmbFilter.SelectedItem = "Active";
            FunctionStatus.Functional objFun = new FunctionStatus.Functional();
            grdSnomed.DataSource = objFun.GetFunctionalStatusList("'Active'").DefaultView;
            grdSnomed.AutoGenerateColumns = false;
            grdSnomed.ReadOnly = true;
            grdSnomed.RowTemplate.Height = 32;

            grdSnomed.ColumnHeadersDefaultCellStyle.BackColor = Color.DeepSkyBlue;
            grdSnomed.EnableHeadersVisualStyles = false;
            grdSnomed.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            grdSnomed.EnableHeadersVisualStyles = false;
            grdSnomed.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            grdSnomed.ColumnHeadersHeight = 25;
            grdSnomed.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
            grdSnomed.ClearSelection();
            btnEdit.TabStop = false;
            btnEdit.FlatStyle = FlatStyle.Flat;
            btnEdit.FlatAppearance.BorderSize = 0;
            btnNew.TabStop = false;
            btnNew.FlatStyle = FlatStyle.Flat;
            btnNew.FlatAppearance.BorderSize = 0;
            btnClose.TabStop = false;
            btnClose.FlatStyle = FlatStyle.Flat;
            btnClose.FlatAppearance.BorderSize = 0;

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            frmFunctionStatus fs = new frmFunctionStatus();
            fs.IsNew = true;
            fs.MdiParent = this.ParentMDI;
            fs.ParentMDI = this.ParentMDI;
            fs.Show();
            this.Close();
        }

        private void grdSnomed_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            IsEnabled = true;
            btnEdit.Enabled = true;
            if (e.RowIndex > -1)
            {
                int rowIndex = e.RowIndex;
                DataGridViewRow row = grdSnomed.Rows[rowIndex];
                if (!string.IsNullOrEmpty(grdSnomed.Rows[rowIndex].Cells[0].Value.ToString()))
                {
                    btnEdit.Tag = int.Parse(row.Cells[0].Value.ToString()) + "#" + grdSnomed.Rows[rowIndex].Cells[3].Value.ToString() + "#"
                        + grdSnomed.Rows[rowIndex].Cells[4].Value.ToString() + "#" + grdSnomed.Rows[rowIndex].Cells[5].Value.ToString() + "#" + grdSnomed.Rows[rowIndex].Cells[8].Value.ToString();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _Status = "";
            if (CmbFilter.SelectedItem.ToString() == "Active")
                _Status = "'Active'";
            else if (CmbFilter.SelectedItem.ToString() == "Inactive")
                _Status = "'Inactive'";
            else
                _Status = "'Active','Inactive'";

            FunctionStatus.Functional objFunFilter = new FunctionStatus.Functional();

            grdSnomed.DataSource = objFunFilter.GetFunctionalStatusList(_Status).DefaultView;
        }

        private void grdSnomed_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView grd = sender as DataGridView;
            grd.ClearSelection();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            string[] ContainerList = (btnEdit).Tag.ToString().Split('#');
            if (ContainerList.Count() == 5)
            {
                frmFunctionStatus frmFS = new frmFunctionStatus();
                frmFS.FunctionalStatusId = Convert.ToInt64(ContainerList[0]);
                frmFS.SnomedCode = Convert.ToInt64(ContainerList[1]);
                frmFS.SnomedDesc = ContainerList[2];
                frmFS.Active = ContainerList[3];
                frmFS.FunctionalValue = ContainerList[4];
                frmFS.MdiParent = this.ParentMDI;
                frmFS.ParentMDI = this.ParentMDI;
                frmFS.Show();
                this.Close();
            }
        }
    }
}
