﻿using DBHelper;
using System;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmDirectMessages : Form
    {
        public string PatientId { get; set; }
        public string AppointmentId { get; set; }
        public string ProviderId { get; set; }
        public string StaffId { get; set; }
        public Object DBObject { get; set; }
        public string FolderPath { get; set; }
        private string ProviderEmailAddress { get; set; }
        private string ResourceName { get; set; }
        private string PatientName { get; set; }
        private bool EncounterStatus { get; set; }
        public Form CurrentForm { get; set; }
        public frmDirectMessages()
        {
            InitializeComponent();
        }

        private void frmDirectMessages_Load(object sender, EventArgs e)
        {
            DBUtility db = new DBUtility();
            db.DbObject = DBObject;
            ClinicalComponent._Connection = db.DbObject as IDbConnection;
            GetProviderDetails();
            DefaultButtonImages();
            button4.Image = Properties.Resources.btn_close;
            CreateProviderDirectories();
            DisableButtonBorder(button1);
            DisableButtonBorder(button2);
            DisableButtonBorder(button3);
            DisableButtonBorder(button4);

            frmInBoundMessages frm = new frmInBoundMessages();
            frm.MdiParent = this;
            frm.StaffId = this.StaffId;
            frm.ProviderKey = this.ProviderId;
            frm.ProviderEmailAddress = this.ProviderEmailAddress;
            frm.FolderPath = this.FolderPath + "\\ProviderAttachments\\";
            CurrentForm = frm;
            frm.Show();
            DefaultButtonImages();
            button1.Image = Properties.Resources.btn_inbox_active;
            CheckEncounterStatus();
        }

        private void CheckEncounterStatus()
        {
            EncounterStatus = false;
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandText = "Select ScheduleStatus, ActivityStatus From dbo.Appointments with(nolock) Where AppointmentId = '" + this.AppointmentId + "'";
                cmd.CommandType = CommandType.Text;
                DataTable dt = new DataTable();
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["ScheduleStatus"].ToString() == "D" && dt.Rows[0]["ActivityStatus"].ToString() == "D")
                    {
                        EncounterStatus = true;
                    }
                }
            }
            ClinicalComponent._Connection.Close();
        }

        private void DefaultButtonImages()
        {
            button1.Image = Properties.Resources.btn_inbox;
            button2.Image = Properties.Resources.btn_sent_items;
            button3.Image = Properties.Resources.btn_new_messages;

        }
        private void DisableButtonBorder(Button btn)
        {
            btn.TabStop = false;
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = 0;
        }
        public void GetProviderDetails()
        {
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandText = "Select ResourceFirstName, ResourceLastName, UP.DirectAddress as DirectAddress From dbo.Resources R with(nolock) Left join dbo.UpdoxProviders UP with(nolock) on R.ResourceId = UP.ResourceId Where r.ResourceId = '" + this.StaffId + "'";
                cmd.CommandType = CommandType.Text;
                DataTable dt = new DataTable();
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
                if (dt.Rows.Count > 0)
                {
                    ResourceName = dt.Rows[0]["ResourceFirstName"].ToString() + " " + dt.Rows[0]["ResourceLastName"].ToString();
                    txtProviderName.Text = dt.Rows[0]["ResourceFirstName"].ToString() + " " + dt.Rows[0]["ResourceLastName"].ToString();
                    ProviderEmailAddress = dt.Rows[0]["DirectAddress"].ToString();
                }
            }

            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandText = "Select FirstName + ' ' + LastName as Name From model.Patients P with(nolock) Where P.Id = '" + this.PatientId + "'";
                cmd.CommandType = CommandType.Text;
                PatientName = cmd.ExecuteScalar().ToString();
            }

            ClinicalComponent._Connection.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CurrentForm != null)
                CurrentForm.Close();
            frmInBoundMessages frm = new frmInBoundMessages();
            frm.MdiParent = this;
            frm.StaffId = this.StaffId;
            frm.ProviderEmailAddress = this.ProviderEmailAddress;
            frm.ProviderKey = this.ProviderId;
            frm.FolderPath = this.FolderPath + "\\ProviderAttachments\\";
            CurrentForm = frm;
            frm.Show();
            DefaultButtonImages();
            button1.Image = Properties.Resources.btn_inbox_active;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (CurrentForm != null)
                CurrentForm.Close();
            frmOutBoundSentItems frm = new frmOutBoundSentItems();
            frm.MdiParent = this;
            frm.ProviderKey = this.ProviderId;
            frm.FolderPath = this.FolderPath + "\\ProviderAttachments\\";
            CurrentForm = frm;
            frm.Show();
            DefaultButtonImages();
            button2.Image = Properties.Resources.btn_sent_items_active;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (EncounterStatus)
            {
                if (CurrentForm != null)
                    CurrentForm.Close();
                frmOutboundMessages frm = new frmOutboundMessages();
                frm.PatientId = this.PatientId;
                frm.ChartRecordId = this.AppointmentId;
                frm.ProviderId = this.ProviderId;
                frm.ProviderEmailAddress = this.ProviderEmailAddress;
                frm.StaffId = this.StaffId;
                frm.folderpath = this.FolderPath + @"\ProviderAttachments\";
                frm.ResourceName = this.ResourceName;
                frm.PatientName = this.PatientName;
                frm.MdiParent = this;
                CurrentForm = frm;
                frm.Show();
                DefaultButtonImages();
                button3.Image = Properties.Resources.btn_new_messages_active;
            }
            else
            {
                MessageBox.Show("You can not send DEP messages for unsigned charts, Please sign the chart");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreateProviderDirectories()
        {
            string AttachmentsPath = FolderPath + "\\ProviderAttachments\\";
            if (!Directory.Exists(AttachmentsPath))
            {
                Directory.CreateDirectory(AttachmentsPath);
            }

            string ProviderPath = AttachmentsPath + "\\" + ProviderId;
            if (!Directory.Exists(ProviderPath))
            {
                Directory.CreateDirectory(ProviderPath);
            }

            if (!Directory.Exists(ProviderPath + "\\TXT\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\TXT\\");
            }

            if (!Directory.Exists(ProviderPath + "\\PDF\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\PDF\\");
            }

            if (!Directory.Exists(ProviderPath + "\\HTML\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\HTML\\");
            }

            if (!Directory.Exists(ProviderPath + "\\JSONS\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\JSONS\\");
            }

            if (!Directory.Exists(ProviderPath + "\\XML\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\XML\\");
            }

            if (!Directory.Exists(ProviderPath + "\\JPG\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\JPG\\");
            }

            if (!Directory.Exists(ProviderPath + "\\PNG\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\PNG\\");
            }

            if (!Directory.Exists(ProviderPath + "\\ZIP\\"))
            {
                Directory.CreateDirectory(ProviderPath + "\\ZIP\\");
            }
        }
    }
}
