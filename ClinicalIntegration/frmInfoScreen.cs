﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    public partial class frmInfoScreen : Form
    {

        #region "Variables"
        public string RuleId;
        public Boolean IsBibilographic;
        private string MEDID;
        private string ConceptId;
        private string FieldValue;
        private string FieldArea;
        private string Operation;
        public Boolean IsHome;
        public string homeUrl;
        public string BibliographicsCitationUrl;
        #endregion
        private string FieldName;

        private void frmInfoScreen_Load(object sender, EventArgs e)
        {
            MEDID = "";
            ConceptId = "";
            BindData();
            if (IsBibilographic)
                ShowInfoBrowserBibliography();
            else
                ShowInfoBrowser();
        }
        private string GetAllergyies()
        {
            string strSql = null;
            strSql = "SELECT Top(1) MEDID FROM fdb.tblCompositeDrug with(nolock) WHERE MED_ROUTED_DF_MED_ID_DESC in (" + FieldValue + ")";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            DataTable dt = new DataTable();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["MEDID"].ToString().Trim()))
                        {
                            MEDID = dt.Rows[index]["MEDID"].ToString().Trim();
                        }
                    }
                }
            }
            return MEDID;
        }
        private string GetMedications()
        {
            string strSql = null;
            strSql = "select top 1 d.DisplayName , nc.MED_ROUTED_DF_MED_ID_DESC, rx.MedId,rx.RxCUI from dbo.drug d with (nolock) inner join fdb.tblCompositeDrug nc with (nolock)";
            strSql += " on d.DisplayName = nc.MED_ROUTED_DF_MED_ID_DESC inner join [fdb].[RXCUI2MEDID] rx on nc.Medid = rx.MedId where d.Drugcode = 'T' and d.DisplayName like '%" + FieldValue.Replace("'", "") + "%'";
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            DataTable dt = new DataTable();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }

            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["RxCUI"].ToString().Trim()))
                        {
                            MEDID = dt.Rows[index]["RxCUI"].ToString().Trim();
                        }
                    }
                }
            }
            return MEDID;
        }

        private string GetProblems()
        {
            string strSql = null;
            strSql = "Select Top(1) * from dbo.Snomed_2016 with (nolock) Where ICD10 =" + FieldValue;
            DataTable dt = new DataTable();
            if (ClinicalComponent._Connection.State.ToString() == "Closed")
                ClinicalComponent._Connection.Open();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["ConceptId"].ToString().Trim()))
                        {
                            ConceptId = dt.Rows[index]["ConceptId"].ToString().Trim();
                        }
                    }
                }
            }
            return ConceptId;
        }


        private void ShowInfoBrowserBibliography()
        {
            if (string.IsNullOrEmpty(Convert.ToString(BibliographicsCitationUrl)))
            {
                MessageBox.Show("Please set BibliographicsCitation");
                this.Close();
            }
            webBrowser1.Navigate(Convert.ToString(BibliographicsCitationUrl).Trim());

        }
        //private void ShowInfoBrowserBibliography()
        //{
        //    switch (FieldArea)
        //    {
        //        case "PATIENT":
        //            webBrowser1.Navigate("http://www.who.int/mediacentre/factsheets/fs404/en/");
        //            break;
        //        case "PROBLEMS":
        //            webBrowser1.Navigate("http://www.webmd.com/diabetes/type-2-diabetes-guide/type-2-diabetes#1");
        //            break;
        //        case "MEDALLERGIES":
        //            webBrowser1.Navigate("http://www.healthline.com/health/allergies/bee-sting-anaphylaxis");
        //            break;
        //        case "MEDS":
        //            webBrowser1.Navigate("http://www.healthline.com/drugs/warfarin/oral-tablet");
        //            break;
        //        case "LABS":
        //            webBrowser1.Navigate("https://www.allinahealth.org/CCS/doc/Thomson%20Consumer%20Lab%20Database/49/150341.htm");
        //            break;
        //    }
        //}

        private void ShowInfoBrowser()
        {
            switch (FieldArea)
            {
                case "PATIENT":
                    homeUrl = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.1&mainSearchCriteria.v.c=" + FieldName;
                    webBrowser1.Navigate(homeUrl);
                    break;
                case "PROBLEMS":
                    GetProblems();
                    homeUrl = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.96&mainSearchCriteria.v.c=" + ConceptId;
                    webBrowser1.Navigate(homeUrl);
                    break;
                case "MEDALLERGIES":
                    //GetAllergyies();
                    GetMedications();
                    homeUrl = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.88&mainSearchCriteria.v.c=" + MEDID;
                    webBrowser1.Navigate(homeUrl);
                    break;
                case "MEDS":
                    GetMedications();
                    homeUrl = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.88&mainSearchCriteria.v.c=" + MEDID;
                    webBrowser1.Navigate(homeUrl);
                    break;
                case "LABS":
                    homeUrl = "https://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.1&mainSearchCriteria.v.c=" + FieldName;
                    webBrowser1.Navigate(homeUrl);
                    break;
            }
        }
        public void BindData()
        {
            string strSql = null;
            strSql = "Select A.RuleId,Slno,FieldArea,FieldName,Operation,FieldValue,AndOrFlag,BibliographicsCitation FROM IE_RulesPhrases A with (nolock) left join IE_rules B with (nolock) on A.ruleid=B.ruleid WHERE A.RuleId=@RuleId";
            DataTable dt = new DataTable();
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                IDbDataParameter parRuleId = cmd.CreateParameter();
                parRuleId.Direction = ParameterDirection.Input;
                parRuleId.Value = RuleId;
                parRuleId.ParameterName = "@RuleId";
                parRuleId.DbType = DbType.Int32;
                cmd.Parameters.Add(parRuleId);

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strSql;
                using (var reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
            }
            if ((dt != null))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int index = 0; index <= dt.Rows.Count - 1; index++)
                    {
                        if (!string.IsNullOrEmpty(dt.Rows[index]["FieldArea"].ToString().Trim()))
                        {
                            FieldArea = dt.Rows[index]["FieldArea"].ToString().Trim();
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["FieldValue"].ToString().Trim()))
                        {
                            FieldValue = dt.Rows[index]["FieldValue"].ToString().Trim();
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["Operation"].ToString().Trim()))
                        {
                            Operation = dt.Rows[index]["Operation"].ToString().Trim();
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["FieldName"].ToString().Trim()))
                        {
                            FieldName = dt.Rows[index]["FieldName"].ToString().Trim();
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[index]["BibliographicsCitation"].ToString().Trim()))
                        {
                            BibliographicsCitationUrl = dt.Rows[index]["BibliographicsCitation"].ToString().Trim();
                        }
                    }
                }
            }
        }

        private void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
        }
        public frmInfoScreen()
        {
            InitializeComponent();

        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(homeUrl))
            {
                webBrowser1.Navigate(homeUrl);
                ManageNavigation();
                IsHome = true;
            }
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            IsHome = false;
            if (webBrowser1.CanGoBack)
                webBrowser1.GoBack();
            ManageNavigation();
        }
        private void ManageNavigation()
        {
            if (!IsHome)
            {
                if (webBrowser1.CanGoBack && !webBrowser1.CanGoForward)
                {
                    btnPrev.Enabled = true;
                    btnNext.Enabled = false;
                }
                else if (webBrowser1.CanGoForward && !webBrowser1.CanGoBack)
                {
                    btnNext.Enabled = true;
                    btnPrev.Enabled = false;
                }
                else if (webBrowser1.CanGoForward && webBrowser1.CanGoBack)
                {
                    btnPrev.Enabled = true;
                    btnNext.Enabled = true;
                }
                else
                {
                    btnPrev.Enabled = false;
                    btnNext.Enabled = false;
                }
            }
            else
            {
                IsHome = false;
                btnPrev.Enabled = false;
                btnNext.Enabled = false;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            IsHome = false;
            if (webBrowser1.CanGoForward)
                webBrowser1.GoForward();
            ManageNavigation();
        }

        private void btnUrl_Click(object sender, EventArgs e)
        {
            MessageBox.Show(homeUrl);
        }
    }
}
