﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class frmDeviceDetail : Form
    {
        public Form ParentMDI { get; set; }

        public bool IsEditMode = false;
        public DeviceIdentifierModel objDeviceModel { get; set; }
        public PatientIdentifierDeviceModel objPatientIdentifierDeviceModel { get; set; }
        public frmDeviceDetail()
        {
            InitializeComponent();
        }
        private void frmDeviceDetail_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            LoadDeviceDetails();
            btnSaveDetail.TabStop = false;
            btnSaveDetail.FlatStyle = FlatStyle.Flat;
            btnSaveDetail.FlatAppearance.BorderSize = 0;
            btnCancel.TabStop = false;
            btnCancel.FlatStyle = FlatStyle.Flat;
            btnCancel.FlatAppearance.BorderSize = 0;
        }
        private void LoadDeviceDetails()
        {
            if (!IsEditMode)
            {
                textBox1.Text = objDeviceModel.GMDNPTName;
                textBox2.Text = objDeviceModel.GmdnPTDefinition;
                textBox3.Text = objDeviceModel.IssuingAgency;
                textBox4.Text = objDeviceModel.BrandName;
                textBox5.Text = objDeviceModel.VersionOrModel;
                textBox6.Text = objDeviceModel.CompanyName;
                textBox7.Text = objDeviceModel.DeviceId;
                textBox8.Text = objDeviceModel.DeviceDescription;
                textBox9.Text = objDeviceModel.lot_number;
                textBox10.Text = objDeviceModel.serialNumber;
                textBox11.Text = objDeviceModel.manufacturing_date;
                textBox12.Text = objDeviceModel.expirationDate;
                textBox13.Text = objDeviceModel.HCTPStatus;
                textBox14.Text = objDeviceModel.HCTPCode;
                textBox15.Text = objDeviceModel.UniqueIdentificationNumber;
                textBox16.Text = objDeviceModel.MRISafetyInformation;
                textBox17.Text = objDeviceModel.RequiresRubberLabelingTypeId;

                DateTime dt = Convert.ToDateTime(ClinicalComponent.AppointmentDate);
                dateTimePicker1.Value = dt.Date;
                comboBox2.SelectedItem = "Active";
            }
            else
            {
                textBox1.Text = objPatientIdentifierDeviceModel.GMDNPTName;
                textBox2.Text = objPatientIdentifierDeviceModel.GmdnPTDefinition;
                textBox3.Text = objPatientIdentifierDeviceModel.IssuingAgency;
                textBox4.Text = objPatientIdentifierDeviceModel.BrandName;
                textBox5.Text = objPatientIdentifierDeviceModel.VersionOrModel;
                textBox6.Text = objPatientIdentifierDeviceModel.CompanyName;
                textBox7.Text = objPatientIdentifierDeviceModel.DeviceId;
                textBox8.Text = objPatientIdentifierDeviceModel.DeviceDescription;
                textBox9.Text = objPatientIdentifierDeviceModel.lot_number;
                textBox10.Text = objPatientIdentifierDeviceModel.serialNumber;
                textBox11.Text = objPatientIdentifierDeviceModel.manufacturing_date;
                textBox12.Text = objPatientIdentifierDeviceModel.expirationDate;
                textBox13.Text = objPatientIdentifierDeviceModel.HCTPStatus;
                textBox14.Text = objPatientIdentifierDeviceModel.HCTPCode;
                textBox15.Text = objPatientIdentifierDeviceModel.UniqueIdentificationNumber;
                textBox16.Text = objPatientIdentifierDeviceModel.MRISafetyInformation;
                textBox17.Text = objPatientIdentifierDeviceModel.RequiresRubberLabelingTypeId;

                DateTime dt = Convert.ToDateTime(objPatientIdentifierDeviceModel.DateofImplantation);
                dateTimePicker1.Value = dt.Date;
                comboBox1.SelectedItem = objPatientIdentifierDeviceModel.Lateraity;
                comboBox2.SelectedItem = objPatientIdentifierDeviceModel.DeviceStatus;
            }

        }

        private void btnSaveDetail_Click(object sender, EventArgs e)
        {
            if (!IsEditMode)
            {
                if (comboBox2.SelectedItem == null)
                {
                    MessageBox.Show("Please Select The Status");
                    return;
                }
                string _Lateraity = comboBox1.SelectedItem == null ? "N/A" : comboBox1.SelectedItem.ToString();
                string _ImplantationDate = dateTimePicker1.Value.ToString();
                string _deviceStatus = comboBox2.SelectedItem == null ? "Active" : comboBox2.SelectedItem.ToString();
                PatientImplantables pImaplntables = new PatientImplantables()
                {
                    objImplantableDevice = objDeviceModel
                };
                bool insertStatus = pImaplntables.SaveImplantableDeviceinDB(_ImplantationDate, _Lateraity, _deviceStatus);
                if (insertStatus)
                {
                    frmImplantableDevice frm = new frmImplantableDevice();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue, Please contact Adminitrator");
                    frmNewDevice frm = new frmNewDevice();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                    this.Close();
                }
            }
            else
            {
                string _Lateraity = comboBox1.SelectedItem.ToString() == "" ? "N/A" : comboBox1.SelectedItem.ToString();
                string _ImplantationDate = dateTimePicker1.Value.ToString();
                string _PatientImplantationId = objPatientIdentifierDeviceModel.ImplantableDeviceId;
                string _Status = comboBox2.SelectedItem.ToString();
                PatientImplantables pImaplntables = new PatientImplantables();
                bool UpdateStatus = pImaplntables.UpdatePatientImplantableDeviceinDB(_ImplantationDate, _PatientImplantationId, _Lateraity, _Status);
                if (UpdateStatus)
                {
                    frmImplantableDevice frm = new frmImplantableDevice();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    frm.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There is some issue, Please contact administrator!");
                    frmNewDevice frm = new frmNewDevice();
                    frm.MdiParent = this.ParentMDI;
                    frm.ParentMDI = this.ParentMDI;
                    this.Close();
                }

                if (comboBox1.SelectedItem.ToString() == "")
                {
                    MessageBox.Show("Please enter the lateraity");
                    return;
                }

                if (comboBox2.SelectedItem.ToString() == "")
                {
                    MessageBox.Show("Please enter the status");
                    return;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            frmImplantableDevice frm = new frmImplantableDevice();
            frm.MdiParent = ParentMDI;
            frm.ParentMDI = ParentMDI;
            frm.Show();
        }
    }
}
