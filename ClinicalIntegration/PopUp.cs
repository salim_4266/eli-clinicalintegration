﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ClinicalIntegration
{
    [ComVisible(true)]
    public partial class PopUp : Form
    {
        public PopUp()
        {
            InitializeComponent();
        }

        private void PopUp_Load(object sender, EventArgs e)
        {
            axWebBrowser1.Dock = DockStyle.Fill;
        }
    }
}
