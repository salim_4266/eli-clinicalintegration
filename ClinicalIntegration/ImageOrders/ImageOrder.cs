﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    public class ImageOrder
    {
        public DataTable GetDataSet_CPTCode(string _SerachContent, string _Category)
        {
            DataTable dtCPTTable = new DataTable();
            string _error = "";
            try
            {
                string _selectQry = "Select Code as Col1, Description as Col2  From model.EncounterServices with (nolock) Where " + _Category + " Like '%" + _SerachContent + "%'";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtCPTTable.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            // System.Windows.Forms.MessageBox.Show(_error);
            return dtCPTTable;
        }

        public bool UpdateExistingOrderDetail(ImageOrderModel objIOM)
        {
            bool retStatus = true;
            string _error = "";
            try
            {
                string _UpdateQry = "Update dbo.PatientImageOrders Set Status = @Status, OrderDate = @OrderDate, LastModifiedDate = GETDATE(), LastModifiedBy = @LastModifiedBy, OrderReason = @OrderReason, CptCode = @CptCode, OrderForm =@OrderForm Where OrderId = @OrderId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter Status = cmd.CreateParameter();
                    Status.Direction = ParameterDirection.Input;
                    //Status.Value = objIOM.Status;
                    Status.Value = objIOM.Status == "Pending" ? 1 : 0;
                    Status.ParameterName = "@Status";
                    Status.DbType = DbType.Int16;
                    cmd.Parameters.Add(Status);

                    IDbDataParameter OrderDate = cmd.CreateParameter();
                    OrderDate.Direction = ParameterDirection.Input;
                    OrderDate.Value = objIOM.OrderDate;
                    OrderDate.ParameterName = "@OrderDate";
                    OrderDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(OrderDate);

                    IDbDataParameter LastModifiedBy = cmd.CreateParameter();
                    LastModifiedBy.Direction = ParameterDirection.Input;
                    LastModifiedBy.Value = objIOM.LastModifiedBy;
                    LastModifiedBy.ParameterName = "@LastModifiedBy";
                    LastModifiedBy.DbType = DbType.String;
                    cmd.Parameters.Add(LastModifiedBy);

                    IDbDataParameter OrderId = cmd.CreateParameter();
                    OrderId.Direction = ParameterDirection.Input;
                    OrderId.Value = objIOM.OrderId;
                    OrderId.ParameterName = "@OrderId";
                    OrderId.DbType = DbType.Int64;
                    cmd.Parameters.Add(OrderId);

                    IDbDataParameter OrderReason = cmd.CreateParameter();
                    OrderReason.Direction = ParameterDirection.Input;
                    OrderReason.Value = objIOM.OrderForReason;
                    OrderReason.ParameterName = "@OrderReason";
                    OrderReason.DbType = DbType.String;
                    cmd.Parameters.Add(OrderReason);

                    IDbDataParameter CptCode = cmd.CreateParameter();
                    CptCode.Direction = ParameterDirection.Input;
                    CptCode.Value = objIOM.CptCode;
                    CptCode.ParameterName = "@CptCode";
                    CptCode.DbType = DbType.String;
                    cmd.Parameters.Add(CptCode);

                    IDbDataParameter OrderForm = cmd.CreateParameter();
                    OrderForm.Direction = ParameterDirection.Input;
                    OrderForm.Value = objIOM.OrderForm;
                    OrderForm.ParameterName = "@OrderForm";
                    OrderForm.DbType = DbType.String;
                    cmd.Parameters.Add(OrderForm);


                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _UpdateQry;

                    cmd.ExecuteNonQuery();

                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }
        public bool PlaceNewOrder(ImageOrderModel objIOM)
        {
            bool retStatus = true;
            string _error = "";
            try
            {
                string _insertQry = "Insert into dbo.PatientImageOrders(PatientId, AppointmentId, CptCode, OrderType,OrderDate,Status,LastModifiedDate,LastModifiedBy,IsArchieved, OrderReason,OrderForm)Values(@PatientId, @AppointmentId, @Code, @OrderType, @OrderDate, @Status, GetDate(), @LastModifiedBy, 0, @OrderReason,@OrderForm); Select Scope_Identity() as OrderId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter PatientId = cmd.CreateParameter();
                    PatientId.Direction = ParameterDirection.Input;
                    PatientId.Value = ClinicalComponent.PatientId;
                    PatientId.ParameterName = "@PatientId";
                    PatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(PatientId);

                    IDbDataParameter AppointmentId = cmd.CreateParameter();
                    AppointmentId.Direction = ParameterDirection.Input;
                    AppointmentId.Value = ClinicalComponent.AppointmentId;
                    AppointmentId.ParameterName = "@AppointmentId";
                    AppointmentId.DbType = DbType.Int64;
                    cmd.Parameters.Add(AppointmentId);

                    IDbDataParameter CptCode = cmd.CreateParameter();
                    CptCode.Direction = ParameterDirection.Input;
                    CptCode.Value = objIOM.CptCode;
                    CptCode.ParameterName = "@Code";
                    CptCode.DbType = DbType.String;
                    cmd.Parameters.Add(CptCode);

                    IDbDataParameter Status = cmd.CreateParameter();
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = objIOM.Status == "Pending" ? 1 : 0;
                    Status.ParameterName = "@Status";
                    Status.DbType = DbType.Int64;
                    cmd.Parameters.Add(Status);

                    IDbDataParameter OrderType = cmd.CreateParameter();
                    OrderType.Direction = ParameterDirection.Input;
                    OrderType.Value = objIOM.OrderType;
                    OrderType.ParameterName = "@OrderType";
                    OrderType.DbType = DbType.String;
                    cmd.Parameters.Add(OrderType);

                    IDbDataParameter OrderDate = cmd.CreateParameter();
                    OrderDate.Direction = ParameterDirection.Input;
                    OrderDate.Value = objIOM.OrderDate;
                    OrderDate.ParameterName = "@OrderDate";
                    OrderDate.DbType = DbType.DateTime;
                    cmd.Parameters.Add(OrderDate);

                    IDbDataParameter LastModifiedBy = cmd.CreateParameter();
                    LastModifiedBy.Direction = ParameterDirection.Input;
                    LastModifiedBy.Value = int.Parse(objIOM.LastModifiedBy);
                    LastModifiedBy.ParameterName = "@LastModifiedBy";
                    LastModifiedBy.DbType = DbType.Int64;
                    cmd.Parameters.Add(LastModifiedBy);

                    IDbDataParameter OrderReason = cmd.CreateParameter();
                    OrderReason.Direction = ParameterDirection.Input;
                    OrderReason.Value = objIOM.OrderForReason;
                    OrderReason.ParameterName = "@OrderReason";
                    OrderReason.DbType = DbType.String;
                    cmd.Parameters.Add(OrderReason);

                    IDbDataParameter OrderForm = cmd.CreateParameter();
                    OrderForm.Direction = ParameterDirection.Input;
                    OrderForm.Value = objIOM.OrderForm;
                    OrderForm.ParameterName = "@OrderForm";
                    OrderForm.DbType = DbType.Int64;
                    cmd.Parameters.Add(OrderForm);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _insertQry;

                    cmd.ExecuteNonQuery();

                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return retStatus;
        }
        public DataTable GetPatientImageOrder(string _Status)
        {
            DataTable dtPatientImageOrder = new DataTable();
            string _error = "";
            try
            {
                string _selectQry = "Select OrderId as Col1, a.OrderDate as Col2, c.ResourceName as Col3, a.OrderType as Col4, d.Description1 as Col10,b.Description as Col6, Case(a.Status) When 1 then 'Pending' else 'Completed' End as Col7, LastModifiedDate as Col8, c.ResourceName as Col9,a.OrderReason as Col11, a.CptCode as Col12 From dbo.PatientImageOrders a inner join model.EncounterServices b on a.CptCode = b.Code inner join dbo.Resources c on c.ResourceId = a.LastModifiedBy inner join dbo.OrderForms d on d.ORDERTESTKEY = a.OrderForm Where a.PatientId = @PatientId and a.Status in (" + _Status + ") Order by OrderDate Desc";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter idbPatientId = cmd.CreateParameter();
                    idbPatientId.Direction = ParameterDirection.Input;
                    idbPatientId.Value = ClinicalComponent.PatientId;
                    idbPatientId.ParameterName = "@PatientId";
                    idbPatientId.DbType = DbType.Int64;
                    cmd.Parameters.Add(idbPatientId);

                    //IDbDataParameter idbAppointmentId = cmd.CreateParameter();
                    //idbAppointmentId.Direction = ParameterDirection.Input;
                    //idbAppointmentId.Value = ClinicalComponent.AppointmentId;
                    //idbAppointmentId.ParameterName = "@AppointmentId";
                    //idbAppointmentId.DbType = DbType.Int64;
                    //cmd.Parameters.Add(idbAppointmentId);

                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtPatientImageOrder.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return dtPatientImageOrder;
        }
        public ImageOrderModel ReteriveOrderDetail(string _orderId)
        {
            ImageOrderModel objIMO = new ImageOrderModel();
            string _error = "";
            try
            {
                DataTable dtImageOrder = new DataTable();
                string _SelectQry = "Select a.OrderId, a.OrderDate, a.CptCode, a.Status, a.OrderReason, a.LastModifiedDate, a.OrderType, a.OrderForm From dbo.PatientImageOrders a inner join dbo.Appointments b on a.AppointmentId = b.AppointmentId inner join dbo.Resources c on c.ResourceId = b.ResourceId1 inner join dbo.Resources d on a.LastModifiedBy = d.ResourceId Where OrderId = @OrderId";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    IDbDataParameter idbOrderId = cmd.CreateParameter();
                    idbOrderId.Direction = ParameterDirection.Input;
                    idbOrderId.Value = _orderId;
                    idbOrderId.ParameterName = "@OrderId";
                    idbOrderId.DbType = DbType.Int64;
                    cmd.Parameters.Add(idbOrderId);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _SelectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtImageOrder.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
                if (dtImageOrder.Rows.Count == 1)
                {
                    objIMO.OrderId = dtImageOrder.Rows[0]["OrderId"].ToString();
                    objIMO.OrderDate = dtImageOrder.Rows[0]["OrderDate"].ToString();
                    objIMO.CptCode = dtImageOrder.Rows[0]["CptCode"].ToString();
                    objIMO.Status = dtImageOrder.Rows[0]["Status"].ToString();
                    objIMO.OrderType = dtImageOrder.Rows[0]["OrderType"].ToString();
                    objIMO.OrderForReason = dtImageOrder.Rows[0]["OrderReason"].ToString();
                    objIMO.OrderForm = dtImageOrder.Rows[0]["OrderForm"].ToString();
                }
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return objIMO;
        }
        public DataTable GetOrderForms()
        {
            DataTable dtOrderTable = new DataTable();
            string _error = "";
            try
            {
                string _selectQry = "Select 0 as Id,'Select' as Name union select ORDERTESTKEY as Id,DESCRIPTION1 as Name from [dbo].[OrderForms] ";
                ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = _selectQry;
                    using (var reader = cmd.ExecuteReader())
                    {
                        dtOrderTable.Load(reader);
                    }
                }
                ClinicalComponent._Connection.Close();
            }
            catch (Exception ee)
            {
                _error = ee.Message;
            }
            //System.Windows.Forms.MessageBox.Show(_error);
            return dtOrderTable;
        }
    }
}
