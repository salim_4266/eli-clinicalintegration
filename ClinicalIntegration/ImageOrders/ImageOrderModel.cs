﻿
namespace ClinicalIntegration
{
   public class ImageOrderModel
    {
        public string OrderId { get; set; }
        public string OrderType { get; set; }
        public string OrderDate { get; set; }
        public string CptCode { get; set; }
        public string Status { get; set; }
        public string LastModifiedBy { get; set; }
        public string LastModifiedDate { get; set; }
        public string OrderForReason { get; set; }
        //public string EnteredBy { get; set; }
        //public string EnteredDate { get; set; }
        public string OrderForm { get; set; }
    }
}
