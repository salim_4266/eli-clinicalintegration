﻿namespace ClinicalIntegration
{
	partial class frmOutboundMessages
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Subject_txtbx = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Send_btn = new System.Windows.Forms.Button();
            this.browse_btn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Message_txtbx = new System.Windows.Forms.TextBox();
            this.To_txtbx = new System.Windows.Forms.TextBox();
            this.From_txtbx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlAddressContent = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.lblSerachResultNotification = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.txtAddressSearchBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlReason4Referal = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlBrowseAttachment = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.pnlLetters = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlReason4Referal.SuspendLayout();
            this.pnlBrowseAttachment.SuspendLayout();
            this.SuspendLayout();
            // 
            // Subject_txtbx
            // 
            this.Subject_txtbx.Location = new System.Drawing.Point(119, 130);
            this.Subject_txtbx.Name = "Subject_txtbx";
            this.Subject_txtbx.Size = new System.Drawing.Size(625, 20);
            this.Subject_txtbx.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(61, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 15);
            this.label8.TabIndex = 50;
            this.label8.Text = "Subject";
            // 
            // Send_btn
            // 
            this.Send_btn.Location = new System.Drawing.Point(655, 400);
            this.Send_btn.Name = "Send_btn";
            this.Send_btn.Size = new System.Drawing.Size(87, 27);
            this.Send_btn.TabIndex = 46;
            this.Send_btn.UseVisualStyleBackColor = true;
            this.Send_btn.Click += new System.EventHandler(this.Send_btn_Click_1);
            // 
            // browse_btn
            // 
            this.browse_btn.Location = new System.Drawing.Point(754, 306);
            this.browse_btn.Name = "browse_btn";
            this.browse_btn.Size = new System.Drawing.Size(75, 23);
            this.browse_btn.TabIndex = 11;
            this.browse_btn.UseVisualStyleBackColor = true;
            this.browse_btn.Click += new System.EventHandler(this.browse_btn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(754, 335);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Message_txtbx
            // 
            this.Message_txtbx.Location = new System.Drawing.Point(119, 166);
            this.Message_txtbx.Multiline = true;
            this.Message_txtbx.Name = "Message_txtbx";
            this.Message_txtbx.Size = new System.Drawing.Size(625, 123);
            this.Message_txtbx.TabIndex = 44;
            // 
            // To_txtbx
            // 
            this.To_txtbx.Location = new System.Drawing.Point(120, 62);
            this.To_txtbx.Name = "To_txtbx";
            this.To_txtbx.ReadOnly = true;
            this.To_txtbx.Size = new System.Drawing.Size(518, 20);
            this.To_txtbx.TabIndex = 38;
            // 
            // From_txtbx
            // 
            this.From_txtbx.BackColor = System.Drawing.Color.White;
            this.From_txtbx.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.From_txtbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.From_txtbx.Location = new System.Drawing.Point(122, 33);
            this.From_txtbx.Name = "From_txtbx";
            this.From_txtbx.ReadOnly = true;
            this.From_txtbx.Size = new System.Drawing.Size(619, 15);
            this.From_txtbx.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(50, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.TabIndex = 37;
            this.label4.Text = "Message";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(88, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 15);
            this.label3.TabIndex = 35;
            this.label3.Text = "To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(73, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 32;
            this.label1.Text = "From";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(27, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 15);
            this.label9.TabIndex = 53;
            this.label9.Text = "Clinical Action";
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.White;
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(120, 96);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(277, 23);
            this.comboBox2.TabIndex = 52;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(120, 304);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(623, 82);
            this.listBox1.TabIndex = 54;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Teal;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(644, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 31);
            this.button2.TabIndex = 55;
            this.button2.Text = "Address Book";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 303);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 15);
            this.label2.TabIndex = 56;
            this.label2.Text = "Attachment";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pnlAddressContent);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.lblSerachResultNotification);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.txtAddressSearchBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(37, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(817, 481);
            this.panel1.TabIndex = 58;
            // 
            // pnlAddressContent
            // 
            this.pnlAddressContent.Location = new System.Drawing.Point(40, 97);
            this.pnlAddressContent.Name = "pnlAddressContent";
            this.pnlAddressContent.Size = new System.Drawing.Size(710, 356);
            this.pnlAddressContent.TabIndex = 6;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Teal;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(654, 58);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(85, 30);
            this.button5.TabIndex = 5;
            this.button5.Text = "Close";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // lblSerachResultNotification
            // 
            this.lblSerachResultNotification.AutoSize = true;
            this.lblSerachResultNotification.Location = new System.Drawing.Point(33, 88);
            this.lblSerachResultNotification.Name = "lblSerachResultNotification";
            this.lblSerachResultNotification.Size = new System.Drawing.Size(0, 13);
            this.lblSerachResultNotification.TabIndex = 4;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Teal;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(560, 58);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 29);
            this.button4.TabIndex = 2;
            this.button4.Text = "Search";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtAddressSearchBox
            // 
            this.txtAddressSearchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddressSearchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddressSearchBox.Location = new System.Drawing.Point(39, 60);
            this.txtAddressSearchBox.Multiline = true;
            this.txtAddressSearchBox.Name = "txtAddressSearchBox";
            this.txtAddressSearchBox.Size = new System.Drawing.Size(506, 26);
            this.txtAddressSearchBox.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Teal;
            this.label5.Location = new System.Drawing.Point(41, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 19);
            this.label5.TabIndex = 0;
            this.label5.Text = "Address Book";
            // 
            // pnlReason4Referal
            // 
            this.pnlReason4Referal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlReason4Referal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlReason4Referal.Controls.Add(this.button6);
            this.pnlReason4Referal.Controls.Add(this.textBox1);
            this.pnlReason4Referal.Controls.Add(this.label6);
            this.pnlReason4Referal.Location = new System.Drawing.Point(37, 75);
            this.pnlReason4Referal.Name = "pnlReason4Referal";
            this.pnlReason4Referal.Size = new System.Drawing.Size(816, 232);
            this.pnlReason4Referal.TabIndex = 59;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Teal;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(34, 141);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(89, 33);
            this.button6.TabIndex = 2;
            this.button6.Text = "Done";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(35, 65);
            this.textBox1.MaxLength = 500;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(716, 61);
            this.textBox1.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(33, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Please add reason";
            // 
            // pnlBrowseAttachment
            // 
            this.pnlBrowseAttachment.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlBrowseAttachment.Controls.Add(this.button3);
            this.pnlBrowseAttachment.Controls.Add(this.button7);
            this.pnlBrowseAttachment.Controls.Add(this.pnlLetters);
            this.pnlBrowseAttachment.Controls.Add(this.label7);
            this.pnlBrowseAttachment.Location = new System.Drawing.Point(38, 54);
            this.pnlBrowseAttachment.Name = "pnlBrowseAttachment";
            this.pnlBrowseAttachment.Size = new System.Drawing.Size(817, 406);
            this.pnlBrowseAttachment.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Teal;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(692, 345);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(92, 35);
            this.button3.TabIndex = 4;
            this.button3.Text = "Done";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(26, 347);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(162, 35);
            this.button7.TabIndex = 3;
            this.button7.Text = "Choose File(s)";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // pnlLetters
            // 
            this.pnlLetters.Location = new System.Drawing.Point(26, 58);
            this.pnlLetters.Name = "pnlLetters";
            this.pnlLetters.Size = new System.Drawing.Size(760, 265);
            this.pnlLetters.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Teal;
            this.label7.Location = new System.Drawing.Point(25, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(214, 18);
            this.label7.TabIndex = 0;
            this.label7.Text = "Please select available document";
            // 
            // frmOutboundMessages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1007, 545);
            this.Controls.Add(this.pnlBrowseAttachment);
            this.Controls.Add(this.pnlReason4Referal);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.browse_btn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.Subject_txtbx);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Send_btn);
            this.Controls.Add(this.Message_txtbx);
            this.Controls.Add(this.To_txtbx);
            this.Controls.Add(this.From_txtbx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(160, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOutboundMessages";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.frmOutboundMessages_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlReason4Referal.ResumeLayout(false);
            this.pnlReason4Referal.PerformLayout();
            this.pnlBrowseAttachment.ResumeLayout(false);
            this.pnlBrowseAttachment.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        #endregion
        private System.Windows.Forms.TextBox Subject_txtbx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button Send_btn;
        private System.Windows.Forms.Button browse_btn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Message_txtbx;
        private System.Windows.Forms.TextBox To_txtbx;
        private System.Windows.Forms.TextBox From_txtbx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAddressSearchBox;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label lblSerachResultNotification;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel pnlAddressContent;
        private System.Windows.Forms.Panel pnlReason4Referal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel pnlBrowseAttachment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlLetters;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button3;
    }
}