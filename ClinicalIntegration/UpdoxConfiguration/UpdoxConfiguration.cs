﻿using System;
using System.Data;

namespace ClinicalIntegration
{
    public class ModelUpdoxConfiguration
    {
        public int ID { get; set; }
        public string AccountId { get; set; }
        public string Environment { get; set; }
        public bool IsActive { get; set; }
    }
    public class UpdoxConfiguration
    {
        public ModelUpdoxConfiguration GetConfiguration()
        {
            ModelUpdoxConfiguration objModelUpdoxConfiguration = new ModelUpdoxConfiguration();
            try
            {
                String query = "SELECT Id, AccountId, Environment, IsActive FROM dbo.UpdoxConfigurationDetails with(nolock)";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            objModelUpdoxConfiguration.ID = Convert.ToInt32(Convert.ToString(reader["ID"]));
                            objModelUpdoxConfiguration.AccountId = Convert.ToString(reader["AccountId"]);
                            objModelUpdoxConfiguration.Environment = Convert.ToString(reader["Environment"]);
                            objModelUpdoxConfiguration.IsActive = Convert.ToBoolean(Convert.ToString(reader["IsActive"]));
                        }
                    }
                }
            }
            catch
            {
            }

            return objModelUpdoxConfiguration;
        }
        public void AddUpateConfiguration(ModelUpdoxConfiguration objModelUpdoxConfiguration)
        {
            using (IDbCommand cmd = ClinicalComponent._Connection.CreateCommand())
            {
                String query = "AddUpdateUpdoxConfigurationDetails";
                if (ClinicalComponent._Connection.State.ToString() == "Closed")
                    ClinicalComponent._Connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = query;

                IDbDataParameter IdbParam1 = cmd.CreateParameter();
                IdbParam1.Direction = ParameterDirection.Input;
                IdbParam1.Value = objModelUpdoxConfiguration.ID;
                IdbParam1.ParameterName = "@ID";
                IdbParam1.DbType = DbType.Int32;
                cmd.Parameters.Add(IdbParam1);

                IDbDataParameter IdbParam4 = cmd.CreateParameter();
                IdbParam4.Direction = ParameterDirection.Input;
                IdbParam4.Value = objModelUpdoxConfiguration.AccountId;
                IdbParam4.ParameterName = "@AccountId";
                IdbParam4.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam4);

                IDbDataParameter IdbParam5 = cmd.CreateParameter();
                IdbParam5.Direction = ParameterDirection.Input;
                IdbParam5.Value = objModelUpdoxConfiguration.Environment;
                IdbParam5.ParameterName = "@Environment";
                IdbParam5.DbType = DbType.String;
                cmd.Parameters.Add(IdbParam5);

                IDbDataParameter IdbParam6 = cmd.CreateParameter();
                IdbParam6.Direction = ParameterDirection.Input;
                IdbParam6.Value = objModelUpdoxConfiguration.IsActive;
                IdbParam6.ParameterName = "@IsActive";
                IdbParam6.DbType = DbType.Boolean;
                cmd.Parameters.Add(IdbParam6);
                
                cmd.ExecuteNonQuery();
            }

        }
    }
}
