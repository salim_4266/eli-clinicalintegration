﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Data.SqlClient;
using ClinicalIntegration;


namespace ErxClient
{
    public partial class ImplantableClient : Form
    {
        public ImplantableClient()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            IDbConnection con = new SqlConnection("Data Source=172.30.3.19\\Test2;Initial Catalog=PracticeRepository_QRDA_Testing;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2017;MultipleActiveResultSets=True;Connect Timeout=1200");
            frmParentForm frm = new frmParentForm();
            frm.UserId = "2140";//textBox1.Text;
            frm.PatientId = "60394";//txtPatientId.Text;
            frm.AppointmentId = "89785";//txtAppointmentId.Text;
            frm.AppointmentDate = "2017-01-02";
            frm.DBObject = con;
            frm.PatientName = "IOPW";
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IDbConnection con = new SqlConnection(@"Data Source=172.30.3.19\Test2;Initial Catalog=PracticeRepositoryACI_Testing_Sandbox;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2016;MultipleActiveResultSets=True;Connect Timeout=1200");
            frmReconcileParent frm = new frmReconcileParent();
            frm.UserId = textBox1.Text;
            frm.PatientId = txtPatientId.Text;
            frm.AppointmentId = txtAppointmentId.Text;
            frm.AppointmentDate = "2017-01-02";
            frm.DBObject = con;
            frm.PatientName = "IOPW";
            frm.Show();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            IDbConnection con = new SqlConnection(@"Data Source=172.30.3.19\Test2;Initial Catalog=PracticeRepository_QRDA_Testing;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2017;MultipleActiveResultSets=True;Connect Timeout=1200");
            frmPatientGeneratedHealthData frm = new frmPatientGeneratedHealthData();
            frm.PatientId = txtPatientId.Text;
            frm.UserId = textBox1.Text;
            frm.PracticeToken = "988C4C26-BE2E-4D1A-A239-87F85DFD052C";
            frm.DBObject = con;
            frm.PinpointDirectory = @"E:\Pinpoint\";
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //IDbConnection con = new SqlConnection("Data Source=172.30.3.19\\Test2;Initial Catalog=PracticeRepository_ACI_Testing_Sept;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2017;MultipleActiveResultSets=True;Connect Timeout=1200");
            //frmOutboundMessages frm = new frmOutboundMessages();
            //frm.staffKey = textBox1.Text;
            //frm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            IDbConnection con = new SqlConnection("Data Source=172.30.3.19\\Test2;Initial Catalog=PracticeRepository_QRDA_Testing;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2017;MultipleActiveResultSets=True;Connect Timeout=1200");
            frmDirectMessages frm = new frmDirectMessages();
            frm.ProviderId = "1134";
            frm.DBObject = con;
            frm.StaffId = "1134";
            frm.PatientId = "57303";
            frm.AppointmentId = "88676";
            frm.FolderPath = @"E:\Pinpoint";
            frm.ShowDialog();
        }

        private void ImplantableClient_Load(object sender, EventArgs e)
        {
            txtPatientId.Text = "51069";
            txtAppointmentId.Text = "81411";
            textBox1.Text = "16";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //frmDataIntegration frm = new frmDataIntegration();
            //IDbConnection con = new SqlConnection("Data Source=172.29.102.50;Initial Catalog=practicerepository_mainbeta;Integrated Security=False;User ID=iouser;Password=iopracticeware123;MultipleActiveResultSets=True;Connect Timeout=1200;");
            //frm.DBObject = con;
            //frm.ProviderId = textBox1.Text;
            //frm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmResourceUpdoxRegistration frm = new frmResourceUpdoxRegistration();
            IDbConnection con = new SqlConnection("Data Source=172.30.3.19\\Test2;Initial Catalog=PracticeRepository_QRDA_Testing;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2016;MultipleActiveResultSets=True;Connect Timeout=1200;");
            frm.DBObject = con;
            frm.ResourceId = textBox1.Text;
            frm.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            frmACIConfiguration frm = new frmACIConfiguration();
            IDbConnection con = new SqlConnection("Data Source=172.29.102.50;Initial Catalog=PracticeRepositoryNemeth;Integrated Security=False;User ID=iouser;Password=iopracticeware123;MultipleActiveResultSets=True;Connect Timeout=1200;");
            frm.DBObject = con;
            frm.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
           
        }

        private void btnDrFirst_Click(object sender, EventArgs e)
        {
            var con = new SqlConnection("Data Source=172.29.102.51;Initial Catalog=PracticeRepositoryWilkes299;Integrated Security=False;User ID=ioclouduser;Password=!0Cee_loud$121;MultipleActiveResultSets=True;Connect Timeout=1200");
            frmErxHelper frm = new frmErxHelper();
            frm.PatientId = "683"; //	
            frm.UserId = "29";
            frm.AppointmentId = "1901";
            frm.PracticeToken = "b1738d8f-54a2-4d9e-b07b-a645ddb4f885";
            frm.DBObject = con;
            frm.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            IDbConnection con = new SqlConnection(@"Data Source=10.1.5.15;Initial Catalog=PracticeRepositoryNemeth;Integrated Security=False;User ID=iouser;Password=iopracticeware123;MultipleActiveResultSets=True;Connect Timeout=1200;");
            frmERPPatientConfirmation frm = new frmERPPatientConfirmation();
            frm._externalId = "11169";
            frm._password = "Ankush123";
            frm._practiceToken = "988C4C26-BE2E-4D1A-A239-87F85DFD052C";
            frm._userName = "Ankush";
            frm._userType = "Patient";
            frm._patientName = "Ankush Jain";
            frm.DBObject = con;
            frm.ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            //IDbConnection con = new SqlConnection("Data Source=172.30.3.19\\Test2;Initial Catalog=PracticeRepository_QRDA_Testing;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2016;MultipleActiveResultSets=True;Connect Timeout=1200;");
            //RCPClient objClient = new RCPClient();
            //objClient.DBObject = con;
            //objClient._externalId = "79323";
            //objClient._resourceId = "1119";
            //objClient._jsonNumber = "07";
            //objClient._path2Directory = @"E:\Pinpoint";
            //bool retStatus = objClient.Push();
            //if (retStatus)
            //{
            //    MessageBox.Show("JSON 7 Successfully processed");
            //}
        }

        private void button11_Click(object sender, EventArgs e)
        {
            IDbConnection con = new SqlConnection("Data Source=172.30.3.19\\Test2;Initial Catalog=PracticeRepository_QRDA_Testing;Integrated Security=False;User ID=iotest;Password=IOP_RCP_2017;MultipleActiveResultSets=True;Connect Timeout=1200;");
            ERPServices objClient = new ERPServices();
            objClient.DBObject = con;
            objClient.externalId = "4162";
            objClient.type = "doctor";
            bool retStatus = objClient.Push();
            if (retStatus)
            {
                MessageBox.Show("Successfully registered to ERP");
            }
        }
    }
}
